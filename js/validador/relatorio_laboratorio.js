/*
  ESSE JS FAZ VALIDAÇÃO SEM AJAX
 */

var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();
}



var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){


    var tempString = $('#laboratorios').val();
    if(tempString == ''){
        showError('#fim','Olá!','O campo \'Laboratório\' é obrigatório.');
        return false;
    }

    var tempString = $('#inicio').val();
    if(tempString == ''){
        showError('#inicio','Olá!','O campo \'Data inicial\' é obrigatório.');
        return false;
    }
    var tempString = $('#fim').val();
    if(tempString == ''){
        showError('#fim','Olá!','O campo \'Data final\' é obrigatório.');
        return false;
    }

    return true;
};
$(document).ready(function(){
    $('#Envia').click(function(){
        if(validateFields()){
            return true;
        }
        return false;
    });
});