

var confirm = function(href,text){
    $('#modal1').html('<div class="modal-content"><h5>'+text+'</h5></div><div class="modal-footer"><button class="btn waves-effect waves-light" type="button" id="confirmar" name="action">Confirmar</button><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();
    $('#confirmar').click(function(){
        loader(href);
    });
}

var loader = function(href){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData(href);
}

var sendData = function(href){

    $.ajax({
        type:"GET",
        url: href,
        cache:false,
        success: function(data) {
            if(data.erro){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
            }else{
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"><a href="/redirect" class="btn waves-effect waves-light">Fechar</a></div>');
                //todo: ver redirecionamento
            }
        },
        error:function (){
            $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"><a href="/redirect" class="btn waves-effect waves-light">Fechar</a></div>');
        }
    });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();
};

$(document).ready(function(){
    $('.delete').click(function(){
        var text = $(this).data("text");
        var href = $(this).data("href");
        confirm(href,text);
    });
});