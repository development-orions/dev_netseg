var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();

}

var sendData = function(){

        $.ajax({
            type:"POST",
            url: $('#prefixo').val()+'/configuracoes',
           data: $('form').serialize(),
            cache:false,
            success: function(data) {
                if(data.erro){
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
                }else{
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"><a href="/redirect" class="btn waves-effect waves-light">Fechar</a></div>');
                }
            },
            error:function (){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"></div>');
            }
        });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){

    var tempString = $('#razaosocial').val();
    if(tempString == ''){
        showError('#razaosocial','Olá!','O campo Razão Social é obrigatório.');
        return false;
    }
    var tempString = $('#cnpj').val();
    if(tempString == ''){
        showError('#cnpj','Olá!','O campo CNPJ é obrigatório.');
        return false;
    }
    var tempString = $('#ie').val();
    if(tempString == ''){
        showError('#ie','Olá!','O campo Inscrição Estadual (IE) é obrigatório.');
        return false;
    }
    var tempString = $('#formajuridica').val();
    if(tempString == ''){
        showError('#formajuridica','Olá!','O campo Forma Jurídica é obrigatório.');
        return false;
    }
    var tempString = $('#cnae').val();
    if(tempString == ''){
        showError('#cnae','Olá!','O campo CNAE é obrigatório.');
        return false;
    }

    var tempString1 = $('#email').val();
    if(tempString1 != '') {
        var tempString = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;
        var tempBoolean = true;
        if (!tempString.test($('#email').val()))
            tempBoolean = false;
        if (!tempBoolean) {
            showError('#email', 'Olá!', 'Email Inválido. Preencha com um e-mail válido!<br> Ex: seuemail@seuprovedor.com.br');
            return false;
        }
    }
    var tempString = $('#telefone').val();
    if(tempString == ''){
        showError('#telefone','Olá!','O campo Telefone é obrigatório.');
        return false;
    }

    var tempString = $('#cep').val();
    if(tempString == ''){
        showError('#cep','Olá!','O campo CEP é obrigatório.');
        return false;
    }
    var tempString = $('#rua').val();
    if(tempString == ''){
        showError('#rua','Olá!','O campo rua é obrigatório.');
        return false;
    }
    var tempString = $('#numero').val();
    if(tempString == ''){
        showError('#numero','Olá!','O campo numero é obrigatório.');
        return false;
    }
    var tempString = $('#bairro').val();
    if(tempString == ''){
        showError('#bairro','Olá!','O campo bairro é obrigatório.');
        return false;
    }
    var tempString = $('#cidade').val();
    if(tempString == ''){
        showError('#cidade','Olá!','O campo cidade é obrigatório.');
        return false;
    }
    var tempString = $('#estado').val();
    if(tempString == ''){
        showError('#estado','Olá!','O campo estado é obrigatório.');
        return false;
    }


    return true;
};
$(document).ready(function(){

    $('#Envia').click(function(){
        if(validateFields()){
            loader();

        }
        return false;
    });
});