var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();
}

var sendData = function(){

        $.ajax({
            type:"POST",
            url: $('#prefixo').val()+'/naoconformidade_edita/'+$('#id').val(),
            data: $('form').serialize(),
            cache:false,
            success: function(data) {
                if(data.erro){
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
                }else{
                    $('#modal1').html('' +
                    '<div class="modal-content">' +
                        '<h4>' +
                    '<i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"><a href="javascript:history.go(-1)"  class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
                }
            },
            error:function (){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"><a href="#!" onclick="parent.$.fn.colorbox.close();" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
            }
        });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){

    var tempString = $('#revisao').val();
    if(tempString == ''){
        showError('#revisao','Olá!','O campo Revisão é obrigatório.');
        return false;
    }
    var tempString = $('#vigencia').val();
    if(tempString == ''){
        showError('#vigencia','Olá!','O campo Vigência é obrigatório.');
        return false;
    }
    var tempString = $('#realpotencial').val();
    if(tempString == ''){
        showError('#realpotencial','Olá!','O campo Não Conformidade é obrigatório.');
        return false;
    }
    var tempString = $('#materialsistema').val();
    if(tempString == ''){
        showError('#materialsistema','Olá!','O campo Tipo é obrigatório.');
        return false;
    }

    var tempString = $('#ocorrencia').val();
    if(tempString == ''){
        showError('#ocorrencia','Olá!','O campo Ocorrência é obrigatório.');
        return false;
    }

    return true;
};
$(document).ready(function(){

    $('#Envia').click(function(){
        if(validateFields()){
            loader()
        }
        return false;
    });
});