var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();
}

var sendData = function(){

        $.ajax({
            type:"POST",
            url: $('#prefixo').val()+'/cliente',
            data: $('form').serialize(),
            cache:false,
            success: function(data) {
                if(data.erro){
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
                }else{
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
                    cleanForm();
                    focusFirst();
                }
            },
            error:function (){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"><a href="/redirect" class="btn waves-effect waves-light">Fechar</a></div>');
            }
        });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){

    var tempString = $('#nome').val();
    if(tempString == ''){
        showError('#nome','Olá!','O campo Nome é obrigatório.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#valor_contrato').val();
    if(tempString == 'Assessoria' && tempString2 == ''){
        showError('#valor_contrato','Olá!','O campo Valor do Contrato é obrigatório para clientes do tipo assessoria.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#data_contrato').val();
    if(tempString == 'Assessoria' && tempString2 == ''){
        showError('#data_contrato','Olá!','O campo Data de inicio do Contrato é obrigatório para clientes do tipo assessoria.');
        return false;
    }
    var tempString = $('#tipo').val();
    var tempString2 = $('#valor_tecnico0').val();
    if(tempString == 'Assessoria' && tempString2 == ''){
        showError('#valor_tecnico0','Olá!','O campo Valor do Técnico é obrigatório para clientes do tipo assessoria.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#dia_tecnico0').val();
    if(tempString == 'Assessoria' && tempString2 == ''){
        showError('#dia_tecnico0','Olá!','O campo Dia do pagamento do Técnico é obrigatório para clientes do tipo assessoria.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#tecnico1').val();
    var tempString3 = $('#valor_tecnico1').val();
    if(tempString == 'Assessoria' && tempString2 != '' && tempString3 == ''){
        showError('#valor_tecnico1','Olá!','O campo Valor do Técnico é obrigatório para clientes do tipo assessoria.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#tecnico1').val();
    var tempString3 = $('#dia_tecnico1').val();
    if(tempString == 'Assessoria' && tempString2 != '' && tempString3 == ''){
        showError('#dia_tecnico1','Olá!','O campo Dia do pagamento do Técnico é obrigatório para clientes do tipo assessoria.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#tecnico2').val();
    var tempString3 = $('#valor_tecnico2').val();
    if(tempString == 'Assessoria' && tempString2 != '' && tempString3 == ''){
        showError('#valor_tecnico2','Olá!','O campo Valor do Técnico é obrigatório para clientes do tipo assessoria.');
        return false;
    }

    var tempString = $('#tipo').val();
    var tempString2 = $('#tecnico2').val();
    var tempString3 = $('#dia_tecnico2').val();
    if(tempString == 'Assessoria' && tempString2 != '' && tempString3 == ''){
        showError('#dia_tecnico2','Olá!','O campo Dia do pagamento do Técnico é obrigatório para clientes do tipo assessoria.');
        return false;
    }
    var tempString = $('#cep').val();
    if(tempString == ''){
        showError('#cep','Olá!','O campo CEP é obrigatório.');
        return false;
    }
    var tempString = $('#rua').val();
    if(tempString == ''){
        showError('#rua','Olá!','O campo rua é obrigatório.');
        return false;
    }
    var tempString = $('#numero').val();
    if(tempString == ''){
        showError('#numero','Olá!','O campo numero é obrigatório.');
        return false;
    }
    var tempString = $('#bairro').val();
    if(tempString == ''){
        showError('#bairro','Olá!','O campo bairro é obrigatório.');
        return false;
    }
    var tempString = $('#cidade').val();
    if(tempString == ''){
        showError('#cidade','Olá!','O campo cidade é obrigatório.');
        return false;
    }
    var tempString = $('#estado').val();
    if(tempString == ''){
        showError('#estado','Olá!','O campo estado é obrigatório.');
        return false;
    }

    var tempString1 = $('#telefone').val();
    if(tempString1 != '') {
        var tempString = /^(\([1-9][1-9]\) [0-9][0-9]{4}-[0-9]{4})|(\([1-9][1-9]\) [0-9]{4}-[0-9]{4})$/;
        var tempBoolean = true;
        if (!tempString.test($('#telefone').val()))
            tempBoolean = false;
        if (!tempBoolean) {
            showError('#telefone', 'Olá!', 'O campo telefone é obrigatório, preencha somente com números conforme o exemplo<br> Ex: (12) 3333-3333 ou (12) 99999-9999(nono dígito)<br> DDD mais o número do telefone!');
            return false;
        }
    }
    var tempString1 = $('#email').val();
    if(tempString1 != '') {
        var tempString = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;
        var tempBoolean = true;
        if (!tempString.test($('#email').val()))
            tempBoolean = false;
        if (!tempBoolean) {
            showError('#email', 'Olá!', 'Email Inválido. Preencha com um e-mail válido!<br> Ex: seuemail@seuprovedor.com.br');
            return false;
        }
    }

    return true;
};
$(document).ready(function(){

    $('#Envia').click(function(){
        if(validateFields()){
            loader()
        }
        return false;
    });
});