var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();
}

var sendData = function(){

        $.ajax({
            type:"POST",
            url: $('#prefixo').val()+'/conta_edita/'+$('#tipo')+'/'+$('#id').val(),
            data: $('form').serialize(),
            cache:false,
            success: function(data) {
                if(data.erro){
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
                }else{
                    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"><a href="javascript:history.go(-1)" class="btn waves-effect waves-light">Fechar</a></div>');
                }
            },
            error:function (){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"><a href="javascript:history.go(-1)" class="btn waves-effect waves-light">Fechar</a></div>');
            }
        });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){

    var tempString = $('#nome').val();
    if(tempString == ''){
        showError('#nome','Olá!','O campo nome é obrigatório.');
        return false;
    }
    var tempString = $('#vencimento').val();
    if(tempString == ''){
        showError('#vencimento','Olá!','O campo data de vencimento é obrigatório.');
        return false;
    }
    var tempString = $('#nf').val();
    if(tempString == ''){
        showError('#nf','Olá!','O campo nota fiscal é obrigatório.');
        return false;
    }
    var tempString = $('#valor').val();
    if(tempString == ''){
        showError('#valor','Olá!','O campo valor é obrigatório.');
        return false;
    }
    var tempString = $('#cliente_id').val();
    if(tempString == ''){
        showError('#cliente_id','Olá!','Escolha um cliente.');
        return false;
    }
    var tempString = $('#formapagamento').val();
    var tempString2 = $('#num_boleto').val();
    if(tempString == 1 && tempString2 == ''){
        showError('#num_boleto','Olá!','O campo número do boleto é obrigatório.');
        return false;
    }
    var tempString = $('#formapagamento').val();
    var tempString2 = $('#portador').val();
    if(tempString2 == 'Netseg' && tempString == 4){
        showError('#formapagamento','Olá!','Forma de pagamento inválida para esse portador.');
        return false;
    }

    var tempString = $('#formapagamento').val();
    var tempString2 = $('#portador').val();
    if(tempString2 == 'Egydio' && tempString == 3){
        showError('#formapagamento','Olá!','Forma de pagamento inválida para esse portador.');
        return false;
    }

    return true;
};
$(document).ready(function(){

    $('#Envia').click(function(){
        if(validateFields()){
            loader()
        }
        return false;
    });
});