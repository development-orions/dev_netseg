var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();
}

var sendData = function(){

    $.ajax({
        type:"POST",
        url: $('#prefixo').val()+'/venda',
        data: $('form').serialize(),
        cache:false,
        success: function(data) {
            if(data.erro){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
            }else{
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"><a href="/redirect" class="btn waves-effect waves-light">Fechar</a></div>');
                cleanForm();
                focusFirst();
            }
        },
        error:function (){
            $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"></div>');
        }
    });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){
    var tempString = $('#cliente_id').val();
    if(tempString == ''){
        showError('#cliente_id','Olá!','Escolha um cliente para a venda.');
        return false;
    }
    var tempString = $('#servico').val();
    if(tempString == 'false'){
        showError('#servico','Olá!','Adicione um serviço a venda.');
        return false;
    }
    var tempString = $('#qtdParcelas').val();
    if(tempString == ''){
        showError('#qtdParcelas','Olá!','Escolha a quantidade de parcelas para a venda.');
        return false;
    }
    var tempString = $('#fechamento').val();
    if(tempString == ''){
        showError('#fechamento','Olá!','A data de fechamento da venda é obrigatório.');
        return false;
    }

    var tempString = $('#valoradministrativo').val();
    if(tempString == ''){
        showError('#valoradministrativo','Olá!','O campo valor Netseg é obrigatório.');
        return false;
    }

    var tempString = $('#tecnico0').val();
    var tempString2 = $('#medico0').val();
    if(tempString == '' && tempString2 == ''){
        showError('#tecnico0','Olá!','Escolha pelo menos um técnico ou um médico para a venda.');
        return false;
    }


    var tempString = $('#tecnico0').val();
    var tempString2 = $('#valor_tecnico0').val();
    if(tempString != '' && tempString2 == ''){
        showError('#valor_tecnico0','Olá!','O campo valor do técnico é obrigatório.');
        return false;
    }

    var tempString = $('#tecnico1').val();
    var tempString2 = $('#valor_tecnico1').val();
    if(tempString != '' && tempString2 == ''){
        showError('#valor_tecnico1','Olá!','O campo valor do técnico é obrigatório.');
        return false;
    }
    var tempString = $('#tecnico2').val();
    var tempString2 = $('#valor_tecnico2').val();
    if(tempString != '' && tempString2 == ''){
        showError('#valor_tecnico2','Olá!','O campo valor do técnico é obrigatório.');
        return false;
    }

    var tempString = $('#medico0').val();
    var tempString2 = $('#valor_medico0').val();
    if(tempString != '' && tempString2 == ''){
        showError('#valor_medico0','Olá!','O campo valor do médico é obrigatório.');
        return false;
    }

    var tempString = $('#medico1').val();
    var tempString2 = $('#valor_medico1').val();
    if(tempString != '' && tempString2 == ''){
        showError('#valor_medico1','Olá!','O campo valor do médico é obrigatório.');
        return false;
    }
    var tempString = $('#medico2').val();
    var tempString2 = $('#valor_medico2').val();
    if(tempString != '' && tempString2 == ''){
        showError('#valor_medico2','Olá!','O campo valor do médico é obrigatório.');
        return false;
    }



    return true;
};
$(document).ready(function(){

    $('#Envia').click(function(){
        if(validateFields()){
            loader()
        }
        return false;
    });
});