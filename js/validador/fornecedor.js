var cleanForm = function(){
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset'){
            $(this).val('');
        }
    });
    $('textarea').each(function(index, Element){

        $(this).val('');
    });
};
var focusFirst = function(){
    var firstElement = undefined;
    $('input').each(function(index, Element){
        if($(this).attr('type') != 'hidden' && $(this).attr('type') != 'button' && $(this).attr('type') != 'submit'){
            if(firstElement == undefined){
                firstElement = this;
                $(firstElement).focus();
                $(firstElement).select();
            }
        }
    });
};
var loader = function(){
    $('#modal1').html('<div class="modal-content"><h4>Enviando...</h4><div class="preloader-wrapper small active"><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div><div class="modal-footer"></div>');
    $('#modal1').openModal();
    sendData();
}

var sendData = function(){

    $.ajax({
        type:"POST",
        url: $('#prefixo').val()+'/fornecedor',
        data: $('form').serialize(),
        cache:false,
        success: function(data) {
            if(data.erro){
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"></div>');
            }else{
                $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">done</i></h4><p>'+data.msg+'</p></div><div class="modal-footer"><a href="/redirect" class="btn waves-effect waves-light">Fechar</a></div>');
            }
        },
        error:function (){
            $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>Ocorreu um erro inesperado, tente novamente.</p></div><div class="modal-footer"><a href="javascript:history.go(-1)" class="btn waves-effect waves-light">Fechar</a></div>');
        }
    });
};

var showError = function(strObject,strTitle,strMessage){
    $('#modal1').html('<div class="modal-content"><h4><i class="medium material-icons">report_problem</i></h4><p>'+strMessage+'</p></div><div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a></div>');
    $('#modal1').openModal();

};
var validateFields = function(){

    var tempString = $('#razaosocial').val();
    if(tempString == ''){
        showError('#razaosocial','Olá!','O campo Razão Social é obrigatório.');
        return false;
    }

    var tempString = $('#datainicio_servico').val();
    if(tempString == ''){
        showError('#datainicio_servico','Olá!','O campo Data de inicio do serviço é obrigatório.');
        return false;
    }

    var tempBoolean = true;

    if(($('#cnpj').val() == '') && ($('#cpf').val() == ''))
        tempBoolean = false;
    if(!tempBoolean){
        showError('#cnpj','Ops!','O campo CPF OU CNPJ deve ser preenchido');
        return false;
    }

    var tempString1 = $('#telefone').val();
    if(tempString1 != '') {
        var tempString = /^(\([1-9][1-9]\) [0-9][0-9]{4}-[0-9]{4})|(\([1-9][1-9]\) [0-9]{4}-[0-9]{4})$/;
        var tempBoolean = true;
        if (!tempString.test($('#telefone').val()))
            tempBoolean = false;
        if (!tempBoolean) {
            showError('#telefone', 'Olá!', 'O campo telefone é obrigatório, preencha somente com números conforme o exemplo<br> Ex: (12) 3333-3333 ou (12) 99999-9999(nono dígito)<br> DDD mais o número do telefone!');
            return false;
        }
    }
    var tempString1 = $('#email').val();
    if(tempString1 != '') {
        var tempString = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;
        var tempBoolean = true;
        if (!tempString.test($('#email').val()))
            tempBoolean = false;
        if (!tempBoolean) {
            showError('#email', 'Olá!', 'Email Inválido. Preencha com um e-mail válido!<br> Ex: seuemail@seuprovedor.com.br');
            return false;
        }
    }


    return true;
};
$(document).ready(function(){

    $('#Envia').click(function(){
        if(validateFields()){
            loader()
        }
        return false;
    });
});
