/**
 * Created by Orions on 11/11/2015.
 */

/* Máscaras ER */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
    v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}
function id( el ){
    return document.getElementById( el );
}
//cnpj
function cnpj(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function mcnpj(v){
    v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"$1.$2");
    v=v.replace(/(\d)(\d{9})$/,"$1.$2");
    v=v.replace(/(\d)(\d{6})$/,"$1/$2");
    v=v.replace(/(\d)(\d{2})$/,"$1-$2");
    return v;
}


//cpf
function cpf(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function mcpf(v){
    v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
    v=v.replace(/^(\d{3})(\d)/g,"$1.$2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{5})$/,"$1.$2"); //Coloca hífen entre o quarto e o quinto dígitos
    v=v.replace(/(\d)(\d{2})$/,"$1-$2");
    return v;
}


//cep
function cep(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function mcep(v){
    v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"$1.$2");
    v=v.replace(/(\d)(\d{3})$/,"$1-$2");
    return v;
}
window.onload = function(){
    id('telefone').onkeyup = function(){
        mascara( this, mtel );
    }
    id('cnpj').onkeyup = function(){
        cnpj( this, mcnpj );
    }
    id('cep').onkeyup = function(){
        cep( this, mcep );
    }
    id('cpf').onkeyup = function(){
        cpf( this, mcpf );
    }
}