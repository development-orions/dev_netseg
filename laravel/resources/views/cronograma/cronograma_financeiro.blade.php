@extends('app')
@section('title', 'Cronograma')
@section('content')
    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('select').material_select();
            $(".iframe").colorbox({iframe:true, width:"900", height:"500"});
            parent.$.fn.colorbox.close();
        });

    </script>

    <div class="container">

        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cronograma Financeiro</p>
            </div>
        </div>
        <div class="row filtro">
            {!! Form::open(['class' => 'col s12']) !!}

            <div class="col m3">
                <div class="input-field col s12">
                    <select name="ano">
                        <?php for($i = 2016; $i <= date('Y')+1; $i++){ ?>
                        <option value="<?= $i ?>" <?= isset($ano) ? ($ano == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '') ?> ><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <label for="ano">Ano</label>
                </div>
            </div>
            <div class="col m3">
                <div class="input-field col s12">
                    <select name="mes">
                        <option value="01" {{ isset($mes) ? ($mes == 01 ? 'selected' : '') : '' }}>Janeiro</option>
                        <option value="02" {{ isset($mes) ? ($mes == 02 ? 'selected' : '') : '' }}>Fevereiro</option>
                        <option value="03" {{ isset($mes) ? ($mes == 03 ? 'selected' : '') : '' }}>Março</option>
                        <option value="04" {{ isset($mes) ? ($mes == 04 ? 'selected' : '') : '' }}>Abril</option>
                        <option value="05" {{ isset($mes) ? ($mes == 05 ? 'selected' : '') : '' }}>Maio</option>
                        <option value="06" {{ isset($mes) ? ($mes == 06 ? 'selected' : '') : '' }}>Junho</option>
                        <option value="07" {{ isset($mes) ? ($mes == 07 ? 'selected' : '') : '' }}>Julho</option>
                        <option value="08" {{ isset($mes) ? ($mes == 8 ? 'selected' : '') : '' }}>Agosto</option>
                        <option value="09" {{ isset($mes) ? ($mes == 9 ? 'selected' : '') : '' }}>Setembro</option>
                        <option value="10" {{ isset($mes) ? ($mes == 10 ? 'selected' : '') : '' }}>Outubro</option>
                        <option value="11" {{ isset($mes) ? ($mes == 11 ? 'selected' : '') : '' }}>Novembro</option>
                        <option value="12" {{ isset($mes) ? ($mes == 12 ? 'selected' : '') : '' }}>Dezembro</option>
                    </select>
                    <label for="ano">Mês</label>
                </div>
            </div>

            <div class="input-field col s2 m2">
                <select name="contas" class="browser-default">
                    <option value="" {{ isset($contas) ? ($contas == '' ? 'selected' : '') : '' }}>Contas lançadas</option>
                    <option value="1" {{ isset($contas) ? ($contas == 1 ? 'selected' : '') : '' }}>Contas baixadas</option>
                </select>
            </div>
            <div class="input-field col s2 m2">
                <select name="portador" class="browser-default">
                    <option value="" {{ isset($portador) ? ($portador == '' ? 'selected' : '') : '' }}>Todos portadores</option>
                    <option value="Netseg" {{ isset($portador) ? ($portador == 'Netseg' ? 'selected' : '') : '' }}>Netseg</option>
                    <option value="Egydio" {{ isset($portador) ? ($portador == 'Egydio' ? 'selected' : '') : '' }}>Egydio</option>
                    <option value="Medicina" {{ isset($portador) ? ($portador == 'Medicina' ? 'selected' : '') : '' }} >Medicina</option>
                </select>
            </div>


            <div class="input-field col m2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Buscar</button>
            </div>
            {!! Form::close() !!}
        </div>

        @if(isset($cronograma))
            <div class="row">
                <table class="responsive-table bordered highlight cronograma">
                    <thead>
                    <tr>
                        <td class="uppercase center principal" data-field="name">Centro de Custo</td>
                        @for($i=1; $i<=$num; $i++)
                            <td class="uppercase center" data-field="price">{{ $nomemes }} <br/>{{ $i }}</td>
                        @endfor
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="<?= $num + 1 ?>" class="full">Contas à receber</td>
                    </tr>

                    @foreach($cronograma as $crono)
                        @if(isset($crono->contas) && sizeof($crono->contas) != 0)
                            <tr>
                                <td class="centro">{{ $crono->nome }}</td>
                                <?php $cont = 0; ?>
                                @for($i=1; $i<=$num; $i++)
                                    <?php
                                    $recebimentos[$i][$crono->id] = 0.0;
                                    $controle = true;
                                    ?>
                                    @foreach($crono->contas as $conta)
                                        <?php
                                        $data = explode('-', $conta->vencimento);
                                        $data = $data[2];
                                        if($data == $i){ ?>
                                        <td class="blue-text text-darken-2"><a href="{{ Route::getCurrentRoute()->getPrefix() }}/contas_cronograma/1/{{ $ano }}/{{ $mes }}/{{ $i }}/{{ base64_encode($crono->id)}}" title="Lista de contas" class="iframe">R${{ $conta->totalvalor }}</a></td>
                                        <?php
                                        $source = array('.', ',');
                                        $replace = array('', '.');
                                        $valor = str_replace($source, $replace, $conta->totalvalor);

                                        $recebimentos[$i][$crono->id] += $valor;
                                        $controle = false;
                                        break; ?>
                                        <?php } ?>

                                    @endforeach
                                    @if($controle)
                                        <td class="vazio">-</td>
                                        <?php
                                        $recebimentos[$i][$crono->id] += 0;
                                        ?>
                                    @endif
                                @endfor
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <td colspan="<?= $num + 1 ?>" class="full">Contas à pagar</td>
                    </tr>
                    @foreach($cronograma_pagamento as $crono)
                        @if(isset($crono->contas) && sizeof($crono->contas) != 0)
                            <tr>
                                <td class="centro">{{ $crono->nome }}</td>
                                <?php $cont = 0; ?>
                                @for($i=1; $i<=$num; $i++)
                                    <?php
                                    $pagamentos[$i][$crono->id] = 0.0;
                                    $controle = true;
                                    ?>
                                    @foreach($crono->contas as $conta)
                                        <?php

                                        $data = explode('-', $conta->vencimento);
                                        $data = $data[2];
                                        if($data == $i){ ?>
                                        <td class="red-text text-darken-2"><a href="{{ Route::getCurrentRoute()->getPrefix() }}/contas_cronograma/2/{{ $ano }}/{{ $mes }}/{{ $i }}/{{ base64_encode($crono->id)}}" title="Lista de contas" class="iframe red-text text-darken-2">R${{ $conta->totalvalor }}</a></td>
                                        <?php
                                        $source = array('.', ',');
                                        $replace = array('', '.');
                                        $valor = str_replace($source, $replace, $conta->totalvalor);

                                        $pagamentos[$i][$crono->id] += $valor;
                                        $controle = false;
                                        break; ?>
                                        <?php } ?>
                                    @endforeach
                                    @if($controle)
                                        <td class="vazio">-</td>
                                        <?php
                                        $pagamentos[$i][$crono->id] += 0;
                                        ?>
                                    @endif
                                @endfor
                            </tr>
                        @endif
                    @endforeach
                    @if(isset($recebimentos))
                        <tr>
                            <td class="blue-text text-darken-2 text-accent-4">TOTAL RECEITA</td>
                            @for($i=1; $i<=$num; $i++)
                                <td>
                                    <?php
                                    $total_receita[$i] = 0.0;
                                    $total = 0.0;
                                    foreach($recebimentos[$i] as $cc){
                                        $total += $cc;
                                    }
                                    $total_receita[$i] += $total;
                                    echo 'R$'.number_format($total, 2, ",", ".");
                                    ?>
                                </td>
                            @endfor
                        </tr>
                    @endif
                    @if(isset($pagamentos))
                        <tr>
                            <td class="red-text text-darken-2 text-accent-4">TOTAL DESPESAS</td>
                            @for($i=1; $i<=$num; $i++)
                                <td>
                                    <?php
                                    $total_despesa[$i] = 0.0;
                                    $total = 0.0;
                                    foreach($pagamentos[$i] as $cc){
                                        $total += $cc;
                                    }
                                    $total_despesa[$i] += $total;
                                    echo 'R$'.number_format($total, 2, ",", ".");
                                    ?>
                                </td>
                            @endfor
                        </tr>
                    @endif
                    <tr>
                        <td class="green-text text-darken-2">SALDO PARCIAL</td>
                        <?php if(!isset($total_receita)){
                            $total_receita = 0;
                        } ?>

                        <?php if(isset($total_receita) && isset($total_despesa)){ ?>
                        @for($i=1; $i<=$num; $i++)
                            <td class="
                            <?php if($total_receita[$i] - $total_despesa[$i] < 0){
                                echo 'red lighten-2';
                            }elseif($total_receita[$i] - $total_despesa[$i] > 0){
                                echo 'green lighten-2';
                            }else{
                                echo 'grey lighten-2';
                            } ?>
                                    ">
                                R$<?= number_format($total_receita[$i] - $total_despesa[$i], 2, ",", ".")  ?>
                            </td>
                        @endfor
                        <?php }elseif(!isset($total_receita)){
                        $total_receita = 0; ?>
                        @for($i=1; $i<=$num; $i++)
                            <td class="
                            <?php if($total_receita - $total_despesa[$i] < 0){
                                echo 'red lighten-2';
                            }elseif($total_receita - $total_despesa[$i] > 0){
                                echo 'green lighten-2';
                            }else{
                                echo 'grey lighten-2';
                            } ?>
                                    ">
                                R$<?= number_format($total_receita - $total_despesa[$i], 2, ",", ".")  ?>
                            </td>
                        @endfor
                        <?php }else{ $total_despesa = 0; ?>
                        @for($i=1; $i<=$num; $i++)
                            <td class="
                            <?php if($total_receita[$i] - $total_despesa < 0){
                                echo 'red lighten-2';
                            }elseif($total_receita[$i] - $total_despesa > 0){
                                echo 'green lighten-2';
                            }else{
                                echo 'grey lighten-2';
                            } ?>
                                    ">
                                R$<?= number_format($total_receita[$i] - $total_despesa, 2, ",", ".")  ?>
                            </td>
                        @endfor

                        <?php } ?>
                    </tr>
                    </tbody>
                </table>
                <div class="col s12">
                    <?php
                    $saldo_tt_receita = 0;
                    $saldo_tt_despeda = 0;

                    if(isset($recebimentos)){
                        foreach($total_receita as $tl_r){
                            $saldo_tt_receita += $tl_r;
                        }
                    }
                    if(isset($pagamentos)){
                        foreach($total_despesa as $tl_r){
                            $saldo_tt_despeda += $tl_r;
                        }
                    }
                    ?>
                    <h5>Saldo Total: R$<?= number_format($saldo_tt_receita - $saldo_tt_despeda, 2, ",", ".") ?></h5>
                </div>
            </div>
        @else
            <p>Escolha um mês e ano para buscar o cronograma.</p>
        @endif
    </div>
    <div id="modal1" class="modal"></div>
@endsection