@extends('app')
@section('title', 'Cronograma')
@section('content')
    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('select').material_select();
            $(".iframe").colorbox({
                iframe: true,
                width: "900",
                height: "500",
                onClosed: function() {
                    parent.location.reload(true);
                }
            });
            parent.$.fn.colorbox.close();
            $('#resultado').hide();
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });

        function seleciona(id, razaosocial, cnpj) {
            $('#res').empty();
            $('#resultadop').hide();
            $('#cliente_id').val(id);
            $('#busca').val(razaosocial).click(function() {
                $(this).val('').unbind('click');
            });
            $('#mostra').html('<h5>Cliente</h5><p>Razão Social: ' + razaosocial + '</p><p>CNPJ: ' + cnpj + '</p>');
        }
        /// busca por cliente
        function pesquisa(event) {
            valor = $('#busca').val();
            if (valor == '') {
                $('#res').empty();
                $('#resultado').hide();
                return false
            }

            $.ajax({
                url: '/buscarcliente/' + valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso) {
                    $('#res').empty();
                    $('#resultado').show();
                    $.each(sucesso, function(i, item) {
                        $('#res').append('<li><a href="#" onClick="seleciona(\'' + item.id + '\',\'' +
                            item.razaosocial + '\', \'' + item.cnpj + '\')" title="' + item
                            .razaosocial + ' - ' + item.cnpj + '"><span>' + item.razaosocial +
                            ' - ' + item.cnpj + '</span></a></li>');
                    });
                    if (jQuery.isEmptyObject(sucesso)) {
                        $('#resultado').hide();
                    }
                },
                error: function() {
                    $('#res').empty();
                    $('#resultado').hide();
                }
            });
        }

    </script>

    <div class="container">

        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cronograma</p>
            </div>
        </div>
        <div class="row filtro">
            {!! Form::open(['class' => 'col s12']) !!}

            <div class="col m4">
                <div class="input-field col s12">
                    {!! Form::select('tecnico_id', $tecnicos, isset($tecnico) ? $tecnico : null, ['id' => 'tecnico']) !!}
                    <label>Técnico</label>
                </div>
            </div>
            <div class="col m3">

                <div class="input-field col s12">
                    <select name="ano_inicio">
                        <?php for ($i = 2016; $i <= date('Y') + 1; $i++) { ?> <option value="<?= $i ?>" <?= isset($ano) ? ($ano == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '') ?> ><?= $i ?></option>
                        <?php } ?>
                        </select>
                        <label for="ano">Início</label>
                </div>
                </div>

                        <div class="col m3">

                            <div class="input-field col s12">
                                <select name="ano_fim">
                                    <?php for ($i = 2016; $i <= date('Y') + 1; $i++) { ?>
                                    <option value="<?= $i ?>" <?= isset($ano) ? ($ano == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '') ?> ><?= $i ?></option>
                                    <?php } ?>
                                </select>
                                <label for="ano">Fim</label>
                            </div>
                        </div>

                        <div class="col m2" style="text-align: center">
                            <button class="btn waves-effect waves-light" type="submit" style="margin-top: 20px">Buscar</button>
                        </div>
                        {!!  Form::close() !!}
                    </div>
            @if (isset($atividades))
                <div class="row">
                    <canvas id="myChart"></canvas>
                </div>
            @else
                <h4 style="margin-left: 30px">Preencha as informações para realizarmos a busca.</h4>
            @endif



            </div>
                
            <script>

                let activities = JSON.parse(`{!! $atividades !!}`);
                console.log(activities);

                let labels = [];
                Object.keys(activities).forEach(function(key) { labels.push(key); });
                let data = [];
                Object.keys(activities).forEach(function(key) { data.push(activities[key]); });
                

                var ctx = document.getElementById('myChart');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels,
                        datasets: [{
                            label: 'Gráfico de atividades',
                            data,
                            backgroundColor: '#4CAF50',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                ticks: {
                                    autoSkip: false,
                                    maxRotation: 90,
                                    minRotation: 90
                                }
                            }]
                        }
                    }
                });


                
            </script>
@endsection
