@extends('iframe')
@section('title', 'Adicionar Atividade')
@section('content')
    @if(isset($cronograma_edit))
        <script type="text/javascript" src="/js/validador/cronograma_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/cronograma.js"></script>
    @endif
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">{{ isset($cronograma_edit) ? 'Editar' : 'Adicionar' }} Cronograma</p>
            </div>
        </div>
        <div class="row">

            <div class="col s6">
                {!! Form::open(['id' => 'arquivoform', 'files' => 'true']) !!}
                <div class="row">
                    <div class="col s12">
                        <p>Cliente: {{ $cliente->nome }}</p>
                        <p>Técnico: {{ $tecnico->nome }}</p>
                        <p>Atividade: {{ $atividade->nome }}</p>
                        <p>Ano: {{ $ano }}</p>
                    </div>
                </div>

                <div class="row">


                    @if(isset($cronograma_edit))
                        {!! Form::hidden('id', (isset($cronograma_edit['id']) ? $cronograma_edit['id']:''), ['id' => 'id']) !!}
                    @endif
                    <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">

                    {!! Form::hidden('cliente_id', $cliente->id, ['id' => 'cliente_id']) !!}
                    {!! Form::hidden('tecnico_id', $tecnico->id, ['id' => 'tecnico_id']) !!}
                    {!! Form::hidden('atividade_id', $atividade->id, ['id' => 'atividade_id']) !!}
                    {!! Form::hidden('ano', $ano, ['id' => 'ano']) !!}
                    {!! Form::hidden('mes', $mes, ['id' => 'mes']) !!}

                        <div class="row">
                            <div class="input-field col m6 s12">
                                {!! Form::label('vencimento', isset($cronograma_edit['vencimento']) ? '' :'* Data de Vencimento:' ) !!}
                                {!! Form::text('vencimento', (isset($cronograma_edit['vencimento']) ? $cronograma_edit['vencimento']:''), ['class' => 'datepicker picker__input', 'id' => 'vencimento', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '', Auth::user()->role_id != 11 ? '' : 'disabled']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6 s12 disabled">
                                {!! Form::select('statu_id', $status, (isset($cronograma_edit['statu_id']) ? $cronograma_edit['statu_id']:''), ['id' => 'statu_id', Auth::user()->role_id != 11 ? '' : 'disabled']) !!}
                                <label>* Status</label>
                            </div>
                        </div>

                    <div class="row">
                        <div class="input-field col s12 m6">
                            {!! Form::text('nome', (isset($cronograma_edit['nome']) ? $cronograma_edit['nome']:''), ['class' => 'materialize-textarea', 'id' => 'nome']) !!}
                            {!! Form::label('nome', 'Nome do Arquivo:') !!}

                        </div>
                    </div>
                    <div class="row arquivo">
                        <div class="input-field col s10 m5">

                            <input type="file" name="arquivo" id="arquivo" onchange="$('#nome_arquivo1').val(this.value)" style="display: none">
                            <input type="text" name="nome_arquivo" id="nome_arquivo1" class="form-control" placeholder="Arquivo" value="{{isset($cronograma_edit['file']) ? $cronograma_edit['file'] : ''}}" disabled>
                            <input type="button" class="btn" value="Selecione um arquivo" onclick="$('#arquivo').click();">
                        </div>
                        @if(isset($cronograma_edit) && isset($cronograma_edit['file']))
                            <div class="col s2 m1" style="margin-top: 22px;">
                                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/deletar_arquivo/{{ base64_encode($cronograma_edit['id']) }}" title="Excluir arquivo" class="margin red delete btn-sm btn-floating"><i class="fa fa-times"></i></a>
                            </div>
                        @endif
                    </div>
                    @if(isset($cronograma_edit))
                        <div class="row">

                            <div class="input-field col m12 s12">
                                {!! Form::textarea('observacao', old('observacao'), ['class' => 'materialize-textarea', 'id' => 'observacao']) !!}
                                {!! Form::label('observacao', 'Observação:') !!}
                            </div>

                        </div>
                    @endif
                    <div class="row">
                        <div class="col m4">
                            <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action"> {{ isset($cronograma_edit) ? 'Editar' : 'Cadastrar' }}</button>

                        </div>
                        @if(isset($cronograma_edit) && Auth::user()->role_id != 11)
                            <div class="col m4">
                                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/remover_cronograma/{{ $cronograma_edit->id }}" class="waves-effect waves-teal btn-flat red white-text">Remover</a>

                            </div>
                        @endif
                    </div>


                </div>
                {!! Form::close() !!}
            </div>
            <div class="col s6">
                @if(isset($cronograma_edit))

                    <p>OBSERVAÇÕES</p>
                    <table class="bordered highlight">
                        <thead>
                        <tr>
                            <th>Data</th>
                            <th>Observação</th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($observacoes as $observacao)
                            <tr>
                                <td>{{ $observacao->created_at }}</td>
                                <td>{{ $observacao->observacao }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">Nenhuma observação encontrada.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    @if(isset($cronograma_edit['file']))
                    <p>ARQUIVO</p>
                    <a href="/uploads/cronograma/{{ $cronograma_edit['file']}}" target="_blank">{{ $cronograma_edit['nome'] }}</a>
                    @endif

                    @if($cronograma_edit['statu_id'] == 12 && Auth::user()->role_id != 11)
                        @forelse($naoconformidade as $naoconf)
                            <p class="right yellow-text bold">Visualizar Não Conformidade <a href="/pdf/naoconformidade/{{ base64_encode($naoconf->id) }}" title="Visualizar Não Conformidade" class="btn-floating yellow iframe"><i class="material-icons">notifications</i></a></p>
                        @empty
                            <p class="right red-text bold">Adicionar Não Conformidade <a href="{{ Route::getCurrentRoute()->getPrefix() }}/naoconformidade/{{ $cronograma_edit['id'] }}" title="Adicionar Não Conformidade" class="btn-floating red iframe"><i class="material-icons">add_alert</i></a></p>
                        @endforelse
                    @endif
                @endif
            </div>

        </div>
    </div>

    <div id="modal1" class="modal"></div>
@endsection


