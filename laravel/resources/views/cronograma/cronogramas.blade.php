@extends('app')
@section('title', 'Cronograma')
@section('content')
    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('select').material_select();
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                    parent.location.reload(true);
                    ;}});
            parent.$.fn.colorbox.close();
            $('#resultado').hide();
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });

        function seleciona(id, razaosocial, cnpj){
            $('#res').empty();
            $('#resultadop').hide();
            $('#cliente_id').val(id);
            $('#busca').val(razaosocial).click(function(){
                $(this).val('').unbind('click');
            });
            $('#mostra').html('<h5>Cliente</h5><p>Razão Social: '+ razaosocial +'</p><p>CNPJ: '+ cnpj +'</p>');
        }
        /// busca por cliente
        function pesquisa(event){
            valor = $('#busca').val();
            if (valor == ''){
                $('#res').empty();
                $('#resultado').hide();
                return false
            }

            $.ajax({
                url: '/buscarcliente/'+valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso){
                    $('#res').empty();
                    $('#resultado').show();
                    $.each(sucesso, function(i, item){
                        $('#res').append('<li><a href="#" onClick="seleciona(\''+item.id+'\',\''+item.razaosocial+'\', \''+item.cnpj+'\')" title="'+item.razaosocial+' - '+item.cnpj+'"><span>'+item.razaosocial+' - '+item.cnpj+'</span></a></li>');
                    });
                    if(jQuery.isEmptyObject(sucesso)){
                        $('#resultado').hide();
                    }
                },
                error: function(){
                    $('#res').empty();
                    $('#resultado').hide();
                }
            });
        }

    </script>

    <div class="container">

        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cronograma</p>
            </div>
        </div>
        <div class="row filtro">
            {!! Form::open(['class' => 'col s12']) !!}
            <div class="col m4">

                <div class="input-field col s12 m12">
                    <input type="text" name="busca" id="busca" onKeyUp="pesquisa(event)" autocomplete="off">
                    <label for="busca">Novo Cliente</label>
                </div>
                <div class="col s12">
                    <div id="resultado"><ul class="list_json" id="res"></ul></div>
                </div>
                <div class="row padding">
                    {!! Form::hidden('cliente_id', old('cliente_id'), ['id' => 'cliente_id']) !!}
                    <div class="col m12" id="mostra">
                        <h5>Cliente</h5>
                        <p>Razão Social: {{ isset($cliente) ? $cliente->razaosocial : '' }}</p>
                        <p>CNPJ: {{ isset($cliente) ? $cliente->cnpj : '' }}</p>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="input-field col s12">
                    {!! Form::select('tecnico_id', $tecnicos, isset($tecnico) ? $tecnico : null, ['id' => 'tecnico']) !!}
                    <label>Técnico</label>
                </div>
            </div>
            <div class="col m3">

                <div class="input-field col s12">
                    <select name="ano">
                        <?php for($i = 2016; $i <= date('Y')+1; $i++){ ?>
                        <option value="<?= $i ?>" <?= isset($ano) ? ($ano == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '') ?> ><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <label for="ano">Ano</label>
                </div>
            </div>
            <div class="col m2">
                <button class="btn waves-effect waves-light" type="submit" name="action">Buscar</button>
            </div>
            {!! Form::close() !!}
        </div>
        @if(isset($atividades))
            <div class="row">
                <table class="responsive-table bordered highlight cronograma">
                    <thead>
                    <tr>
                        <td class="uppercase center" data-field="name">Atividade</td>
                        <td class="uppercase center" data-field="price">JAN <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">FEV <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">MAR <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">ABR <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">MAI <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">JUN <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">JUL <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">AGO <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">SET <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">OUT <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">NOV <br/>{{ $ano }}</td>
                        <td class="uppercase center" data-field="price">DEZ <br/>{{ $ano }}</td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($atividades as $atividade)
                        <tr>
                            <td>{{ $atividade->nome }}</td>

                            @for($i = 1; $i <=12; $i++)
                                <?php $controle = true; ?>
                                @if(sizeof($atividade->cronogramas) != 0)
                                    @foreach($atividade->cronogramas as $crono)
                                        <?php
                                        $bg = 'bgverde2';
                                        switch($crono->statu_id){
                                            case 8: $bg='bg-amarelo'; break;
                                            case 9: $bg='bgverde2'; break;
                                            case 10: $bg='bg-azul'; break;
                                            case 11: $bg='bg-azulescuro'; break;
                                            case 12: $bg='bg-vermelho'; break;
                                            case 13: $bg='bg-cinza'; break;
                                        }
                                        ?>
                                        @if($crono->mes == $i)
                                            <td class="{{ $bg }}">
                                                <a <?= Auth::user()->role_id != 11 ? 'title="Editar Atividade"': 'title="Adicionar arquivo"' ?> href="{{ Route::getCurrentRoute()->getPrefix() }}/cronograma_edita/{{ $crono->id }}" class="iframe"><?= Auth::user()->role_id != 11 ? '<i class="fa fa-edit fa-3x"></i>': '<i class="fa fa-file fa-2x"></i>' ?></a>
                                            </td>
                                            <?php
                                            $controle = false;
                                            break; ?>
                                        @endif
                                    @endforeach
                                    @if($controle)
                                        <td>
                                            @if(Auth::user()->role_id != 11)
                                            <a href="{{ Route::getCurrentRoute()->getPrefix() }}/adicionar_atividade/{{ $cliente->id }}/{{$tecnico}}/{{ $atividade->id }}/{{ $ano }}/{{ $i }}" class="iframe"><i class="fa fa-plus-square fa-3x"></i></a>
                                            @endif
                                        </td>
                                    @endif
                                @else
                                    <td>
                                        @if(Auth::user()->role_id != 11)
                                        <a href="{{ Route::getCurrentRoute()->getPrefix() }}/adicionar_atividade/{{ $cliente->id }}/{{$tecnico}}/{{ $atividade->id }}/{{ $ano }}/{{ $i }}" class="iframe"><i class="fa fa-plus-square fa-3x"></i></a>
                                        @endif
                                    </td>
                                @endif
                            @endfor
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p>Escolha um cliente, um técnico e um ano para buscar o cronograma.</p>
        @endif



    </div>
    <div id="modal1" class="modal"></div>
@endsection