@extends('iframe')
@section('title', 'Adicionar Atividade')
@section('content')
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Contas do dia {{ $dia }} - {{ $centro_nome->nome }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Conta</th>
                        <th>Cliente</th>
                        <th>Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contas as $conta)
                        <tr>
                            <td>{{ $conta->contas['nome'] }}</td>
                            @if($tipo == 1)
                                <td>{{ $conta->clientes['razaosocial'] }}</td>
                            @else
                                <td>{{ $conta->fornecedores['razaosocial'] }}</td>
                            @endif
                            <td>R$ {{ $conta->contas['total'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection