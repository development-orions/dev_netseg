@extends('app')
@section('title', 'Listar Atendimentos')
@section('content')

    <script type="text/javascript" src="/js/validador/delete.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Atendimentos</p>
            </div>
        </div>

        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">

                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/atendimento_lista" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m2 s3">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col s3 m2">
                    {!! Form::select('statu_id', ['' => 'Todos status', '1' => 'Ativos', '3' => 'Cancelado'], isset($data['statu_id']) ? $data['statu_id'] : null, ['id' => 'status']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Descrição</th>
                        <th>Data</th>
                        <th>Paciente</th>
                        <th>Medico</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($atendimentos as $atendimento)
                        <tr>
                            <td>{{ $atendimento->descricao }}</td>
                            <td>{{ $atendimento->data }}</td>
                            <td>{{ $atendimento->paciente->nome }}</td>
                            <td>{{ $atendimento->medico->nome }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/atendimento_edita/{{ base64_encode($atendimento->id) }}" title="Editar atendimento" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>

                                            <li><a href="#" data-href="{{ Route::getCurrentRoute()->getPrefix() }}/atendimento_cancela/{{ base64_encode($atendimento->id) }}" data-text="Tem certeza que deseja excluir este atendimento?" title="Excluir atendimento" class="btn-floating red delete"><i class="fa fa-times"></i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @if($atendimento->statu_id != 3)
                        <tr>
                            <td colspan="6">
                                <table>
                                    <span class="font-weight-bold">Exames</span><br>
                                    @foreach($atendimento->atendimentoExames as $exame)
                                        <tr>
                                            <td class="p-0"> {{$exame->exame->nome}}</td>
                                            <td class="p-0">   {{$exame->laboratorio->nome}} </td>
                                            <td class="p-0">   <span class="font-weight-bold">Valor: </span>{{$exame->valor_r}}</td>
                                            <td class="p-0">   <span class="font-weight-bold">Valor Médico: </span>{{$exame->valor_med}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                        </tr>
                        @endif
                    @empty
                        <tr>
                            <td colspan="6">Nenhum atendimento encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao"><?php echo $atendimentos->appends(Request::all())->render(); ?></div>
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection