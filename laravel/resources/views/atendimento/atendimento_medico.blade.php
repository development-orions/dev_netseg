@extends('app')
@section('title', 'Listar Atendimentos')
@section('content')

    <script type="text/javascript" src="/js/validador/relatorio_medico.js"></script>
    <script type="text/javascript" src="/js/validador/delete.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatórios de atendimento por médico</p>
            </div>
        </div>

        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">

                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_exame" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" id="Envia">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m2 s3">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col s4 m3">
                    {!! Form::select('medico_id', ['' => 'Selecione o médico']+$medicos, isset($data['medico_id']) ? $data['medico_id'] : null, ['id' => 'medicos']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        @if(isset($atendimentos))
            <div class="row">
                <div class="col s12 text-right">
                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/atendimento_medico_pdf?{{(http_build_query(Request::all()))}}" target="_blank" class="waves-effect waves-light btn btn-lh"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Gerar PDF</a>
                </div>
                <div class="col s12 mt-2">
                    <table class="bordered highlight">
                        <thead>
                        <tr>
                            <th>Paciente</th>
                            <th>Data do atendimento</th>
                            <th>Descrição</th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($atendimentos as $atendimento)
                            <tr>
                                <td>{{ $atendimento->nome }}</td>
                                <td>{{ $atendimento->data }}</td>
                                <td>{{ $atendimento->descricao }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Nenhum atendimento encontrado.</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    <div id="modal1" class="modal"></div>
@endsection