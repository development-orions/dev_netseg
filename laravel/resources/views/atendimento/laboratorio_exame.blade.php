@extends('app')
@section('title', 'Listar Atendimentos')
@section('content')

    <script type="text/javascript" src="/js/validador/relatorio_laboratorio.js"></script>
    <script type="text/javascript" src="/js/validador/delete.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de exame por Laboratório</p>
            </div>
        </div>

        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">

                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/laboratorio_exame" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" id="Envia">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m2 s3">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col s4 m3">
                    {!! Form::select('laboratorio_id', ['' => 'Selecione o laboratório']+$laboratorios, isset($data['laboratorio_id']) ? $data['laboratorio_id'] : null, ['id' => 'laboratorios']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        @if(isset($atendimentos))
            <div class="row">
                <div class="col s12 text-right">
                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/laboratorio_exame_pdf?{{(http_build_query(Request::all()))}}" target="_blank" class="waves-effect waves-light btn btn-lh"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Gerar PDF</a>
                </div>
                <div class="col s12">
                    <table class="bordered highlight">
                        <thead>
                        <tr>
                            <th class="small-th">Nº de exames</th>
                            <th>Exame</th>
                            <th>Paciente</th>
                            <th>Valor Exame</th>
                            <th>Valor Médico</th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($atendimentos as $atendimento)
                            <tr>
                                <td class="text-center">1</td>
                                <td>{{ $atendimento->exame }}</td>
                                <td>{{ $atendimento->nome }}</td>
                                <td>{{ $atendimento->valor_r }}</td>
                                <td>{{ $atendimento->valor_med }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">Nenhum exame encontrado.</td>
                            </tr>
                        @endforelse

                        @if(sizeof($atendimentos)>0)
                            <tr>
                                <td class="text-center font-weight-bold">Total</td>
                                <td></td>
                                <td></td>
                                <td class="font-weight-bold">Valor Exame total</td>
                                <td class="font-weight-bold">Valor Médico total</td>
                            </tr>
                            <tr>
                                <td class="text-center">{{sizeof($atendimentos)}}</td>
                                <td></td>
                                <td></td>
                                <td>{{$valor_total}}</td>
                                <td>{{$valor_medico_total}}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    <div id="modal1" class="modal"></div>
@endsection