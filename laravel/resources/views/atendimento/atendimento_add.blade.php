@extends('app')
@section('title', 'Cadastrar Atendimento')
@section('content')

    <link href="/css/select2-materialize.css" rel="stylesheet" />
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/i18n/pt-BR.js" type="text/javascript"></script>
    <script src="/js/select2-materialize.js"></script>

    @if(isset($atendimento_edit))
        <script type="text/javascript" src="/js/validador/atendimento_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/atendimento.js"></script>
    @endif


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Atendimentos</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                @if(isset($atendimento_edit))
                    {!! Form::hidden('id', (isset($atendimento_edit['id']) ? $atendimento_edit['id']:''), ['id' => 'id']) !!}

                @endif

                <div class="input-field col m12 s12">
                    {!! Form::textarea('descricao', (isset($atendimento_edit['descricao']) ? $atendimento_edit['descricao']:''), ['class' => 'validate materialize-textarea', 'id' => 'descricao']) !!}
                    {!! Form::label('descricao', '*Descrição: ') !!}
                </div>

                <div class="input-field col m6 s12">
                    {!! Form::label('data', '* Data do Atendimento: ') !!}
                    {!! Form::text('data', (isset($atendimento_edit['data']) ? $atendimento_edit['data']:''), ['class' => 'datepicker picker__input', 'id' => 'data', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>


                <div class="input-field col m6 s12">
                    <select name="medico" id="medico">
                        <option value="">Selecione</option>
                        @foreach($medicos as $key => $medico)
                            <option {{isset($atendimento_edit)&& $atendimento_edit['medico_id'] == $key ? 'selected' : ''}} value="{{$key}}">{{$medico}}</option>
                        @endforeach
                    </select>
                    {!! Form::label('medico', '*  Médico: ') !!}
                </div>

                <div class="input-field col m6 s12">
                    <select name="paciente" id="paciente">
                        <option value="">Selecione</option>
                    </select>
                    {!! Form::label('pacicente', '*  Paciente: ' ,array('class' => 'paciente_select')) !!}
                </div>

                <div class="input-field col m12 s12">
                    {!! Form::textarea('observacao', (isset($atendimento_edit['observacao']) ? $atendimento_edit['observacao']:''), ['class' => 'validate materialize-textarea', 'id' => 'observacao']) !!}
                    {!! Form::label('observacao', '* Observações: ') !!}
                </div>



                <div class="col s12 bgverde mb-4">
                    <p class="bold font20 corbranca">Exames</p>
                </div>

                <div class="row" id="dynamic_field">

                    @if(!isset($atendimento_exames) || sizeof($atendimento_exames) == 0)
                        <div class="row">
                            <div class="input-field col m6 s12">
                                {!! Form::select('exame_id[]',['' => 'Selecionar Exame']+$exames, null, ['id' =>'exame_id']) !!}
                                {!! Form::label('exame_id', 'Exame:') !!}
                            </div>
                            <div class="input-field col m2 s2 mt-2" id="addbtn">
                                <button type="button" name="add" id="add" class="btn-floating"><i class="fa fa-plus"></i></button>
                            </div>

                        </div>

                        <div class="row">
                            <div class="input-field col m6 s12">
                                {!! Form::select('laboratorio_id[]',['' => 'Selecionar Laboratório']+$laboratorios, null, ['id' =>'laboratorio_id']) !!}
                                {!! Form::label('laboratorio_id', 'Laboratório:') !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s3">
                                {!! Form::text('valor[]','', ['class' => 'validate', 'id' => 'valor']) !!}
                                {!! Form::label('valor', 'Valor: ') !!}
                            </div>

                            <div class="input-field col s3">
                                {!! Form::text('valor_medico[]','', ['class' => 'validate', 'id' => 'valor_medico']) !!}
                                {!! Form::label('valor_medico', 'Valor médico: ') !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col m6 s12">
                                <select name="medico_id[]" id="medico">
                                    <option value="">Selecione</option>
                                    @foreach($medicos as $key => $medico)
                                        <option value="{{$key}}">{{$medico}}</option>
                                    @endforeach
                                </select>
                                {!! Form::label('medico', 'Médico (opcional): ') !!}
                            </div>
                        </div>


                    @else


                        @foreach($atendimento_exames as $key => $mp)
                            <div class="row" {{$key != 0 ? 'id=row'.$key : '' }}>
                                <div class="row">
                                    <div class="input-field col s6">
                                        {!! Form::select('exame_id[]',['' => 'Selecionar Exame']+$exames, $mp['exame_id'], ['id' =>'exame_id']) !!}
                                        {!! Form::label('exame_id', 'Exame:') !!}
                                    </div>


                                    @if($key == 0)
                                        <div class="input-field col m2 s2 mt-2" id="addbtn">
                                            <button type="button" name="add" id="add" class="btn-floating"><i class="fa fa-plus"></i></button>
                                        </div>
                                    @else
                                        <div class="input-field col m2 s2 mt-2">
                                            <button type="button" name="remove" id="{{$key}}" class="btn-floating red btn_remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    @endif

                                </div>

                                <div class="row">
                                    <div class="input-field col s6">
                                        {!! Form::select('laboratorio_id[]',['' => 'Selecionar Laboratório']+$laboratorios, $mp['laboratorio_id'], ['id' =>'laboratorio_id']) !!}
                                        {!! Form::label('laboratorio_id', 'Laboratório:') !!}
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s3">
                                        {!! Form::text('valor[]',$mp['valor'], ['class' => 'validate', 'id' => 'valor']) !!}
                                        {!! Form::label('valor', 'Valor: ') !!}
                                    </div>

                                    <div class="input-field col s3">
                                        {!! Form::text('valor_medico[]',$mp['valor_medico'], ['class' => 'validate', 'id' => 'valor_medico']) !!}
                                        {!! Form::label('valor_medico', 'Valor médico: ') !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col m6 s12">
                                        <select name="medico_id[]" id="medico">
                                            <option value="">Selecione</option>
                                            @foreach($medicos as $key => $medico)
                                                <option {{isset($mp)&& $mp['medico_id'] == $key ? 'selected' : ''}} value="{{$key}}">{{$medico}}</option>
                                            @endforeach
                                        </select>
                                        {!! Form::label('medico', 'Médico (opcional): ') !!}
                                    </div>
                                </div>

                            </div>

                        @endforeach
                    @endif
                </div>

            </div>
        </div>



        <div class="row right-align">

            <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($atendimento_edit) ? 'Salvar' : 'Cadastrar'  }}
                <i class="material-icons right">send</i>
            </button>
            @if(isset($atendimento_edit))
                <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
            @endif

        </div>

        {!! Form::close() !!}
    </div>
    </div>
    <div id="modal1" class="modal"></div>

    <script>
        $(document).ready(function() {
            $('#paciente').select2({
                language: "pt-BR",
                minimumInputLength: 5,

                ajax: {
                    url: $('#prefixo').val()+'/pacientes_ajax',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                }
            });


                    @if(!isset($atendimento_exames) || sizeof($atendimento_exames) == 0)
            var i=1;
                    @else
            var i={{count($atendimento_exames) -1}};
            @endif


            $('#add').click(function(){
                i++;
                $('#dynamic_field').append(
                    '<div class="row bordert" id="row'+i+'">'+
                    '<div class="row">'+
                    '<div class="input-field col s6">'+
                    '{!! Form::select('exame_id[]',['' => 'Selecionar Exame']+$exames, null, ['class' => 'select','id' =>'exame_id']) !!}'+
                    '{!! Form::label('exame_id', 'Exame:') !!}'+
                    '</div>'+
                    '<div class="input-field m2 s2 mt-2">'+
                    '<button type="button" name="remove" id="'+i+'" class="btn-floating red btn_remove"><i class="fa fa-times"></i></button>'+
                    '</div>'+
                    '</div>'+
                    '<div class="row">'+
                    '<div class="input-field col s6">'+
                    '{!! Form::select('laboratorio_id[]',['' => 'Selecionar Laboratório']+$laboratorios, null, ['class' => 'select','id' =>'laboratorio_id']) !!}'+
                    '{!! Form::label('laboratorio_id', 'Laboratório:') !!}'+
                    '</div>'+
                    '</div>'+
                    '<div class="row">'+
                    '<div class="input-field col s3">'+
                    '{!! Form::text('valor[]','', ['class' => 'validate', 'id' => 'valor']) !!}'+
                    '{!! Form::label('valor', 'Valor: ') !!}'+
                    '</div>'+
                    '<div class="input-field col s3">'+
                    '{!! Form::text('valor_medico[]','', ['class' => 'validate', 'id' => 'valor_medico']) !!}'+
                    '{!! Form::label('valor_medico', 'Valor médico: ') !!}'+
                    '</div>'+
                    '</div>'+
                    '<div class="row">'+
                    '<div class="input-field col m6 s12">'+
                    '<select name="medico_id[]" id="medico" class="select">' +
                    '<option value="">Selecione</option>' +
                    '@foreach($medicos as $key => $medico)' +
                    '<option {{isset($mp)&& $mp["medico_id"] == $key ? "selected" : ""}} value="{{$key}}">{{$medico}}</option>' +
                    '@endforeach' +
                    '</select>' +
                    '{!! Form::label("medico", "Médico (opcional):") !!}' +
                    '</div>' +
                    '</div>'+
                    '</div>');

                $('.select').material_select('destroy');
                $('.select').material_select();
            });


            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });


                    @if(isset($atendimento_edit) && !empty($atendimento_edit))
            var data = {
                    id: {{$atendimento_edit['paciente_id']}},
                    text: '{{$atendimento_edit['paciente_nome']}}'
                };

            var newOption = new Option(data.text, data.id, false, false);
            $('#paciente').append(newOption).trigger('change');

            $('#paciente').val('{{$atendimento_edit['paciente_id']}}').trigger('change');
            @endif

        });
    </script>
@endsection
