@extends('app')
@section('title', 'Listar Atendimentos')
@section('content')

    <link href="/css/select2-materialize.css" rel="stylesheet" />
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/i18n/pt-BR.js" type="text/javascript"></script>
    <script src="/js/select2-materialize.js"></script>

    <script type="text/javascript" src="/js/validador/relatorio_cliente.js"></script>
    <script type="text/javascript" src="/js/validador/delete.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de exame por Empresa</p>
            </div>
        </div>

        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/empresa_exame" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" id="Envia">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m2 s12">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s12">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>

                <div class="input-field col s12 m6  text-left">
                    <select name="empresa_id" id="empresa_id">
                        <option value="">Selecione</option>
                    </select>
                    {!! Form::label('empresa_id', 'Empresa: ' ,array('class' => 'paciente_select')) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>

        @if(isset($atendimentos))
            <div class="row">
                <div class="col s12 text-right">
                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/empresa_exame_pdf?{{(http_build_query(Request::all()))}}" target="_blank" class="waves-effect waves-light btn btn-lh"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Gerar PDF</a>
                </div>
                <div class="col s12">
                    <table class="bordered highlight">
                        <thead>
                        <tr>
                            <th>Data</th>
                            <th>Funcionário/Paciente</th>
                            <th colspan="2">Exame</th>
                            <th>Valor</th>
                            {{--<th>Valor Médico</th>--}}
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($atendimentos as $atendimento)
                            <tr>
                                <td>{{ $atendimento->data }}</td>
                                <td>{{ $atendimento->nome }}<br>
                                    <small>{{ $atendimento->funcao }}</small>
                                </td>
                                <td>{{ $atendimento->descricao }}</td>
                                <td>{{ $atendimento->exame }}</td>
                                <td>{{ $atendimento->valor_r }}</td>
                                {{--<td>{{ $atendimento->valor_med }}</td>--}}
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">Nenhum exame encontrado.</td>
                            </tr>
                        @endforelse

                        @if(sizeof($atendimentos)>0)
                            <tr>
                                <td></td>
                                <td class="text-center font-weight-bold"><strong>Total</strong></td>
                                <td></td>
                                <td></td>
                                <td class="font-weight-bold">Valor Exame total</td>
                                {{--<td class="font-weight-bold">Valor Médico total</td>--}}
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-center">{{sizeof($atendimentos)}}</td>
                                <td></td>
                                <td></td>
                                <td>{{$valor_total}}</td>
                                {{--<td>{{$valor_medico_total}}</td>--}}
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    <div id="modal1" class="modal"></div>

    <script>
        $(document).ready(function() {
            $('#empresa_id').select2({
                language: "pt-BR",
                minimumInputLength: 5,
                ajax: {
                    url: $('#prefixo').val()+'/empresas_ajax',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                }
            });

            @if(!empty($cliente))
                var data = {
                    id: {{$cliente['id']}},
                    text: '{{$cliente['nome']}} - {{$cliente['cnpj']}}'
                };

                var newOption = new Option(data.text, data.id, false, false);
                $('#empresa_id').append(newOption).trigger('change');

                $('#empresa_id').val('{{$cliente['id']}}').trigger('change');

            @endif

        });
    </script>
@endsection

