@extends('app')
@section('title', 'Gráfico Resultado')
@section('content')
    <script type="text/javascript" src="/js/validador/contas_relatorio.js"></script>

    <div class="container"  id="app">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Gráfico do Resultado</p>
            </div>
        </div>


        <graficoresultado></graficoresultado>
    </div>
    <script src="/js/app.js"></script>

    <div id="modal1" class="modal"></div>
@endsection