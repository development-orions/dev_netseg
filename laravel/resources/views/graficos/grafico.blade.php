@extends('app')
@section('title', 'Gráfico')
@section('content')
    <script type="text/javascript" src="/js/validador/contas_relatorio.js"></script>

    <div class="container"  id="app">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Gráfico</p>
            </div>
        </div>


        <example></example>
    </div>
    <script src="/js/app.js"></script>

    <div id="modal1" class="modal"></div>
@endsection