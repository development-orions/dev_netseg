@extends('app')
@section('title', 'Listar Cliente')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Clientes {{ $status == 1 ? 'Ativos' : 'Inativos' }}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons right">search</i>
                    </button>
                    <div class="input-field col m2">
                        {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar Cliente']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Razão Social</th>
                        <th>Nome Fantasia</th>
                        <th>CNPJ/CPF</th>
                        <th>Cidade</th>
                        <th>Telefone</th>
                        <th>Tipo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->razaosocial }}</td>
                            <td>{{ $cliente->nome }}</td>
                            <td>{{ !empty($cliente->cnpj)? $cliente->cnpj : $cliente->cpf }}</td>
                            <td>{{ $cliente->cidade }}</td>
                            <td>{{ $cliente->telefone_responsavel }}</td>
                            <td>{{ $cliente->tipo }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        @if(Auth::user()->role_id != 7)
                                        <li><a href="/visualizar/cliente_view/{{ base64_encode($cliente->id) }}" title="Visualizar cliente" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/cliente_edita/{{ base64_encode($cliente->id) }}" title="Editar cliente" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @endif
                                        @if($cliente->statu_id == 1)
                                            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 6 || Auth::user()->role_id == 7 )
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/cliente_inativa/{{ base64_encode($cliente->id) }}" title="Inativar cliente" class="btn-floating red"><i class="material-icons">not_interested</i></a></li>
                                            @endif
                                        @else
                                            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 4 || Auth::user()->role_id == 6 || Auth::user()->role_id == 7 )
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/cliente_ativa/{{ base64_encode($cliente->id) }}" title="Ativar cliente" class="btn-floating green"><i class="material-icons">done</i></a></li>
                                            @endif
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">Nenhum cliente encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $clientes->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection