@extends('app')
@section('title', 'Listar Contas')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script src="/js/plugins/paginate.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Contas à {{ $tipo == 1 ? 'Receber' : 'Pagar' }}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">

                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/{{ $tipo == 1 ? 'conta_lista/1' : 'conta_lista_pagar/2' }}" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m1">
                    {!! Form::text('nf', (isset($data['nf']) ? $data['nf']:''), ['class'=>'validate', 'placeholder'=>'NF', 'id' => 'nf']) !!}
                </div>
                <div class="input-field col m2">
                    {!! Form::text('q', (isset($data['q']) ? $data['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar '.($tipo == 1 ? 'Cliente' : 'Fornecedor'), 'id'=>'q']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col s3 m2">
                    {!! Form::select('statu_id', ['' => 'Todos status'] + $status, isset($data['statu_id']) ? $data['statu_id'] : null, ['id' => 'status']) !!}
                </div>
                <div class="input-field col m1">
                    {!! Form::text('conta', (isset($data['conta']) ? $data['conta']:''), ['class'=>'validate', 'placeholder'=>'Nome', 'id' => 'conta']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Conta</th>
                        <th>{{ $tipo == 1 ? 'Cliente' : 'Fornecedor' }}</th>
                        <th>Vencimento</th>
                        <th>Baixa</th>
                        <th>Valor Total</th>
                        <th>Portador</th>
                        <th>NF</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($contas as $conta)
                        <tr>
                            <td>{{ $conta->contas->nome }}</td>
                            <!-- <td>{{ $conta->contas->created_at }}</td> -->
                            <td>{{ $tipo == 1 ? $conta->clientes->nome : $conta->fornecedores->nome }}</td>
                            <td>{{ $conta->contas->vencimento }}</td>
                            <td>{{ $conta->contas->baixa }}</td>
                            <td>R$ {{ $conta->contas->total }}</td>
                            <td>{{ $conta->contas->portador }}</td>
                            <td>{{ $conta->contas->nf }}</td>
                            <td><span>{{ $conta->contas->status->nome }}</span></td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/conta_view/{{ $tipo }}/{{ base64_encode($conta->contas->id) }}" title="Visualizar conta" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        @if(Auth::user()->role_id != 1)
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/{{ $tipo == 1 ? 'conta_edita' : 'conta_edita_pagar' }}/{{ $tipo }}/{{ base64_encode($conta->contas->id) }}" title="Editar conta" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @endif
                                        @if( ($conta->contas->statu_id != 3) && (Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 6))
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/conta_cancela/{{ $tipo }}/{{ base64_encode($conta->contas->id) }}" title="Cancelar conta" class="btn-floating red"><i class="fa fa-times"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhuma conta encontrada.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao"><?php echo $contas->appends(Request::all())->render(); ?></div>
        </div>
    </div>
@endsection