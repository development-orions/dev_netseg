@extends('app')
@section('title', 'Cadastrar Conta')
@section('content')
    @if(isset($conta_edit))
        <script type="text/javascript" src="/js/validador/conta_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/conta.js"></script>
    @endif
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $('#resultado').hide();
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });

        function seleciona(id, razaosocial, cnpj){
            $('#res').empty();
            $('#resultadop').hide();
            $('#cliente_id').val(id);
            $('#busca').val(razaosocial).click(function(){
                $(this).val('').unbind('click');
            });
            $('#mostra').html('<h5>Cliente</h5><p>Razão Social: '+ razaosocial +'</p><p>CNPJ: '+ cnpj +'</p>');
        }
        /// busca por cliente
        function pesquisa(event){
            valor = $('#busca').val();
            if (valor == ''){
                $('#res').empty();
                $('#resultado').hide();
                return false
            }

            $.ajax({
                url: '/buscarcliente/'+valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso){
                    $('#res').empty();
                    $('#resultado').show();
                    $.each(sucesso, function(i, item){
                        $('#res').append('<li><a href="#" onClick="seleciona(\''+item.id+'\',\''+item.razaosocial+'\', \''+item.cnpj+'\')" title="'+item.razaosocial+' - '+item.cnpj+'"><span>'+item.nome+' - '+item.razaosocial+' - '+item.cnpj+'</span></a></li>');
                    });
                    if(jQuery.isEmptyObject(sucesso)){
                        $('#resultado').hide();
                    }
                },
                error: function(){
                    $('#res').empty();
                    $('#resultado').hide();
                }
            });
        }

    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Conta à Receber</p>
            </div>
        </div>
        <div class="row">
            <div class="row padding">
                <div class="col s12">
                    <form class="col s12">
                        <div class="input-field col s12 m6">
                            <input type="text" name="busca" id="busca" onKeyUp="pesquisa(event)" autocomplete="off">
                            <label for="busca">Novo Cliente</label>
                        </div>
                    </form>
                </div>
                <div class="col s12">
                    <div id="resultado"><ul class="list_json" id="res"></ul></div>
                </div>
            </div>
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row padding">
                {!! Form::hidden('cliente_id', (isset($conta_edit->recebimentos[0]->clientes->id) ? $conta_edit->recebimentos[0]->clientes->id:''), ['id' => 'cliente_id']) !!}
                <div class="col m6" id="mostra">
                    <h5>Cliente</h5>
                    <p>Razão Social: {{ (isset($conta_edit) ? $conta_edit->recebimentos[0]->clientes->razaosocial : '') }} </p>
                    <p>CNPJ: {{ (isset($conta_edit) ? $conta_edit->recebimentos[0]->clientes->cnpj: '') }} </p>
                </div>
            </div>
            <div class="row">
                {!! Form::hidden('tipo', $tipo, ['id' => 'tipo']) !!}
                @if(isset($conta_edit))
                    {!! Form::hidden('id', (isset($conta_edit['id']) ? $conta_edit['id']:''), ['id' => 'id']) !!}
                    {!! Form::hidden('statu_id', (isset($conta_edit['statu_id']) ? $conta_edit['statu_id']:'')) !!}
                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($conta_edit['nome']) ? $conta_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    <select name="portador" id="portador">
                        <option value="Egydio" {{ isset($conta_edit['portador']) ? $conta_edit['portador'] == 'Egydio' ? 'selected' : '' : '' }}>Egydio</option>
                        <option value="Netseg" {{ isset($conta_edit['portador']) ? $conta_edit['portador'] == 'Netseg' ? 'selected' : '' : '' }} >Netseg</option>
                        <option value="Medicina" {{ isset($conta_edit['portador']) ? $conta_edit['portador'] == 'Medicina' ? 'selected' : '' : '' }} >Medicina</option>
                    </select>
                    {!! Form::label('portador', '*  Portador: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::label('vencimento', '* Data vencimento: ') !!}
                    {!! Form::text('vencimento', (isset($conta_edit['vencimento']) ? $conta_edit['vencimento']:''), ['class' => 'datepicker picker__input', 'id' => 'vencimento', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('baixa', 'Data da baixa: ') !!}
                    {!! Form::text('baixa', (isset($conta_edit['baixa']) ? $conta_edit['baixa']:''), ['class' => 'datepicker picker__input', 'id' => 'baixa', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s8">
                    {!! Form::text('nf', (isset($conta_edit['nf']) ? $conta_edit['nf']:''), ['class' => 'validate', 'id' => 'nf']) !!}
                    {!! Form::label('nf', 'Núm. * Nota Fiscal: ') !!}
                </div>
                <div class="input-field col m2 s4">
                    {!! Form::text('valor', (isset($conta_edit['valor']) ? $conta_edit['valor']:''), ['class' => 'validate', 'id' => 'valor']) !!}
                    {!! Form::label('valor', '* Valor: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m3 s6">

                    <input name="tipov" type="radio" id="acrescimo" value="1" {{ isset($conta_edit['acrescimo']) ? $conta_edit['acrescimo'] ? 'checked' : '' : ''}} />
                    {!! Form::label('acrescimo', 'Acréscimo') !!}


                    <input name="tipov" type="radio" id="desconto" value="2" {{ isset($conta_edit['desconto']) ? $conta_edit['desconto'] ? 'checked' : '' : '' }} />
                    {!! Form::label('desconto', 'Desconto') !!}

                </div>
                <div class="input-field col m3 s6">
                    {!! Form::text('valor2', (isset($conta_edit['acrescimo']) ? $conta_edit['acrescimo'] : isset($conta_edit['desconto']) ? $conta_edit['desconto'] : ''), ['class' => 'validate', 'id' => 'valor2']) !!}

                </div>
            </div>
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('observacao',(isset($conta_edit['observacao']) ? $conta_edit['observacao']:''), ['class' => 'validate materialize-textarea', 'id' => 'observacao']) !!}
                    {!! Form::label('observacao', 'Observação: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('statu_id', $status, isset($conta_edit['statu_id']) ? $conta_edit['statu_id'] : null, ['id' => 'status']) !!}
                    {!! Form::label('status', '* Status: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6">
                    {!! Form::label('centrocusto_id', '* Centro de Custo: ') !!}
                    {!! Form::select('centrocusto_id', $centrocustos, isset($conta_edit['centrocusto_id']) ? $conta_edit['centrocusto_id'] : null, ['class' => 'browser-default', 'id' => 'centrocusto_id']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m4">
                    {!! Form::select('formapagamento_id', $formapagamento, isset($conta_edit['formapagamento_id']) ? $conta_edit['formapagamento_id'] : null, ['id' => 'formapagamento']) !!}
                    {!! Form::label('formapagamento', '* Forma de Pagamento: ') !!}
                </div>
                <div class="input-field col m4 s12">
                    {!! Form::text('num_boleto', (isset($conta_edit['num_boleto']) ? $conta_edit['num_boleto']:''), ['class' => 'validate', 'id' => 'num_boleto']) !!}
                    {!! Form::label('num_boleto', 'Núm. Boleto: ') !!}
                </div>
                @if(!isset($conta_edit))
                <div class="input-field col m4 s12">
                    <select id="replica" name="replica">
                        <option value="0" selected="selected">Não Replicar</option>
                        <option value="1">1x</option>
                        <option value="2">2X</option>
                        <option value="3">3X</option>
                        <option value="4">4X</option>
                        <option value="5">5X</option>
                        <option value="6">6X</option>
                    </select>
                    <label for="replica"> Replicar: </label>
                </div>
                @endif
            </div>
            <div class="row right-align">            
                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($conta_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($conta_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection



