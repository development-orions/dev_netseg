@extends('app')
@section('title', 'Listar Arquivos')
@section('content')



    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de arquivos</p>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Data</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($arquivos as $arquivo)

                        <tr>
                            <td>{{ $arquivo->nome }}</td>
                            <td>{{ $arquivo->created_at }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/uploads/arquivo/{{$arquivo->file}}" target="_blank" title="Visualizar Arquivo" class="btn-floating yellow"><i class="material-icons">visibility</i></a></li>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum Arquivo encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $arquivos->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection