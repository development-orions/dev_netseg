@extends('app')
@section('title', 'Cadastrar Arquivo')
@section('content')


    <link href="/css/jquery-ui.min.css" rel="stylesheet">
    <script src="/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/validador/arquivo.js"></script>

    <script>
        $(document).ready(function () {



 $(function() {
                var availableTags = {!! $clientes !!};
                function split( val ) {
                    return val.split( /,\s*/ );
                }
                function extractLast( term ) {
                    return split( term ).pop();
                }

                $( "#busca" )
                // don't navigate away from the field on tab when selecting an item
                    .bind( "keydown", function( event ) {
                        if ( event.keyCode === $.ui.keyCode.TAB &&
                            $( this ).autocomplete( "instance" ).menu.active ) {
                            event.preventDefault();
                        }
                    })
                    .autocomplete({
                        minLength: 0,
                        source: function( request, response ) {
                            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(extractLast(request.term)), "i" );
                            var select_el = availableTags; // get dom element
                            var rep = new Array(); // response array
                            var maxRepSize = 10; // maximum response size
                            // simple loop for the options
                            for (var i = 0; i < select_el.length; i++) {
                                var text = select_el[i];
                                if ( select_el[i] && ( !request.term || matcher.test(text) ) )
                                // add element to result array
                                    rep.push({
                                        label: text, // no more bold
                                        value: text,
                                    });
                                if ( rep.length > maxRepSize ) {
                                    rep.push({
                                        label: "...",
                                        value: "maxRepSizeReached",
                                    });
                                    break;
                                }
                            }
                            // send response
                            response( rep );
                        },
                        select: function( event, ui ) {
                            if ( ui.item.value == "maxRepSizeReached") {
                                return false;
                            } else {
                                var terms = split(this.value);
                                // remove the current input
                                terms.pop();
                                // add the selected item
                                terms.push(ui.item.value);
                                // add placeholder to get the comma-and-space at the end
                                terms.push("");
                                this.value = terms.join(", ");
                                return false;
                            }

                        },
                        focus: function( event, ui ) {
                            if ( ui.item.value == "maxRepSizeReached") {
                                return false;
                            }
                        }
                    });
            });








            $(function() {
                enable_cb();
                $("#filled-in-box").click(enable_cb);
            });

            function enable_cb() {
                if (!this.checked) {
                    $("#busca").removeAttr("disabled");
                } else {
                    $("#busca").attr("disabled", true);
                    $("#busca").val('');
                }
            }

        });

    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Arquivo</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12','id' => 'arquivoform' ,'files' => 'true']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">

            <div class="row padding">
                <div class="col s12">
                    <div class="input-field col s12 m6">
                        <input type="text" name="nome" id="nome" autocomplete="off">
                        <label for="nome">Nome</label>
                    </div>
                </div>

                <div class="col s12">
                    <div class="input-field col s12 m6">
                        <input type="text" name="busca" id="busca" autocomplete="off">
                        <label for="busca">Clientes</label>
                    </div>
                </div>

                <div class="btn-flat waves-effect  waves-light">
                    <input type="checkbox" class="filled-in" id="filled-in-box" name="todos">
                    <label for="filled-in-box">Todos</label>
                </div>


                <div class="col s12">
                    <div class="input-field col s12 m6">

                        <input type="file" name="arquivo" id="arquivo" onchange="$('#nome_arquivo1').val(this.value)" style="display: none">
                        <input type="text" name="nome_arquivo" id="nome_arquivo1" class="form-control" placeholder="Arquivo" value="{{isset($configuracoes['jornal']) ? $configuracoes['jornal'] : ''}}" disabled>
                        <input type="button" class="btn" value="Selecione um arquivo" onclick="$('#arquivo').click();">
                    </div>
                </div>
            </div>


            <div class="row right-align">
                <button class="btn waves-effect waves-light" type="button" id="Envia" name="action">{{ isset($orcamento_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($orcamento_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection



