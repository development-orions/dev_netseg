@extends('app')
@section('title', 'Listar Arquivos')
@section('content')



    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de arquivos</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/arquivo_lista" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    <i class="material-icons right">search</i>
                </button>

                <div class="input-field col m2 s3">

                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::text('arquivo', (isset($data['arquivo']) ? $data['arquivo']:''), ['class'=>'validate', 'placeholder'=>'Por Nome']) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Clientes</th>
                        <th>Arquivo</th>
                        <th>Data</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($arquivos as $arquivo)

                        <tr>
                            <td>{{ $arquivo->nome }}</td>
                            <td>{!! $arquivo->clientes !!}</td>
                            <td>{{ $arquivo->file }}</td>
                            <td>{{ $arquivo->created_at }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/uploads/arquivo/{{$arquivo->file}}" target="_blank" title="Visualizar Arquivo" class="btn-floating yellow"><i class="material-icons">visibility</i></a></li>
                                        @if((Auth::user()->role_id == 6) || (Auth::user()->role_id == 7) || (Auth::user()->role_id == 1))
                                            @if($arquivo->statu_id != 3)
                                                {{--<li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/cancelar_arquivo/{{ base64_encode($arquivo->id) }}" title="Cancelar arquivo" class="iframe btn-floating red"><i class="fa fa-times"></i></a></li>--}}
                                            @endif
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum Arquivo encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $arquivos->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection