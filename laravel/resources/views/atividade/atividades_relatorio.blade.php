@extends('app')
@section('title', 'Relatório de Atividades')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script type="text/javascript" src="/js/validador/atividades_relatorio.js"></script>
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();
        });
    </script>


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de Atividades</p>
            </div>
        </div>
        <div class="row filtro">
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="col m7">
                <div class="input-field col s8">
                    <input type="text" disabled class="validate" name="cliente" value="{{ isset(Session::get('cliente_cronograma')->nome) ? Session::get('cliente_cronograma')->nome : '' }}" id="cliente">
                    <label for="nomeUser">Cliente</label>

                    {!! Form::hidden('cliente_id', isset(Session::get('cliente_cronograma')->id) ? Session::get('cliente_cronograma')->id : '' , ['id' => 'cliente_id']) !!}

                </div>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/buscar_cliente_cronograma" title="Buscar cliente" class="btn btn-cronograma waves-effect waves-light iframe"><i class="material-icons">search</i></a>
                <a title="Remover Cliente" href="{{ Route::getCurrentRoute()->getPrefix() }}/remover_cliente" class="btn btn-cronograma waves-effect waves-light"><i class="material-icons">clear</i></a>
            </div>


            <div class="col m5">
                <div class="input-field col s12">
                    {!! Form::select('tecnico_id', $tecnicos, isset(Session::get('cliente_cronograma')->tecnico_id) ? Session::get('cliente_cronograma')->tecnico_id: null, ['id' => 'tecnico']) !!}
                    <label>Técnico</label>
                </div>
            </div>

            <div class="col m3">
                <div class="input-field col s12">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input validate', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>

            <div class="col m3">
                <div class="input-field col s12">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input validate', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="col m3">
                <div class="input-field col s12">
                    {!! Form::select('atividade_id', ['' => 'Todas as Atividades']+$atividads, isset($ativ) ? $ativ : null, ['id' => 'ativ']) !!}
                    <label>Atividades</label>
                </div>
            </div>
            <div class="col m3">
                <div class="input-field col s12">
                    {!! Form::select('statu_id', ['' => 'Todos os status']+$status, isset($statu) ? $statu : null, ['id' => 'statu']) !!}
                    <label>Status</label>
                </div>
            </div>
            <div class="col s12 right-align">
                <button id="Envia" class="btn-floating waves-effect right-align waves-light orange darken-1" type="submit" name="action"><i class="material-icons">print</i></button>
                <a title="limpar filtros" href="{{ Route::getCurrentRoute()->getPrefix() }}/limpar_filtro" class="btn-floating waves-effect waves-light green darken-1"><i class="material-icons">delete_sweep</i></a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection