@extends('app')
@section('title', 'Listar Orçamentos')
@section('content')


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de ASO | Médico: {{$medico->nome}}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_aso/{{base64_encode($medico->id)}}" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    <i class="material-icons right">search</i>
                </button>

                <div class="input-field col m2 s3">

                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>

                @if($total != 0)
                <div class="total">Valor total: R$ {{$total}}</div>
                @endif
            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Número</th>
                        <th>Paciente</th>
                        <th>Valor</th>
                        <th>Data</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>

                    @forelse($asos as $aso)
                        <tr>

                            <td>{{$aso->id}}</td>
                            <td>{{$aso->nome}}</td>
                            <td>{{$aso->valor}}</td>
                            <td>{{$aso->created_at}}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/gerar_aso/{{ base64_encode($aso->medico_id.'-'.$aso->paciente_id) }}" title="Imprimir ASO" target="_blank" class="btn-floating orange"><i class="material-icons">print</i></a></li>
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/gerar_fichaclinica/{{ base64_encode($aso->medico_id.'-'.$aso->paciente_id) }}" title="Imprimir Ficha clinica" target="_blank" class="btn-floating blue "><i class="material-icons">print</i></a></li>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">Nenhum ASO encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $asos->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection