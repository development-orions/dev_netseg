@extends('app')
@section('title', 'Cadastrar Arquivo')
@section('content')


    <link href="/css/select2-materialize.css" rel="stylesheet" />
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/i18n/pt-BR.js" type="text/javascript"></script>
    <script src="/js/select2-materialize.js"></script>
    <script type="text/javascript" src="/js/validador/aso.js"></script>

    <script>

    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de ASO</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12','id' => 'asoform' ,'files' => 'true']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">

            <div class="row padding">


                <div class="input-field col m6 s12">
                    <select name="medico" id="medico">
                        <option value="">Selecione</option>
                        @foreach($medicos as $key => $medico)
                        <option value="{{$key}}">{{$medico}}</option>
                        @endforeach
                    </select>
                    {!! Form::label('medico', '*  Médico: ') !!}
                </div>


                <div class="input-field col m6 s12">
                    <select name="paciente" id="paciente">
                        <option value="">Selecione</option>
                    </select>
                    {!! Form::label('pacicente', '*  Paciente: ' ,array('class' => 'paciente_select')) !!}
                </div>

                <div class="input-field col m2 s4">
                    {!! Form::text('valor','', ['class' => 'validate', 'id' => 'valor']) !!}
                    {!! Form::label('valor', '* Valor: ') !!}
                </div>

            </div>


            <div class="row right-align">
                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">Cadastrar <i class="material-icons right">send</i>
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>

    <script>
        $(document).ready(function() {
            $('#paciente').select2({
                language: "pt-BR",
                minimumInputLength: 5,

                ajax: {
                    url: $('#prefixo').val()+'/pacientes_ajax',
                    dataType: 'json',
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                }
            });


        });
    </script>
@endsection



