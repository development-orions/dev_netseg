@extends('app')
@section('title', 'Listar Paciente')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Pacientes {{ $status == 1 ? 'Ativos' : 'Inativos' }}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12']) !!}
                <div class="row">
                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/{{ $status == 1 ? 'paciente_lista/1' : 'paciente/2' }}" class="waves-effect waves-light btn">Limpar</a>
                    <button class="btn waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons right">search</i>
                    </button>

                    <div class="input-field col m2">
                        {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar Paciente']) !!}
                    </div>
                    <div class="input-field col m2">
                        {!! Form::text('empresa', (isset($texto['empresa']) ? $texto['empresa']:''), ['class'=>'validate', 'placeholder'=>'Empresa']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Função</th>
                        <th>Setor</th>
                        <th>Empresa</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($pacientes as $paciente)

                        <tr>
                            <td>{{ $paciente->nome }}</td>
                            <td>{{ $paciente->funcao }}</td>
                            <td>{{ $paciente->setor }}</td>
                            <td>{{ isset($paciente->clientes->razaosocial) ? $paciente->clientes->razaosocial : $paciente->cliente}}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/paciente_view/{{ base64_encode($paciente->id) }}" title="Visualizar paciente" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <li><a href="/paciente_edita/{{ base64_encode($paciente->id) }}" title="Editar paciente" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @if($paciente->statu_id == 1)
                                            <li><a href="/paciente_inativa/{{ base64_encode($paciente->id) }}" title="Inativar paciente" class="btn-floating red"><i class="material-icons">not_interested</i></a></li>
                                        @else
                                            <li><a href="/paciente_ativa/{{ base64_encode($paciente->id) }}" title="Ativar paciente" class="btn-floating green"><i class="material-icons">done</i></a></li>
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum paciente encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $pacientes->render(); ?>
            </div>
        </div>
    </div>
@endsection