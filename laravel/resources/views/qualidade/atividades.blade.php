@extends('app')
@section('title', 'Listar Atividade')
@section('content')

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Atividades {{ $status == 1 ? 'Ativas' : 'Inativas' }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($atividades as $atividade)
                        <tr>
                            <td>{{ $atividade->nome }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/atividade_edita/{{ base64_encode($atividade->id) }}" title="Editar atividade" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @if($atividade->statu_id == 1)
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/atividade_inativa/{{ base64_encode($atividade->id) }}" title="Inativar atividade" class="btn-floating red"><i class="material-icons">not_interested</i></a></li>
                                        @else
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/atividade_ativa/{{ base64_encode($atividade->id) }}" title="Ativar atividade" class="btn-floating green"><i class="material-icons">done</i></a></li>
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">Nenhuma atividade encontrada.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $atividades->render(); ?>
            </div>
        </div>
    </div>
@endsection