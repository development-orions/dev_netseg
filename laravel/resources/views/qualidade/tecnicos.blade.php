@extends('app')
@section('title', 'Listar tecnico')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Tecnicos {{ $status == 1 ? 'Ativos' : 'Inativos' }}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12']) !!}
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons right">search</i>
                    </button>
                    <div class="input-field col m2">
                        {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar Tecnico']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Número de Registro</th>
                        <th>Cidade</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($tecnicos as $tecnico)
                        <tr>
                            <td>{{ $tecnico->nome }}</td>
                            <td>{{ $tecnico->num_registro }}</td>
                            <td>{{ $tecnico->cidade }}</td>
                            <td>{{ $tecnico->telefone }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/tecnico_view/{{ base64_encode($tecnico->id) }}" title="Visualizar tecnico" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/tecnico_edita/{{ base64_encode($tecnico->id) }}" title="Editar tecnico" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @if($tecnico->statu_id == 1)
                                            <li><a href="/tecnico_inativa/{{ base64_encode($tecnico->id) }}" title="Inativar tecnico" class="btn-floating red"><i class="material-icons">not_interested</i></a></li>
                                        @else
                                            <li><a href="/tecnico_ativa/{{ base64_encode($tecnico->id) }}" title="Ativar tecnico" class="btn-floating green"><i class="material-icons">done</i></a></li>
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum tecnico encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $tecnicos->render(); ?>
            </div>
        </div>
    </div>
@endsection