@extends('app')
@section('title', 'Cadastrar Cliente')
@section('content')
    @if(isset($atividade_edit))
        <script type="text/javascript" src="/js/validador/atividade_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/atividade.js"></script>
    @endif

    <script type="text/javascript" src="/js/core.js"></script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Atividade</p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                @if(isset($atividade_edit))
                    {!! Form::hidden('id', (isset($atividade_edit['id']) ? $atividade_edit['id']:''), ['id' => 'id']) !!}
                    {!! Form::hidden('statu_id', (isset($atividade_edit['statu_id']) ? $atividade_edit['statu_id']:'')) !!}
                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($atividade_edit['nome']) ? $atividade_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($atividade_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($atividade_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
