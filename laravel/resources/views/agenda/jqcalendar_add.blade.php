<!DOCTYPE html>
<html lang="pt-br">
<head id="Head1">
    <title>Agenda NetSeg Medicina</title>
    <link href="/wdCalendar/css/dailog.css" rel="stylesheet" type="text/css" />
    <link href="/wdCalendar/css/calendar.css" rel="stylesheet" type="text/css" />
    <link href="/wdCalendar/css/dp.css" rel="stylesheet" type="text/css" />
    <link href="/wdCalendar/css/alert.css" rel="stylesheet" type="text/css" />
    <link href="/wdCalendar/css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/estilo.css" type="text/css">

    <script src="/wdCalendar/src/jquery.js" type="text/javascript"></script>

    <script src="/wdCalendar/src/Plugins/Common.js" type="text/javascript"></script>
    <script src="/wdCalendar/src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>
    <script src="/wdCalendar/src/Plugins/jquery.datepicker.js" type="text/javascript"></script>

    <script src="/wdCalendar/src/Plugins/jquery.alert.js" type="text/javascript"></script>
    <script src="/wdCalendar/src/Plugins/jquery.ifrmdailog.js" defer="defer" type="text/javascript"></script>
    <script src="/wdCalendar/src/Plugins/wdCalendar_lang_US.js" type="text/javascript"></script>
    <script src="/wdCalendar/src/Plugins/jquery.calendar.js" type="text/javascript"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script type="text/javascript">
        $(document).ready(function() {
            var view="week";

            var DATA_FEED_URL = "/agenda";
            var op = {
                view: view,
                token: "<?= csrf_token() ?>",
                theme:3,
                showday: new Date(),
                EditCmdhandler:Edit,
                DeleteCmdhandler:Delete,
                ViewCmdhandler:View,
                onWeekOrMonthToDay:wtd,
                onBeforeRequestData: cal_beforerequest,
                onAfterRequestData: cal_afterrequest,
                onRequestDataError: cal_onerror,
                autoload:true,
                url: DATA_FEED_URL + "?method=list",
                quickAddUrl: DATA_FEED_URL + "?method=add",
                quickUpdateUrl: DATA_FEED_URL + "?method=update",
                quickDeleteUrl: DATA_FEED_URL + "?method=remove",

            };
            var $dv = $("#calhead");
            var _MH = document.documentElement.clientHeight;
            var dvH = $dv.height() + 2;
            op.height = _MH - dvH;
            op.eventItems =[];

            var p = $("#gridcontainer").bcalendar(op).BcalGetOp();
            if (p && p.datestrshow) {
                $("#txtdatetimeshow").text(p.datestrshow);
            }
            $("#caltoolbar").noSelect();

            $("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: $("#txtdatetimeshow"),
                onReturn:function(r){
                    var p = $("#gridcontainer").gotoDate(r).BcalGetOp();
                    if (p && p.datestrshow) {
                        $("#txtdatetimeshow").text(p.datestrshow);
                    }
                }
            });
            $('#msg').hide();
            function cal_beforerequest(type)
            {
                var t="Loading data...";
                switch(type)
                {
                    case 1:
                        t="Carregando dados...";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        t="A solicitação está sendo processada ...";
                        break;
                }
                $("#errorpannel").hide();
                $("#loadingpannel").html(t).show();
            }
            function cal_afterrequest(type)
            {
                switch(type)
                {
                    case 1:
                        $("#loadingpannel").hide();
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $("#loadingpannel").html("Sucesso!");
                        window.setTimeout(function(){ $("#loadingpannel").hide();},2000);
                        break;
                }

            }
            function cal_onerror(type,data)
            {
                $("#errorpannel").show();
            }
            function Edit(data)
            {
                var eurl="/edit?id={0}&start={2}&end={3}&isallday={4}&title={1}";
                if(data)
                {
                    var url = StrFormat(eurl,data);
                    OpenModelWindow(url,{ width: 600, height: 400, caption:"Gerenciar a agenda",onclose:function(){
                        $("#gridcontainer").reload();
                    }});
                }
            }
            function View(data)
            {
                var str = "";
                $.each(data, function(i, item){
                    str += "[" + i + "]: " + item + "\n";
                });
                alert(str);
            }
            function Delete(data,callback)
            {

                $.alerts.okButton="Ok";
                $.alerts.cancelButton="Cancelar";
                hiConfirm("Tem certeza que deseja excluir este evento ?", 'Confirmação',function(r){ r && callback(0);});
            }
            function wtd(p)
            {
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $("#showdaybtn").addClass("fcurrent");
            }
            //to show day view
            $("#showdaybtn").click(function(e) {
                //document.location.href="#day";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("day").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            //to show week view
            $("#showweekbtn").click(function(e) {
                //document.location.href="#week";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("week").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //to show month view
            $("#showmonthbtn").click(function(e) {
                //document.location.href="#month";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("month").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });

            $("#showreflashbtn").click(function(e){
                $("#gridcontainer").reload();
            });

            //Add a new event
            $("#faddbtn").click(function(e) {
                var url ="/edit";
                OpenModelWindow(url,{ width: 500, height: 400, caption: "Criar Novo Calendário"});
            });
            //go to today
            $("#showtodaybtn").click(function(e) {
                var p = $("#gridcontainer").gotoDate().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            //previous date range
            $("#sfprevbtn").click(function(e) {
                var p = $("#gridcontainer").previousRange().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            //next date range
            $("#sfnextbtn").click(function(e) {
                var p = $("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });


            $('#resultado').hide();
            $('#resultadop').hide();
        });
        function seleciona_medico(id, nome){
            $('#res').empty();
            $('#resultado').hide();
            $('#medico_id').val(id);
            $('#medico').val(nome).click(function(){
                $('#medico_id').val('');
                $(this).val('').unbind('click');
            });
        }
        function seleciona_paciente(id, nome){
            $('#res').empty();
            $('#resultadop').hide();
            $('#paciente_id').val(id);
            $('#paciente').val(nome).click(function(){
                $('#paciente_id').val('');
                $(this).val('').unbind('click');
            });
        }

        function pesquisa_medico(event){
            valor = $('#medico').val();
            if (event.keyCode == 8 || valor == '' ){
                $('#res').empty();
                $('#resultado').hide();
                return false
            }

            $.ajax({
                url: '/buscarmedico/'+valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso){
                    $('#res').empty();
                    $('#resultado').show();
                    $.each(sucesso, function(i, item){
                        $('#res').append('<li><a href="#" onClick="seleciona_medico(\''+item.id+'\',\''+item.nome+'\')" title="'+item.nome+' - '+item.crm+'"><span>'+item.nome+'</span></a></li>');
                    });
                    if(jQuery.isEmptyObject(sucesso)){
                        $('#resultado').hide();
                    }
                },
                error: function(){
                    $('#res').empty();
                    $('#resultado').hide();
                }
            });
        }
        function pesquisa_paciente(event){
            valor = $('#paciente').val();
            if (event.keyCode == 8 || valor == '' ){
                $('#resp').empty();
                $('#resultadop').hide();
                return false
            }

            $.ajax({
                url: '/buscarpaciente/'+valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso){
                    $('#resp').empty();
                    $('#resultadop').show();
                    $.each(sucesso, function(i, item){
                        $('#resp').append('<li><a href="#" onClick="seleciona_paciente(\''+item.id+'\',\''+item.nome+'\')" title="'+item.nome+' - '+item.cpf+'"><span>'+item.nome+' - '+item.cliente+'</span></a></li>');
                    });
                    if(jQuery.isEmptyObject(sucesso)){
                        $('#resultadop').hide();
                    }
                },
                error: function(){
                    $('#resp').empty();
                    $('#resultadop').hide();
                }
            });
        }
    </script>
</head>
<body>
<div>

    <div id="calhead" style="padding-left:1px;padding-right:1px;">
        <div class="cHead"><div class="ftitle">Agenda</div>
            <div id="loadingpannel" class="ptogtitle loadicon" style="display: none;">Carregando dados...</div>
            <div id="errorpannel" class="ptogtitle loaderror" style="display: none;">Desculpe, não foi possível carregar os seus dados, por favor, tente novamente mais tarde</div>
        </div>

        <div id="caltoolbar" class="ctoolbar">
            <div id="faddbtn" class="fbutton">
                <div><span title='Click to Create New Event' class="addcal">

                Novo Evento
                </span></div>
            </div>
            <div class="btnseparator"></div>
            <div id="showtodaybtn" class="fbutton">
                <div><span title='Click to back to today ' class="showtoday">
                Hoje</span></div>
            </div>
            <div class="btnseparator"></div>

            <div id="showdaybtn" class="fbutton">
                <div><span title='Day' class="showdayview">Dia</span></div>
            </div>
            <div  id="showweekbtn" class="fbutton fcurrent">
                <div><span title='Week' class="showweekview">Semana</span></div>
            </div>
            <div  id="showmonthbtn" class="fbutton">
                <div><span title='Month' class="showmonthview">Mês</span></div>

            </div>
            <div class="btnseparator"></div>
            <div  id="showreflashbtn" class="fbutton">
                <div><span title='Refresh view' class="showdayflash">Atualizar</span></div>
            </div>
            <div class="btnseparator"></div>
            <div id="sfprevbtn" title="Prev"  class="fbutton">
                <span class="fprev"></span>

            </div>
            <div id="sfnextbtn" title="Next" class="fbutton">
                <span class="fnext"></span>
            </div>
            <div class="fshowdatep fbutton">
                <div>
                    <input type="hidden" name="txtshow" id="hdtxtshow" />
                    <span id="txtdatetimeshow">Calendário</span>

                </div>
            </div>


            <div class="clear"></div>
        </div>
    </div>
    <div style="padding:1px;">

        <div class="t1 chromeColor">
            &nbsp;</div>
        <div class="t2 chromeColor">
            &nbsp;</div>
        <div id="dvCalMain" class="calmain printborder">
            <div id="gridcontainer" style="overflow-y: visible;">
            </div>
        </div>
        <div class="t2 chromeColor">

            &nbsp;</div>
        <div class="t1 chromeColor">
            &nbsp;
        </div>
    </div>

</div>

</body>
</html>
