@extends('app')
@section('title', 'Listar Médicos')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Médicos {{ $status == 1 ? 'Ativos' : 'Inativos' }}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons right">search</i>
                    </button>
                    <div class="input-field col m2">
                        {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar Médico']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CRM</th>
                        <th>NIT</th>
                        <th>Data de Contrato</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($medicos as $medico)
                        <tr>
                            <td>{{ $medico->nome }}</td>
                            <td>{{ $medico->crm }}</td>
                            <td>{{ $medico->nit }}</td>
                            <td>{{ $medico->datacontrato }}</td>
                            <td>{{ $medico->telefone }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">

                                        <li><a href="/visualizar/medico_view/{{ base64_encode($medico->id) }}" title="Visualizar médico" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_edita/{{ base64_encode($medico->id) }}" title="Editar médico" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>

                                        @if((Auth::user()->role_id == 4) || (Auth::user()->role_id == 6 || (Auth::user()->role_id == 5)))
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_aso/{{ base64_encode($medico->id) }}" title="Visualizar ASO" class="btn-floating yellow darken-1"><i class="material-icons">local_hospital</i></a></li>
                                        @endif

                                        @if($medico->statu_id == 1)

                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_inativa/{{ base64_encode($medico->id) }}" title="Inativar médico" class="btn-floating red"><i class="material-icons">not_interested</i></a></li>

                                        @else

                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_ativa/{{ base64_encode($medico->id) }}" title="Ativar médico" class="btn-floating green"><i class="material-icons">done</i></a></li>

                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">Nenhum médico encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $medicos->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection