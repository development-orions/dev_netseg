@extends('app')
@section('title', 'Cadastrar Médico')
@section('content')
    @if(isset($medico_edit))
        <script type="text/javascript" src="/js/validador/medico_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/medico.js"></script>
    @endif

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript"  >
        function getEndereco(cep) {
            // Se o campo CEP não estiver vazio
            if($.trim(cep) != ""){
                $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
                    if(resultadoCEP["resultado"]){
                        $("#rua").val(unescape(resultadoCEP["logradouro"]));
                        $("#bairro").val(unescape(resultadoCEP["bairro"]));
                        $("#cidade").val(unescape(resultadoCEP["cidade"]));
                        $("#estado").val(unescape(resultadoCEP["uf"]));
                    }
                });
            }
        }
        $(document).ready(function() {
            $("#cep").blur(function(){
                getEndereco($("#cep").val());
            });
        });

        $('.datepicker').pickadate({
            selectMonths: true // Creates a dropdown to control month
        });

        //nit
        function nit(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }

        function mnit(v){
            v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
            v=v.replace(/^(\d{3})(\d)/g,"$1.$2");
            v=v.replace(/(\d)(\d{3})$/,"$1.$2");
            v=v.replace(/(\d)(\d{1})$/,"$1-$2");
            return v;
        }

        /* Máscaras ER */
        function mascara(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }
        function execmascara(){
            v_obj.value=v_fun(v_obj.value)
        }
        function mtel(v){
            v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
            v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
            v=v.replace(/(\d)(\d{4})$/,"$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
            return v;
        }
        function id( el ){
            return document.getElementById( el );
        }

        //cep
        function cep(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }

        function mcep(v){
            v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
            v=v.replace(/^(\d{2})(\d)/g,"$1.$2");
            v=v.replace(/(\d)(\d{3})$/,"$1-$2");
            return v;
        }


        window.onload = function(){
            id('nit').onkeyup = function(){
                nit( this, mnit );
            }
            id('telefone').onkeyup = function(){
                mascara( this, mtel );
            }
            id('cep').onkeyup = function(){
                cep( this, mcep );
            }
        }
    </script><!-- === chamada de endereço por CEP === -->
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Médico</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            
            <div class="row">


                @if(isset($medico_edit))
                    {!! Form::hidden('id', (isset($medico_edit['id']) ? $medico_edit['id']:''), ['id' => 'id']) !!}
                    {!! Form::hidden('statu_id', (isset($medico_edit['statu_id']) ? $medico_edit['statu_id']:'')) !!}
                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($medico_edit['nome']) ? $medico_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('crm', (isset($medico_edit['crm']) ? $medico_edit['crm']:''), ['class' => 'validate', 'id' => 'crm']) !!}
                    {!! Form::label('crm', '* CRM: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('nit', (isset($medico_edit['nit']) ? $medico_edit['nit']:''), ['class' => 'validate', 'id' => 'nit', 'maxlength' => '14']) !!}
                    {!! Form::label('nit', '* NIT: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('datacontrato', '* Data de Contrato: ') !!}
                    {!! Form::text('datacontrato', (isset($medico_edit['datacontrato']) ? $medico_edit['datacontrato']:''), ['class' => 'datepicker picker__input', 'id' => 'datacontrato', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('telefone', (isset($medico_edit['telefone']) ? $medico_edit['telefone']:''), ['class' => 'validate', 'id' => 'telefone', 'maxlength' => '15']) !!}
                    {!! Form::label('telefone', 'Telefone: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('cep', (isset($medico_edit['cep']) ? $medico_edit['cep']:''), ['class' => 'validate', 'id' => 'cep', 'maxlength' => '10']) !!}
                    {!! Form::label('cep', 'CEP: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('rua', (isset($medico_edit['rua']) ? $medico_edit['rua']:''), ['class' => 'validate', 'id' => 'rua']) !!}
                    {!! Form::label('rua', 'Rua: ') !!}
                </div>
                <div class="input-field col m2 s6">
                    {!! Form::text('numero', (isset($medico_edit['numero']) ? $medico_edit['numero']:''), ['class' => 'validate', 'id' => 'numero']) !!}
                    {!! Form::label('numero', 'Número: ') !!}
                </div>
                <div class="input-field col m4 s6">
                    {!! Form::text('complemento', (isset($medico_edit['complemento']) ? $medico_edit['complemento']:'')) !!}
                    {!! Form::label('complemento', 'Complemento: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('bairro',(isset($medico_edit['bairro']) ? $medico_edit['bairro']:''), ['class' => 'validate', 'id' => 'bairro']) !!}
                    {!! Form::label('bairro', 'Bairro: ') !!}
                </div>
                <div class="input-field col m4 s8">
                    {!! Form::text('cidade', (isset($medico_edit['cidade']) ? $medico_edit['cidade']:''), ['class' => 'validate', 'id' => 'cidade']) !!}
                    {!! Form::label('cidade', 'Cidade: ') !!}
                </div>
                <div class="input-field col m2 s4">
                    {!! Form::text('estado', (isset($medico_edit['estado']) ? $medico_edit['estado']:''), ['class' => 'validate', 'id' => 'estado']) !!}
                    {!! Form::label('estado', 'Estado: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('observacao', (isset($medico_edit['observacao']) ? $medico_edit['observacao']: ''), ['class' => 'materialize-textarea', 'id' => 'observacao']) !!}
                    {!! Form::label('observacao', 'Observações:') !!}
                </div>
            </div>


            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($medico_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($medico_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
