@extends('app')
@section('title', 'Configurações do Sistema')
@section('content')

    <script type="text/javascript"  >
        function getEndereco(cep) {
            // Se o campo CEP não estiver vazio
            if($.trim(cep) != ""){
                $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
                    if(resultadoCEP["resultado"]){
                        $("#rua").val(unescape(resultadoCEP["logradouro"]));
                        $("#bairro").val(unescape(resultadoCEP["bairro"]));
                        $("#cidade").val(unescape(resultadoCEP["cidade"]));
                        $("#estado").val(unescape(resultadoCEP["uf"]));
                    }
                });
            }
        }
        $(document).ready(function() {
            $("#cep").blur(function(){
                getEndereco($("#cep").val());
            });
        });
    </script><!-- === chamada de endereço por CEP === -->
    <script type="text/javascript" src="/js/validador/configuracoes.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Configurações do Sistema</p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">
                @if(isset($configuracoes))
                    {!! Form::hidden('id', (isset($configuracoes['id']) ? $configuracoes['id']:''), ['id' => 'id']) !!}
                @endif
                <div class="input-field col m6 s12">
                        {!! Form::text('razaosocial', (isset($configuracoes['razaosocial']) ? $configuracoes['razaosocial']: old('razaosocial')), ['class' => 'validate', 'id' => 'razaosocial']) !!}
                        {!! Form::label('razaosocial', 'R* azão Social: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('nomefantasia', (isset($configuracoes['nomefantasia']) ? $configuracoes['nomefantasia']: old('nomefantasia')), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nomefantasia', '* Nome fantasia: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('cnpj', (isset($configuracoes['cnpj']) ? $configuracoes['cnpj']:old('cnpj')), ['class' => 'validate', 'id' => 'cnpj']) !!}
                    {!! Form::label('cnpj', '* CNPJ: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('ie', (isset($configuracoes['ie']) ? $configuracoes['ie']:old('ie')), ['class' => 'validate', 'id' => 'ie']) !!}
                    {!! Form::label('ie', '* IE: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('formajuridica', (isset($configuracoes['formajuridica']) ? $configuracoes['formajuridica']:old('formajuridica')), ['class' => 'validate', 'id' => 'formajuridica']) !!}
                    {!! Form::label('formajuridica', '* Forma Jurídica: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('cnae', (isset($configuracoes['cnae']) ? $configuracoes['cnae']: old('cnae')), ['class' => 'materialize-textarea', 'id' => 'cnae']) !!}
                    {!! Form::label('cnae', '* CNAE:') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('email', (isset($configuracoes['email']) ? $configuracoes['email']:old('email')), ['class' => 'validate', 'id' => 'email']) !!}
                    {!! Form::label('email', '* Email: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('site', (isset($configuracoes['site']) ? $configuracoes['site']:old('site')), ['class' => 'validate', 'id' => 'site']) !!}
                    {!! Form::label('site', 'Site: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::textarea('telefone', (isset($configuracoes['telefone']) ? $configuracoes['telefone']: old('telefone')), ['class' => 'materialize-textarea', 'id' => 'telefone']) !!}
                    {!! Form::label('telefone', 'Telefones:') !!}
                </div>

            </div>
            <div class="row">
                <div class="input-field col m2 s4">
                    {!! Form::text('cep', (isset($configuracoes['cep']) ? $configuracoes['cep']:old('cep')), ['class' => 'validate', 'id' => 'cep', 'maxlength' => '10']) !!}
                    {!! Form::label('cep', '* CEP: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('rua', (isset($configuracoes['rua']) ? $configuracoes['rua']:old('rua')), ['class' => 'validate', 'id' => 'rua']) !!}
                    {!! Form::label('rua', '* Rua: ') !!}
                </div>
                <div class="input-field col m2 s6">
                    {!! Form::text('numero', (isset($configuracoes['numero']) ? $configuracoes['numero']:old('numero')), ['class' => 'validate', 'id' => 'numero']) !!}
                    {!! Form::label('numero', '* Número: ') !!}
                </div>
                <div class="input-field col m4 s6">
                    {!! Form::text('complemento', (isset($configuracoes['complemento']) ? $configuracoes['complemento']:old('complemento'))) !!}
                    {!! Form::label('complemento', 'Complemento: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('bairro',(isset($configuracoes['bairro']) ? $configuracoes['bairro']:old('bairro')), ['class' => 'validate', 'id' => 'bairro']) !!}
                    {!! Form::label('bairro', '* Bairro: ') !!}
                </div>
                <div class="input-field col m4 s8">
                    {!! Form::text('cidade', (isset($configuracoes['cidade']) ? $configuracoes['cidade']:old('cidade')), ['class' => 'validate', 'id' => 'cidade']) !!}
                    {!! Form::label('cidade', '* Cidade: ') !!}
                </div>
                <div class="input-field col m2 s4">
                    {!! Form::text('estado', (isset($configuracoes['estado']) ? $configuracoes['estado']:old('estado')), ['class' => 'validate', 'id' => 'estado']) !!}
                    {!! Form::label('estado', '* Estado: ') !!}
                </div>
            </div>

            <div class="row right-align">
                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">Salvar
                    <i class="material-icons right">send</i>
                </button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
