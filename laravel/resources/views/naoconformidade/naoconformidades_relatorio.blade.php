@extends('app')
@section('title', 'Relatório de Não Conformidades')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de não Conformidades</p>
            </div>
        </div>
        <div class="row filtro">
            {!! Form::open(['class' => 'col s12']) !!}
            <div class="col m4">
                <div class="input-field col s7">
                    <input type="text" disabled class="validate" name="cliente" value="{{ isset(Session::get('cliente_cronograma')->nome) ? Session::get('cliente_cronograma')->nome : '' }}" id="cliente">
                    <label for="nomeUser">Cliente</label>

                    {!! Form::hidden('cliente_id', isset(Session::get('cliente_cronograma')->id) ? Session::get('cliente_cronograma')->id : '' , ['id' => 'cliente_id']) !!}

                </div>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/buscar_cliente_cronograma" title="Buscar cliente" class="btn btn-cronograma waves-effect waves-light iframe"><i class="material-icons">search</i></a>
                <a title="Remover Cliente" href="{{ Route::getCurrentRoute()->getPrefix() }}/remover_cliente" class="btn btn-cronograma waves-effect waves-light"><i class="material-icons">clear</i></a>
            </div>
            <div class="col m4">
                <div class="input-field col s12">
                    {!! Form::select('tecnico_id', $tecnicos, isset($tecnico) ? $tecnico : null, ['id' => 'tecnico']) !!}
                    <label>Técnico</label>
                </div>
            </div>
            <div class="col m2">
                <div class="input-field col s12">
                    <select name="mes">
                        <?php for($i = 01; $i <= 12; $i++){ ?>
                        <option value="<?= $i ?>" <?= isset($mes) ? ($mes == $i ? 'selected' : '') : ($i == date('m') ? 'selected' : '') ?>  ><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <label for="mes">Mês</label>
                </div>
            </div>
            <div class="col m2">
                <div class="input-field col s12">
                    <select name="ano">
                        <?php for($i = 2016; $i <= date('Y')+1; $i++){ ?>
                        <option value="<?= $i ?>" <?= isset($ano) ? ($ano == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '') ?> ><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <label for="ano">Ano</label>
                </div>
            </div>
            <div class="col m6">
                <a href="/pdf/naoconformidades/lista" target="_blank" class="waves-effect waves-light btn"><i class="material-icons right">print</i>Imprimir Sem filtros</a>
            </div>
            <div class="col m6 right-align">
                <button class="btn-floating waves-effect right-align waves-light orange darken-1" type="submit" name="action"><i class="material-icons">print</i></button>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/limpar_filtro" class="btn-floating waves-effect waves-light green darken-1"><i class="material-icons">delete_sweep</i></a>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
@endsection