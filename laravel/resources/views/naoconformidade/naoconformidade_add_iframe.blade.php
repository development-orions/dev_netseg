
    @extends('iframe')

@section('title', 'Cadastrar Não conformidade')
@section('content')

    @if(isset($naoconformidade_edit))
        <script type="text/javascript" src="/js/validador/naoconformidade_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/naoconformidade_iframe.js"></script>
    @endif


    <script type="text/javascript">

        function filtro(valor){
            $('#eficaz_observacoes').hide();
            $('#label').hide();

            valor = parseInt(valor);
            setTimeout(function() {
                $('.alert').hide('slow');}, 4000);

            switch (valor){
                case 1:
                    $('#eficaz_observacoes').show('slow');
                    $('#label').show('slow');
                    break;
                case 0:
                    $('#eficaz_observacoes').hide('slow');
                    $('#label').hide('slow');
                    break;
                default:
                    $('#eficaz_observacoes').hide('slow');
                    $('#label').hide('slow');
                    break;
            }
        }

    </script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Não Conformidade</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                @if(isset($naoconformidade_edit))
                    {!! Form::hidden('id', (isset($naoconformidade_edit['id']) ? $naoconformidade_edit['id']:''), ['id' => 'id']) !!}
                @endif


                {!! Form::hidden('cliente_id', $cliente->id, ['id' => 'cliente_id']) !!}
                {!! Form::hidden('tecnico_id', $tecnico->id, ['id' => 'tecnico_id']) !!}
                {!! Form::hidden('cronograma_id', $cronograma->id, ['id' => 'cronograma_id']) !!}

                <div class="input-field col m4 s12">
                    {!! Form::text('codigo', (isset($naoconformidade_edit['codigo']) ? $naoconformidade_edit['codigo']:'RG-SGQ-002'), ['class' => 'validate']) !!}
                    {!! Form::label('codigo', '* Código: ') !!}
                </div>
                <div class="input-field col m2 s12">
                    {!! Form::text('revisao', (isset($naoconformidade_edit['revisao']) ? $naoconformidade_edit['revisao']:'02'), ['class' => 'validate']) !!}
                    {!! Form::label('revisao', '* Revisão: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('vigencia', '* Vigência: ') !!}
                    {!! Form::text('vigencia', (isset($naoconformidade_edit['vigencia']) ? $naoconformidade_edit['vigencia']:'3 Junho, 2011'), ['class' => 'datepicker picker__input', 'id' => 'vigencia', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    <select name="realpotencial" id="realpotencial">
                        <option value="1" {{ isset($naoconformidade_edit['realpotencial']) ? $naoconformidade_edit['realpotencial'] == '1' ? 'selected' : '' : '' }} >Real</option>
                        <option value="0" {{ isset($naoconformidade_edit['realpotencial']) ? $naoconformidade_edit['realpotencial'] == '0' ? 'selected' : '' : '' }}>Potencial</option>
                    </select>
                    {!! Form::label('realpotencial', '* Não Conformidade: ') !!}
                </div>

                <div class="input-field col m6 s12">
                    <select name="materialsistema" id="materialsistema">
                        <option value="0" onchange="filtro2(this.value())" {{ isset($naoconformidade_edit['materialsistema']) ? $naoconformidade_edit['materialsistema'] == '1' ? 'selected' : '' : '' }} >Material / Processo</option>
                        <option value="0" {{ isset($naoconformidade_edit['materialsistema']) ? $naoconformidade_edit['materialsistema'] == '0' ? 'selected' : '' : '' }}>Sistema / Serviço</option>
                    </select>
                    {!! Form::label('materialsistema', '* Tipo: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    {!! Form::textarea('ocorrencia',(isset($naoconformidade_edit['ocorrencia']) ? $naoconformidade_edit['ocorrencia']:''), ['class' => 'validate materialize-textarea', 'id' => 'ocorrencia']) !!}
                    {!! Form::label('ocorrencia', '* Ocorrência: ') !!}
                </div>
                <div class="input-field col m8 s12">
                    {!! Form::text('ocorrencia_responsavel', (isset($naoconformidade_edit['ocorrencia_responsavel']) ? $naoconformidade_edit['ocorrencia_responsavel']:''), ['class' => 'validate', 'id' => 'ocorrencia_responsavel']) !!}
                    {!! Form::label('ocorrencia_responsavel', 'Responsável: ') !!}
                </div>
                <div class="input-field col m4 s12">
                    {!! Form::label('ocorrencia_data', 'Data: ') !!}
                    {!! Form::text('ocorrencia_data', (isset($naoconformidade_edit['ocorrencia_data']) ? $naoconformidade_edit['ocorrencia_data']:''), ['class' => 'datepicker picker__input', 'id' => 'ocorrencia_data', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    {!! Form::textarea('acaoimediata',(isset($naoconformidade_edit['acaoimediata']) ? $naoconformidade_edit['acaoimediata']:''), ['class' => 'validate materialize-textarea', 'id' => 'acaoimediata']) !!}
                    {!! Form::label('acaoimediata', 'Ação Imediata: ') !!}
                </div>
                <div class="input-field col m8 s12">
                    {!! Form::text('acaoimediata_responsavel', (isset($naoconformidade_edit['acaoimediata_responsavel']) ? $naoconformidade_edit['acaoimediata_responsavel']:''), ['class' => 'validate', 'id' => 'acaoimediata_responsavel']) !!}
                    {!! Form::label('acaoimediata_responsavel', 'Responsável: ') !!}
                </div>
                <div class="input-field col m4 s12">
                    {!! Form::label('acaoimediata_data', 'Data: ') !!}
                    {!! Form::text('acaoimediata_data', (isset($naoconformidade_edit['acaoimediata_data']) ? $naoconformidade_edit['acaoimediata_data']:''), ['class' => 'datepicker picker__input', 'id' => 'acaoimediata_data', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <p class="corlabel"><br/>Causa Provável da não conformidade:</p>
                </div>
                <div class="input-field col m3 s6">
                    <input name="causaprovavel_op" type="radio" id="sim" value="1" {{ isset($naoconformidade_edit['causaprovavel_op']) && $naoconformidade_edit['causaprovavel_op'] == 1 ? 'checked' : '' }} />
                    {!! Form::label('sim', 'Relatório improcedente') !!}

                    <input name="causaprovavel_op" type="radio" id="nao" value="0" {{ isset($naoconformidade_edit['causaprovavel_op']) && $naoconformidade_edit['causaprovavel_op'] == 0 ? 'checked' : '' }} />
                    {!! Form::label('nao', 'Relatório procedente') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    {!! Form::textarea('causaprovavel',(isset($naoconformidade_edit['causaprovavel']) ? $naoconformidade_edit['causaprovavel']:''), ['class' => 'validate materialize-textarea', 'id' => 'causaprovavel']) !!}
                    {!! Form::label('causaprovavel', 'Causa Provável da não conformidade: ') !!}
                </div>
                <div class="input-field col m8 s12">
                    {!! Form::text('causaprovavel_responsavel', (isset($naoconformidade_edit['causaprovavel_responsavel']) ? $naoconformidade_edit['causaprovavel_responsavel']:''), ['class' => 'validate', 'id' => 'causaprovavel_responsavel']) !!}
                    {!! Form::label('causaprovavel_responsavel', 'Responsável: ') !!}
                </div>
                <div class="input-field col m4 s12">
                    {!! Form::label('causaprovavel_data', 'Data: ') !!}
                    {!! Form::text('causaprovavel_data', (isset($naoconformidade_edit['causaprovavel_data']) ? $naoconformidade_edit['causaprovavel_data']:''), ['class' => 'datepicker picker__input', 'id' => 'causaprovavel_data', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    {!! Form::textarea('acaocorretiva',(isset($naoconformidade_edit['acaocorretiva']) ? $naoconformidade_edit['acaocorretiva']:''), ['class' => 'validate materialize-textarea', 'id' => 'acaocorretiva']) !!}
                    {!! Form::label('acaocorretiva', 'Ações Corretivas / Ações Preventivas: ') !!}
                </div>
                <div class="input-field col m8 s12">
                    {!! Form::text('acaocorretiva_responsavel', (isset($naoconformidade_edit['acaocorretiva_responsavel']) ? $naoconformidade_edit['acaocorretiva_responsavel']:''), ['class' => 'validate', 'id' => 'acaocorretiva_responsavel']) !!}
                    {!! Form::label('acaocorretiva_responsavel', 'Responsável: ') !!}
                </div>
                <div class="input-field col m4 s12">
                    {!! Form::label('acaocorretiva_prazo', 'Data: ') !!}
                    {!! Form::text('acaocorretiva_prazo', (isset($naoconformidade_edit['acaocorretiva_prazo']) ? $naoconformidade_edit['acaocorretiva_prazo']:''), ['class' => 'datepicker picker__input', 'id' => 'acaocorretiva_prazo', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <p class="corlabel"><br/>A  Ação Corretiva foi implementada?</p>
                </div>
                <div class="input-field col m3 s6">
                    <input name="foiimplementada" type="radio" id="sim" value="1" {{ isset($naoconformidade_edit['foiimplementada']) && $naoconformidade_edit['foiimplementada'] == 1 ? 'checked' : '' }} />
                    {!! Form::label('sim', 'Sim') !!}

                    <input name="foiimplementada" type="radio" id="nao" value="0" {{ isset($naoconformidade_edit['foiimplementada']) && $naoconformidade_edit['foiimplementada'] == 0 ? 'checked' : '' }} />
                    {!! Form::label('nao', 'Não') !!}
                </div>

            </div>

            <div class="row">
                <div class="col s12">
                    <p class="corlabel"><br/>A ação implementada é eficaz para evitar a Reincidência da não conformidade?</p>
                </div>
                <div class="input-field col m6 s12">
                    <select name="eeficaz" onchange="filtro(this.value)" id="eeficaz">
                        <option value="1" {{ isset($naoconformidade_edit['eeficaz']) ? $naoconformidade_edit['eeficaz'] == '1' ? 'selected' : '' : '' }} >Sim</option>
                        <option value="0" {{ isset($naoconformidade_edit['eeficaz']) ? $naoconformidade_edit['eeficaz'] == '0' ? 'selected' : '' : 'selected'}}>Não</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    {!! Form::textarea('eficaz_observacoes',(isset($naoconformidade_edit['eficaz_observacoes']) ? $naoconformidade_edit['eficaz_observacoes']:''), ['class' => 'validate materialize-textarea', 'id' => 'eficaz_observacoes']) !!}
                    {!! Form::label('eficaz_observacoes', 'Observações: ', ['id' => 'label']) !!}
                </div>
                <div class="input-field col m8 s12">
                    {!! Form::text('eficaz_responsavel', (isset($naoconformidade_edit['eficaz_responsavel']) ? $naoconformidade_edit['eficaz_responsavel']:''), ['class' => 'validate', 'id' => 'eficaz_responsavel']) !!}
                    {!! Form::label('eficaz_responsavel', 'Responsável: ') !!}
                </div>
                <div class="input-field col m4 s12">
                    {!! Form::label('eficaz_data', 'Data: ') !!}
                    {!! Form::text('eficaz_data', (isset($naoconformidade_edit['eficaz_data']) ? $naoconformidade_edit['eficaz_data']:''), ['class' => 'datepicker picker__input', 'id' => 'eficaz_data', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($naoconformidade_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($naoconformidade_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
