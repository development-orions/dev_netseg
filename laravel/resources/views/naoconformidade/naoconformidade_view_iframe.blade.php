@extends('iframe')
@section('title', 'Visualizar Cliente')
@section('content')

    <div class="container">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">RNC - Relatório de Não Conformidade </p>
            </div>
        </div>
        <div class="row iframe-p">

            <div class="col s3 center-align">
                <p>Código: <b>{{ $naoconformidade->codigo }}</b></p>
                <p>Revisão: <b>0{{ $naoconformidade->revisao }}</b></p>
                <p>Vigência: <b>{{ $naoconformidade->vigencia }}</b></p>
                <br>
            </div>
            <div class="col s6 center">
                <h5>Nº: {{ $naoconformidade->numero }}</h5>
                <p>Data: <b>{{ $naoconformidade->created_at->format('d/m/Y') }}</b></p>
                <br>
            </div>
            <div class="col s3 right-align">
                <a onclick="parent.$.fn.colorbox.close();" target="_blank" href="/pdf/naoconformidade/{{ base64_encode($naoconformidade->id) }}" class="btn-floating waves-effect waves-light orange darken-1" title="Imprimir Não Conformidade"><i class="material-icons">print</i></a>                 </p>
            </div>
        </div>
        <hr>
        <div class="row iframe-p">
            <div class="col s6">
                <p>Não Conformidade {{ $naoconformidade->realpotencial == 1?'Real':'Potencial' }}  </p>
            </div>
            <div class="col s5 right">
                <h5>{{ $naoconformidade->materialsistema != 0 ? 'Material / Processo':'Sistema / Serviço' }}</h5>
            </div>
        </div>
        <div class="row iframe-p">
            <div class="col s6">
                <p class="text-uppercase"><b>{{ $cliente->nome }}</b></p>
                <p><b>SETOR INTERNO:</b> {{ $tecnico->nome }}</p>
                <br>
            </div>
            <div class="col s5 right">
                <p>Atividade: <b>{{ $atividade->id > 9 ? '':'0' }}{{ $atividade->id }}</b></p>
                <p>Data da Atividade: <b>{{ $mes > 9 ? '':'0' }}{{ $mes }}/{{ $ano }}</b></p>
                <br>
            </div>
        </div>
        <hr>
        @if(isset($naoconformidade->ocorrencia))
            <div class="row ">
                <div class="col s12 ">
                    <h5>1 - Ocorrência:</h5>
                    <div class="col s12 {{ isset($naoconformidade->ocorrencia)? 'marginconformidade' : 'espacoconformidade' }}">{{ $naoconformidade->ocorrencia }}</div>
                    <div class="col s4 left">Responsável: {{ $naoconformidade->ocorrencia_responsavel }} </div>
                    <div class="col s4 right-align">Data:  {{ isset($naoconformidade->ocorrencia_data) ? $naoconformidade->ocorrencia_data : '___/___/______' }}</div>
                </div>
            </div>
            <hr>
        @endif

        @if(isset($naoconformidade->acaoimediata))
            <div class="row ">
                <div class="col s12 ">
                    <h5>2 - Ação Imediata:</h5>
                    <div class="col s12 {{ isset($naoconformidade->acaoimediata)? 'marginconformidade' : 'espacoconformidade' }}">{{ $naoconformidade->acaoimediata }}</div>
                    <div class="col s4 left">Responsável: {{ $naoconformidade->acaoimediata_responsavel }} </div>
                    <div class="col s4 right-align">Data:  {{ isset($naoconformidade->acaoimediata_data) ? $naoconformidade->acaoimediata_data : '___/___/______' }}</div>
                </div>
            </div>
            <hr>
        @endif

        @if(isset($naoconformidade->causaprovavel))
            <div class="row ">
                <div class="col s12 ">
                    <div class="col s8"><h5>3 - Causa Provável da não conformidade:</h5></div>
                    <div class="col s4 "><p>{{ $naoconformidade->causaprovavel_op == 1?'Relatório improcedente':'Relatório procedente' }}</p></div>
                    <div class="col s12 {{ isset($naoconformidade->causaprovavel)? 'marginconformidade' : 'espacoconformidade' }}">{{ $naoconformidade->causaprovavel }}</div>
                    <div class="col s4 left">Responsável: {{ $naoconformidade->causaprovavel_responsavel }} </div>
                    <div class="col s4 right-align">Data:  {{ isset($naoconformidade->causaprovavel_data) ? $naoconformidade->causaprovavel_data : '___/___/______' }}</div>
                </div>
            </div>
            <hr>
        @endif

        @if(isset($naoconformidade->acaocorretiva))
            <div class="row ">
                <div class="col s12 ">
                    <div class="col s8"><h5>4- Ações Corretivas / Ações Preventivas:</h5></div>
                    <div class="col s12 {{ isset($naoconformidade->acaocorretiva)? 'marginconformidade' : 'espacoconformidade' }}">{{ $naoconformidade->acaocorretiva }}</div>
                    <div class="col s4 left">Responsável: {{ $naoconformidade->acaocorretiva_responsavel }} </div>
                    <div class="col s4 right-align">Data:  {{ isset($naoconformidade->acaocorretiva_prazo) ? $naoconformidade->acaocorretiva_prazo : '___/___/______' }}</div>
                </div>
            </div>
            <hr>
        @endif

        <div class="row ">
            <div class="col s12 ">
                <div class="col s12"><h5>5- Eficácia da Ação Corretiva / Ação Preventiva:</h5></div>
                <div class="col s10 "><p>A Ação Corretiva foi implementada?</p></div>
                <div class="col s2 "><p><b>{{ $naoconformidade->foiimplementada == 1?'Sim':'Não' }}</b></p></div>
                <div class="col s10 "><p>A ação implementada é eficaz para evitar a Reincidência da Não Conformidade?</p></div>
                <div class="col s2 "><p><b>{{ $naoconformidade->eeficaz == 1?'Sim':'Não' }}</b></p></div>
                @if($naoconformidade->eeficaz == 1)
                    <div class="col s12">{{ $naoconformidade->eficaz_observacoes }}</div>
                @endif
                @if($naoconformidade->foiimplementada == 1)
                    <div class="col s4 left">Responsável: {{ $naoconformidade->eficaz_responsavel }} </div>
                    <div class="col s4 right-align">Data:  {{ isset($naoconformidade->eficaz_data) ? $naoconformidade->eficaz_data : '___/___/______' }}</div>
                @endif
            </div>
        </div>
        <hr>
    </div>
@endsection