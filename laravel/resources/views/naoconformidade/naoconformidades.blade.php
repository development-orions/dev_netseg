@extends('app')
@section('title', 'Listar Não Conformidades')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de não Conformidades</p>
            </div>
        </div>
        <div class="row filtro">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="col m4">
                <div class="input-field col s7">
                    <input type="text" disabled class="validate" name="cliente" value="{{ isset(Session::get('cliente_cronograma')->nome) ? Session::get('cliente_cronograma')->nome : '' }}" id="cliente">
                    <label for="nomeUser">Cliente</label>

                    {!! Form::hidden('cliente_id', isset(Session::get('cliente_cronograma')->id) ? Session::get('cliente_cronograma')->id : '' , ['id' => 'cliente_id']) !!}

                </div>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/buscar_cliente_cronograma" title="Buscar cliente" class="btn btn-cronograma waves-effect waves-light iframe"><i class="material-icons">search</i></a>
                <a title="Remover Cliente" href="{{ Route::getCurrentRoute()->getPrefix() }}/remover_cliente" class="btn btn-cronograma waves-effect waves-light"><i class="material-icons">clear</i></a>
            </div>
            <div class="col m4">
                <div class="input-field col s12">
                    {!! Form::select('tecnico_id', $tecnicos, isset($tecnico) ? $tecnico : null, ['id' => 'tecnico']) !!}
                    <label>Técnico</label>
                </div>
            </div>
            <div class="col m2">
                <div class="input-field col s12">
                    <select name="mes" id="mes">
                        <option value="" >Todos os meses</option>
                        <?php for($i = 01; $i <= 12; $i++){ ?>
                        <option value="<?= $i ?>" <?= isset($mes) ? ($mes == $i ? 'selected' : '') : ($i == date('m') ? 'selected' : '') ?>  ><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <label for="mes">Mês</label>
                </div>
            </div>
            <div class="col m2">
                <div class="input-field col s12">
                    <select name="ano">
                        <?php for($i = 2016; $i <= date('Y')+1; $i++){ ?>
                        <option value="<?= $i ?>" <?= isset($ano) ? ($ano == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '') ?> ><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <label for="ano">Ano</label>
                </div>
            </div>
            <div class="col m12 right-align">
                <button class="btn btn-cronograma waves-effect waves-light" type="submit" name="action"><i class="material-icons">BUSCAR</i></button>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/limpar_filtro" class="btn btn-cronograma waves-effect waves-light"><i class="material-icons">delete_sweep</i></a>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Número</th>
                        <th>Cliente</th>
                        <th>Tecnico</th>
                        <th>Atividade</th>
                        <th>Data da atividade</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($naoconformidades as $naoconformidade)

                        <tr>
                            <td>{{ $naoconformidade->numero }}</td>
                            <td>{{ $naoconformidade->clientes->nome}}</td>
                            <td>{{ $naoconformidade->tecnicos->nome }}</td>
                            <td>{{ $naoconformidade->atividades->nome }}</td>
                            <td>{{ ($naoconformidade->mes > 9 ? '' : '0').$naoconformidade->mes.'/'.$naoconformidade->ano }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/naoconformidade_view/{{ base64_encode($naoconformidade->id) }}" title="Visualizar Não Conformidade" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/naoconformidade_edita/{{ base64_encode($naoconformidade->id) }}" title="Editar não conformidade" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>

                                        <li><a href="/pdf/naoconformidade/{{ base64_encode($naoconformidade->id) }}" target="_blank" class="btn-floating waves-effect waves-light orange darken-1" title="Imprimir Não Conformidade"><i class="material-icons">print</i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhuma Não Conformidade encontrada.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $naoconformidades->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection