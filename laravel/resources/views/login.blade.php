<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
<meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <title>@yield('title') NetSeg Segurança do Trabalho</title>

    <!-- Scripts -->
    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/materialize/js/materialize.js"></script>
    <script type="text/javascript" src="/js/plugins/alert.js"></script>
    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="/materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">

    <link rel="stylesheet" href="/css/font-awesome-4.4.0/css/font-awesome.min.css" type="text/css"/>

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="/css/estilo.css" rel="stylesheet">
</head>
<body>
<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="cyan">
            <div class="nav-wrapper">
                <nav>
                    <div class="nav-wrapper bold">
                        <a href="/" class="left"><img src="/img/logo.png" alt="Logo NetSeg"/></a>
                        <ul class="hide-on-med-and-down">
                            <li class="right"><p class="corcinza capitalize text-font12">{{ Date::now()->format('l') }}, {{ Date::now()->format('j') }} <span class="lowercase">de</span> {{ Date::now()->format(' F') }} <span class="lowercase">de</span>{{ Date::now()->format(' Y') }} </p> </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
</header>
<!-- END HEADER -->

<!-- START MAIN -->
<div id="main2">
    <!-- START WRAPPER -->
    <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content">
            <!--start container-->
            <div class="container">
                <div class="row" id="form">
                    <div class="col s12 m6 l6 bg-login">
                        @yield('content')
                    </div>
                </div>
            </div>
        </section>
        <!-- END CONTENT -->

    </div>
    <!-- END WRAPPER -->

</div>
<!-- END MAIN -->

<!-- START FOOTER -->
<footer class="page-footer login-footer">
    <div class="footer-copyright">
        <div class="container col s12 center-align center">
            NETSEG V1.0.0 © 2015
        </div>
    </div>
</footer>
<!-- jQuery Library -->
<!--scrollbar-->
<script type="text/javascript" src="/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>


<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="/js/plugins.js"></script>
<!-- Toast Notification -->
@yield('script')
</body>
</html>
