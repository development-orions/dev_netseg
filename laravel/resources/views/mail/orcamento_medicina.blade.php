<?php $orcamento = unserialize($orcamento); ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>NetSeg Segurança do Trabalho</title>
    <style type="text/css">
        body{color: #2e2e2e; font-family: 'Arial';
        }
        *{margin: 0;}
        .cabecalho h2{ padding: 5px 0; font-size: 130%;}
        .cabecalho p{font-size: 14px; font-weight: normal}

        .orcamento caption{text-align: left;}


        .orcamento thead{
            background: #5B9C38;
        }
        .orcamento th{
            color: #ffffff;
        }
        .orcamento, .orcamento tr, .orcamento th, .orcamento td{border: #5b9b38 solid 1px;}
        .orcamento td{padding: 10px; font-size: 90%}
        .text-center{text-align: center;}
        .padding{padding: 10px 0}
        .font12{font-size: 12px;}
    </style>
</head>
<body style="background: #F2F1EF; text-align: left;">
<table cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <th><img src="{{ URL::to('').'/img/logo.png' }}" alt="NetSeg" style="margin-right: 20px;"></th>
        <th>
            <table class="cabecalho">
                <tbody>
                <tr>
                    <td><h2>{{ $configuracoes['nomefantasia'] }}</h2></td>
                </tr>
                <tr>
                    <td><p>Razão Social: {{ $configuracoes['razaosocial'] }}</p></td>
                </tr>
                <tr>
                    <td><p>CNPJ: {{ $configuracoes['cnpj'] }}</p></td>
                </tr>
                <tr>
                    <td><p>Endereço: {{ $configuracoes['rua'] }}, {{ $configuracoes['numero'] }} - {{ $configuracoes['bairro'] }}, {{ $configuracoes['cidade'] }} - {{ $configuracoes['estado'] }} CEP: {{ $configuracoes['cep'] }}</p></td>
                </tr>
                <tr>
                    <td><p>Telefone: {{ $configuracoes['telefone'] }}</p></td>
                </tr>
                <tr>
                    <td><p>E-mail: {{ $configuracoes['email'] }} </p></td>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>

    </tbody>

</table>
<hr>
<h2 class="padding text-center">Orçamento</h2>
<hr>

<h4 class="padding">Dados</h4>
<p>Criação: {{ $orcamento->created_at }}</p>
<p>Validade do orçamento: {{ $orcamento->validade }} (15 dias)</p>
<p>Realizado por: {{ $orcamento->users['nome'] }}</p>

<h4 class="padding">Cliente</h4>
<p>{{$orcamento->clientes['nome'] }}</p>
<p>CNPJ: {{ $orcamento->clientes['cnpj'] }}</p>
<p>Endereço: {{ $orcamento->clientes['rua'] }}, {{ $orcamento->clientes['numero'] }} - {{ $orcamento->clientes['bairro'] }}, {{ $orcamento->clientes['cidade'] }} - {{ $orcamento->clientes['estado'] }} CEP: {{ $orcamento->clientes['cep'] }}</p>
<p>Responsável: {{ $orcamento->clientes['responsavel'] }}</p>

<table class="orcamento" cellspacing="0">
    <caption><h4 class="padding">Serviços Inclusos</h4></caption>
    <thead>
    <tr>
        <th>Nome</th>
        <th>Descrição</th>
        <th>Valor</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orcamento->itens as $servico)
    <tr>
        <td>{{ $servico->servicos['nome'] }}</td>
        <td>{!! $servico['descricao'] !!}</td>
        <td class="text-center">{{ $servico['valor'] }}</td>
    </tr>
        @endforeach
    </tbody>
</table>

<h4 class="padding">Valor</h4>
<p>Total: {{ $orcamento['valor'] }}</p>

<h4 class="padding">Pagamento</h4>
<p>Forma de Pagamento: {{ $orcamento->formapagamentos['nome'] }}</p>
<p>Nota Fiscal: {{ $orcamento['temnota'] == 0 ? 'não' : 'sim' }}</p>
<div class="padding">
    @if(isset($orcamento['parcela']) && !empty($orcamento['parcela']))
        <table class="orcamento" cellspacing="0">
            <thead>
            <tr>
                <th>Condições de pagamento</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>{{ $orcamento['parcela'] }}</td>
            </tr>
            </tbody>
        </table>
    @else
        <table class="orcamento" cellspacing="0">
            <thead>
            <tr>
                <th>Parcelas</th>
                <th>Data</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $orcamento->parcelas as $parcelas)
                <tr>
                    <td class="text-center">0{{ $parcelas['numero'] }}</td>
                    <td>{{ $parcelas['data'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>
</body>
</html>
