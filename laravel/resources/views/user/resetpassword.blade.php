@extends('app')
@section('title', 'Resetar senha de usuários')
@section('content')

    <script type="text/javascript" src="/js/validador/resetpassword.js"></script>
    <script>
        $(document).ready(function() {
            //$('#selectUser').material_select();
            //$('select').material_select();
        });
    </script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Resetar senha de Usuários</p>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                @if (count($errors) > 0)
                    <div class="">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row">
                @if(isset($user_edit))
                    {!! Form::hidden('id', (isset($user_edit['id']) ? $user_edit['id']:''), ['id' => 'id']) !!}
                @endif
                    {!! Form::hidden('email', (isset($user_edit['email']) ? $user_edit['email']:''), ['id' => 'email']) !!}
                    {!! Form::hidden('nome', (isset($user_edit['nome']) ? $user_edit['nome']:''), ['id' => 'nome']) !!}
                    {!! Form::hidden('username', (isset($user_edit['username']) ? $user_edit['username']:''), ['id' => 'username']) !!}
                    {!! Form::hidden('statu_id', (isset($user_edit['statu_id']) ? $user_edit['statu_id']:''), ['id' => 'statu_id']) !!}
                    {!! Form::hidden('funcao', (isset($user_edit['funcao']) ? $user_edit['funcao']:''), ['id' => 'funcao']) !!}
                    {!! Form::hidden('role_id', (isset($user_edit['role_id']) ? $user_edit['role_id']:''), ['id' => 'role_id']) !!}
            </div>
            <div class="row">

                <div class="input-field col s6">
                    <input type="password" class="validate" name="password" id="passUser">
                    <label for="passUser">Senha</label>
                </div>
            </div>
            <div class="row">

                <div class="input-field col s6">
                    <input type="password" class="validate" id="cpassUser" name="password_confirmation">
                    <label for="cpassUser">Confirmar Senha</label>
                </div>
            </div>
            <div class="row right-align">
                <div class="col s6">
                    <button class="btn waves-effect waves-light" type="submit" id="envia" name="action">Resetar<i class="material-icons right">vpn_key</i>
                    </button>
                    @if(isset($user_edit))
                        <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar</a>
                    @endif
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
