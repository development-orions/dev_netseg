@extends('app')
@section('title', 'Cadastrar usuários')
@section('content')
    <script type="text/javascript" src="/js/validador/user.js"></script>
    <script>
        $(document).ready(function() {
            //$('#selectUser').material_select();
            //$('select').material_select();
        });
    </script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Usuários</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                @if (count($errors) > 0)
                    <div class="">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::open(['class' => 'col s12']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="input-field col s6">
                    <input type="text" class="validate" name="nome" value="{{ old('nome') }}" id="nomeUser">
                    <label for="nomeUser">* Nome</label>
                </div>

                <div class="input-field col s6">
                    <input type="text" class="validate" name="funcao" value="{{ old('funcao') }}" id="funcaoUser">
                    <label for="funcaoUser">* Função</label>
                </div>

                <div class="input-field col s6">
                    <input type="email" class="validate" name="email" value="{{ old('email') }}" id="emailUser">
                    <label for="emailUser">* Email</label>
                </div>

                <div class="input-field col s6">
                    <input type="text" class="validate" name="username" value="{{ old('username') }}" id="loginUser">
                    <label for="loginUser">* Login</label>
                </div>

                <div class="input-field col s6">
                    <input type="password" class="validate" name="password" id="passUser">
                    <label for="passUser">* Senha</label>
                </div>

                <div class="input-field col s6">
                    <input type="password" class="validate" id="cpassUser" name="password_confirmation">
                    <label for="cpassUser">* Confirmar Senha</label>
                </div>

                <div class="input-field col s12 m6">
                    {!! Form::select('role_id', $roles, ['id' => 'selectUser', 'value' => isset($user_edit['role_id']) ? $user_edit['role_id']:'' ] ) !!}
                </div>

                <div class="col s12 ">
                    <button type="submit" id="envia" class="waves-effect waves-light btn">
                        <i class="material-icons left">send</i>Cadastrar
                    </button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
