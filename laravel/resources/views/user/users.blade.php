@extends('app')
@section('title', 'Listar usuários')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Usuários</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m2">
                    {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar Usuário']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Login</th>
                        <th>Nível</th>
                        <th>Nome</th>
                        <th>Função</th>
                        <th>Email</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>

                    @forelse($users as $forn)
                        <tr>

                            <td>{{ $forn->username }}</td>
                            <td>{{ $forn->roles->nome }}</td>
                            <td>{{ $forn->nome }}<span class="font12 bg-inativo"> {{$forn->statu_id == 2 ? 'inativo ': '' }}</span></td>
                            <td>{{ $forn->funcao }}</td>
                            <td>{{ $forn->email }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/user_view/{{ base64_encode($forn->id) }}" title="Visualizar Usuário" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        @if((Auth::user()->role_id == 6) || (Auth::user()->role_id == 7) || (Auth::user()->id == $forn->id))
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/user_edita/{{ base64_encode($forn->id) }}" title="Editar usuário" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/user_resetarsenha/{{ base64_encode($forn->id) }}" title="Resetar senha" class="btn-floating orange darken-1"><i class="material-icons">vpn_key</i></a></li>
                                        @endif
                                        @if((Auth::user()->role_id == 6) || (Auth::user()->role_id == 7) || (Auth::user()->role_id == 2))
                                            @if($forn->role_id != 1 && $forn->role_id != 6)
                                                @if($forn->statu_id == 1)
                                                    <li><a  title="Inativar usuário" class="btn-floating red" href="{{ Route::getCurrentRoute()->getPrefix() }}/user_ativa/{{ base64_encode($forn->id) }}"><i class="material-icons">not_interested</i></a></li>
                                                @else
                                                    <li><a title="Ativar usuário" class="btn-floating green" href="{{ Route::getCurrentRoute()->getPrefix() }}/user_inativa/{{ base64_encode($forn->id) }}"><i class="material-icons">done</i></a></li>
                                                @endif
                                            @endif
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum usuário encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                    <?php echo $users->appends(Request::all())->render(); ?>
                <!--Resolvido: descobrir o que acontece em formulários de pesquisa que utilizam $query - é necessário criar uma condicional no metodo do service -->
            </div>
        </div>
    </div>
@endsection