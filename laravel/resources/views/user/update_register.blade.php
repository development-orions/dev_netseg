@extends('app')
@section('title', 'Editar usuários')
@section('content')

    <script type="text/javascript" src="/js/validador/user_edit.js"></script>
    <script>
        $(document).ready(function() {
            //$('#selectUser').material_select();
            //$('select').material_select();
        });
    </script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">{{ isset($user_edit) ? 'Editar usuário': 'Cadastro de Usuários' }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                @if (count($errors) > 0)
                    <div class="">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row">
                @if(isset($user_edit))
                    {!! Form::hidden('id', (isset($user_edit['id']) ? $user_edit['id']:''), ['id' => 'id']) !!}
                @endif
                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($user_edit['nome']) ? $user_edit['nome']:''), ['class' => 'validate', 'id' =>'nome']) !!}
                    {!! Form::label('nome', 'Nome: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('funcao', (isset($user_edit['funcao']) ? $user_edit['funcao']:''), ['class' => 'validate', 'id' => 'funcao']) !!}
                    {!! Form::label('funcao', 'Função: ') !!}
                </div>
                    <div class="input-field col s6">
                        {!! Form::text('email', (isset($user_edit['email']) ? $user_edit['email']:''), ['class' => 'validate', 'id' => 'email']) !!}
                        {!! Form::label('email', 'Email: ') !!}
                    </div>
                    @if((Auth::user()->role_id == 6) || (Auth::user()->role_id == 8) || (Auth::user()->role_id == 1))
                    <div class="input-field col s12 m6">
                        {!! Form::select('role_id', $roles, ['id' => 'role_id', 'value' => isset($user_edit['role_id']) ? $user_edit['role_id']:'' ] ) !!}
                        {!! Form::label('role_id', 'Perfil usuário: ') !!}
                    </div>
                        @else
                        <input type="hidden" name="role_id" value="{{$user_edit['role_id']}}">
                    @endif
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="envia" name="action">{{ isset($user_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($user_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}


        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
