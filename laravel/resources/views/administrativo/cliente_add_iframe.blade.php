@extends('iframe')
@section('title', 'Cadastrar Cliente')
@section('content')

	<script type="text/javascript" src="/js/validador/cliente_iframe.js"></script>

	<script type="text/javascript" src="/js/core.js"></script>
	<script type="text/javascript"  >
		function getEndereco(cep) {
			// Se o campo CEP não estiver vazio
			if($.trim(cep) != ""){
				$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
					if(resultadoCEP["resultado"]){
						$("#rua").val(unescape(resultadoCEP["logradouro"]));
						$("#bairro").val(unescape(resultadoCEP["bairro"]));
						$("#cidade").val(unescape(resultadoCEP["cidade"]));
						$("#estado").val(unescape(resultadoCEP["uf"]));
					}
				});
			}
		}
		$(document).ready(function() {
			$("#cep").blur(function(){
				getEndereco($("#cep").val());
			});
		});
	</script><!-- === chamada de endereço por CEP === -->
	<div class="container">
		<div class="row  bg">
			<div class="col s12 bgverde">
				<p class="bold font20 corbranca">Cadastro de Clientes</p>
			</div>
		</div>
		<div class="row">

			{!! Form::open(['class' => 'col s12']) !!}
			<div class="row">

				@if(isset($cliente_edit))
					{!! Form::hidden('id', (isset($cliente_edit['id']) ? $cliente_edit['id']:''), ['id' => 'id']) !!}
					{!! Form::hidden('statu_id', (isset($cliente_edit['statu_id']) ? $cliente_edit['statu_id']:'')) !!}
				@endif

				<div class="input-field col m6 s12">
					{!! Form::text('nome', (isset($cliente_edit['nome']) ? $cliente_edit['nome']:''), ['class' => 'validate']) !!}
					{!! Form::label('nome', 'Nome: ') !!}
				</div>
				<div class="input-field col m6 s12">
					{!! Form::text('razaosocial', (isset($cliente_edit['razaosocial']) ? $cliente_edit['razaosocial']:''), ['class' => 'validate', 'id' => 'razaosocial']) !!}
					{!! Form::label('razaosocial', 'Razão Social: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('cnpj', (isset($cliente_edit['cnpj']) ? $cliente_edit['cnpj']:''), ['class' => 'validate', 'id' => 'cnpj', 'maxlength' => '18']) !!}
					{!! Form::label('cnpj', 'CNPJ: ') !!}
				</div>
				<div class="input-field col m6 s12">
					{!! Form::text('ie', (isset($cliente_edit['ie']) ? $cliente_edit['ie']:''), ['class' => 'validate', 'id' => 'ie']) !!}
					{!! Form::label('ie', 'IE: ') !!}
				</div>
			</div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('valor_contrato', (isset($cliente_edit['valor_contrato']) ? $cliente_edit['valor_contrato']:''), ['class' => 'validate', 'id' => 'valor_contrato']) !!}
                    {!! Form::label('valor_contrato', 'Valor do contrato: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('data_contrato', 'Data de Contrato: ') !!}
                    {!! Form::text('data_contrato', (isset($cliente_edit['data_contrato']) ? $cliente_edit['data_contrato']:''), ['class' => 'datepicker picker__input', 'id' => 'data_contrato', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
			<div class="row">
				<div class="input-field col m2 s4">
					{!! Form::text('cep', (isset($cliente_edit['cep']) ? $cliente_edit['cep']:''), ['class' => 'validate', 'id' => 'cep', 'maxlength' => '10']) !!}
					{!! Form::label('cep', 'CEP: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('rua', (isset($cliente_edit['rua']) ? $cliente_edit['rua']:''), ['class' => 'validate', 'id' => 'rua']) !!}
					{!! Form::label('rua', 'Rua: ') !!}
				</div>
				<div class="input-field col m2 s6">
					{!! Form::text('numero', (isset($cliente_edit['numero']) ? $cliente_edit['numero']:''), ['class' => 'validate', 'id' => 'numero']) !!}
					{!! Form::label('numero', 'Número: ') !!}
				</div>
				<div class="input-field col m4 s6">
					{!! Form::text('complemento', (isset($cliente_edit['complemento']) ? $cliente_edit['complemento']:'')) !!}
					{!! Form::label('complemento', 'Complemento: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('bairro',(isset($cliente_edit['bairro']) ? $cliente_edit['bairro']:''), ['class' => 'validate', 'id' => 'bairro']) !!}
					{!! Form::label('bairro', 'Bairro: ') !!}
				</div>
				<div class="input-field col m4 s8">
					{!! Form::text('cidade', (isset($cliente_edit['cidade']) ? $cliente_edit['cidade']:''), ['class' => 'validate', 'id' => 'cidade']) !!}
					{!! Form::label('cidade', 'Cidade: ') !!}
				</div>
				<div class="input-field col m2 s4">
					{!! Form::text('estado', (isset($cliente_edit['estado']) ? $cliente_edit['estado']:''), ['class' => 'validate', 'id' => 'estado']) !!}
					{!! Form::label('estado', 'Estado: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					{!! Form::text('formajuridica', (isset($cliente_edit['formajuridica']) ? $cliente_edit['formajuridica']:''), ['class' => 'validate']) !!}
					{!! Form::label('formajuridica', 'Forma Jurídica: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('responsavel', (isset($cliente_edit['responsavel']) ? $cliente_edit['responsavel']:''), ['class' => 'validate']) !!}
					{!! Form::label('responsavel', 'Responsável: ') !!}
				</div>
				<div class="input-field col m6 s12">
					{!! Form::text('funcao', (isset($cliente_edit['funcao']) ? $cliente_edit['funcao']:''), ['class' => 'validate']) !!}
					{!! Form::label('funcao', 'Função: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::email('email_responsavel', (isset($cliente_edit['email_responsavel']) ? $cliente_edit['email_responsavel']:''), ['class' => 'validate', 'id' => 'email']) !!}
					{!! Form::label('email_responsavel', 'Email: ', ['data-error' => 'Email inválido']) !!}
				</div>
				<div class="input-field col m6 s12">
					{!! Form::text('telefone_responsavel', (isset($cliente_edit['telefone_responsavel']) ? $cliente_edit['telefone_responsavel']:''), ['class' => 'validate', 'id' => 'telefone', 'maxlength' => '15']) !!}
					{!! Form::label('telefone_responsavel', 'Telefone: ') !!}
				</div>
			</div>
			<div class="row right-align">

				<button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($cliente_edit) ? 'Salvar' : 'Cadastrar'  }}
					<i class="material-icons right">send</i>
				</button>
				@if(isset($cliente_edit))
					<a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
				@endif

			</div>

			{!! Form::close() !!}
		</div>
	</div>
	<div id="modal1" class="modal"></div>
@endsection
