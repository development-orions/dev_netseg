@extends('app')
@section('title', 'Cadastrar Serviço')
@section('content')
    @if(isset($servico_edit))
        <script type="text/javascript" src="/js/validador/servico_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/servico.js"></script>
    @endif


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Serviços</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row">

                @if(isset($servico_edit))
                    {!! Form::hidden('id', (isset($servico_edit['id']) ? $servico_edit['id']:''), ['id' => 'id']) !!}

                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($servico_edit['nome']) ? $servico_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', 'Nome: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('valor', (isset($servico_edit['valor']) ? $servico_edit['valor']:''), ['class' => 'validate', 'id' => 'valor']) !!}
                    {!! Form::label('valor', 'Valor: ') !!}
                </div>
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($servico_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($servico_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
