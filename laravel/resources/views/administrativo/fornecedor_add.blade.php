@extends('app')

@section('content')
    @if(isset($fornecedor_edit))
        <script type="text/javascript" src="/js/validador/fornecedor_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/fornecedor.js"></script>
    @endif
    <script type="text/javascript" src="/js/core.js"></script>
    <script type="text/javascript"  >
        function getEndereco(cep) {
            // Se o campo CEP não estiver vazio
            if($.trim(cep) != ""){
                $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
                    if(resultadoCEP["resultado"]){
                        $("#rua").val(unescape(resultadoCEP["logradouro"]));
                        $("#bairro").val(unescape(resultadoCEP["bairro"]));
                        $("#cidade").val(unescape(resultadoCEP["cidade"]));
                        $("#estado").val(unescape(resultadoCEP["uf"]));
                    }
                });
            }
        }
        $(document).ready(function() {
            $("#cep").blur(function(){
                getEndereco($("#cep").val());
            });
        });
    </script><!-- === chamada de endereço por CEP === -->
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">{{ isset($fornecedor_edit) ? 'Editar' : 'Cadastro de'  }} Fornecedor</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row">
                @if(isset($fornecedor_edit))
                    {!! Form::hidden('id', (isset($fornecedor_edit['id']) ? $fornecedor_edit['id']:''), ['id' => 'id']) !!}
                    {!! Form::hidden('statu_id', (isset($fornecedor_edit['statu_id']) ? $fornecedor_edit['statu_id']:'')) !!}
                @endif
                
                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($fornecedor_edit['nome']) ? $fornecedor_edit['nome']:''), ['class' => 'validate']) !!}
                    {!! Form::label('nome', 'Nome: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('razaosocial', (isset($fornecedor_edit['razaosocial']) ? $fornecedor_edit['razaosocial']:''), ['class' => 'validate', 'id' => 'razaosocial']) !!}
                    {!! Form::label('razaosocial', 'Razão Social: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('cnpj', (isset($fornecedor_edit['cnpj']) ? $fornecedor_edit['cnpj']:''), ['class' => 'validate', 'id' => 'cnpj', 'maxlength' => '18']) !!}
                    {!! Form::label('cnpj', 'CNPJ: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('cpf', (isset($fornecedor_edit['cpf']) ? $fornecedor_edit['cpf']:''), ['class' => 'validate', 'id' => 'cpf', 'maxlength' => '15']) !!}
                    {!! Form::label('cpf', 'CPF: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('ie', (isset($fornecedor_edit['ie']) ? $fornecedor_edit['ie']:''), ['class' => 'validate', 'id' => 'ie']) !!}
                    {!! Form::label('ie', 'IE: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('cei_autonomo', (isset($fornecedor_edit['cei_autonomo']) ? $fornecedor_edit['cei_autonomo']:''), ['class' => 'validate', 'id' => 'cei_autonomo']) !!}
                    {!! Form::label('cei_autonomo', 'CEI/Autonomo: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('datainicio_servico', 'Data de inicio de Serviço: ') !!}
                    {!! Form::text('datainicio_servico', (isset($fornecedor_edit['datainicio_servico']) ? $fornecedor_edit['datainicio_servico']:''), ['class' => 'datepicker picker__input', 'id' => 'datainicio_servico', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m2 s4">
                    {!! Form::text('cep', (isset($fornecedor_edit['cep']) ? $fornecedor_edit['cep']:''), ['class' => 'validate', 'id' => 'cep', 'maxlength' => '10']) !!}
                    {!! Form::label('cep', 'CEP: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('rua', (isset($fornecedor_edit['rua']) ? $fornecedor_edit['rua']:''), ['class' => 'validate', 'id' => 'rua']) !!}
                    {!! Form::label('rua', 'Rua: ') !!}
                </div>
                <div class="input-field col m2 s6">
                    {!! Form::text('numero', (isset($fornecedor_edit['numero']) ? $fornecedor_edit['numero']:''), ['class' => 'validate', 'id' => 'numero']) !!}
                    {!! Form::label('numero', 'Número: ') !!}
                </div>
                <div class="input-field col m4 s6">
                    {!! Form::text('complemento', (isset($fornecedor_edit['complemento']) ? $fornecedor_edit['complemento']:'')) !!}
                    {!! Form::label('complemento', 'Complemento: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('bairro', (isset($fornecedor_edit['bairro']) ? $fornecedor_edit['bairro']:''), ['class' => 'validate', 'id' => 'bairro']) !!}
                    {!! Form::label('bairro', 'Bairro: ') !!}
                </div>
                <div class="input-field col m4 s8">
                    {!! Form::text('cidade', (isset($fornecedor_edit['cidade']) ? $fornecedor_edit['cidade']:''), ['class' => 'validate', 'id' => 'cidade']) !!}
                    {!! Form::label('cidade', 'Cidade: ') !!}
                </div>
                <div class="input-field col m2 s4">
                    {!! Form::text('estado', (isset($fornecedor_edit['estado']) ? $fornecedor_edit['estado']:''), ['class' => 'validate', 'id' => 'estado']) !!}
                    {!! Form::label('estado', 'Estado: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('responsavel', (isset($fornecedor_edit['responsavel']) ? $fornecedor_edit['responsavel']:''), ['class' => 'validate']) !!}
                    {!! Form::label('responsavel', 'Responsável: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('funcao', (isset($fornecedor_edit['funcao']) ? $fornecedor_edit['funcao']:''), ['class' => 'validate']) !!}
                    {!! Form::label('funcao', 'Função: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::email('email_responsavel', (isset($fornecedor_edit['email_responsavel']) ? $fornecedor_edit['email_responsavel']:''), ['class' => 'validate', 'id' => 'email']) !!}
                    {!! Form::label('email_responsavel', 'Email: ', ['data-error' => 'Email inválido']) !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('telefone_responsavel', (isset($fornecedor_edit['telefone_responsavel']) ? $fornecedor_edit['telefone_responsavel']:''), ['class' => 'validate', 'id' => 'telefone', 'maxlength' => '15']) !!}
                    {!! Form::label('telefone_responsavel', 'Telefone: ') !!}
                </div>
            </div>
            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($fornecedor_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($fornecedor_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
