@extends('app')
@section('title', 'Cadastrar Orçamento')
@section('content')
    @if(isset($conta_edit))
        <script type="text/javascript" src="/js/validador/orcamento_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/orcamento.js"></script>
    @endif

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });

    </script>


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Orçamento</p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row padding">
                @if(Session::has('cliente_orcamento'))
                    <input type="hidden" id="cliente" value="true">
                    {!! Form::hidden('cliente_id', Session::get('cliente_orcamento')->id , ['id' => 'cliente_id']) !!}
                    <div class="col m6" id="mostra">
                        <h5>Cliente</h5>
                        <p>Razão Social: {{ Session::get('cliente_orcamento')->razaosocial }}</p>
                        <p>CNPJ: {{ Session::get('cliente_orcamento')->cnpj }}</p>
                    </div>
                @else
                    <input type="hidden" id="cliente" value="false">
                @endif
                <div class="input-field col s12 m6">
                    <a href="/administrativo/buscar_orcamento" class="iframe btn btn-md waves-effect waves-light blue-grey"><i class="material-icons right">search</i>Buscar Cliente</a>
                </div>
            </div>
            <div class="row">
                @if(isset($orcamento_edit))
                    {!! Form::hidden('id', (isset($orcamento_edit['id']) ? $orcamento_edit['id']:''), ['id' => 'id']) !!}

                @endif
            </div>

            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('formapagamento_id', $formapagamento, isset($orcamento_edit['formapagamento_id']) ? $orcamento_edit['formapagamento_id'] : null, ['id' => 'formapagamento']) !!}
                    {!! Form::label('formapagamento', 'Forma de Pagamento: ') !!}
                </div>

                <div class="input-field col m6 s12">
                    {!! Form::text('num_boleto', (isset($orcamento_edit['num_boleto']) ? $orcamento_edit['num_boleto']:''), ['class' => 'validate', 'id' => 'num_boleto']) !!}
                    {!! Form::label('num_boleto', 'Núm. Boleto: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('nf', (isset($orcamento_edit['nf']) ? $orcamento_edit['nf']:''), ['class' => 'validate', 'id' => 'nf']) !!}
                    {!! Form::label('nf', 'Núm. Nota Fiscal: ') !!}
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m6">
                    <a href="/administrativo/adicionar_servico" class="iframe btn btn-md waves-effect waves-light blue-grey">Adicionar Serviço <i class="fa fa-plus-square"></i></a>
                </div>
            </div>

            <div class="row padding">

                @if(Session::has('servico'))
                    <?php
                        $key = array_keys(Session::get('servico'));
                    $id = 0;
                    ?>
                        <input type="hidden" id="servico" value="true">
                    @foreach(Session::get('servico') as $servicos)

                    <div class="col m12 servico">
                        <h5>Serviço - {{ $servicos['servico_id']->nome }}
                            <a href="/administrativo/remover_servico/{{ $key[$id++] }}" class="btn-floating waves-effect waves-light orange right"><i class="fa fa-remove"></i></a> <a class="btn-floating waves-effect waves-light blue right"><i class="fa fa-edit"></i></a></h5>
                        <div class="col m6">
                            <p><span class="green-text text-darken-2">Descrição:</span> {{ $servicos['descricao'] }}</p>
                        </div>
                        <div class="col m5 offset-l1">
                            <p><span class="green-text text-darken-2">Valor:</span> {{ $servicos['valor'] }}</p>
                            <p><span class="green-text text-darken-2">Carga horária:</span> {{ $servicos['cargahoraria'] }}</p>
                            <p><span class="green-text text-darken-2">Núm. Participantes:</span> {{ $servicos['participantes'] }}</p>
                            <p><span class="green-text text-darken-2">Período:</span> {{ $servicos['periodo'] }} / Horário: {{ $servicos['horario'] }}</p>
                            <p><span class="green-text text-darken-2">Técnico:</span> {{ $servicos['tecnico_id']->nome }} - <span class="green-text text-darken-2">Num. Registro:</span> {{ $servicos['tecnico_id']->num_registro }}</p>
                        </div>
                    </div>

                    @endforeach
                @else
                    <input type="hidden" id="servico" value="false">
                @endif
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($orcamento_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($orcamento_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection



