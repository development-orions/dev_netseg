@extends('iframe')
@section('title', 'Buscar Cliente')
@section('content')

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Buscar {{ $tipo == 1 ? 'Cliente' : 'Fornecedor' }}</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}

            <div class="row">
                <div class="input-field col s12 m12">
                    <div class="input-field col m10 s8">
                        {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate']) !!}
                        {!! Form::label('buscarcliente', 'Buscar '.($tipo == 1 ? 'Cliente' : 'Fornecedor')) !!}
                    </div>
                    <button class="btn btn-md waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons right">search</i>Pesquisar
                    </button>

                </div>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Razão Social</th>
                        <th>Nome</th>
                        <th>CNPJ</th>
                        <th>Cidade</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @if(isset($clientes))
                        @forelse($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->razaosocial }}</td>
                                <td>{{ $cliente->nome }}</td>
                                <td>{{ $cliente->cnpj }}</td>
                                <td>{{ $cliente->cidade }}</td>
                                <td>{{ $cliente->telefone_responsavel }}</td>
                                <td>
                                    <div class="fixed-action-btn horizontal">
                                        <a href="{{ Route::getCurrentRoute()->getPrefix() }}/adicionar/{{ $cliente->id }}/{{$tipo}}" title="Adicionar" class="btn-floating waves-effect waves-light red"><i class="material-icons">add</i></a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">Nenhum cliente encontrado.</td>
                            </tr>
                            <tr>
                                <td colspan="5"><a href="{{ Route::getCurrentRoute()->getPrefix() }}/cliente_iframe" class="waves-effect waves-light btn blue iframe">Adicionar cliente</a></td>
                            </tr>
                        @endforelse
                    @endif
                    @if(isset($fornecedores))
                        @forelse($fornecedores as $fornecedor)
                            <tr>
                                <td>{{ $fornecedor->razaosocial }}</td>
                                <td>{{ $fornecedor->nome }}</td>
                                <td>{{ $fornecedor->cnpj }}</td>
                                <td>{{ $fornecedor->cidade }}</td>
                                <td>{{ $fornecedor->telefone_responsavel }}</td>
                                <td>
                                    <div class="fixed-action-btn horizontal">
                                        <a href="{{ Route::getCurrentRoute()->getPrefix() }}/adicionar/{{ $fornecedor->id }}/{{$tipo}}" title="Adicionar" class="btn-floating waves-effect waves-light red"><i class="material-icons">add</i></a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center">Nenhum fornecedor encontrado.</td>
                            </tr>
                            <tr>
                                <td colspan="5"><a href="{{ Route::getCurrentRoute()->getPrefix() }}/fornecedor_conta" class="waves-effect waves-light btn blue iframe">Adicionar fornecedor</a></td>
                            </tr>
                        @endforelse
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                @if(isset($clientes))
                    <?php echo $clientes->render(); ?>
                @endif
                @if(isset($fornecedores))
                    <?php echo $fornecedores->render(); ?>
                @endif
            </div>
        </div>
    </div>

@endsection


