@extends('app')
@section('title', 'Listar Fornecedores')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Fornecedores {{ $status == 1 ? 'Ativos' : 'Inativos' }}</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12']) !!}
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons right">search</i>
                    </button>
                    <div class="input-field col m2">
                        {!! Form::text('q', (isset($texto['q']) ? $texto['q']:''), ['class'=>'validate', 'placeholder'=>'Buscar Fornecedor']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Razão Social</th>
                        <th>Nome</th>
                        <th>CNPJ</th>
                        <th>Cidade</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($fornecedor as $forn)
                        <tr>
                            <td>{{ $forn->razaosocial }}</td>
                            <td>{{ $forn->nome }}</td>
                            <td>{{ $forn->cnpj }}</td>
                            <td>{{ $forn->cidade }}</td>
                            <td>{{ $forn->telefone_responsavel }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/fornecedor_view/{{ base64_encode($forn->id) }}" title="Visualizar fornecedor" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <li><a href="/administrativo/fornecedor_edita/{{ base64_encode($forn->id) }}" title="Editar cliente" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @if($forn->statu_id == 1)
                                            <li><a  title="Inativar cliente" class="btn-floating red" href="/fornecedor_inativa/{{ base64_encode($forn->id) }}"><i class="material-icons">not_interested</i></a></li>
                                        @else
                                            <li><a title="Ativar cliente" class="btn-floating green" href="/fornecedor_ativa/{{ base64_encode($forn->id) }}"><i class="material-icons">done</i></a></li>
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum fornecedor encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $fornecedor->render(); ?>
            </div>
        </div>
    </div>
@endsection