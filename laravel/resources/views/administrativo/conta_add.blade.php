@extends('app')
@section('title', 'Cadastrar Conta')
@section('content')
    @if(isset($conta_edit))
        <script type="text/javascript" src="/js/validador/conta_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/conta.js"></script>
    @endif

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });

    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Conta à Receber</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row padding">
                @if(Session::has('id'))
                    {!! Form::hidden('cliente_id', Session::get('id')->id , ['id' => 'cliente_id']) !!}
                    <div class="col m6" id="mostra">
                        <h5>Cliente</h5>
                        <p>Razão Social: {{ Session::get('id')->razaosocial }}</p>
                        <p>CNPJ: {{ Session::get('id')->cnpj }}</p>
                    </div>
                @endif
                <div class="input-field col s12 m6">
                    <a href="/administrativo/buscar/1/{{ $tipo }}" class="iframe btn btn-md waves-effect waves-light blue-grey"><i class="material-icons right">search</i>Buscar Cliente</a>
                </div>
            </div>
            <div class="row">
                {!! Form::hidden('tipo', $tipo, ['id' => 'tipo']) !!}
                @if(isset($conta_edit))
                    {!! Form::hidden('id', (isset($conta_edit['id']) ? $conta_edit['id']:''), ['id' => 'id']) !!}
                    {!! Form::hidden('statu_id', (isset($conta_edit['statu_id']) ? $conta_edit['statu_id']:'')) !!}
                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($conta_edit['nome']) ? $conta_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', 'Nome: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    <select name="portador" id="portador">
                        <option value="Netseg" {{ isset($conta_edit['portador']) ? $conta_edit['portador'] == 'Netseg' ? 'selected' : '' : '' }} >Netseg</option>
                        <option value="Egydio" {{ isset($conta_edit['portador']) ? $conta_edit['portador'] == 'Egydio' ? 'selected' : '' : '' }}>Egydio</option>
                        <option value="Medicina" {{ isset($conta_edit['portador']) ? $conta_edit['portador'] == 'Medicina' ? 'selected' : '' : '' }}>Medicina</option>
                    </select>
                    {!! Form::label('portador', 'Portador: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::label('vencimento', 'Data vencimento: ') !!}
                    {!! Form::text('vencimento', (isset($conta_edit['vencimento']) ? $conta_edit['vencimento']:''), ['class' => 'datepicker picker__input', 'id' => 'vencimento', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('baixa', 'Data da baixa: ') !!}
                    {!! Form::text('baixa', (isset($conta_edit['baixa']) ? $conta_edit['baixa']:''), ['class' => 'datepicker picker__input', 'id' => 'baixa', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s8">
                    {!! Form::text('nf', (isset($conta_edit['nf']) ? $conta_edit['nf']:''), ['class' => 'validate', 'id' => 'nf']) !!}
                    {!! Form::label('nf', 'Núm. Nota Fiscal: ') !!}
                </div>
                <div class="input-field col m2 s4">
                    {!! Form::text('valor', (isset($conta_edit['valor']) ? $conta_edit['valor']:''), ['class' => 'validate', 'id' => 'valor']) !!}
                    {!! Form::label('valor', 'Valor: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m3 s6">

                    <input name="tipov" type="radio" id="acrescimo" value="1" {{ isset($conta_edit['acrescimo']) ? $conta_edit['acrescimo'] ? 'checked' : '' : ''}} />
                    {!! Form::label('acrescimo', 'Acrescimo') !!}


                    <input name="tipov" type="radio" id="desconto" value="2" {{ isset($conta_edit['desconto']) ? $conta_edit['desconto'] ? 'checked' : '' : '' }} />
                    {!! Form::label('desconto', 'Desconto') !!}

                </div>
                <div class="input-field col m3 s6">
                    {!! Form::text('valor2', (isset($conta_edit['acrescimo']) ? $conta_edit['acrescimo'] : isset($conta_edit['desconto']) ? $conta_edit['desconto'] : ''), ['class' => 'validate', 'id' => 'valor2']) !!}

                </div>
            </div>
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('observacao',(isset($conta_edit['observacao']) ? $conta_edit['observacao']:''), ['class' => 'validate materialize-textarea', 'id' => 'observacao']) !!}
                    {!! Form::label('observacao', 'Observação: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('statu_id', $status, isset($conta_edit['statu_id']) ? $conta_edit['statu_id'] : null, ['id' => 'status']) !!}
                    {!! Form::label('status', 'Status: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('centrocusto_id', $centrocustos, isset($conta_edit['centrocusto_id']) ? $conta_edit['centrocusto_id'] : null, ['id' => 'centrocusto']) !!}
                    {!! Form::label('centrocusto', 'Centro de Custo: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('formapagamento_id', $formapagamento, isset($conta_edit['formapagamento_id']) ? $conta_edit['formapagamento_id'] : null, ['id' => 'formapagamento']) !!}
                    {!! Form::label('formapagamento', 'Forma de Pagamento: ') !!}
                </div>

                <div class="input-field col m6 s12">
                    {!! Form::text('num_boleto', (isset($conta_edit['num_boleto']) ? $conta_edit['num_boleto']:''), ['class' => 'validate', 'id' => 'num_boleto']) !!}
                    {!! Form::label('num_boleto', 'Núm. Boleto: ') !!}
                </div>
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($conta_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($conta_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection



