@extends('iframe')
@section('title', 'Adicionar Serviço')
@section('content')

    <script type="text/javascript" src="/js/validador/servico_orcamento.js"></script>
    <script type="text/javascript">
        function buscavalor($id){
            $.ajax({
                type:"GET",
                url: '/servico_valor/'+$id,
                cache:false,
                success: function(data) {
                    console.log(data);
                    if(data.valor){
                        $('#valor').val(data.valor);
                    }
                }
            });
        }
    </script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Adicionar Serviço </p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}
            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('servico_id', $servicos, isset($servico_edit['servico_id']) ? $servico_edit['servico_id'] : null, ['id' => 'servico', 'onchange' => 'buscavalor(this.value)']) !!}
                    {!! Form::label('servico', 'Serviço: ') !!}
                </div>

                <div class="input-field col m6 s12">
                    {!! Form::text('valor', (isset($servico_edit['valor']) ? $servico_edit['valor']:''), ['class' => 'validate', 'id' => 'valor']) !!}
                    {!! Form::label('valor', 'Valor: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('descricao',(isset($servico_edit['descricao']) ? $servico_edit['descricao']:''), ['class' => 'validate materialize-textarea', 'id' => 'descricao']) !!}
                    {!! Form::label('descricao', 'Descrição: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('cargahoraria', (isset($servico_edit['cargahoraria']) ? $servico_edit['cargahoraria']:''), ['class' => 'validate', 'id' => 'cargahoraria']) !!}
                    {!! Form::label('cargahoraria', 'Carga horária: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('participantes', (isset($servico_edit['participantes']) ? $servico_edit['participantes']:''), ['class' => 'validate', 'id' => 'participantes']) !!}
                    {!! Form::label('participantes', 'Número de Participantes: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('periodo', (isset($servico_edit['periodo']) ? $servico_edit['periodo']:''), ['class' => 'validate', 'id' => 'periodo']) !!}
                    {!! Form::label('periodo', 'Período: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('horario', (isset($servico_edit['horario']) ? $servico_edit['horario']:''), ['class' => 'validate', 'id' => 'horario']) !!}
                    {!! Form::label('horario', 'Horário: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('tecnico_id', $tecnicos, isset($servico_edit['tecnico_id']) ? $servico_edit['tecnico_id'] : null, ['id' => 'tecnico']) !!}
                    {!! Form::label('tecnico', 'Técnico: ') !!}
                </div>
                <div class="input-field col s12 m6 right-align">
                    <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($servico_edit) ? 'Salvar' : 'Cadastrar'  }}
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div id="modal1" class="modal"></div>
@endsection


