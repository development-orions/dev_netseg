@extends('iframe')
@section('title', 'Enviar orçamento por email')
@section('content')
    <script type="text/javascript" src="/js/validador/orcamento_email.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Enviar orçamento por email</p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}
            {!! Form::hidden('id', (isset($id) ? $id :''), ['id' => 'id']) !!}

            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('destinatarios', old('destinatarios'), ['class' => 'validate materialize-textarea', 'id' => 'destinatarios']) !!}
                    {!! Form::label('destinatarios', 'Destinatários: ') !!}
                </div>
                <p><small>Obs.: Digite os emails separados por ponto e vírgula ( ; ) <br> Ex.: contato@orions.com.br;dev@orions.com.br</small></p>
            </div>
            <div class="row right-align">
                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">Enviar
                    <i class="material-icons right">send</i>
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection