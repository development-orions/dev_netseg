@extends('iframe')
@section('title', 'Converter Orçamento em Venda')
@section('content')
    <script type="text/javascript" src="/js/validador/orcamento_converter.js"></script>
    <div class="container">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Converter orçamento em venda</p>
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <p><span class="green-text text-darken-3">Gerado em:</span> {{ $orcamento->created_at }}</p>
                <p><span class="green-text text-darken-3">Validade:</span> {{ $orcamento->validade }}</p>
                <p><span class="green-text text-darken-3">Emitido por:</span> {{ $orcamento->users['nome'] }}</p>
            </div>
            <div class="col s6">
                <p><span class="green-text text-darken-3">Valor:</span> {{ $orcamento->valor }}</p>
                <p><span class="green-text text-darken-3">Forma de Pagamento:</span> {{ $orcamento->formapagamentos['nome'] }}</p>
                <p><span class="green-text text-darken-3">Nota Fiscal:</span> {{ $orcamento->temnota == 0 ? 'não' : 'sim' }}</p>
            </div>

            <div class="col s12">
                <hr>
                <p><span class="green-text text-darken-3">Cliente:</span> {{ $orcamento->clientes['nome'] }}</p>
                <p><span class="green-text text-darken-3">CNPJ:</span> {{ $orcamento->clientes['cnpj'] }}</p>
                <hr>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}

            {!! Form::hidden('id', (isset($orcamento->id) ? base64_encode($orcamento->id) :''), ['id' => 'id']) !!}

            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">

            <div class="row">
                <div class="input-field col s12 m12">
                    <div class="input-field col s6">
                        {!! Form::label('fechamento', 'Data fechamento: ') !!}
                        {!! Form::text('fechamento', (isset($orcamento_edit['fechamento']) ? $orcamento_edit['fechamento']:''), ['class' => 'datepicker picker__input', 'id' => 'fechamento', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                    </div>
                    <div class="input-field col s6">
                        {!! Form::label('entrega', 'Data programada da entrega: ') !!}
                        {!! Form::text('entrega', (isset($orcamento_edit['entrega']) ? $orcamento_edit['entrega']:''), ['class' => 'datepicker picker__input', 'id' => 'entrega', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s6">
                    {!! Form::text('valoradministrativo', old('valoradministrativo'), ['class' => 'validate', 'id' => 'nf']) !!}
                    {!! Form::label('valoradministrativo', 'Valor Administrativo: ') !!}
                </div>
            </div>
            <input type="hidden" id="tipo" name="tipo" value="{{ $tipo }}">
            @if($tipo != 'medicina')
                <div class="row">
                    <?php
                    for($x=0; $x < 3; $x++){
                    if($x == 0){
                    ?>
                    <div class="input-field col m6 s12">
                        {!! Form::select('tecnico_id['.$x.']', ['' => 'Escolha o técnico'] + $tecnicos, '', ['id' => 'tecnico'.$x]) !!}
                        {!! Form::label('tecnico', 'Técnico/Instrutor: ') !!}
                    </div>
                    <?php }else{ ?>
                    <div class="input-field col m6 s12">
                        {!! Form::select('tecnico_id['.$x.']', ['' => 'Escolha o técnico'] + $tecnicos, '', ['id' => 'tecnico'.$x]) !!}
                        {!! Form::label('tecnico', 'Técnico/Instrutor: ') !!}
                    </div>

                    <?php } ?>
                    <div class="input-field col m3 s6">
                        {!! Form::text('valor_tecnico['.$x.']', '', ['class' => 'validate', 'id' => 'valor_tecnico'.$x]) !!}
                        {!! Form::label('valor_tecnico', 'Valor do técnico: ') !!}
                    </div>
                    <?php } ?>
                </div>
            @endif
            @if($tipo == 'medicina' || $tipo =='ambos')
                <div class="row">
                    <?php
                    for($x=0; $x < 3; $x++){
                    if($x == 0){
                    ?>
                    <div class="input-field col m6 s12">
                        {!! Form::select('medico_id['.$x.']', $medicos, '', ['id' => 'medico'.$x]) !!}
                        {!! Form::label('medico', 'Médico: ') !!}
                    </div>
                    <?php }else{ ?>
                    <div class="input-field col m6 s12">
                        {!! Form::select('medico_id['.$x.']', ['' => 'Escolha o médico'] + $medicos, '', ['id' => 'medico'.$x]) !!}
                        {!! Form::label('medico', 'Médico: ') !!}
                    </div>

                    <?php } ?>
                    <div class="input-field col m3 s6">
                        {!! Form::text('valor_medico['.$x.']', '', ['class' => 'validate', 'id' => 'valor_medico'.$x]) !!}
                        {!! Form::label('valor_medico', 'Valor do médico: ') !!}
                    </div>
                    <?php } ?>
                </div>

            @endif
            @if($orcamento->temnota == 1)
                <div class="row">
                    <div class="input-field col s6">
                        {!! Form::text('nf', (isset($orcamento_edit['nf']) ? $orcamento_edit['nf']:''), ['class' => 'validate', 'id' => 'nf']) !!}
                        {!! Form::label('nf', 'Núm. Nota Fiscal: ') !!}
                    </div>
                </div>
            @endif
            @if($orcamento->formapagamento_id == 1)
                <div class="row">
                    <div class="input-field col s6">
                        {!! Form::text('num_boleto', (isset($orcamento_edit['num_boleto']) ? $orcamento_edit['num_boleto']:''), ['class' => 'validate', 'id' => 'num_boleto']) !!}
                        {!! Form::label('num_boleto', 'Núm. Boleto: ') !!}
                    </div>
                </div>
            @endif
            <div class="row right-align">
                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">Converter
                    <i class="material-icons right">send</i>
                </button>
            </div>

            {!! Form::close() !!}
        </div>

    </div>
    <div id="modal1" class="modal"></div>
@endsection