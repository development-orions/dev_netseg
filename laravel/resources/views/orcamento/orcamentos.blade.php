@extends('app')
@section('title', 'Listar Orçamentos')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"900", height:"500"});
            parent.$.fn.colorbox.close();
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Orçamentos Não Convertidos em Venda</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento_lista/1" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    <i class="material-icons right">search</i>
                </button>

                <div class="input-field col m2 s3">

                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::text('venda', (isset($data['venda']) ? $data['venda']:''), ['class'=>'validate', 'placeholder'=>'Por Número']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::select('statu_id', ['' => 'Todos status'] + $status, isset($data['statu_id']) ? $data['statu_id'] : null, ['id' => 'status']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::text('q', (isset($data['q']) ? $data['q']:''), ['class'=>'validate', 'placeholder'=>'Por Cliente']) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Número</th>
                        <th>Cliente</th>
                        <th>Data</th>
                        <th>Útima modificação</th>
                        <th>Status</th>
                        <th>Tipo</th>
                        <th>Total</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>

                    @forelse($vendas as $venda)


                        <tr>
                            <td>{{ $venda->id }}</td>
                            <td>{{ $venda->razaosocial }}</td>
                            <td>{{ $venda->created_at->format('d/m/Y') }}</td>
                            <td>{{ $venda->updated_at != $venda->created_at ? $venda->updated_at->format('d/m/Y') : '-' }}</td>
                            <td>{{ $venda->status->nome }}</td>
                            <td>{{ $venda->tipo }}</td>
                            <td>{{ $venda->valor }}</td>


                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        @if($venda->statu_id == 2)
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento_ativa/{{ base64_encode($venda->id) }}" title="Ativar Orçamento" class="btn-floating green"><i class="material-icons">check</i></a></li>
                                        @else
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento_inativa/{{ base64_encode($venda->id) }}" title="Inativar Orçamento" class="btn-floating red"><i class="material-icons">block</i></a></li>

                                        @endif
                                        <li><a href="/visualizar/orcamento_view/{{ base64_encode($venda->id) }}" title="Visualizar Orçamento" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        @if(Auth::user()->role_id != 1 && $venda->statu_id != 2)
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento_edita/{{ base64_encode($venda->id) }}" title="Editar Orçamento" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                        @endif
                                        @if($venda->statu_id == 6 && $venda->statu_id != 2)
                                            <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento_aprova/{{ base64_encode($venda->id) }}" title="Aprovar Orçamento" class="btn-floating green"><i class="material-icons">done</i></a></li>
                                        @endif

                                        <?php

                                        if($venda->statu_id != 2){ ?>
                                        <li><a href="/pdf/{{ base64_encode($venda->id) }}" target="_blank" title="Imprimir Orçamento" class="btn-floating orange"><i class="material-icons">print</i></a></li>
                                        <?php } ?>
                                        @if(Auth::user()->role_id != 1 && $venda->statu_id == 7)
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/converter_orcamento/{{ base64_encode($venda->id) }}" title="Converter orçamento em venda" class="btn-floating purple iframe"><i class="material-icons">loop</i></a></li>
                                        @endif
                                            @if($venda->statu_id != 2)
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/email_orcamento/{{ base64_encode($venda->id) }}" title="Enviar Orçamento por email" class="btn-floating deep-purple accent-1 iframe"><i class="material-icons">email</i></a></li>

                                           @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">Nenhum Orçamento encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $vendas->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection