@extends('app')
@section('title', 'Orçamento Finalizado')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"600", height:"500"});
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Orçamento finalizado com sucesso</p>
            </div>
        </div>
        <div class="row padding">
            <div class="col s12">
                <h5>Orçamento {{ $orcamento->id }}</h5>
                <p>Data: {{ $orcamento->created_at }} <br>
                Última modificação: {{ $orcamento->updated_at }} <br>
                Data de validade: {{ $orcamento->validade }}<br>
                Realizado por: {{ $orcamento->users['nome'] }}<br>
                    Cliente: {{ $orcamento->clientes['razaosocial'] }}

                </p>
            </div>
        </div>
        <div class="row padding">
            <div class="col s12 btn-link">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento_lista/1" class="waves-effect waves-light btn-large teal darken-1">Ir para lista de orçamentos</a>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/orcamento" class="waves-effect waves-light btn-large green lighten-2">Novo orçamento</a>
                <a href="/pdf/{{ base64_encode($orcamento->id)}}" target="_blank" class="waves-effect waves-light btn-large orange">Imprimir</a>
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/email_orcamento/{{ base64_encode($orcamento->id) }}" class="waves-effect waves-light btn-large light-blue iframe">Enviar por email</a>


            </div>
        </div>
    </div>

    <div id="modal1" class="modal"></div>
@endsection