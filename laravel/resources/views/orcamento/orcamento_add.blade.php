@extends('app')
@section('title', 'Cadastrar Orçamento')
@section('content')
    @if(isset($orcamento_edit))
        <script type="text/javascript" src="/js/validador/orcamento_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/orcamento.js"></script>
    @endif

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {

            $('select').material_select();
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();

            $('#resultado').hide();
        });

        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });


        /* Máscaras ER */
        function mascara(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }
        function execmascara(){
            v_obj.value=v_fun(v_obj.value)
        }
        function mdata(v){
            v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
            v=v.replace(/(\d)(\d{6})$/,"$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
            v=v.replace(/(\d)(\d{4})$/,"$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
            return v;
        }
        function id( el ){
            return document.getElementById( el );
        }


        function filtro(valor){
            valor = parseInt(valor, 10);
            var campos_max = valor;
            $(".parcelas").remove();
            $('#parcelas').hide('slow');
            for(var x = 1; x <= campos_max; x++) {

                $('#telefones').append('<div class="parcelas">\
                    <div class="input-field col s12 m3 offset-m2">\
                   <p>'+x+' Parcela</p>\
                    <input type="hidden" value="'+x+'" name="data[Parcela][' + x + '][numero]">\
                    </div>\
                    <div class="input-field col s12 m6">\
                    <input type="text" placeholder="dd-mm-aaaa" maxlength="10" id="data'+x+'" name="data[Parcela][' + x + '][data]">\
                    </div>\
                    </div>\
                    ');

                id('data'+x).onkeyup = function(){
                    mascara( this, mdata );
                }
            }


        }

        function seleciona(id, razaosocial, cnpj){
            $('#res').empty();
            $('#resultadop').hide();
            $('#cliente_id').val(id);
            $('#busca').val(razaosocial).click(function(){
                $(this).val('').unbind('click');
            });
            $('#mostra').html('<h5>Cliente</h5><p>Razão Social: '+ razaosocial +'</p><p>CNPJ: '+ cnpj +'</p>');
        }
        /// busca por cliente
        function pesquisa(event){
            valor = $('#busca').val();
            if (event.keyCode == 8 || valor == '' ){
                $('#res').empty();
                $('#resultado').hide();
                return false
            }

            $.ajax({
                url: '/buscarcliente/'+valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso){
                    $('#res').empty();
                    $('#resultado').show();
                    $.each(sucesso, function(i, item){
                        $('#res').append('<li><a href="#" onClick="seleciona(\''+item.id+'\',\''+item.razaosocial+'\', \''+item.cnpj+'\')" title="'+item.razaosocial+' - '+item.cnpj+'"><span>'+item.nome+' - '+item.razaosocial+' - '+item.cnpj+'</span></a></li>');
                    });
                    if(jQuery.isEmptyObject(sucesso)){
                        $('#resultado').hide();
                    }
                },
                error: function(){
                    $('#res').empty();
                    $('#resultado').hide();
                }
            });
        }

    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">{{ isset($orcamento_edit) ? 'Editar' : 'Cadastrar ' }} Orçamento</p>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="input-field col s12 m6">
                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/adicionar_servico" class="iframe btn btn-md waves-effect waves-light blue-grey">Adicionar Serviço <i class="fa fa-plus-square"></i></a>
                </div>
            </div>



            <div class="row padding">
                @if(Session::has('servico'))
                    <?php

                    $key = array_keys(Session::get('servico'));
                    $id = 0;
                    ?>
                    <input type="hidden" id="servico" value="true">
                    @foreach(Session::get('servico') as $servicos)

                        <div class="col m12 servico">
                            <h5>Serviço - {{ $servicos['servico_id']->nome }}

                                @if(isset($orcamento_edit))
                                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/remover_servico_edit/{{ $orcamento_edit['id'] }}/{{ $key[$id++] }}" class="btn-floating waves-effect waves-light orange right"><i class="fa fa-remove"></i></a>
                                @else
                                    <a href="{{ Route::getCurrentRoute()->getPrefix() }}/remover_servico/{{ $key[$id++] }}" class="btn-floating waves-effect waves-light orange right"><i class="fa fa-remove"></i></a>
                                @endif



                                 <!--<a class="btn-floating waves-effect waves-light blue right"><i class="fa fa-edit"></i></a>-->
                            </h5>

                            @if($servicos['servico_id']->id == 3)
                                <p><span class="green-text text-darken-2">Serviços inclusos:</span></p>
                                <ul>@if(isset($servicos['servicos_combo']))
                                    @foreach($servicos['servicos_combo'] as $sv_combo)
                                        <li>{{ $sv_combo }}</li>
                                    @endforeach @endif
                                </ul>
                            @endif

                            <div class="col m6">
                                <p><span class="green-text text-darken-2 col m2">Descrição:</span> <span class="col m10">{!! $servicos['descricao']  !!}</span> </p>
                            </div>
                            <div class="col m5 offset-l1">
                                <p><span class="green-text text-darken-2">Valor:</span> {{ $servicos['valor'] }}</p>
                                @if($servicos['servico_id']->id == 1 || $servicos['servico_id']->id == 3)
                                    <p><span class="green-text text-darken-2">Carga horária:</span> {{ $servicos['cargahoraria'] }}</p>
                                    <p><span class="green-text text-darken-2">Núm. Participantes:</span> {{ $servicos['participantes'] }}</p>
                                    <p><span class="green-text text-darken-2">Período:</span> {{ $servicos['periodo'] }}  <span class="green-text text-darken-2">Horário:</span> {{ $servicos['horario'] }}</p>
                                @endif
                            </div>
                        </div>

                    @endforeach
                @else
                    <input type="hidden" id="servico" value="false">
                @endif
            </div>
            <div class="row padding">
                <div class="col s12">
                    <form class="col s12">
                        <div class="input-field col s12 m6">
                            <input type="text" name="busca" id="busca" onKeyUp="pesquisa(event)" autocomplete="off">
                            <label for="busca">Novo Cliente</label>
                        </div>
                    </form>
                </div>
                <div class="col s12">
                    <div id="resultado"><ul class="list_json" id="res"></ul></div>
                </div>
            </div>
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" name="orcamento" value="1">
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row padding">

                    {!! Form::hidden('cliente_id', isset($orcamento_edit['cliente_id']) ? $orcamento_edit['cliente_id']:'' , ['id' => 'cliente_id']) !!}
                <div class="col m6" id="mostra">
                    <h5>Cliente</h5>
                    <p>Razão Social: {{ (isset($orcamento_edit) ? $orcamento_edit->clientes['razaosocial'] : '') }} </p>
                    <p>CNPJ: {{ (isset($orcamento_edit) ? $orcamento_edit->clientes['cnpj']: '') }} </p>
                </div>

            </div>
            <div class="row">
                @if(isset($orcamento_edit))
                    {!! Form::hidden('id', (isset($orcamento_edit['id']) ? $orcamento_edit['id']:''), ['id' => 'id']) !!}

                @endif
            </div>


            <div class="row">
                <div class="input-field col s12 m6">
                    {!! Form::select('formapagamento_id', $formapagamento, isset($orcamento_edit['formapagamento_id']) ? $orcamento_edit['formapagamento_id'] : null, ['id' => 'formapagamento']) !!}
                    {!! Form::label('formapagamento', 'Forma de Pagamento: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    <input type="checkbox" class="filled-in" id="temnota" name="temnota" {{ isset($orcamento_edit['temnota']) && $orcamento_edit['temnota'] == 1 ? 'checked' : '' }}  />
                    <label for="temnota">Nota Fiscal</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('parcela', (isset($orcamento_edit['parcela']) ? $orcamento_edit['parcela']:''), ['class' => 'validate', 'id' => 'parcela']) !!}
                    {!! Form::label('parcela', '* Condições de pagamento: ') !!}
                </div>

            </div>



            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($orcamento_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($orcamento_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection



