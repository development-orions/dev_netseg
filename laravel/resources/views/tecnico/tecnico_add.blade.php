@extends('app')
@section('title', 'Cadastrar Tecnico')
@section('content')
	@if(isset($tecnico_edit))
		<script type="text/javascript" src="/js/validador/tecnico_edit.js"></script>
	@else
		<script type="text/javascript" src="/js/validador/tecnico.js"></script>
	@endif

	<script type="text/javascript" src="/js/core2.js"></script>
	<script type="text/javascript"  >
		function getEndereco(cep) {
			// Se o campo CEP não estiver vazio
			if($.trim(cep) != ""){
				$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
					if(resultadoCEP["resultado"]){
						$("#rua").val(unescape(resultadoCEP["logradouro"]));
						$("#bairro").val(unescape(resultadoCEP["bairro"]));
						$("#cidade").val(unescape(resultadoCEP["cidade"]));
						$("#estado").val(unescape(resultadoCEP["uf"]));
					}
				});
			}
		}
		$(document).ready(function() {
			$("#cep").blur(function(){
				getEndereco($("#cep").val());
			});
		});
	</script><!-- === chamada de endereço por CEP === -->
	<div class="container">
		<div class="row  bg">
			<div class="col s12 bgverde">
				<p class="bold font20 corbranca">Cadastro de Tecnicos</p>
			</div>
		</div>
		<div class="row">

			{!! Form::open(['class' => 'col s12']) !!}
			<input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
			<div class="row">

				@if(isset($tecnico_edit))
					{!! Form::hidden('id', (isset($tecnico_edit['id']) ? $tecnico_edit['id']:''), ['id' => 'id']) !!}
					{!! Form::hidden('statu_id', (isset($tecnico_edit['statu_id']) ? $tecnico_edit['statu_id']:'')) !!}
				@endif

				<div class="input-field col m6 s12">
					{!! Form::text('nome', (isset($tecnico_edit['nome']) ? $tecnico_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
					{!! Form::label('nome', '* Nome: ') !!}
				</div>
				<div class="input-field col m6 s12">
					{!! Form::text('num_registro', (isset($tecnico_edit['num_registro']) ? $tecnico_edit['num_registro']:''), ['class' => 'validate', 'id' => 'num_registro']) !!}
					{!! Form::label('num_registro', '* Número de Registro: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('cep', (isset($tecnico_edit['cep']) ? $tecnico_edit['cep']:''), ['class' => 'validate', 'id' => 'cep', 'maxlength' => '10']) !!}
					{!! Form::label('cep', '* CEP: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('rua', (isset($tecnico_edit['rua']) ? $tecnico_edit['rua']:''), ['class' => 'validate', 'id' => 'rua']) !!}
					{!! Form::label('rua', '* Rua: ') !!}
				</div>
				<div class="input-field col m2 s6">
					{!! Form::text('numero', (isset($tecnico_edit['numero']) ? $tecnico_edit['numero']:''), ['class' => 'validate', 'id' => 'numero']) !!}
					{!! Form::label('numero', '* Número: ') !!}
				</div>
				<div class="input-field col m4 s6">
					{!! Form::text('complemento', (isset($tecnico_edit['complemento']) ? $tecnico_edit['complemento']:'')) !!}
					{!! Form::label('complemento', 'Complemento: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col m6 s12">
					{!! Form::text('bairro',(isset($tecnico_edit['bairro']) ? $tecnico_edit['bairro']:''), ['class' => 'validate', 'id' => 'bairro']) !!}
					{!! Form::label('bairro', '* Bairro: ') !!}
				</div>
				<div class="input-field col m4 s8">
					{!! Form::text('cidade', (isset($tecnico_edit['cidade']) ? $tecnico_edit['cidade']:''), ['class' => 'validate', 'id' => 'cidade']) !!}
					{!! Form::label('cidade', '* Cidade: ') !!}
				</div>
				<div class="input-field col m2 s4">
					{!! Form::text('estado', (isset($tecnico_edit['estado']) ? $tecnico_edit['estado']:''), ['class' => 'validate', 'id' => 'estado']) !!}
					{!! Form::label('estado', '* Estado: ') !!}
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
					{!! Form::textarea('descricao', (isset($tecnico_edit['descricao']) ? $tecnico_edit['descricao']:''), ['size' => '30x5', 'class' => 'materialize-textarea validate', 'id' => 'descricao']) !!}
					{!! Form::label('descricao', 'Descrição: ') !!}
				</div>
				<div class="input-field col m6 s6">
					{!! Form::email('email', (isset($tecnico_edit['email']) ? $tecnico_edit['email']:''), ['class' => 'validate', 'id' => 'email']) !!}
					{!! Form::label('email', '* Email: ', ['data-error' => 'Email inválido']) !!}
				</div>
				<div class="input-field col m6 s6">
					{!! Form::text('telefone', (isset($tecnico_edit['telefone']) ? $tecnico_edit['telefone']:''), ['class' => 'validate', 'id' => 'telefone', 'maxlength' => '15']) !!}
					{!! Form::label('telefone', '* Telefone: ') !!}
				</div>
			</div>
			<div class="row right-align">

				<button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($tecnico_edit) ? 'Salvar' : 'Cadastrar'  }}
					<i class="material-icons right">send</i>
				</button>
				@if(isset($tecnico_edit))
					<a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
				@endif
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div id="modal1" class="modal"></div>
@endsection
