@extends('app')
@section('title', 'Relatório de Vendas')
@section('content')
    <script type="text/javascript" src="/js/validador/relatorio_vendas.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de Vendas por {{ $tipo == 1 ? 'Médico' : 'Técnico/Instrutor'}}</p>
            </div>
        </div>
    </div>
    <div class="row buscar">
        {!! Form::open(['class' => 'col s12']) !!}
        <div class="row">
            <button class="btn waves-effect waves-light" type="submit" id="Envia">
                <i class="material-icons right">search</i>
            </button>
            @if($tipo == 2)
                <div class="input-field col s4 m3">
                    {!! Form::select('tecnico_id', $tecnicos, isset($data['tecnico_id']) ? $data['tecnico_id'] : null, ['id' => 'tecnico']) !!}
                </div>
            @else
                <div class="input-field col s4 m3">
                    {!! Form::select('medico_id', $medicos, isset($data['medico_id']) ? $data['medico_id'] : null, ['id' => 'medico']) !!}
                </div>
            @endif
            <div class="input-field col m2 s4">
                {!! Form::label('fim', 'Data final: ') !!}
                {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
            </div>
            <div class="input-field col m2 s4">
                {!! Form::label('inicio', 'Data inicial: ') !!}
                {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div id="modal1" class="modal"></div>
@endsection