@extends('app')
@section('title', 'Relatório de Pagamento de Técnico')
@section('content')
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de Pagamento de Técnico</p>
            </div>
        </div>
    </div>
    <div class="row buscar">
        {!! Form::open(['class' => 'col s12']) !!}
        <div class="row left">
            <button class="btn waves-effect waves-light" type="submit">
                <i class="material-icons right">search</i>
            </button>
            <div class="input-field col s9 m9">
                {!! Form::select('tecnico_id', $tecnicos, isset($data['tecnico_id']) ? $data['tecnico_id'] : null, ['id' => 'tecnico']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div id="modal1" class="modal"></div>
@endsection