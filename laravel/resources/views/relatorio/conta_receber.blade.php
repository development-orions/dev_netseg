@extends('app')
@section('title', 'Relatório de Conta à Receber')
@section('content')
    <script type="text/javascript" src="/js/validador/contas_relatorio.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Relatório de Contas à Receber</p>
            </div>
        </div>
    </div>
    <div class="row buscar">
        {!! Form::open(['class' => 'col s12']) !!}
        <div class="row">

            <a href="{{ Route::getCurrentRoute()->getPrefix() }}/relatorio_conta/2" class="waves-effect waves-light btn">Limpar</a>
            <button class="btn waves-effect waves-light" type="submit" id="Envia">
                <i class="material-icons right">search</i>
            </button>
            <div class="input-field col m2">
                {!! Form::text('q', (isset($data['q']) ? $data['q']:''), ['class'=>'validate', 'placeholder'=>'Cliente ']) !!}
            </div>
            <div class="input-field col m2 s3">
                {!! Form::label('fim', 'Data final: ') !!}
                {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
            </div>
            <div class="input-field col m2 s3">
                {!! Form::label('inicio', 'Data inicial: ') !!}
                {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
            </div>
            <div class="input-field col s3 m2">
                {!! Form::select('statu_id', ['' => 'Todos status'] + $status, isset($data['statu_id']) ? $data['statu_id'] : null, ['id' => 'status']) !!}
            </div>
            <div class="input-field col s3 m2">
                {!! Form::select('centrocusto_id', ['' => 'Todos Centro de Custo'] + $centrocusto, isset($data['centrocusto_id']) ? $data['centrocusto_id'] : null, ['id' => 'centrocusto']) !!}
            </div>
            <input type="hidden" id="conta" name="conta" value="">
        </div>
        {!! Form::close() !!}
    </div>
    @if(isset($mensagem))
        <div class="row">
            <div class="col m11 offset-m1">
                <p>{{ $mensagem }}</p>
            </div>
        </div>
    @endif
    <div id="modal1" class="modal"></div>
@endsection