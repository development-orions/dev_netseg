<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>RNC - Relatório de Não Conformidade</title>
    <link rel="stylesheet" href="pdfcss/style_naoconformidade.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }}</div>
                <div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        <tr>
            <td class="left-align">
                <h2>RNC - Relatório de Não Conformidade</h2>
            </td>
        </tr>
        </tbody>
    </table>
</header>
    <div id="details" class="clearfix">
        <table class="topo topo2">
            <tbody>
            <tr>
                <td class="client2 left-align">
                    @if(isset($mes))
                        <h4>CLIENTE: <span class="to"> <b>{{ isset(Session::get('cliente_cronograma')->nome) ? Session::get('cliente_cronograma')->nome : '' }}</b></span></h4>
                    @else
                        <h4>TODAS AS NÃO CONFORMIDADES</h4>
                    @endif
                </td>
                @if(isset($mes))
                    <td class="client3 right-align">
                        <h4 class="name">DATA DE ATIVIDADE: <span class="to"><b>{{ ($mes > 9 ? '' : '0').$mes.'/'.$ano }}</b></span></h4>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
    </div>
    <div><h3>Lista de Não Conformidades</h3></div>

    <table id="geral" border="0"  style="table-layout: fixed; width: 700px;" >
        <thead>
        <tr>
            <th class="no left-align">Número</th>
            @if(isset(Session::get('cliente_cronograma')->nome) ? Session::get('cliente_cronograma')->nome : '')
                <th class="no left-align">Cliente</th>
            @endif
            <th class="no left-align">Tecnico</th>
            <th class="no left-align">Atividade</th>
            @if(!isset($mes))
                <th class="no left-align">Data da atividade</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @forelse($naoconformidades as $naoconformidade)
            <tr>
                <td>{{ $naoconformidade->numero }}</td>
                @if(isset(Session::get('cliente_cronograma')->nome) ? Session::get('cliente_cronograma')->nome : '')
                    <td>{{ $naoconformidade->clientes->nome}}</td>
                @endif
                <td>{{ $naoconformidade->tecnicos->nome }}</td>
                <td>{{ $naoconformidade->atividades->nome }}</td>
                @if(!isset($mes))
                    <td>{{ ($naoconformidade->mes > 9 ? '' : '0').$naoconformidade->mes.'/'.$naoconformidade->ano }}</td>
                @endif
            </tr>
        @empty
            <tr>
                <td class="desc" colspan="5">Nenhuma Não Conformidade encontrada.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
<footer>

</footer>
</body>
</html>