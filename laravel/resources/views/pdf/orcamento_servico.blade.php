<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Orçamento - Serviço</title>
    <link rel="stylesheet" href="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/style.css" media="all" />
</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/logo.jpg" width="150">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }}, {{ $configuracoes->bairro }}, <br> {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }} - <a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        </tbody>
    </table>
</header>
<main>
    <div id="details" class="clearfix">
        <table class="topo">
            <tbody>
            <tr>
                <td id="client">
                    <h2 class="name">{{ $orcamento->clientes['nome'] }}</h2>
                    <div>CNPJ: {{ $orcamento->clientes['cnpj'] }}</div>
                    <div class="address">{{ $orcamento->clientes['rua'] }}, {{ $orcamento->clientes['numero'] }}, {{ $orcamento->clientes['bairro'] }}, {{ $orcamento->clientes['cidade'] }} - {{ $orcamento->clientes['estado'] }} <br> CEP: {{ $orcamento->clientes['cep'] }}</div>
                    <div>Responsável: {{ $orcamento->clientes['responsavel'] }}</div>
                    <div class="email"><a href="mailto:{{ $orcamento->clientes['email_responsavel'] }}">{{ $orcamento->clientes['email_responsavel'] }}</a></div>
                </td>
                <td id="invoice">
                    <h1>ORÇAMENTO {{ $orcamento->id }}</h1>
                    <div class="date">Data do orçamento: {{ $orcamento->created_at }}</div>
                    <div class="date">Validade: {{ $orcamento->validade }}</div>
                    <div class="date">Realizado por: {{ $orcamento->users['nome'] }}</div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <table border="0" style="table-layout: fixed; width:100%;" >
        <thead>
        <tr>
            <th class="no">#</th>
            <th class="desc">SERVIÇO</th>
            <?php if($orcamento['valor'] != 0){  ?>
                <th class="total">VALOR</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php $n = 1; ?>
        @foreach($orcamento->itens as $servico)
            <tr class="teste">
                <td class="no" rowspan="2">{{ $n }}</td>
                <td class="desc"><h3>{{ $servico->servicos['nome'] }}</h3></td>
                <?php if($orcamento['valor'] != 0){  ?>
                    <td class="total" rowspan="2">{{ $servico['valor'] }}</td>
                 <?php } ?>
            </tr>
            <tr class="teste">
                <td class="desc">{!! $servico['descricao']  !!} </td>
            </tr>
            <?php
            $n++;
            ?>
        @endforeach
        </tbody>
    </table>
    <?php if($orcamento['valor'] != 0){  ?>
    <div id="total">
        <div>Total:</div>
        <div class="notice">{{ $orcamento['valor'] }}</div>
    </div>
    <?php } ?>
    <div>
        @if(isset($orcamento['parcela']) && !empty($orcamento['parcela']))
            <table class="parcelas" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th>Condições de pagamento</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>{{ $orcamento['parcela'] }}</td>
                </tr>
                </tbody>
            </table>
        @else
            <table class="parcelas" border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th class="no">PARCELAS</th>
                    <th>DATA</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $orcamento->parcelas as $parcelas)
                    <tr>
                        <td class="no">0{{ $parcelas['numero'] }}</td>
                        <td>{{ $parcelas['data'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

    </div>
    <div id="notices">
        <div>PAGAMENTO:</div>
        <div class="notice">Forma de pagamento: {{ $orcamento->formapagamentos['nome'] }}</div>
        <div>Nota fiscal: {{ $orcamento->temnota == 0 ? 'não' : 'sim' }}</div>
    </div>
</main>

<div id="thanks">Netseg agradece a preferência.</div>
<footer>
    Orçamento gerado em um computador e é válido sem a assinatura e carimbo.
</footer>


</body>
</html>
