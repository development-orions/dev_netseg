<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Relatorio de exame por Médico</title>
    <link rel="stylesheet" href="pdfcss/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>(12) 2103-4490</div>
                {{--<div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>--}}
                <a href="mailto:medicina@netsegtrabalho.com">medicina@netsegtrabalho.com</a>
            </td>
        </tr>
        <tr>
            <td class="left-align" colspan="2">
                <h2>Relatorio de exame por Médico</h2>
                <h3>{{$medico->nome}}</h3>
                @if(!empty($data['inicio']) && !empty($data['fim']))
                    <h4>{{$data['inicio']}} à {{$data['fim']}}</h4>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
</header>

<div id="details" class="clearfix">
    <table class="bordered highlight" style="table-layout: fixed;" border="0">
        <thead>
        <tr>
            <th class="total">Paciente</th>
            <th class="total">Data do atendimento</th>
            <th class="total">Descrição</th>
        </tr>
        </thead>
        <tbody>

        @if(sizeof($atendimentos) != 0)
            @foreach($atendimentos as $atendimento)
            <tr>
                <td class="text-center unit">{{ $atendimento->nome }}</td>
                <td>{{ $atendimento->data }}</td>
                <td class="unit">{{ $atendimento->descricao }}</td>
            </tr>
        @endforeach


            @else
            <tr>
                <td colspan="3">Nenhum resultado encontrado</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

<footer>

</footer>
</body>
</html>