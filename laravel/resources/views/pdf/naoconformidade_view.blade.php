<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>RNC - Relatório de Não Conformidade</title>
    <link rel="stylesheet" href="pdfcss/style_naoconformidade.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }}</div>
                <div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        <tr>
            <td class="left-align">
                <h2>RNC - Relatório de Não Conformidade</h2>
            </td>
            <td class="right-align">
                <div><b>Código: {{ $naoconformidade->codigo }}</b></div>
                <div><b>Revisão: 0{{ $naoconformidade->revisao }}</b></div>
                <div><b>Vigência: {{ $naoconformidade->vigencia }}</b></div>
            </td>
        </tr>
        </tbody>
    </table>
</header>
<main>
    <div id="details" class="clearfix">
        <table class="topo">
            <tbody>
            <tr>
                <td class="left-align">
                    <div>ATIVIDADE: <b>{{ $atividade->id > 9 ? '':'0' }}{{ $atividade->id }}</b></div>
                    <div>DATA DA ATIVIDADE: <b>{{ $mes > 9 ? '':'0' }}{{ $mes }}/{{ $ano }}</b></div>
                </td>
                <td class="left-align">
                    <div>Não Conformidade {{ $naoconformidade->realpotencial == 1?'Real':'Potencial' }}  </div>
                    <h3>{{ $naoconformidade->materialsistema != 0 ? 'Material / Processo':'Sistema / Serviço' }}</h3>
                </td>
                <td class="right-align">
                    <h3>Nº: {{ $naoconformidade->digito > 9 ? '':'0' }}{{ $naoconformidade->numero }}</h3>
                    <div>Data: <b>{{ $naoconformidade->created_at->format('d/m/Y') }}</b></div>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="topo topo2">
            <tbody>
            <tr>
                <td class="client2 left-align">
                    <h4>CLIENTE: <span class="to"> <b>{{ $cliente->nome }}</b></span></h4>
                </td>
                <td class="client3 right-align">
                    <h4 class="name">SETOR INTERNO: <span class="to"><b>{{ $tecnico->nome }}</b></span></h4>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <table id="geral" border="0" style="table-layout: fixed; width: 700px;" >
        <thead>
        <tr>
            <th class="no left-align" colspan="3">1 - Ocorrência:</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="3" class="desc">{{ $naoconformidade->ocorrencia }}</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td>RESPONSÁVEL: {{ $naoconformidade->ocorrencia_responsavel }}</td>
            <td class="center-align">ASSINATURA: ___________________</td>
            <td>DATA: {{ isset($naoconformidade->ocorrencia_data) ? $naoconformidade->ocorrencia_data : '___/___/______'  }}</td>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th class="no left-align" colspan="3">2 - Ação Imediata:</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($naoconformidade->acaoimediata))
            <tr>
                <td colspan="3" class="desc">{{ $naoconformidade->acaoimediata }}</td>
            </tr>
        @else
            <tr>
                <td colspan="3" class="desc"></td>
            </tr>
            <tr>
                <td colspan="3" class="desc"></td>
            </tr>
        @endif
        </tbody>
        <tfoot>
        <tr>
            <td>RESPONSÁVEL: {{ $naoconformidade->acaoimediata_responsavel }}</td>
            <td class="center-align">ASSINATURA: ___________________</td>
            <td>DATA: {{ isset($naoconformidade->acaoimediata_data) ? $naoconformidade->acaoimediata_data : '___/___/______'  }}</td>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th class="no left-align" colspan="3">3 - Causa Provável da não conformidade: <span class="font12 right-align">{{ $naoconformidade->causaprovavel_op == 1?'Relatório improcedente':'Relatório procedente' }}</span></th>
        </tr>
        </thead>
        <tbody>
        @if(isset($naoconformidade->causaprovavel))
            <tr>
                <td colspan="3" class="desc">{{ $naoconformidade->causaprovavel }}</td>
            </tr>
        @else
            <tr>
                <td colspan="3" class="desc"></td>
            </tr>
            <tr>
                <td colspan="3" class="desc"></td>
            </tr>
        @endif
        </tbody>
        <tfoot>
        <tr>
            <td>RESPONSÁVEL: {{ $naoconformidade->causaprovavel_responsavel }}</td>
            <td class="center-align">ASSINATURA: ___________________</td>
            <td>DATA: {{ isset($naoconformidade->causaprovavel_data) ? $naoconformidade->causaprovavel_data : '___/___/______'  }}</td>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th class="no left-align" colspan="3">4- Ações Corretivas / Ações Preventivas: </th>
        </tr>
        </thead>
        <tbody>
        @if(isset($naoconformidade->acaocorretiva))
            <tr>
                <td colspan="3" class="desc">{{ $naoconformidade->acaocorretiva }}</td>
            </tr>
        @else
            <tr>
                <td colspan="3" class="desc"></td>
            </tr>
            <tr>
                <td colspan="3" class="desc"></td>
            </tr>
        @endif
        </tbody>
        <tfoot>
        <tr>
            <td>RESPONSÁVEL: {{ $naoconformidade->acaocorretiva_responsavel }}</td>
            <td class="center-align">ASSINATURA: ___________________</td>
            <td>DATA: {{ isset($naoconformidade->acaocorretiva_prazo) ? $naoconformidade->acaocorretiva_prazo : '___/___/______'  }}</td>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th class="no left-align" colspan="3">5- Eficácia da Ação Corretiva / Ação Preventiva: </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="2" class="desc">A Ação Corretiva foi implementada?</td>
            <td class="desc"><b>{{ $naoconformidade->foiimplementada == 1?'Sim':'Não' }}</b></td>
        </tr>
        <tr>
            <td colspan="2" class="desc">A ação implementada é eficaz para evitar a Reincidência da Não Conformidade?</td>
            <td class="desc"><b>{{ $naoconformidade->eeficaz == 1?'Sim':'Não' }}</b></td>
        </tr>

            @if(isset($naoconformidade->eficaz_observacoes))
                <tr>
                    <td colspan="3" class="desc">{{ $naoconformidade->eficaz_observacoes }}</td>
                </tr>
            @else
                <tr>
                    <td colspan="3" class="desc"><small class="corcinza">Obs.:</small></td>
                </tr>
                <tr>
                    <td colspan="3" class="desc"></td>
                </tr>
            @endif
        </tbody>
        <tfoot>
        <tr>
            <td>RESPONSÁVEL: {{ $naoconformidade->eficaz_responsavel }}</td>
            <td class="center-align">ASSINATURA: ___________________</td>
            <td>DATA: {{ isset($naoconformidade->eficaz_data) ? $naoconformidade->eficaz_data : '___/___/______'  }}</td>
        </tr>
        </tfoot>
    </table>
</main>
<footer>

</footer>
</body>
</html>