<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Relatorio de exame por Laboratório</title>
    <link rel="stylesheet" href="pdfcss/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>(12) 2103-4490</div>
                {{--<div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>--}}
                <a href="mailto:medicina@netsegtrabalho.com">medicina@netsegtrabalho.com</a>
            </td>
        </tr>
        <tr>
            <td class="left-align" colspan="2">
                <h2>Relatorio de exame por Laboratório</h2>
                <h3>{{$laboratorio->nome}}</h3>
                @if(!empty($data['inicio']) && !empty($data['fim']))
                    <h4>{{$data['inicio']}} à {{$data['fim']}}</h4>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
</header>

<div id="details" class="clearfix">
    <table class="bordered highlight" style="table-layout: fixed;" border="0">
        <thead>
        <tr>
            <th class="total">Nº de exames</th>
            <th class="total">Exame</th>
            <th class="total">Paciente</th>
            <th class="total">Valor Exame</th>
            <th class="total">Valor Médico</th>
        </tr>
        </thead>
        <tbody>

        @if(sizeof($atendimentos) != 0)
            @foreach($atendimentos as $atendimento)
                <tr>
                    <td class="text-center unit">1</td>
                    <td>{{ $atendimento->exame }}</td>
                    <td class="unit">{{ $atendimento->nome }}</td>
                    <td>{{ $atendimento->valor_r }}</td>
                    <td class="unit">{{ $atendimento->valor_med }}</td>
                </tr>
            @endforeach
            <tr>
                <td class="text-center font-weight-bold total">Total</td>
                <td class="total"></td>
                <td class="total"></td>
                <td class="font-weight-bold total">Valor Exame total</td>
                <td class="font-weight-bold total">Valor Médico total</td>
            </tr>
            <tr>
                <td class="text-center corverde unit">{{sizeof($atendimentos)}}</td>
                <td></td>
                <td class="unit"></td>
                <td class="corverde">{{$valor_total}}</td>
                <td class="unit corverde ">{{$valor_medico_total}}</td>
            </tr>

        @else
            <tr>
                <td colspan="5">Nenhum resultado encontrado</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

<footer>

</footer>
</body>
</html>