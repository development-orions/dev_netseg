<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>ASO - ATESTADO DE SAÚDE OCUPACIONAL</title>
    <link rel="stylesheet" href="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/style_aso.css" media="all"/>

</head>

<body>
<section class="clearfix">
    <table class="topo">
        <tbody>
        <tr class="header text-center">
            <td colspan="4">
                <img src="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/logo-aso.jpg" class="logo">
            </td>
            <td colspan="8">
                <h1 class="margin-0 tittle">
                    ASO ATESTADO DE SAÚDE OCUPACIONAL
                </h1>
            </td>
        </tr>
        <tr>
            <td colspan="7" class="programa">
                <div>Programa de Controle Médico de Saúde Ocupacional</div>
                Portaria nº 24 de 29 de Dezembro de 94 e nº 8 de 08 de 05 de 96 -NR-7
            </td>
            <td colspan="5" class="vias">1- Via Empresa - 2 - Empregado - 3 Arquivo NETSEG</td>
        </tr>

        <tr class="border1">
            <td colspan="9">EMPRESA: {{$cliente->nome}}</td>
            <td colspan="3">SETOR: {{$paciente->setor}}</td>
        </tr>

        <tr class="border1">
            <td colspan="10">NOME: {{$paciente->nome}}</td>
            <td colspan="2">IDADE: {{$paciente->idade}}</td>
        </tr>

        <tr class="border1">
            <td colspan="6">FUNÇÃO: {{$paciente->funcao}}</td>
            <td colspan="6">RG: {{$paciente->rg}}</td>
        </tr>

        <tr class="border1">
            <td colspan="12" class="p4"> </td>
        </tr>

        <tr class="border1 bgcinza">
            <td colspan="12" class="text-center">EXAME</td>
        </tr>

        <tr class="border1 exame">
            <td colspan="4"><strong>(&nbsp;&nbsp;&nbsp;)</strong> ADMISSIONAL <br><br><strong>(&nbsp;&nbsp;&nbsp;)</strong> MUDANÇA DE FUNÇÃO</td>
            <td colspan="3"><strong>(&nbsp;&nbsp;&nbsp;)</strong> PERIÓDICO <br><br><strong>(&nbsp;&nbsp;&nbsp;)</strong> DEMISSIONAL </td>
            <td colspan="5" class="v-top"><strong>(&nbsp;&nbsp;&nbsp;)</strong> RETORNO AO TRABALHO </td>
        </tr>

        <tr class="border1">
            <td colspan="12" class="p4"> </td>
        </tr>

        <tr class="border1 bgcinza">
            <td colspan="12" class="text-center">RISCOS OCUPACIONAIS ESPECÍFICIOS</td>
        </tr>

        <tr class="border1 exame">
            <td colspan="1" class="riscos_left"><strong>(&nbsp;&nbsp;&nbsp;)</strong></td>
            <td colspan="5" class="riscos">FISICOS: </td>

            <td colspan="1" class="riscos_left"><strong>(&nbsp;&nbsp;&nbsp;)</strong></td>
            <td colspan="5" class="riscos">BIOLÓGICOS: </td>
        </tr>
        <tr class="border1 exame">
            <td colspan="1" class="riscos_left"><strong>(&nbsp;&nbsp;&nbsp;)</strong></td>
            <td colspan="5" class="riscos">QUÍMICO: </td>

            <td colspan="1" class="riscos_left"><strong>(&nbsp;&nbsp;&nbsp;)</strong></td>
            <td colspan="5" class="riscos">ACIDENTES: </td>
        </tr>
        <tr class="border1 exame">
            <td colspan="1" class="text-center"><strong>(&nbsp;&nbsp;&nbsp;)</strong></td>
            <td colspan="5" class="riscos">ERGÔNOMICO: </td>

            <td colspan="1" class="text-center"><strong>(&nbsp;&nbsp;&nbsp;)</strong></td>
            <td colspan="5" class="riscos">OUTROS: </td>
        </tr>

        <tr class="border1">
            <td colspan="12" class="p4"> </td>
        </tr>
        <tr class="border1 bgcinza">
            <td colspan="12" class="text-center">EXAMES REALIZADOS</td>
        </tr>

        <tr class="border1 p-12 text-center">
            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>

            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>
        </tr>
        <tr class="border1 p-12 text-center">
            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>

            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>
        </tr>
      
        <tr class="border1 p-12 text-center">
            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>

            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>
        </tr>
        <tr class="border1 p-12 text-center">
            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>

            <td colspan="6" class="b-right b-bottom-w">________________________________ ______/______/______</td>
        </tr>

        <tr class="border1 p-12 text-center">
            <td colspan="6" class="b-right pb-12">________________________________ ______/______/______</td>

            <td colspan="6" class="b-right pb-12">________________________________ ______/______/______</td>
        </tr>

        <tr class="border1 p-12">
            <td colspan="12">EXAME CLÍNICO: _______/_______/_______</td>
        </tr>

        <tr class="border1 exame">
            <td colspan="2"><strong>(&nbsp;&nbsp;&nbsp;)</strong> APTO </td>
            <td colspan="4"><strong>(&nbsp;&nbsp;&nbsp;)</strong> APTO COM RESTRIÇÕES </td>
            <td colspan="3"><strong>(&nbsp;&nbsp;&nbsp;)</strong> TEMPORÁRIA </td>
            <td colspan="3"><strong>(&nbsp;&nbsp;&nbsp;)</strong> DEFINITIVO </td>
        </tr>

        <tr class="border1 exame">
            <td colspan="2"><strong>(&nbsp;&nbsp;&nbsp;)</strong> INAPTO </td>
            <td colspan="3"><strong>(&nbsp;&nbsp;&nbsp;)</strong> TEMPORÁRIO </td>
            <td colspan="7"><strong>(&nbsp;&nbsp;&nbsp;)</strong> DEFINITIVO </td>
        </tr>

        <tr class="border1 exame">
            <td colspan="4"><strong>(&nbsp;&nbsp;&nbsp;)</strong> APTO PARA ALTURAS </td>
            <td colspan="8"><strong>(&nbsp;&nbsp;&nbsp;)</strong> APTO PARA ESPAÇO CONFINADO </td>
        </tr>

        <tr class="border1 bgcinza">
            <td colspan="12" class="text-center">OBSERVAÇÕES</td>
        </tr>
        <tr class="border1">
            <td colspan="12" class="obs"> </td>
        </tr>
        <tr class="border1">
            <td colspan="12" class="p4"> </td>
        </tr>
        <tr class="border1">
            <td colspan="12">MÉDICO COORDENADOR: </td>
        </tr>
        <tr class="border1">
            <td colspan="12" class="p4"> </td>
        </tr>
        <tr class="border1">
            <td colspan="12">Guaratinguetá, _____/_____/_____</td>
        </tr>

        <tr class="border1">
            <td colspan="6" class="b-right"><div class="text-center recebi">RECEBI A 2ª VIA DO ATESTADO OCUPACIONAL EM:</div><br>
                Data:_____/_____/_____
                <br>
                <div class="right-align">__________________________________ <br>
                    <div class="ciente">Ciente</div></div>
            </td>

            <td colspan="6" class="examinador"><div class="text-center">MÉDICO EXAMINADOR / CARIMBO / CRM</div></td>
        </tr>


        </tbody>
    </table>
    <div class="text-center footer">
        <br>
        Rua {{$configuracoes->rua}}, nº {{$configuracoes->numero}} / Telefone: (12) 3133-5694 - ramal 1<br>
        e-mail: medicina@netsegtrabalho.com.br
    </div>
</section>

</body>
</html>

