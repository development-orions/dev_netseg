<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Relatório de Pagamento do Técnico {{ $tecnico->nome }}</title>
    <link rel="stylesheet" href="pdfcss/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }}</div>
                <div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        <tr>
            <td class="left-align" colspan="2">
                <h2>Relatório de Pagamento do Técnico {{ $tecnico->nome }}</h2>
            </td>
        </tr>
        </tbody>
    </table>
</header>

<div id="details" class="clearfix">
    <table class="bordered highlight" style="table-layout: fixed; width: 700px;" border="0">
        <thead>
        <tr>
            <th class="total">Empresa</th>
            <th class="total">Pagamento</th>
            <th class="total">Valor do Contrato</th>
            <th class="total">Valor Recebido</th>
            <th class="total">%</th>
        </tr>
        </thead>
        <tbody>
        @foreach($resultado as $cliente)
            <tr>
                <td class="unit">{{ $cliente->razaosocial }}</td>
                <td>Dia {{ $cliente->dia_tecnico }}</td>
                <td class="unit">R${{ $cliente->valor_contrato }}</td>
                <td>R${{ $cliente->valor_tecnico }}</td>
                <td class="unit">{{ $cliente->total }}%</td>
            </tr>
        @endforeach
        <tr>
            <td class="total" colspan="2">TOTAL</td>
            <td class="unit corazul">
                {{ Session::has('total_contrato') ? 'R$'.Session::get('total_contrato') : '' }}
            </td>
            <td class="corazul">{{ Session::has('total_tecnico') ? 'R$'.Session::get('total_tecnico') : '' }}</td>
            <td class="unit corazul">{{ Session::has('total_p') ? Session::get('total_p')."%" : '' }}</td>
        </tr>
        </tbody>
    </table>
</div>

<footer>

</footer>
</body>
</html>