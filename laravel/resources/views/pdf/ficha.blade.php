<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Ficha Clínica Ocupacional</title>
    <link rel="stylesheet" href="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/style_ficha.css" media="all"/>

</head>

<body>
<section class="clearfix">
    <table>
        <tbody>
        <tr class="text-center">
            <td>
                <img src="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/logo-aso.jpg" class="logo">
            </td>
        </tr>
        <tr>
            <td class="text-center mb-10">
                <div class="tittle">Ficha Clínica Ocupacional</div>
            </td>
        </tr>

        <tr class="bb">
            <td><span class="label">Nome:</span>&nbsp;{{$paciente->nome}}</td>
        </tr>

        </tbody>
    </table>
    <table class="mt-10">
        <tbody>
        <tr class="bb">
            <td><span class="label">RG:</span>&nbsp;{{$paciente->rg}}</td>
            <td><span class="label">CPF:</span>&nbsp;{{$paciente->cpf}}</td>
            <td><span class="label">Data Nascimento:</span> {{ $paciente->nascimento[2]}}/{{ $paciente->nascimento[1]}}/{{ $paciente->nascimento[0]}}</td>
        </tr>
        </tbody>
    </table>

    <table>
        <tbody>
        <tr class="bb">
            <td class="pt-10"><span class="label">Empresa:</span>&nbsp;{{$cliente->nome}}</td>
        </tr>
        <tr class="bb">
            <td class="pt-10"><span class="label">Setor:</span>&nbsp;{{$paciente->setor}}</td>
        </tr>
        <tr class="bb">
            <td class="pt-10"><span class="label">Função:</span>&nbsp;{{$paciente->funcao}}</td>
        </tr>
        </tbody>
    </table>

    <table class="bordered mt-10">
        <tbody>
        <tr>
            <td class="pt-10 pb-10"><span class="label">&nbsp;Exame Ocupacional:</span></td>
            <td><span class="square"></span><span>Admissional</span> </td>
            <td><span class="square"></span><span>Periódico</span> </td>
            <td><span class="square"></span><span>Demissional</span> </td>
        </tr>
        <tr>
            <td></td>
            <td class="pb-10"><span class="square"></span><span>Mudança de Função</span> </td>
            <td class="pb-10"><span class="square"></span><span>Retorno ao Trabalho</span> </td>
            <td></td>
        </tr>
        </tbody>
    </table>

    <table class="bordered mt-10">
        <tbody>
        <tr>
            <td colspan="16" class="text-center bold">Antecedentes Pessoais e Profissionais</td>
        </tr>
        <tr class="antecedentes">
            <td colspan="5" class="bold">&nbsp;Responda as questões abaixo:</td>
            <td colspan="1" class="text-center bold">Não</td>
            <td colspan="1" class="text-center bold">Tenho</td>
            <td colspan="1" class="text-center bold">Tive</td>
            <td colspan="5"></td>
            <td colspan="1" class="text-center bold">Não</td>
            <td colspan="1" class="text-center bold">Tenho</td>
            <td colspan="1" class="text-center bold">Tive</td>
        </tr>
        <tr class="antecedentes">
            <td colspan="5" class="p-2"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="5"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
        </tr>
        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Doença do Coração/Pressão Alta</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Diabetes</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Bronquite ou Asma</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Convulsões, Desmaios ou Tonturas</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Rinites ou Alergias</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Doenças de pele</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Problemas de ouvidos/Audição</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Dores nas costas</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>
        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Gastrite/Ulceras</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Dores nas articulações</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Hepatite</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Dores no pescoço</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Hérnias</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Varizes</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Doenças do rins</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Câncer</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Problemas de visão</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="5" class="fs-13">Depressão</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
        </tr>

        </tbody>
    </table>

    <table class="mt-10">
        <tbody>
        <tr>
            <td colspan="5" class="bold">Responda as questões abaixo:</td>
            <td colspan="1" class="text-center bold">Sim</td>
            <td colspan="1" class="text-center bold"></td>
            <td colspan="1" class="text-center bold">Não</td>
        </tr>
        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Sofreu Alguma Fratura?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual ou quais?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Você fuma?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Quantas vezes ao dia?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Usa ou usou algum tipo de droga?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual ou quais?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Já esteve internado?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual motivo?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Já fez alguma cirurgia?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual cirurgia?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Faz uso regular de medicação?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual ou quais?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Consome bebida alcoolica?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Quantas vezes por semana?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Faz atividade física?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Quantas vezes por semana?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Sofreu algum acidente de trabalho?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Quando?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Esteve afastado mais de 15 dias?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual motivo?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Passou por reabilitação no INSS?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Quando?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Já utilizou EPI? (óculos, plug etc.)</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Quais?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">Teve problema com uso de EPI?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual ou quais?</td>
        </tr>

        <tr class="antecedentes">
            <td colspan="5" class="fs-13">É portador de deficiência?</td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="1" class="text-center"></td>
            <td colspan="1" class="text-center"><span class="check"></span></td>
            <td colspan="8" class="fs-13">Qual?</td>
        </tr>
        </tbody>
    </table>

    <table class="mt-10">
        <tbody>
        <tr class="bold antecedentes p-2">
            <td colspan="4">&nbsp;Ultimas Empresas: </td>
            <td colspan="2" class="text-center">Tempo</td>
            <td colspan="6">&nbsp;Função</td>
        </tr>
        <tr class="ultimas"><td colspan="4"></td><td colspan="2"></td><td colspan="6"></td></tr>
        <tr class="ultimas"><td colspan="4"></td><td colspan="2"></td><td colspan="6"></td></tr>
        <tr class="ultimas"><td colspan="4"></td><td colspan="2"></td><td colspan="6"></td></tr>
        </tbody>
    </table>

    <table class="mt-10">
        <tbody class="bordered">
        <tr class="bold p-2">
            <td colspan="16" class="pb-25">&nbsp;Assino como prova de ter declarado a verdade.</td>
        </tr>
        <tr class="bold p-2">
            <td colspan="5" class="bb"></td>
            <td colspan="1"></td>
            <td colspan="3" class="bb text-center"> / <span class="space"></span> / </td>
            <td colspan="1"></td>
            <td colspan="6" class="bb"></td>
        </tr>
        <tr class="bold p-2">
            <td colspan="5" class="text-center">Local</td>
            <td colspan="1"></td>
            <td colspan="3" class="text-center">Data</td>
            <td colspan="1"></td>
            <td colspan="6" class="text-center">Assinatura do Colaborador</td>
        </tr>
        </tbody>
    </table>

    <div class="quebra"></div>



    <div class="bold text-center fs-16">Exame Médico</div>

    <table class="mt-5 bordered p-5">
        <tbody>
        <tr>
            <td colspan="3" class="fs-13 p-10 bold">PA: _______x_______</td>
            <td colspan="3" class="fs-13 bold">DUM: _____/_____/_____</td>
            <td colspan="3" class="fs-13 bold">Acuidade Visual</td>
            <td colspan="3" class="fs-13 bold">Lentes/Óculos</td>
        </tr>
        <tr>
            <td colspan="3" class="fs-13 p-10 bold v-top">Peso: _______________</td>
            <td colspan="3" class="fs-13 p-10  bold v-top">Altura: ____,_________</td>
            <td colspan="3" class="fs-13 bold"><span class="od">OD: ______/______</span>OE: ______/______<br><br></td>
            <td colspan="3" class="fs-13 p-10  bold v-top">  &nbsp;&nbsp;Sim &nbsp;&nbsp; Não</td>
        </tr>
        </tbody>
    </table>
    <span class="exame">&nbsp;Exame Físico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    <table class="mt-10 bordered">
        <tbody>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold">Cabeça e Pescoço:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Pulmões:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Coração:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Abdome:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Torax:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Coluna:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Pele:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Deformidades:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Edemas:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Cistos Sinoviais:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Limitação á Movimentos:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Diminuição da Força:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Dor (Palpação/Movimentação):</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Sinal de Phalen:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Sinal de filkeinstei:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Sinal de tinel:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold v-top">Outras:</td>
        </tr>

        </tbody>
    </table>

    <table class="mt-10 bordered">
        <tbody>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold">Resultados de Exames Ocupacionais:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>

        </tbody>
    </table>

    <table class="mt-10 bordered">
        <tbody>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-5 bold">Anotação:</td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>
        <tr class="bb">
            <td colspan="3" class="fs-13 p-12 bold"></td>
        </tr>

        </tbody>
    </table>

    <table class="mt-10 bordered">
        <tbody>
        <tr>
            <td colspan="12" class="fs-13 pl-5 bold">Sendo considerado:</td>
        </tr>
        <tr>
            <td colspan="12" class="fs-13 p-5 bold"><span class="check"></span> Apto para função</td>
        </tr>
        <tr>
            <td colspan="5" class="fs-13 p-5 bold"><span class="check"></span> Apto para função com restrição </td>
            <td colspan="3" class="fs-13 p-5 bold"><span class="check"></span> Temporário</td>
            <td colspan="3" class="fs-13 p-5 bold"><span class="check"></span> Definitiva</td>
            <td colspan="1" class="fs-13 p-5 bold"></td>
        </tr>
        <tr class="bb bt">
            <td colspan="12" class="fs-13 p-5 bold"></td>
        </tr>
        <tr>
            <td colspan="4" class="fs-13 p-5 bold"><span class="check"></span> Inapto para função  </td>
            <td colspan="3" class="fs-13 p-5 bold"><span class="check"></span> Temporário</td>
            <td colspan="3" class="fs-13 p-5 bold"><span class="check"></span> Definitiva</td>
            <td colspan="2" class="fs-13 p-5 bold"></td>
        </tr>


        </tbody>
    </table>
</section>

</body>
</html>

