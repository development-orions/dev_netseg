<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Relatório de Atividades</title>
    <link rel="stylesheet" href="pdfcss/style_atividade.css" media="all"/>
</head>

<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name"></h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }}</div>
                <div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center-align">
                <h2>Relatório de Atividades</h2>
                @if($inicio != null && $fim != null)

                    <h4>VENCIMENTO: <span class="to"> {{ $inicio }} </span> à <span class="to"> {{ $fim }}</span></h4>

                @endif
            </td>

        </tr>
        </tbody>
    </table>
</header>
@forelse($tecnicos_list as $tecnic)

    <div id="details" class="clearfix">
        <h2 class="tecnico"><b>{{ $tecnic->nome }}</b></h2>
    </div>
    @forelse($atividades as $cliente)
        @if(sizeof($cliente->cronogramas) != 0)
            <div><h3>Cliente: {{ $cliente->nome }}</h3>
                <p>Razão Social: {{ $cliente->razaosocial }} - CNPJ: {{ $cliente->cnpj }}</p>
            </div>
            <table style="table-layout: fixed; width: 700px;" border="0">
                <thead>
                <tr>
                    <th class="no">VENCIMENTO</th>
                    <th class="no">ATIVIDADE</th>
                    <th class="no">STATUS</th>
                </tr>
                </thead>
                <tbody>
                @forelse($cliente->cronogramas as $cronograma)

                    <tr>
                        <td>{{ date('d/m/Y', strtotime($cronograma->vencimento)) }}</td>
                        <td>{{ $cronograma->atividade }}</td>
                        <td>{{ $cronograma->statu }}</td>
                    </tr>
                    @if($tecnic->id != $cronograma->tecnico_id)
                        <?php break; ?>
                    @endif
                @empty
                    <tr>
                        <td colspan="3"><h3>Nenhum registro encontrado para {{ $cliente->nome }}</h3></td>
                    </tr>
                @endforelse

                </tbody>

            </table>
        @endif
    @empty
        <main>
            <div>
                <h3>Nenhum registro encontrado para {{ $cliente->nome }}</h3>
            </div>
        </main>
    @endforelse

@empty
    <main>
        <div><h3>Nenhum registro encontrado</h3></div>
    </main>
@endforelse
</body>
</html>

