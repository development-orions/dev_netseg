<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Lista de Exames Netseg</title>
    <link rel="stylesheet" href="pdfcss/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>(12) 2103-4490</div>
                {{--<div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>--}}
                <a href="mailto:medicina@netsegtrabalho.com">medicina@netsegtrabalho.com</a>
            </td>
        </tr>
        <tr>
            <td class="left-align" colspan="2">
                <h2>Lista de Exames Netseg</h2>
            </td>
        </tr>
        </tbody>
    </table>
</header>

<div id="details" class="clearfix">
    <table class="bordered highlight" style="table-layout: fixed;" border="0">
        <thead>
        <tr>
            <th class="total">Nome</th>
            <th class="total">Valor</th>
        </tr>
        </thead>
        <tbody>

        @if(sizeof($exames) != 0)
            @foreach($exames as $exame)
                <tr>
                    <td>{{ $exame->nome }}</td>
                    <td class="unit">{{ $exame->valor_r }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">Nenhum resultado encontrado</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

<footer>

</footer>
</body>
</html>