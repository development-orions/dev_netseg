<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Relatório de Contas à {{ $tipo == 1 ? 'Pagar' : 'Receber' }}</title>
    <link rel="stylesheet" href="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"}}/pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }}</div>
                <div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        <tr>
            <td class="left-align" colspan="2">
                <h2>Relatório de Contas à {{ $tipo == 1 ? 'Pagar' : 'Receber' }}</h2>
            </td>
        </tr>
        </tbody>
    </table>
</header>

<div id="details" class="clearfix">
    <table class="bordered highlight" style="table-layout: fixed; width: 100%;" border="0">
        <thead>
        <tr>
            <th class="total">Conta</th>
            <th class="total">Centro de Custo</th>
            <th class="total">{{ $tipo == 1 ? 'Fornecedor' : 'Cliente' }}</th>
            <th class="total">Vencimento</th>
            <th class="total">Baixa</th>
            <th class="total">Valor Total</th>
            <th class="total">Portador</th>
            <th class="total">NF</th>
            <th class="total">Status</th>
        </tr>
        </thead>

        <tbody>
        @forelse($resultado as $conta)
            <tr>
                <td class="unit">{{ $conta->contas->nome }}</td>
                <td>{{ $conta->contas->centrocustos->nome }}</td>
            <!-- <td>{{ $conta->contas->created_at }}</td> -->
                <td class="unit">{{ $tipo == 1 ? $conta->fornecedores->nome : $conta->clientes->nome }}</td>
                <td>{{ $conta->contas->vencimento }}</td>
                <td class="unit">{{ $conta->contas->baixa }}</td>
                <td>R${{ $conta->contas->total }}</td>
                <td class="unit">{{ $conta->contas->portador }}</td>
                <td>{{ $conta->contas->nf }}</td>
                <td class="unit"><span>{{ $conta->contas->status->nome }}</span></td>
            </tr>
        @empty
            <tr>
                <td colspan="9">Nenhuma conta encontrada.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<div class="col s12" id="notices">
    <h5 class="notice">Saldo Total: R$ {{ $total }}</h5>
</div>

</body>
</html>