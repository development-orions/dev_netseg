<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Relatório de Vendas por {{ $tipo == 1 ? 'Médico' : 'Técnico/Instrutor' }}</title>
    <link rel="stylesheet" href="pdfcss/style.css" media="all" />

</head>
<body>
<header class="clearfix">
    <table class="topo">
        <tbody>
        <tr>
            <td id="logo">
                <img src="pdfcss/logo.jpg">
            </td>
            <td id="company">
                <h2 class="name">{{ $configuracoes->nomefantasia }}</h2>
                <div>CNPJ: {{ $configuracoes->cnpj }}</div>
                <div>{{ $configuracoes->rua }}, {{ $configuracoes->numero }} <br> {{ $configuracoes->bairro }}, {{ $configuracoes->cidade }} - {{ $configuracoes->estado }} <br> CEP: {{ $configuracoes->cep }}</div>
                <div>{{ $configuracoes->telefone }}</div>
                <div><a href="mailto:{{ $configuracoes->email }}">{{ $configuracoes->email }}</a></div>
            </td>
        </tr>
        <tr>
            <td class="left-align" colspan="2">
                <h2>Relatório de Vendas {{ $tipo == 1 ? 'Médico' : 'Técnico/Instrutor' }}</h2>
                <h3>{{ $tipo == 1 ?  $medico->nome : $tecnico->nome  }}</h3>
            </td>
        </tr>
        </tbody>
    </table>
</header>

<div id="details" class="clearfix">
    <table class="bordered highlight" style="table-layout: fixed;" border="0">
        <thead>
        <tr>
            <th class="total">Data</th>
            <th class="total">Venda</th>
            <th class="total">Cliente</th>
            <th class="total">Serviços</th>
            <th class="total">Valor Total</th>
            <th class="total">Valor Netseg</th>
            <th class="total">Valor {{ $tipo == 1 ? 'Médico' : 'Técnico/Instrutor' }}</th>
        </tr>
        </thead>
        <tbody>

        @if(sizeof($resultado) != 0)
        @foreach($resultado as $venda)
            <tr>
                <td class="unit">{{ $venda->created_at }}</td>
                <td>{{ $venda->id }}</td>
                <td class="unit">{{ $venda->clientes['razaosocial'] }}</td>
                <td>
                    @foreach($venda->servicos as $servico)
                        <p>- {{$servico->nome}}</p>
                    @endforeach
                </td>
                <td class="unit">R${{$venda->valor}}</td>
                <td>R$ {{ $venda->valoradministrativo }} <span class="corazul">({{ $venda->adm }}%)</span></td>
                <td class="unit">R$ {{ $venda->valor_medico }} <span class="corazul">({{ $venda->total }}%)</span></td>
            </tr>
        @endforeach
        <tr>
            <td colspan="4" class="total">TOTAIS</td>
            <td class="unit">R$ {{$resultado->totalgeral}}</td>
            <td class="corverde">R$ {{$resultado->totaladm}} <span class="corazul">({{  $resultado->totaladmporcentagem }}%)</span></td>
            <td class="corverde unit">R$ {{$resultado->total}} <span class="corazul">({{ $resultado->totalporcentagem }}%)</span></td>
        </tr>
            @else
            <tr>
                <td colspan="7">Nenhum resultado encontrado</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

<footer>

</footer>
</body>
</html>