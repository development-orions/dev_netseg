@extends('app')
@section('title', 'Listar Vendas')
@section('content')

    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".iframe").colorbox({iframe:true, width:"900", height:"500", onClosed: function() {
                parent.location.reload(true);
                ;}});
            parent.$.fn.colorbox.close();
        });
    </script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Vendas</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/venda_lista/0" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    <i class="material-icons right">search</i>
                </button>

                <div class="input-field col m2 s3">

                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::text('venda', (isset($data['venda']) ? $data['venda']:''), ['class'=>'validate', 'placeholder'=>'Por Número']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::select('statu_id', ['' => 'Todos status'] + $status, isset($data['statu_id']) ? $data['statu_id'] : null, ['id' => 'status']) !!}
                </div>

                <div class="input-field col m2 s3">
                    {!! Form::text('q', (isset($data['q']) ? $data['q']:''), ['class'=>'validate', 'placeholder'=>'Por Cliente']) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Número</th>
                        <th>Cliente</th>
                        <th>Data</th>
                        <th>Status</th>
                        <th>Nota Fiscal</th>
                        <th>Forma de pagamento</th>
                        <th>Tipo</th>
                        <th>Total</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($vendas as $venda)

                        <tr>
                            <td>{{ $venda->id }}</td>
                            <td>{{ $venda->razaosocial }}</td>
                            <td>{{ $venda->created_at->format('d/m/Y') }}</td>
                            <td>{{ $venda->status->nome }}</td>
                            <td>{{ $venda->nf }}</td>
                            <td>{{ $venda->nome }}  </td>
                            <td>{{ $venda->tipo }}  </td>
                            <td>{{ $venda->valor }}</td>

                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="/visualizar/venda_view/{{ base64_encode($venda->id) }}" title="Visualizar Venda" class="btn-floating yellow iframe"><i class="material-icons">visibility</i></a></li>
                                        <!--<li><a href="/pdf/{{ base64_encode($venda->id) }}" target="_blank" title="Imprimir Orçamento" class="btn-floating orange"><i class="material-icons">print</i></a></li>-->

                                        @if((Auth::user()->role_id == 6) || (Auth::user()->role_id == 7) || (Auth::user()->role_id == 1))
                                            @if($venda->statu_id != 3)
                                                <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/cancelar_venda/{{ base64_encode($venda->id) }}" title="Cancelar venda" class="iframe btn-floating red"><i class="fa fa-times"></i></a></li>
                                            @endif
                                        @endif

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhuma Venda encontrada.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col s12 paginacao">
                <?php echo $vendas->appends(Request::all())->render(); ?>
            </div>
        </div>
    </div>
@endsection