@extends('iframe')
@section('title', 'Cancelar Venda')
@section('content')
    <script type="text/javascript" src="/js/validador/venda_cancelar.js"></script>
    <div class="container">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cancelar venda</p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}

            {!! Form::hidden('id', (isset($id) ? $id :''), ['id' => 'id']) !!}

            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">

            <div class="row">
                <div class="input-field col s12 m12">
                    <div class="input-field col s12">
                        {!! Form::textarea('motivo', old('motivo'), ['class' => 'validate materialize-textarea', 'id' => 'motivo']) !!}
                        {!! Form::label('motivo', 'Motivo: ') !!}
                    </div>                   
                </div>
            </div>

            <div class="row right-align">
                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">Cancelar Venda
                    <i class="material-icons right">send</i>
                </button>
            </div>

            {!! Form::close() !!}
        </div>

    </div>
    <div id="modal1" class="modal"></div>
@endsection