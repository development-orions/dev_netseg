@extends('app')
@section('title', 'Listar Serviços')
@section('content')

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Serviços</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                       <!-- <th>Valor</th>-->
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($servicos as $servico)
                        <tr>
                            <td>{{ $servico->nome }}</td>
                           <!-- <td>R$ {{ str_replace('.', ',', $servico->valor )}}</td>-->
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/servico_edita/{{ base64_encode($servico->id) }}" title="Editar serviço" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">Nenhum serviço encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection