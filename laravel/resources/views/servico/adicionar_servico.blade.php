@extends('iframe')
@section('title', 'Adicionar Serviço')
@section('content')

    <script type="text/javascript" src="/js/validador/servico_orcamento.js"></script>
    <script type="text/javascript">
        function buscavalor($id){
            $.ajax({
                type:"GET",
                url: '/servico_valor/'+$id,
                cache:false,
                success: function(data) {
                    console.log(data);
                    if(data.valor){
                        $('#valor').val(data.valor);
                    }
                }
            });
        }

        $(document).ready(function(){
            $('#sv_combo').hide();
        });

        function filtro(){

            if ($('#servico1').is(":checked")) {
                $('#treinamento').show('slow');
            }else{
                $('#treinamento').hide('slow');
                $('#cargahoraria').val('');
                $('#participantes').val('');
                $('#periodo').val('');
                $('#horario').val('');
            }

        };

        function filtro2(valor){
            valor = parseInt(valor, 10);
            switch (valor){
                case 1:
                    $('#treinamento').show('slow');
                    $('#sv_combo').hide('slow');
                    break;
                case 2:
                    $('#treinamento').hide('slow');
                    $('#sv_combo').hide('slow');
                    break;
                case 3:
                    $('#sv_combo').show('slow');
                    $('#treinamento').hide('slow');
                    break;
                default:
                    $('#treinamento').hide('slow');
            }
        };



    </script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Adicionar Serviço </p>
            </div>
        </div>
        <div class="row">
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">
                <div class="col s12 m6">
                    {!! Form::label('servico', 'Serviço: ') !!}
                        {!! Form::select('servico_id', $servicos, isset($servico_edit['servico_id']) ? $servico_edit['servico_id'] : null, ['class' => 'browser-default', 'id' => 'servico', 'onchange' => 'filtro2(this.value)']) !!}

                </div>

                <div class="input-field col m6 s12">
                    {!! Form::text('valor', (isset($servico_edit['valor']) ? $servico_edit['valor']:''), ['class' => 'validate', 'id' => 'valor']) !!}
                    {!! Form::label('valor', 'Valor: ') !!}
                </div>
            </div>
            <div class="row" id="sv_combo">
                <div class="input-field col s12 m6">
                @foreach($servicos_combo as $servico)
                    <p>
                        <input type="checkbox" class="filled-in" id="servico{{$servico['id']}}" name="servicos_combo[]" value="{{$servico['nome']}}" onclick="filtro()" />
                        {!! Form::label('servico'.$servico['id'], $servico['nome']) !!}
                    </p>
                @endforeach
                    </div>
            </div>
            <div class="row">
                <div class="input-field col m12 s12">
                    {!! Form::textarea('descricao',(isset($servico_edit['descricao']) ? $servico_edit['descricao']:''), ['class' => 'validate materialize-textarea', 'id' => 'descricao']) !!}
                    {!! Form::label('descricao', 'Descrição: ') !!}
                </div>
            </div>
            <div id="treinamento">
                <div class="row">
                    <div class="input-field col m6 s12">
                        {!! Form::text('cargahoraria', (isset($servico_edit['cargahoraria']) ? $servico_edit['cargahoraria']:''), ['class' => 'validate', 'id' => 'cargahoraria']) !!}
                        {!! Form::label('cargahoraria', 'Carga horária: ') !!}
                    </div>
                    <div class="input-field col m6 s12">
                        {!! Form::text('participantes', (isset($servico_edit['participantes']) ? $servico_edit['participantes']:''), ['class' => 'validate', 'id' => 'participantes']) !!}
                        {!! Form::label('participantes', 'Número de Participantes: ') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col m6 s12">
                        {!! Form::text('periodo', (isset($servico_edit['periodo']) ? $servico_edit['periodo']:''), ['class' => 'validate', 'id' => 'periodo']) !!}
                        {!! Form::label('periodo', 'Período: ') !!}
                    </div>
                    <div class="input-field col m6 s12">
                        {!! Form::text('horario', (isset($servico_edit['horario']) ? $servico_edit['horario']:''), ['class' => 'validate', 'id' => 'horario']) !!}
                        {!! Form::label('horario', 'Horário: ') !!}
                    </div>
                </div>
            </div>
            <!--<div class="row" id="medicina">
                <div class="input-field col m6 s12">
                    {!! Form::text('frequenciavisita', (isset($servico_edit['frequenciavisita']) ? $servico_edit['frequenciavisita']:''), ['class' => 'validate', 'id' => 'frequenciavisita']) !!}
                    {!! Form::label('frequenciavisita', 'Frequência das visitas: ') !!}
                </div>
            </div>-->
            <div class="row">
                <div class="input-field col s12 m6 right-align">
                    <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($servico_edit) ? 'Salvar' : 'Cadastrar'  }}
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div id="modal1" class="modal"></div>
@endsection


