@extends('iframe')
@section('title', 'Visualizar Cliente')
@section('content')


    <div class="container iframe-corpo">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold corbranca">{{ $cliente->razaosocial }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p>CNPJ/CPF: {{ !empty($cliente->cnpj)? $cliente->cnpj : $cliente->cpf }}</p>
                <p>IE: {{ $cliente->ie }}</p>
                <p>CEI/Autonomo: {{ $cliente->cei_autonomo }}</p>
                <p>Nome Fantasia: {{ $cliente->nome }}</p>
                @if($cliente->tipo == 'Assessoria')
                    <p>Valor do contrato: R${{ $cliente->valor_contrato }}</p>
                    <p>Data de validade do contrato: {{ $cliente->data_contrato }}</p>
                @endif
                <p>Razão Social: {{ $cliente->razaosocial }}</p>
                <p>Forma Jurídica: {{ $cliente->formajuridica }}</p>
                <p>Tipo: {{ $cliente->tipo }}</p>
            </div>
            <!--<div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/cliente_edita/{{ base64_encode($cliente->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar Cliente"><i class="material-icons">description</i></a>

            </div>-->
        </div>
        @if($cliente->tipo == 'Assessoria')
            <div class="row">
                <div class="col s12">
                    <p class="green-text text-darken-3">Técnicos Responsáveis:</p>
                    @foreach($cliente->clientestecnicos as $tecnico)
                        <div class="col s6">
                            <p>Técnico: {{  $tecnico->tecnicos->nome }}</p>
                            <p>Valor do técnico: R${{ $tecnico->valor_tecnico }}</p>
                            <p>Pagamento: Dia {{ $tecnico->dia_tecnico }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col s6">
                <p class="green-text text-darken-3">Endereço:</p>
                <p>{{ $cliente->rua }}, {{ $cliente->numero }}  {{ $cliente->complemento }} - {{ $cliente->bairro }}<br>
                    {{ $cliente->cidade }} - {{ $cliente->estado }}<br>
                    CEP: {{ $cliente->cep }}
                </p>
            </div>
            <div class="col s6">
                <p class="green-text text-darken-3">Dados do Responsável:</p>
                <p>Nome: {{ $cliente->responsavel }}</p>
                <p>Função: {{ $cliente->funcao }}</p>
                <p>Telefone: {{ $cliente->telefone_responsavel }}</p>
                <p>Email: {{ $cliente->email_responsavel }}</p>
            </div>
            <div class="col s6">
                <p class="green-text text-darken-3">Dados de acesso:</p>
                <p>Usuário: {{ $cliente->usuario }}</p>
                <p>Senha: {{ $cliente->senha }}</p>
            </div>
        </div>
    </div>
@endsection