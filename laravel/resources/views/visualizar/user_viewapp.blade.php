@extends('app')
@section('title', 'Visualizar usuário')
@section('content')

    <div class="container">
        <div class="row bg">
            <div class="col s12 bgverde">
                <div class="col s10">
                    <p class="bold font20 corbranca">Visualizar conta</p>
                </div>
                <div class="col s2">
                    @if((Auth::user()->role_id == 2) || (Auth::user()->role_id == 6) || (Auth::user()->role_id == 7))
                        <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/user_edita/{{ base64_encode(Auth::user()->id) }}" class="btn-editar btn-floating waves-effect waves-light blue darken-1" title="Editar Cliente"><i class="material-icons">description</i></a>
                    @else
                        <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/user_edita" class="btn-editar btn-floating waves-effect waves-light blue darken-1" title="Editar Cliente"><i class="material-icons">description</i></a>
                    @endif
                </div>
            </div>
        </div>
        <hr>
        <div class="row iframe-p">
            <div class="col s12">
                <h5>Nome</h5>
                <p>{{ $user->nome }}</p>
            </div>
        </div>
        <hr>
        <div class="row padding">
            <div class="col s12 padding">
                <h5>Login:</h5>
                <div class="notice">{{ $user->username }}</div>
                <hr>
            </div>

            <div class="col s12 padding">
                <table class="striped">
                    <thead>
                    <tr>
                        <th class="no">E-MAIL</th>
                        <th>FUNÇÃO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                        <td>{{ $user->funcao }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection