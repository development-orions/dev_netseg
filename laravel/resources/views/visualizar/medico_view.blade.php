@extends('iframe')
@section('title', 'Visualizar medico')
@section('content')


    <div class="container">
        <div class="row bg">
            <div class="col s12 font20 bgverde">
                <p class="bold corbranca">Dr(a). {{ $medico->nome }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p>CRM: {{ $medico->crm }}</p>
                <p>NIT: {{ $medico->nit }}</p>
                <p>Data do contrato: {{ $medico->datacontrato }}</p>
                <p>Telefone: {{ $medico->telefone }}</p>
            </div>
           <!-- <div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/medico_edita/{{ base64_encode($medico->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar medico"><i class="material-icons">description</i></a>

            </div>-->
        </div>
        <div class="row">
            <div class="col s12">
                <p class="green-text text-darken-3">Endereço:</p>
                <p>{{ $medico->rua }}, {{ $medico->numero }}  {{ $medico->complemento }} - {{ $medico->bairro }}<br>
                    {{ $medico->cidade }} - {{ $medico->estado }}<br>
                    CEP: {{ $medico->cep }}
                </p>
            </div>
            <div class="col s12">
                <p class="green-text text-darken-3">Observações:</p>
                <p>{{ $medico->observacao }}</p>
            </div>
        </div>
    </div>
@endsection