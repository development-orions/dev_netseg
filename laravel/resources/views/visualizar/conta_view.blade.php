@extends('iframe')
@section('title', 'Visualizar Conta')



    <div class="container iframe-corpo">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold corbranca">Conta à {{ $tipo == 1 ? 'Receber' : 'Pagar' }}: {{ $conta->nome }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p><span class="green-text text-darken-4">Data de lançamento:</span> {{ $conta->created_at }}</p>
                <p><span class="green-text text-darken-4">Data de vencimento:</span> {{ $conta->vencimento }}</p>
                <p><span class="green-text text-darken-4">Data da baixa:</span> {{ $conta->baixa }}</p>
                <p><span class="green-text text-darken-4">Valor sem desconto e sem acréscimo:</span>R$ {{ $conta->valor }}</p>
                <p><span class="green-text text-darken-4">Acréscimo:</span> {{ isset($conta->acrescimo) ? 'R$ '.$conta->acrescimo : '-' }}</p>
                <p><span class="green-text text-darken-4">Desconto:</span> {{ isset($conta->desconto) ?  'R$ '.$conta->desconto : '-'}}</p>
                <p class="bold"><span class="green-text text-darken-4">Valor Total:</span>R$ {{ $conta->total }}</p>
            </div>
            <!--<div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/conta_edita/{{ $tipo }}/{{ base64_encode($conta->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar Conta"><i class="material-icons">description</i></a>

            </div>-->
        </div>
        <div class="row">
            <div class="col s10">
                <p><span class="green-text text-darken-4">Portador:</span> {{ $conta->portador }}</p>
                <p><span class="green-text text-darken-4">Núm. Nota Fiscal:</span> {{ $conta->nf }}</p>
                <p><span class="green-text text-darken-4">Centro de custo:</span> {{ $conta->centrocustos->nome }}</p>
                <p><span class="green-text text-darken-4">Status:</span> {{ $conta->status->nome }}</p>
                <p><span class="green-text text-darken-4">Forma de Pagamento:</span> {{ $conta->formapagamentos->nome }}</p>
                @if($conta->formapagamento_id == 1)
                    <p><span class="green-text text-darken-4">Núm. Boleto:</span> {{ $conta->num_boleto }}</p>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s10">
                @if($tipo == 1)
                    <p><span class="green-text text-darken-4">Cliente: </span>{{ $conta->recebimentos[0]->clientes->razaosocial }}</p>
                    <p><span class="green-text text-darken-4">CNPJ: </span>{{ $conta->recebimentos[0]->clientes->cnpj }}</p>
                @else
                    <p><span class="green-text text-darken-4">Fornecedor: </span>{{ $conta->pagamentos[0]->fornecedores->razaosocial }}</p>
                    <p><span class="green-text text-darken-4">CNPJ: </span>{{ $conta->pagamentos[0]->fornecedores->cnpj }}</p>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col s10">
                <p><span class="green-text text-darken-4">Observação: </span>{{ $conta->observacao }}</p>
            </div>
        </div>

    </div>