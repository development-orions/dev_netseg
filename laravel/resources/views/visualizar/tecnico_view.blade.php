@extends('iframe')
@section('title', 'Visualizar Tecnico')
@section('content')


    <div class="container iframe-corpo">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold corbranca">{{ $tecnico->nome }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p>Número de Registro: {{ $tecnico->num_registro }}</p>
                <p>Nome: {{ $tecnico->nome }}</p>
                <p>Descrição: {{ $tecnico->descricao }}</p>
            </div>
           <!-- <div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/tecnico_edita/{{ base64_encode($tecnico->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar Tecnico"><i class="material-icons">description</i></a>

            </div>-->
        </div>
        <div class="row">
            <div class="col s6">
                <p class="green-text text-darken-3">Endereço:</p>
                <p>{{ $tecnico->rua }}, {{ $tecnico->numero }}  {{ $tecnico->complemento }} - {{ $tecnico->bairro }}<br>
                    {{ $tecnico->cidade }} - {{ $tecnico->estado }}<br>
                    CEP: {{ $tecnico->cep }}
                </p>
            </div>
            <div class="col s6">
                <p class="green-text text-darken-3">Dados de Contato:</p>
                <p>Telefone: {{ $tecnico->telefone }}</p>
                <p>Email: {{ $tecnico->email }}</p>
            </div>
        </div>
    </div>
@endsection