@extends('iframe')
@section('title', 'Visualizar usuário')
@section('content')


    <div class="container iframe-corpo">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold corbranca">{{ $user->nome }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p><span class="green-text text-darken-3">Nome: </span>{{ $user->nome }}</p>
                <p><span class="green-text text-darken-3">Função: </span>{{ $user->funcao }}</p>
                <p><span class="green-text text-darken-3">Email: </span>{{ $user->email }}</p>
                <p><span class="green-text text-darken-3">Nome de Usuário: </span> {{ $user->username }}</p>
            </div>
           <!-- <div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/user_edita/{{ base64_encode($user->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar Cliente"><i class="material-icons">description</i></a>

            </div> -->
        </div>
    </div>
@endsection