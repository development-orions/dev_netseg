@extends('iframe')
@section('title', 'Visualizar Venda')
@section('content')


    <div class="container">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Venda {{ $venda->id }} - ( <span class="font12">{{ $venda->status->nome }}</span> )</p>
            </div>
        </div>
        @if($venda->status->id == 3)
            <div class="row">
                <div class="col s12 padding grey lighten-3">
                    <p class="red-text text-darken-1"><span class="red-text text-darken-2">CANCELADA:</span> {{ $venda->motivo }}</p>
                </div>
            </div>
        @endif
        <div class="row iframe-p">
            <div class="col s6">
                <p>Data: {{ $venda->created_at }}</p>
                <p>Realizado por: {{ $venda->users['nome'] }}</p>
                <br>
            </div>
            <div class="col s6">
                <p>Data de Fechamento: {{ $venda->fechamento }}</p>
                <p>Data prevista de Entrega: {{ $venda->entrega }}</p>
                <p>{{ $venda->valoradministrativo != null ? 'Valor pagamento ao Administrativo: '.$venda->valoradministrativo : '' }}</p>
                <br>
            </div>

            @if(sizeof($tecnicos) != 0)
                <div class="col s12">
                    <table>
                        <thead>
                        <tr>
                            <th>Ténico</th>
                            <th>Valor pagamento</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tecnicos as $tecnico)
                            <tr>
                                <td>{{ $tecnico->tecnicos['nome'] }}</td>
                                <td>{{ $tecnico->valor }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            @if(sizeof($medicos) != 0)
                <div class="col s12">
                    <table>
                        <thead>
                        <tr>
                            <th>Médico</th>
                            <th>Valor pagamento</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($medicos as $medico)
                            <tr>
                                <td>{{ $medico->medicos['nome'] }}</td>
                                <td>{{ $medico->valor }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

        </div>
        <hr>
        <div class="row iframe-p">
            <div class="col s12">
                <h5>Cliente</h5>
                <p>{{ $venda->clientes['nome'] }}</p>
                <p>CNPJ: {{ $venda->clientes['cnpj'] }}</p>
                <p class="address">{{ $venda->clientes['rua'] }}, {{ $venda->clientes['numero'] }}, {{ $venda->clientes['bairro'] }}, {{ $venda->clientes['cidade'] }} - {{ $venda->clientes['estado'] }} <br> CEP: {{ $venda->clientes['cep'] }}</p>
                <p>Responsável: {{ $venda->clientes['responsavel'] }}</p>
                <p class="email"><a href="mailto:{{ $venda->clientes['email_responsavel'] }}">{{ $venda->clientes['email_responsavel'] }}</a></p>
            </div>
        </div>
        <hr>
        <div class="row padding">
            <div class="col s12 padding">
                <table class="striped">
                    <thead>
                    <tr>
                        <th class="no">#</th>
                        <th class="desc">SERVIÇO</th>
                        <th class="unit">CARGA <br>HORÁRIA</th>
                        <th>Nº <br> PARTIC.</th>
                        <th class="unit">PERÍODO</th>
                        <th>HORÁRIO</th>
                        <th class="total">VALOR</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $n = 1; ?>
                    @foreach($venda->itens as $servico)
                        <tr>
                            <td class="no">{{ $n }}</td>
                            <td class="desc">
                                <h5>{{ $servico->servicos['nome'] }}</h5>
                                <?php
                                if(isset($servico['servico']) && !empty($servico['servico'])){
                                $data = explode(',', $servico['servico']); ?>
                                Serviços Inclúsos:
                                <?php foreach ($data as $d){ ?>
                                <?= $d ?><br>
                                <?php } ?>
                                <br>
                                <?php
                                }
                                ?>
                                {!! $servico['descricao'] !!}
                            </td>
                            <td class="unit">{{ $servico['cargahoraria'] }}</td>
                            <td>{{ $servico['participantes'] }}</td>
                            <td class="unit">{{ $servico['periodo'] }}</td>
                            <td>{{ $servico['horario'] }}</td>
                            <td class="total">{{ $servico['valor'] }}</td>
                        </tr>
                        <?php $n++; ?>
                    @endforeach
                    <tr class="total">
                        <td colspan="6">TOTAL</td>
                        <td class="green-text text-darken-3">{{ $venda->valor }}</td>
                    </tr>
                    </tbody>
                </table>
                <hr>
            </div>
            <div class="col s12 padding">
                <h5>Pagamento:</h5>
                <div class="notice">Forma de pagamento: {{ $venda->formapagamentos['nome'] }}</div>
                @if($venda->formapagamentos['id'] == 1)
                    <div>Núm. Boleto: {{ $venda->num_boleto }}</div>
                @endif
                @if($venda->temnota != 0)
                    <div>Nota fiscal: {{ $venda->nf }}</div>
                @endif
                <hr>
            </div>

            <div class="col s12 padding">
                @if(isset($venda->parcela) && !empty($venda->parcela))
                    <h5>Condições de Pagamento:</h5>
                    <div class="notice">{{ $venda->parcela }}</div>

                @else
                    <table class="striped">
                        <thead>
                        <tr>
                            <th class="no">PARCELAS</th>
                            <th>DATA</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $venda->parcelas as $parcelas)
                            <tr>
                                <td class="no">0{{ $parcelas['numero'] }}</td>
                                <td>{{ $parcelas['data'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection