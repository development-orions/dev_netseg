@extends('iframe')
@section('title', 'Visualizar Fornecedor')
@section('content')


    <div class="container iframe-corpo">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold corbranca">{{ $fornecedor->razaosocial }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p>CNPJ/CPF: {{ isset($fornecedor->cnpj)? $fornecedor->cnpj: $fornecedor->cpf }}</p>
                <p>IE: {{ $fornecedor->ie }}</p>
                <p>CEI/Autonomo: {{ $fornecedor->cei_autonomo }}</p>
                <p>Nome: {{ $fornecedor->nome }}</p>
                <p>Razão Social: {{ $fornecedor->razaosocial }}</p>
            </div>
            <!--<div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="{{ Route::getCurrentRoute()->getPrefix() }}/fornecedor_edita/{{ base64_encode($fornecedor->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar Cliente"><i class="material-icons">description</i></a>

            </div>-->
        </div>
        <div class="row">
            <div class="col s6">
                <p class="green-text text-darken-3">Endereço:</p>
                <p>{{ $fornecedor->rua }}, {{ $fornecedor->numero }}  {{ $fornecedor->complemento }} - {{ $fornecedor->bairro }}<br>
                    {{ $fornecedor->cidade }} - {{ $fornecedor->estado }}<br>
                    CEP: {{ $fornecedor->cep }}
                </p>
            </div>
            <div class="col s6">
                <p class="green-text text-darken-3">Dados do Responsável:</p>
                <p>Nome: {{ $fornecedor->responsavel }}</p>
                <p>Função: {{ $fornecedor->funcao }}</p>
                <p>Telefone: {{ $fornecedor->telefone_responsavel }}</p>
                <p>Email: {{ $fornecedor->email_responsavel }}</p>
            </div>
        </div>
    </div>
@endsection