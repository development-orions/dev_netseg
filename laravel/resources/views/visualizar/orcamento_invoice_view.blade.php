@extends('iframe')
@section('title', 'Visualizar Orçamento')

<div class="container">
    <div class="row bg">
        <div class="col s12 bgverde">
            <p class="bold font20 corbranca">Orçamento {{ $orcamento->id }}</p>
        </div>
    </div>
    <div class="row iframe-p">
        <div class="col s6">
            <p>Data: {{ $orcamento->created_at }}</p>
            <p>Realizado por: {{ $orcamento->users['nome'] }}</p>
            <br>
        </div>

    </div>
    <hr>
    <div class="row iframe-p">
        <div class="col s12">
            <h5>Cliente</h5>
            <p>{{ $orcamento->clientes['nome'] }}</p>
            <p>CNPJ: {{ $orcamento->clientes['cnpj'] }}</p>
            <p class="address">{{ $orcamento->clientes['rua'] }}, {{ $orcamento->clientes['numero'] }}, {{ $orcamento->clientes['bairro'] }}, {{ $orcamento->clientes['cidade'] }} - {{ $orcamento->clientes['estado'] }} <br> CEP: {{ $orcamento->clientes['cep'] }}</p>
            <p>Responsável: {{ $orcamento->clientes['responsavel'] }}</p>
            <p class="email"><a href="mailto:{{ $orcamento->clientes['email_responsavel'] }}">{{ $orcamento->clientes['email_responsavel'] }}</a></p>
        </div>
    </div>
    <hr>
    <div class="row padding">
        <div class="col s12 padding">
            <table class="striped">
                <thead>
                <tr>
                    <th class="no">#</th>
                    <th class="desc">SERVIÇO</th>
                    <!--<th class="unit">CARGA <br>HORÁRIA</th>-->
                    <th>Nº <br> PARTIC.</th>
                    <th class="unit">PERÍODO</th>
                    <th>HORÁRIO</th>
                    <th class="total">VALOR</th>
                </tr>
                </thead>
                <tbody>
                <?php $n = 1; ?>
                @foreach($orcamento->itens as $servico)
                    <tr>
                        <td class="no">{{ $n }}</td>
                        <td class="desc">
                            <h5>{{ $servico->servicos['nome'] }}</h5>
                            <?php
                            if(isset($servico['servico']) && !empty($servico['servico'])){
                            $data = explode(',', $servico['servico']); ?>
                            Serviços Inclúsos:
                            <?php foreach ($data as $d){ ?>
                            <?= $d ?><br>
                            <?php } ?>
                            <br>
                            <?php
                            }
                            ?>
                            {!! $servico['descricao'] !!}
                        </td>
                    <!--<td class="unit">{{ $servico['cargahoraria'] }}</td>-->
                        <td>{{ $servico['participantes'] }}</td>
                        <td class="unit">{{ $servico['periodo'] }}</td>
                        <td>{{ $servico['horario'] }}</td>
                        <td class="total">{{ $servico['valor'] }}</td>
                    </tr>
                    <?php $n++; ?>
                @endforeach
                <tr class="total">
                    <td colspan="5">TOTAL</td>
                    <td class="green-text text-darken-3">{{ $orcamento->valor }}</td>
                </tr>
                </tbody>
            </table>
            <hr>
        </div>
        <div class="col s12 padding">
            <h5>Pagamento:</h5>
            <div class="notice">Forma de pagamento: {{ $orcamento->formapagamentos['nome'] }}</div>
            @if($orcamento->formapagamentos['id'] == 1)
                <div>Núm. Boleto: {{ $orcamento->num_boleto }}</div>
            @endif
            <hr>
        </div>

        <div class="col s12 padding">
            @if(isset($orcamento->parcela) && !empty($orcamento->parcela))
                <h5>Condições de Pagamento:</h5>
                <div class="notice">{{ $orcamento->parcela }}</div>

            @else
                <table class="striped">
                    <thead>
                    <tr>
                        <th class="no">PARCELAS</th>
                        <th>DATA</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $orcamento->parcelas as $parcelas)
                        <tr>
                            <td class="no">0{{ $parcelas['numero'] }}</td>
                            <td>{{ $parcelas['data'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>