@extends('iframe')
@section('title', 'Visualizar Paciente')
@section('content')


    <div class="container iframe-corpo">
        <div class="row bg">
            <div class="col s12 bgverde">
                <p class="bold corbranca">{{ $paciente->nome }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <p><span class="green-text text-darken-3">Paciente desde:</span> {{ $paciente->created_at }}</p>
                <p><span class="green-text text-darken-3">Núm. Prontuário:</span> {{ $paciente->id }}</p>
            </div>
            <!--<div class="col s2">
                <a onclick="parent.$.fn.colorbox.close();" target="_top" href="/paciente_edita/{{ base64_encode($paciente->id) }}" class="btn-floating waves-effect waves-light blue darken-1" title="Editar Paciente"><i class="material-icons">description</i></a>

            </div>-->
        </div>
        <div class="row">
            <div class="col s10">
                <p>Nome: {{ $paciente->nome }}</p>
                <p>RG: {{ $paciente->rg }}</p>
                <p>CPF: {{ $paciente->cpf }}</p>
                <p>Data de Nascimento: {{ $paciente->datanascimento }}</p>
                <p>Sexo: {{ $paciente->sexo }}</p>
                <p>Estado Civil: {{ $paciente->estadocivil }}</p>
                <p>Grau de Instrução: {{ $paciente->grau_instrucao }}</p>
                <p>Função: {{ $paciente->funcao }}</p>
                <p>Setor: {{ $paciente->setor }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col s6">
                <p class="green-text text-darken-3">Endereço:</p>
                <p>{{ $paciente->rua }}, {{ $paciente->numero }}  {{ $paciente->complemento }} - {{ $paciente->bairro }}<br>
                    {{ $paciente->cidade }} - {{ $paciente->estado }}<br>
                    CEP: {{ $paciente->cep }}
                </p>
            </div>
            <div class="col s6">
                <p class="green-text text-darken-3">Dados de Contato:</p>
                <p>Telefone: {{ $paciente->telefone }}</p>
                <p>Email: {{ $paciente->email }}</p>
            </div>
        </div>
    </div>
@endsection