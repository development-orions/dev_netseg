@extends('app')
@section('title', 'Listar Laboratorios')
@section('content')

    <script type="text/javascript" src="/js/validador/delete.js"></script>

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Laboratorios</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                       <!-- <th>Valor</th>-->
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($laboratorios as $laboratorio)
                        <tr>
                            <td>{{ $laboratorio->nome }}</td>
                           <!-- <td>R$ {{ str_replace('.', ',', $laboratorio->valor )}}</td>-->
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/laboratorio_edita/{{ base64_encode($laboratorio->id) }}" title="Editar laboratorio" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>

                                        <li><a href="#" data-href="{{ Route::getCurrentRoute()->getPrefix() }}/exame_delete/{{ base64_encode($laboratorio->id) }}" data-text="Tem certeza que deseja excluir este Laboratório?" title="Excluir laboratório" class="btn-floating red delete"><i class="fa fa-times"></i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">Nenhum laboratorio encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection