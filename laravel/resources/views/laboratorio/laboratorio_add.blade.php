@extends('app')
@section('title', 'Cadastrar Laboratorio')
@section('content')
    @if(isset($laboratorio_edit))
        <script type="text/javascript" src="/js/validador/laboratorio_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/laboratorio.js"></script>
    @endif


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Laboratorios</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                @if(isset($laboratorio_edit))
                    {!! Form::hidden('id', (isset($laboratorio_edit['id']) ? $laboratorio_edit['id']:''), ['id' => 'id']) !!}

                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($laboratorio_edit['nome']) ? $laboratorio_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
            </div>
            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($laboratorio_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($laboratorio_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
