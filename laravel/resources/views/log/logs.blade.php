@extends('app')
@section('title', 'Logs do Sistema')
@section('content')
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Logs do Sistema</p>
            </div>
        </div>
        <div class="row buscar">
            {!! Form::open(['class' => 'col s12', 'method' => 'GET']) !!}
            <div class="row">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/logs" class="waves-effect waves-light btn">Limpar</a>
                <button class="btn waves-effect waves-light" type="submit">
                    <i class="material-icons right">search</i>
                </button>
                <div class="input-field col m2 s3">
                    {!! Form::label('fim', 'Data final: ') !!}
                    {!! Form::text('fim', (isset($data['fim']) ? $data['fim']:''), ['class' => 'datepicker picker__input', 'id' => 'fim', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
                <div class="input-field col m2 s3">
                    {!! Form::label('inicio', 'Data inicial: ') !!}
                    {!! Form::text('inicio', (isset($data['inicio']) ? $data['inicio']:''), ['class' => 'datepicker picker__input', 'id' => 'inicio', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <table class="bordered highlight">
                <thead>
                <tr>
                    <th>Data/Hora</th>
                    <th>Log</th>
                </tr>
                </thead>

                <tbody>
                @forelse($logs as $log)
                    <tr>
                        <td>{{ $log->created_at }}</td>
                        <td>{{ $log->log }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">Nenhum resultado encontrado.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="col s12 paginacao">
            <?php echo $logs->appends(Request::all())->render(); ?>
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection