@extends('app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Posts</div>

					<div class="panel-body">
						lista de post
						@forelse($posts as $post)
							<p>Titulo: {{ $post->titulo }}</p>
							<p>Conteudo {{ $post->conteudo }}</p>
						@empty
							<p>Nenhum post cadastrado.</p>
						@endforelse
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
