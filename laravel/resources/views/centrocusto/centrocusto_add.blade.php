@extends('app')
@section('title', 'Cadastrar Centro de Custo')
@section('content')
    @if(isset($centro_edit))
        <script type="text/javascript" src="/js/validador/centrocusto_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/centrocusto.js"></script>
    @endif


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Centro de Custo</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                @if(isset($centro_edit))
                    {!! Form::hidden('id', (isset($centro_edit['id']) ? $centro_edit['id']:''), ['id' => 'id']) !!}

                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($centro_edit['nome']) ? $centro_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
            </div>

            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($centro_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($centro_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
