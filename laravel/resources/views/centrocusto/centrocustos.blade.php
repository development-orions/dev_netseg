@extends('app')
@section('title', 'Listar Centros de Custo')
@section('content')

    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Centros de Custo</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($centros as $centro)
                        <tr>
                            <td>{{ $centro->nome }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/centrocusto_edita/{{ base64_encode($centro->id) }}" title="Editar centro de custo" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">Nenhum centro de custo encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection