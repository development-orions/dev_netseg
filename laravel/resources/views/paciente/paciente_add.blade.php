@extends('app')
@section('title', 'Cadastrar Paciente')
@section('content')
    @if(isset($paciente_edit))
        <script type="text/javascript" src="/js/validador/paciente_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/paciente.js"></script>
    @endif

    <script type="text/javascript" src="/js/core2.js"></script>
    <link rel="stylesheet" href="/css/colorbox.css" type="text/css">
    <script src="/js/jquery.colorbox.js" type="text/javascript"></script>
    <script type="text/javascript"  >
        function getEndereco(cep) {
            // Se o campo CEP não estiver vazio
            if($.trim(cep) != ""){
                $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){
                    if(resultadoCEP["resultado"]){
                        $("#rua").val(unescape(resultadoCEP["logradouro"]));
                        $("#bairro").val(unescape(resultadoCEP["bairro"]));
                        $("#cidade").val(unescape(resultadoCEP["cidade"]));
                        $("#estado").val(unescape(resultadoCEP["uf"]));
                    }
                });
            }
        }
        $(document).ready(function() {
            $("#cep").blur(function(){
                getEndereco($("#cep").val());
            });
            $('#resultado').hide();
        });

        $('.datepicker').pickadate({
            selectMonths: true // Creates a dropdown to control month
        });

        function seleciona(id, razaosocial, cnpj){
            $('#res').empty();
            $('#resultadop').hide();
            $('#cliente_id').val(id);
            $('#busca').val(razaosocial).click(function(){
                $(this).val('').unbind('click');
            });
            $('#mostra').html('<h5>Cliente</h5><p>Razão Social: '+ razaosocial +'</p><p>CNPJ: '+ cnpj +'</p>');
        }


        /// busca por cliente
        function pesquisa(event){
            valor = $('#busca').val();
            if (valor == '' ){
                $('#res').empty();
                $('#resultado').hide();
                return false
            }

            $.ajax({
                url: '/buscarcliente/'+valor,
                type: "GET",
                dataType: "json",
                success: function(sucesso){
                    $('#res').empty();
                    $('#resultado').show();
                    $.each(sucesso, function(i, item){
                        $('#res').append('<li><a href="#" onClick="seleciona(\''+item.id+'\',\''+item.razaosocial+'\', \''+item.cnpj+'\')" title="'+item.razaosocial+' - '+item.cnpj+'"><span>'+item.razaosocial+' - '+item.cnpj+'</span></a></li>');
                    });
                    if(jQuery.isEmptyObject(sucesso)){
                        $('#resultado').hide();
                    }
                },
                error: function(){
                    $('#res').empty();
                    $('#resultado').hide();
                }
            });
        }
    </script><!-- === chamada de endereço por CEP === -->
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Paciente</p>
            </div>
        </div>
        <div class="row">
            <div class="row padding">
                <div class="col s12">
                    <form class="col s12">
                        <div class="input-field col s12 m6">
                            <input type="text" name="busca" id="busca" onKeyUp="pesquisa(event)" autocomplete="off">
                            <label for="busca">Novo Cliente</label>
                        </div>
                    </form>
                </div>
                <div class="col s12">
                    <div id="resultado"><ul class="list_json" id="res"></ul></div>
                </div>
            </div>
            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row padding">
                    {!! Form::hidden('cliente_id', isset($paciente_edit['cliente_id']) ? $paciente_edit['cliente_id']:'' , ['id' => 'cliente_id']) !!}
                <div class="col m6" id="mostra">
                    <h5>Cliente</h5>
                    <p>Razão Social: {{ (isset($paciente_edit) ? $paciente_edit->clientes['razaosocial'] : '') }} </p>
                    <p>CNPJ: {{ (isset($paciente_edit) ? $paciente_edit->clientes['cnpj']: '') }} </p>
                </div>
            </div>
            <div class="row">


                @if(isset($paciente_edit))
                    {!! Form::hidden('id', (isset($paciente_edit['id']) ? $paciente_edit['id']:''), ['id' => 'id']) !!}
                    {!! Form::hidden('statu_id', (isset($paciente_edit['statu_id']) ? $paciente_edit['statu_id']:'')) !!}
                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($paciente_edit['nome']) ? $paciente_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('rg', (isset($paciente_edit['rg']) ? $paciente_edit['rg']:''), ['class' => 'validate', 'id' => 'rg']) !!}
                    {!! Form::label('rg', '* RG: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('cpf', (isset($paciente_edit['cpf']) ? $paciente_edit['cpf']:''), ['class' => 'validate', 'id' => 'cpf', 'maxlength' => '14']) !!}
                    {!! Form::label('cpf', '* CPF: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::label('datanascimento', 'Data de Nascimento: ') !!}
                    {!! Form::text('datanascimento', (isset($paciente_edit['datanascimento']) ? $paciente_edit['datanascimento']:''), ['class' => 'datepicker picker__input', 'id' => 'datanascimento', 'tabindex' => -1, 'aria-expanded' => 'false', 'aria-readonly' => 'false', 'aria-haspopup' => 'true', 'readonly' => '']) !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    <select name="sexo" id="sexo">
                        <option value="Feminino" {{ isset($paciente_edit['sexo']) ? $paciente_edit['sexo'] == 'Feminino' ? 'selected' : '' : '' }} >Feminino</option>
                        <option value="Masculino" {{ isset($paciente_edit['sexo']) ? $paciente_edit['sexo'] == 'Masculino' ? 'selected' : '' : '' }}>Masculino</option>
                    </select>
                    {!! Form::label('sexo', 'Sexo: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    <select name="estadocivil" id="estadocivil">
                        <option value="Solteiro" {{ isset($paciente_edit['estadocivil']) ? $paciente_edit['estadocivil'] == 'Solteiro' ? 'selected' : '' : '' }} >Solteiro(a)</option>
                        <option value="Casado" {{ isset($paciente_edit['estadocivil']) ? $paciente_edit['estadocivil'] == 'Casado' ? 'selected' : '' : '' }}>Casado(a)</option>
                        <option value="Separado" {{ isset($paciente_edit['estadocivil']) ? $paciente_edit['estadocivil'] == 'Separado' ? 'selected' : '' : '' }}>Separado(a)</option>
                        <option value="Divorciado" {{ isset($paciente_edit['estadocivil']) ? $paciente_edit['estadocivil'] == 'Divorciado' ? 'selected' : '' : '' }}>Divorciado(a)</option>
                        <option value="Viúvo" {{ isset($paciente_edit['estadocivil']) ? $paciente_edit['estadocivil'] == 'Viúvo' ? 'selected' : '' : '' }}>Viúvo(a)</option>
                    </select>
                    {!! Form::label('estadocivil', 'Estado Civil: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('naturalidade', (isset($paciente_edit['naturalidade']) ? $paciente_edit['cpf']:''), ['class' => 'validate', 'id' => 'naturalidade']) !!}
                    {!! Form::label('naturalidade', 'Naturalidade: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    <select name="grau_instrucao" id="grau_instrucao">
                        <option value="Analfabeto" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Analfabeto' ? 'selected' : '' : '' }} >Analfabeto</option>
                        <option value="Até o 5º ano incompleto" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Até o 5º ano incompleto' ? 'selected' : '' : '' }}>Até o 5º ano incompleto</option>
                        <option value="5º ano completo do Ensino Fundamental" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == '5º ano completo do Ensino Fundamental' ? 'selected' : '' : '' }}>5º ano completo do Ensino Fundamental</option>
                        <option value="6º ao 9º ano do Ensino Fundamental incompleto" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == '6º ao 9º ano do Ensino Fundamental incompleto' ? 'selected' : '' : '' }}>6º ao 9º ano do Ensino Fundamental incompleto</option>
                        <option value="Ensino Fundamental completo" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Ensino Fundamental completo' ? 'selected' : '' : '' }}>Ensino Fundamental completo</option>
                        <option value="Ensino Médio incompleto" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Ensino Médio incompleto' ? 'selected' : '' : '' }}>Ensino Médio incompleto</option>
                        <option value="Ensino Médio completo" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Ensino Médio completo' ? 'selected' : '' : '' }}>Ensino Médio completo</option>
                        <option value="Superior incompleto" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Superior incompleto' ? 'selected' : '' : '' }}>Superior incompleto</option>
                        <option value="Superior completo" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Superior completo' ? 'selected' : '' : '' }}>Superior completo</option>
                        <option value="Mestrado" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Mestrado' ? 'selected' : '' : '' }}>Mestrado</option>
                        <option value="Doutorado" {{ isset($paciente_edit['grau_instrucao']) ? $paciente_edit['grau_instrucao'] == 'Doutorado' ? 'selected' : '' : '' }}>Doutorado</option>
                    </select>
                    {!! Form::label('grau_instrucao', 'Grau de Instrução: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('funcao', (isset($paciente_edit['funcao']) ? $paciente_edit['funcao']:''), ['class' => 'validate', 'id' => 'funcao']) !!}
                    {!! Form::label('funcao', '* Função: ') !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('setor', (isset($paciente_edit['setor']) ? $paciente_edit['setor']:''), ['class' => 'validate', 'id' => 'setor']) !!}
                    {!! Form::label('setor', '* Setor: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::email('email', (isset($paciente_edit['email']) ? $paciente_edit['email']:''), ['class' => 'validate', 'id' => 'email']) !!}
                    {!! Form::label('email', 'Email: ', ['data-error' => 'Email inválido']) !!}
                </div>
                <div class="input-field col m6 s12">
                    {!! Form::text('telefone', (isset($paciente_edit['telefone']) ? $paciente_edit['telefone']:''), ['class' => 'validate', 'id' => 'telefone', 'maxlength' => '15']) !!}
                    {!! Form::label('telefone', 'Telefone: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m2 s4">
                    {!! Form::text('cep', (isset($paciente_edit['cep']) ? $paciente_edit['cep']:''), ['class' => 'validate', 'id' => 'cep', 'maxlength' => '10']) !!}
                    {!! Form::label('cep', 'CEP: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('rua', (isset($paciente_edit['rua']) ? $paciente_edit['rua']:''), ['class' => 'validate', 'id' => 'rua']) !!}
                    {!! Form::label('rua', 'Rua: ') !!}
                </div>
                <div class="input-field col m2 s6">
                    {!! Form::text('numero', (isset($paciente_edit['numero']) ? $paciente_edit['numero']:''), ['class' => 'validate', 'id' => 'numero']) !!}
                    {!! Form::label('numero', 'Número: ') !!}
                </div>
                <div class="input-field col m4 s6">
                    {!! Form::text('complemento', (isset($paciente_edit['complemento']) ? $paciente_edit['complemento']:'')) !!}
                    {!! Form::label('complemento', 'Complemento: ') !!}
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 s12">
                    {!! Form::text('bairro',(isset($paciente_edit['bairro']) ? $paciente_edit['bairro']:''), ['class' => 'validate', 'id' => 'bairro']) !!}
                    {!! Form::label('bairro', 'Bairro: ') !!}
                </div>
                <div class="input-field col m4 s8">
                    {!! Form::text('cidade', (isset($paciente_edit['cidade']) ? $paciente_edit['cidade']:''), ['class' => 'validate', 'id' => 'cidade']) !!}
                    {!! Form::label('cidade', 'Cidade: ') !!}
                </div>
                <div class="input-field col m2 s4">
                    {!! Form::text('estado', (isset($paciente_edit['estado']) ? $paciente_edit['estado']:''), ['class' => 'validate', 'id' => 'estado']) !!}
                    {!! Form::label('estado', 'Estado: ') !!}
                </div>
            </div>


            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($paciente_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($paciente_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
