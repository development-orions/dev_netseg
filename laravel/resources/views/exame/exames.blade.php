@extends('app')
@section('title', 'Listar Exames')
@section('content')

    <script type="text/javascript" src="/js/validador/delete.js"></script>
    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Lista de Exames</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12 right-align">
                <a href="{{ Route::getCurrentRoute()->getPrefix() }}/exames_pdf" target="_blank" class="waves-effect waves-light btn btn-lh"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Gerar PDF</a>
            </div>
            <div class="col s12">
                <table class="bordered highlight">
                    <thead>
                    <tr>
                        <th>Nome</th>
                       <th>Valor</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($exames as $exame)
                        <tr>
                            <td>{{ $exame->nome }}</td>
                            <td>{{ $exame->valor_r }}</td>
                            <td>
                                <div class="fixed-action-btn horizontal">
                                    <a class="btn-floating btn-sm green">
                                        <i class="large mdi-navigation-menu"></i>
                                    </a>
                                    <ul class="opcoes">
                                        <li><a href="{{ Route::getCurrentRoute()->getPrefix() }}/exame_edita/{{ base64_encode($exame->id) }}" title="Editar exame" class="btn-floating blue darken-1"><i class="material-icons">description</i></a></li>

                                        <li><a href="#" data-href="{{ Route::getCurrentRoute()->getPrefix() }}/exame_delete/{{ base64_encode($exame->id) }}" data-text="Tem certeza que deseja excluir este exame?" title="Excluir exame" class="btn-floating red delete"><i class="fa fa-times"></i></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">Nenhum exame encontrado.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection