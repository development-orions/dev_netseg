@extends('app')
@section('title', 'Cadastrar Exame')
@section('content')
    @if(isset($exame_edit))
        <script type="text/javascript" src="/js/validador/exame_edit.js"></script>
    @else
        <script type="text/javascript" src="/js/validador/exame.js"></script>
    @endif


    <div class="container">
        <div class="row  bg">
            <div class="col s12 bgverde">
                <p class="bold font20 corbranca">Cadastro de Exames</p>
            </div>
        </div>
        <div class="row">

            {!! Form::open(['class' => 'col s12']) !!}
            <input type="hidden" id="prefixo" value="{{ Route::getCurrentRoute()->getPrefix() }}">
            <div class="row">

                @if(isset($exame_edit))
                    {!! Form::hidden('id', (isset($exame_edit['id']) ? $exame_edit['id']:''), ['id' => 'id']) !!}

                @endif

                <div class="input-field col m6 s12">
                    {!! Form::text('nome', (isset($exame_edit['nome']) ? $exame_edit['nome']:''), ['class' => 'validate', 'id' => 'nome']) !!}
                    {!! Form::label('nome', '* Nome: ') !!}
                </div>
                    <div class="input-field col m6 s12">
                        {!! Form::text('valor', (isset($exame_edit['valor']) ? $exame_edit['valor']:''), ['class' => 'validate', 'id' => 'valor']) !!}
                        {!! Form::label('valor', '* Valor: ') !!}
                    </div>
            </div>
            <div class="row right-align">

                <button class="btn waves-effect waves-light" type="submit" id="Envia" name="action">{{ isset($exame_edit) ? 'Salvar' : 'Cadastrar'  }}
                    <i class="material-icons right">send</i>
                </button>
                @if(isset($exame_edit))
                    <a href="javascript:history.go(-1)" class="btn waves-effect waves-light grey">Cancelar Edição</a>
                @endif

            </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div id="modal1" class="modal"></div>
@endsection
