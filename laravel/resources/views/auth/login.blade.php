@extends('login')

@section('content')
        <div class="row">
            <div class="col s12 border-login">
                <p class="bold font20 green-text">Acesso</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Atenção!</strong> Erro ao efetuar o login.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form role="form" method="POST" action="{{ url('/auth/login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="col s12">
                        <div class="input-field col s12">
                            <input type="text" id="username" class="validate" name="username" value="{{ old('username') }}" required>
                            <label for="username">Nome de usuário</label>
                        </div>

                        <div class="input-field col s12">
                            <input type="password" id="password" class="validate" name="password" required>
                            <label for="password">Senha</label>
                        </div>

                        <div class="input-field col s12">
                            <button class="btn btn-login waves-effect waves-light" type="submit" name="action">Acessar <i class="material-icons right">send</i> </button>
                            <div class="btn-flat waves-effect  waves-light">
                                <input type="checkbox" class="filled-in" id="filled-in-box" />
                                <label for="filled-in-box">Lembre-me</label>
                            </div>
                        </div>
                        <div class="input-field col s12">
                            <a class="green-text" href="{{ url('/password/email') }}">Esqueceu sua senha?</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
@endsection
