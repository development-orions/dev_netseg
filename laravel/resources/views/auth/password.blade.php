@extends('login')

@section('content')
    <div class="row">
        <div class="col s12 border-login">
            <p class="bold font20 green-text">Redefinir senha</p>
        </div>
    </div>
	<div class="row">
		<div class="col s12">
			<div class="panel panel-default">
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="input-field col s10">
                            <input type="email" id="email" class="validate" name="email" value="{{ old('email') }}">
                            <label for="email">Email</label>
                        </div>

                        <div class="input-field col s10">

                            <button class="btn waves-effect waves-light" type="submit" name="action">Enviar <i class="material-icons right">send</i> </button>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
