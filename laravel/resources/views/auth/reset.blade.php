@extends('login')

@section('content')
    <div class="row">
        <div class="col s12 border-login">
            <p class="bold font20 green-text">Redefinir senha</p>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="col s12" role="form" method="POST" action="{{ url('/password/reset') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                        <label for="email">E-mail</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input id="pass" type="password" class="validate" name="password">
                        <label for="pass">Senha</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="confirmpass" type="password" class="validate" name="password_confirmation">
                        <label for="confirmpass">Confirmar Senha</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <button type="submit" class="btn waves-effect waves-light">
                            Redefinir senha <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection