import VueResource from 'vue-resource'
import VueHighcharts from 'vue-highcharts'
import Exporting from 'highcharts/modules/exporting'
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Highcharts from 'highcharts'

require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Exporting(Highcharts);
Vue.use(VueResource);
Vue.use(VueHighcharts);


Vue.http.headers.common['Authorization'] = 'Bearer ' + window.Laravel.bearer;
Vue.http.headers.common['Content-Type'] = 'application/json';

Vue.component('example', require('./components/Example.vue'));
Vue.component('graficoresultado', require('./components/Graficoresultado.vue'));

const app = new Vue({
    el: '#app'
});