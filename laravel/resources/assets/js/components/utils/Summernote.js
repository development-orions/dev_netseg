module.exports = {

    template: '<textarea :name="name"></textarea>',

    props: {
        model: {
            required: true,
        },
        lang: {
           type: String,
            default: 'pt-BR'
        },
        name: {
            type: String,
            required: true,
        },

        height: {
            type: String,
            default: '600'
        },
        toolbar: {
            type: Array,
            required: false,
            default: function() {
                return [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'picture', 'video', 'hr']],
                    ['view', ['undo', 'redo', 'codeview']]
                ];
            }
        },
    },

    mounted() {


        let config = {
            height: this.height,
            toolbar: this.toolbar,
            lang: this.lang
        };

        let vm = this;

        config.callbacks = {

            onInit: function () {
                $(vm.$el).summernote("code", vm.model);
            },

            onChange: function () {
                vm.$emit('change', $(vm.$el).summernote('code'));
            },

            onBlur: function () {
                vm.$emit('change', $(vm.$el).summernote('code'));
            }
        };

        $(this.$el).summernote(config);

    },

}