<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;
use App\Statu;
use App\Formapagamento;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('RoleTableSeeder');
        $this->call('StatusTableSeeder');
        $this->call('FormapagamentosTableSeeder');
        $this->call('UserTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        $users = User::all();
        if($users->count() == 0) {
            User::create(array(
                'id' => 1,
                'nome' => 'Administrativo',
                'email' => 'financeiro@netsegtrabalho.com.br',
                'password' => Hash::make('admin@2012'),
                'funcao'  => 'Administrativo',
                'username'  => 'financeiro',
                'role_id'  => 2,
                'statu_id'  => 1,
            ));
        }
    }

}
class RoleTableSeeder extends Seeder {

    public function run()
    {
        $roles = Role::all();
        if($roles->count() == 0) {
            Role::create(array(
                'id' => 1,
                'nome' => 'Administrador'
            ));
            Role::create(array(
                'id' => 2,
                'nome' => 'Financeiro/Compras'
            ));
            Role::create(array(
                'id' => 3,
                'nome' => 'Qualidade'
            ));
            Role::create(array(
                'id' => 4,
                'nome' => 'Medicina'
            ));
            Role::create(array(
                'id' => 5,
                'nome' => 'Vendas'
            ));
            Role::create(array(
                'id' => 6,
                'nome' => 'Suporte'
            ));
            Role::create(array(
                'id' => 7,
                'nome' => 'TI'
            ));
            Role::create(array(
                'id' => 8,
                'nome' => 'Tecnico'
            ));

        }
    }

}
class StatusTableSeeder extends Seeder {

    public function run()
    {
        $roles = Statu::all();
        if($roles->count() == 0) {
            Statu::create(array(
                'id' => 1,
                'nome' => 'Ativo'
            ));
            Statu::create(array(
                'id' => 2,
                'nome' => 'Inativo'
            ));
            Statu::create(array(
                'id' => 3,
                'nome' => 'Cancelada'
            ));
            Statu::create(array(
                'id' => 4,
                'nome' => 'Pago'
            ));
            Statu::create(array(
                'id' => 5,
                'nome' => 'Pendente'
            ));
            Statu::create(array(
                'id' => 6,
                'nome' => 'Aguardando Aprovação'
            ));
            Statu::create(array(
                'id' => 7,
                'nome' => 'Aprovado'
            ));
            Statu::create(array(
                'id' => 8,
                'nome' => 'Em andamento'
            ));
            Statu::create(array(
                'id' => 9,
                'nome' => 'Concluído'
            ));
            Statu::create(array(
                'id' => 10,
                'nome' => 'Enviado Relatório'
            ));
            Statu::create(array(
                'id' => 11,
                'nome' => 'Enviado Email'
            ));
            Statu::create(array(
                'id' => 12,
                'nome' => 'Vencido'
            ));
        }
    }

}

class FormapagamentosTableSeeder extends Seeder {

    public function run()
    {
        $roles = Formapagamento::all();
        if($roles->count() == 0) {
            Formapagamento::create(array(
                'id' => 1,
                'nome' => 'Boleto'
            ));
            Formapagamento::create(array(
                'id' => 2,
                'nome' => 'Recibo'
            ));
            Formapagamento::create(array(
                'id' => 3,
                'nome' => 'Depósito - Netseg (conta 287-7)'
            ));
            Formapagamento::create(array(
                'id' => 4,
                'nome' => 'Depósito - Egydio (conta 1361-9)'
            ));
        }
    }

}