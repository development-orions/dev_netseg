<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medicos', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->timestamps();
			$table->string('nome');
			$table->string('crm', 45)->unique();
			$table->integer('nit');
			$table->date('datacontrato');
			$table->string('rua',100)->nullable();
			$table->string('numero',5)->nullable();
			$table->string('bairro',50)->nullable();
			$table->string('cidade',60)->nullable();
			$table->string('estado',2)->nullable();
			$table->string('cep',15)->nullable();
			$table->string('complemento',45)->nullable();
			$table->string('telefone',15)->nullable();
			$table->text('observacao')->nullable();
			$table->integer('statu_id')->unsigned();
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medicos');
	}

}
