<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('nome',255)->nullable();
			$table->string('razaosocial',255);
			$table->string('cnpj',45)->nullable()->unique();
			$table->string('ie',45);
			$table->string('rua',100);
			$table->string('numero',45);
			$table->string('bairro',60);
			$table->string('complemento',45)->nullable();
			$table->string('cidade',60);
			$table->string('estado',2);
			$table->string('cep',11);
			$table->string('formajuridica',45)->nullable();
			$table->string('responsavel',255)->nullable();
			$table->string('funcao',45)->nullable();
			$table->string('telefone_responsavel',45)->nullable();
			$table->string('email_responsavel',45)->nullable();
			$table->integer('statu_id')->unsigned();
			$table->double('valor_contrato');
			$table->date('data_contrato');
			$table->timestamps();
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}

}
