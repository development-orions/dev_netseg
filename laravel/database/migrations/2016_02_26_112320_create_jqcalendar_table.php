<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJqcalendarTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jqcalendar', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
			$table->increments('Id')->unsugned();
            $table->string('Subject',1000)->nullable();
            $table->string('Location',200)->nullable();
            $table->string('Description',250)->nullable();
            $table->dateTime('StartTime')->nullable();
            $table->dateTime('EndTime')->nullable();
            $table->integer('IsAllDayEvent');
            $table->string('Color',200)->nullable();
            $table->string('RecurringRule',500)->nullable();

            //$table->integer('ano');
            //$table->string('mes', 45);
            $table->string('exame',200)->nullable();
            $table->text('observacoes')->nullable();
            $table->integer('medico_id')->unsigned();
            $table->integer('paciente_id')->unsigned();


            $table->foreign('medico_id')->references('id')->on('medicos')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('paciente_id')->references('id')->on('pacientes')->onDelete('restrict')->onUpdate('cascade');

			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jqcalendar');
    }
}
