<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicosvendasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medicosvendas', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->double('valor');
			$table->integer('venda_id')->unsigned();
			$table->integer('medico_id')->unsigned();
			$table->foreign('venda_id')->references('id')->on('vendas')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('medico_id')->references('id')->on('medicos')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medicosvendas');
	}

}
