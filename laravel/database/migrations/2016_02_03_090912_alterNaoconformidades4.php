<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNaoconformidades4 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('naoconformidades', function(Blueprint $table)
		{
            $table->dropColumn('vigencia');
            $table->integer('ano');
            $table->integer('digito');;

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('naoconformidades', function(Blueprint $table)
		{
			//
		});
	}

}
