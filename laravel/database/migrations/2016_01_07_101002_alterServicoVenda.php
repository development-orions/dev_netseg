<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServicoVenda extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('servico_venda', function(Blueprint $table)
		{
			$table->string('frequenciavisita', 255)->nullable();
			$table->text('servicos')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('servico_venda', function(Blueprint $table)
		{
			//
		});
	}

}
