<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVendas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendas', function(Blueprint $table)
		{
			$table->integer('temnota')->default(0);
			$table->integer('user_id')->unsigned();
			$table->integer('statu_id')->unsigned();
			$table->date('fechamento')->nullable();
			$table->date('entrega')->nullable();
			$table->integer('venda_id')->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendas', function(Blueprint $table)
		{

		});
	}

}
