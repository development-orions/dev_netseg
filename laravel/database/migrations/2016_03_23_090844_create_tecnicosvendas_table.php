<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTecnicosvendasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tecnicosvendas', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->double('valor');
			$table->integer('venda_id')->unsigned();
			$table->integer('tecnico_id')->unsigned();
			$table->foreign('venda_id')->references('id')->on('vendas')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('tecnico_id')->references('id')->on('tecnicos')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tecnicosvendas');
	}

}
