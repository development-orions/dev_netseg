<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronogramasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cronogramas', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->date('vencimento');
			$table->integer('ano');
			$table->string('mes', 45);
			$table->integer('statu_id')->unsigned();
			$table->integer('cliente_id')->unsigned();
			$table->integer('tecnico_id')->unsigned();
			$table->integer('atividade_id')->unsigned();
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('tecnico_id')->references('id')->on('tecnicos')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('atividade_id')->references('id')->on('atividades')->onDelete('restrict')->onUpdate('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cronogramas');
	}

}
