<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTecnicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tecnicos', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('nome',255);
			$table->string('num_registro',45);
			$table->string('rua',100);
			$table->string('numero',5);
			$table->string('bairro',60);
			$table->string('complemento',45)->nullable();
			$table->string('cidade',60);
			$table->string('estado',2);
			$table->string('cep',11);
			$table->string('telefone',45);
			$table->string('email',100);
			$table->text('descricao')->nullable();
			$table->integer('statu_id')->unsigned();
			$table->timestamps();
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tecnicos');
	}

}
