<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNaoconformidades6 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('naoconformidades', function(Blueprint $table)
		{
            $table->integer('causaprovavel_op')->nullable();
            $table->renameColumn('fornecedor_id', 'tecnico_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('naoconformidades', function(Blueprint $table)
		{
			//
		});
	}

}
