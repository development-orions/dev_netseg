<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contas', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('nome',255);
			$table->date('vencimento');
			$table->date('baixa')->nullable();
			$table->double('valor');
			$table->string('portador',45);
			$table->text('observacao')->nullable();
			$table->double('desconto')->nullable();
			$table->double('acrescimo')->nullable();
			$table->string('nf',100);
			$table->integer('tipo');
			$table->double('num_boleto')->nullable();
			$table->timestamps();
			$table->integer('centrocusto_id')->unsigned();
			$table->integer('formapagamento_id')->unsigned();
			$table->integer('statu_id')->unsigned();
			$table->foreign('centrocusto_id')->references('id')->on('centrocustos')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('formapagamento_id')->references('id')->on('formapagamentos')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contas');
	}

}
