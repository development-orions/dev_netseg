<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNaoconformidades1 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('naoconformidades', function(Blueprint $table)
		{
            $table->renameColumn('vigenvia', 'vigencia');
            $table->dropColumn('datanascimento');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('naoconformidades', function(Blueprint $table)
		{
			//
		});
	}

}
