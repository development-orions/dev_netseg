<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pacientes', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('nome',255);
			$table->string('rg',45);
			$table->string('cpf',45);
			$table->date('datanascimento');
			$table->string('sexo',20)->nullable();
			$table->string('estadocivil',45)->nullable();
			$table->string('naturalidade',50)->nullable();
			$table->string('grau_instrucao',80)->nullable();
			$table->string('funcao',80);
			$table->string('setor',80);
			$table->string('email',100)->nullable();
			$table->string('rua',100)->nullable();
			$table->string('numero',5)->nullable();
			$table->string('bairro',50)->nullable();
			$table->string('cidade',60)->nullable();
			$table->string('estado',2)->nullable();
			$table->string('cep',15)->nullable();
			$table->string('complemento',45)->nullable();
			$table->string('telefone',15)->nullable();
			$table->timestamps();
			$table->integer('statu_id')->unsigned();
			$table->integer('cliente_id')->unsigned();
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pacientes');
	}

}
