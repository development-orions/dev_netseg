<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNaoconformidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('naoconformidades', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';

			$table->increments('id')->unsigned();

            $table->string('codigo',100)->unique();
            $table->integer('revisao');
            $table->string('vigenvia');
            $table->date('datanascimento');
            $table->string('numero', 45);
            $table->integer('realpotencial');
            $table->integer('materialsistema');

            $table->string('ocorrencia',255);
            $table->string('ocorrencia_responsavel',100)->nullable();
            $table->date('ocorrencia_data')->nullable();

            $table->string('acaoimediata',255)->nullable();
            $table->string('acaoimediata_responsavel',100)->nullable();
            $table->date('acaoimediata_data')->nullable();

            $table->string('causaprovavel',255)->nullable();
            $table->string('causaprovavel_responsavel',100)->nullable();
            $table->date('causaprovavel_data')->nullable();


            $table->string('acaocorretiva',255)->nullable();
            $table->string('acaocorretiva_responsavel',100)->nullable();
            $table->date('acaocorretiva_prazo')->nullable();

            $table->integer('foiimplementada')->nullable();

            $table->integer('eeficaz')->nullable();
            $table->string('eficaz_observacoes',255)->nullable();
            $table->string('eficaz_responsavel',100)->nullable();
            $table->date('eficaz_data')->nullable();


            $table->integer('cliente_id')->nullable();
            $table->integer('fornecedor_id')->nullable();
            $table->integer('cronograma_id')->unsigned();

            $table->foreign('cronograma_id')->references('id')->on('cronogramas')->onDelete('restrict')->onUpdate('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('naoconformidades');
	}

}
