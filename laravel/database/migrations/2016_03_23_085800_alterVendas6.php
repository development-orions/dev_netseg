<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVendas6 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendas', function(Blueprint $table)
		{
			$table->dropColumn('valormedico');
			$table->dropColumn('valortecnico');
			$table->dropColumn('valorinstrutor');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendas', function(Blueprint $table)
		{
			//
		});
	}

}
