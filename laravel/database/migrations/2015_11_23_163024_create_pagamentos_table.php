<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagamentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pagamentos', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->integer('fornecedore_id')->unsigned();
			$table->integer('conta_id')->unsigned();
			$table->foreign('fornecedore_id')->references('id')->on('fornecedores')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('conta_id')->references('id')->on('contas')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pagamentos');
	}

}
