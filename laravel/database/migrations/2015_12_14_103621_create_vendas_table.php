<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vendas', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->timestamps();
			$table->integer('orcamento')->default(0);
			$table->integer('aprovado')->default(0);
			$table->date('validade');
			$table->string('nf', 255)->nullable();
			$table->string('num_boleto', 255)->nullable();
			$table->integer('cliente_id')->unsigned();
			$table->integer('formapagamento_id')->unsigned();
			$table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('formapagamento_id')->references('id')->on('formapagamentos')->onDelete('restrict')->onUpdate('cascade');
		});

		Schema::create('servico_venda', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->integer('servico_id')->unsigned()->index();
			$table->integer('venda_id')->unsigned()->index();
			$table->text('descricao');
			$table->double('valor');
			$table->string('cargahoraria', 45)->nullable();
			$table->string('participantes', 5)->nullable();
			$table->string('periodo', 45)->nullable();
			$table->string('horario', 45)->nullable();
			$table->integer('tecnico_id');
			$table->foreign('servico_id')->references('id')->on('servicos')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('venda_id')->references('id')->on('vendas')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('servico_venda');
		Schema::drop('vendas');
	}

}
