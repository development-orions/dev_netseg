<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fornecedores', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('nome',255)->nullable();
			$table->string('razaosocial',255);
            $table->string('cnpj',45)->nullable()->unique();
            $table->string('cpf',15)->nullable()->unique();
			$table->string('ie',45)->nullable();
			$table->string('rua',100)->nullable();
			$table->string('numero',45)->nullable();
			$table->string('bairro',60)->nullable();
			$table->string('complemento',45)->nullable();
			$table->string('cidade',60)->nullable();
			$table->string('estado',2)->nullable();
			$table->string('cep',11)->nullable();
			$table->string('responsavel',255)->nullable();
			$table->string('funcao',45)->nullable();
			$table->string('telefone_responsavel',45)->nullable();
			$table->string('email_responsavel',45)->nullable();
            $table->integer('statu_id')->unsigned();
			$table->string('cei_autonomo',255)->nullable();
			$table->date('datainicio_servico');
			$table->timestamps();
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fornecedores');
	}


}
