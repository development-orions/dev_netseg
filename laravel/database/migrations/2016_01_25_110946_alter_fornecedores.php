<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFornecedores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fornecedores', function(Blueprint $table)
		{
			$table->dropUnique('fornecedores_cnpj_unique');
			$table->dropUnique('fornecedores_cpf_unique');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fornecedores', function(Blueprint $table)
		{
			//
		});
	}

}
