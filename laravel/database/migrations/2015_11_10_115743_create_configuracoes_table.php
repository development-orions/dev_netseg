<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configuracoes', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('nomefantasia', 255);
			$table->string('razaosocial', 255);
			$table->string('email', 100);
			$table->string('rua', 100);
			$table->string('numero', 5);
			$table->string('bairro', 60);
			$table->string('cidade', 60);
			$table->string('estado', 2);
			$table->string('cep', 11);
			$table->string('complemento', 45)->nullable();
			$table->string('cnpj', 45);
			$table->string('ie', 45);
			$table->string('formajuridica', 255);
			$table->string('site', 255)->nullable();
			$table->text('cnae');
			$table->text('telefone');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configuracoes');
	}

}
