<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('nome');
			$table->string('email');
			$table->string('password', 60);
			$table->string('funcao', 100);
			$table->string('username', 45)->unique();
			$table->integer('role_id')->unsigned();
			$table->integer('statu_id')->unsigned();
			$table->rememberToken();
			$table->timestamps();
			$table->foreign('role_id')->references('id')->on('roles')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('statu_id')->references('id')->on('status')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
