<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Arquivo extends Model {

    protected $table = 'arquivos';


    public function arquivosclientes(){
        return $this->hasMany('App\Arquivoscliente', 'arquivo_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

}
