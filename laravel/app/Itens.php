<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Itens extends Model {

    protected $table = 'servico_venda';

    protected $fillable = ['id', 'servico_id', 'venda_id', 'descricao', 'valor', 'cargahoraria', 'participantes', 'periodo', 'horario', 'tecnico_id', 'frequenciavisita', 'servicos'];

    public function servicos(){
        return $this->belongsTo('App\Servico', 'servico_id');
    }
    public function vendas(){
        return $this->belongsTo('App\Venda', 'venda_id');
    }

    public function tecnicos(){
        return $this->belongsTo('App\Tecnico', 'tecnico_id');
    }
}
