<?php namespace App;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Auth;
use Role;
use Statu;
use Validator;

trait AuthenticatesAndRegistersUsers {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * The registrar implementation.
	 *
	 * @var Registrar
	 */
	protected $registrar;

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister()
	{
		$status = Statu::lists('nome', 'id');
		$roles = Role::lists('nome', 'id');
		return view('auth.register', compact('roles', 'status'));
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator(Request::all());

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		//$this->auth->login($this->registrar->create(Request::all()));
        $this->registrar->create(Request::all());

		//return redirect($this->redirectPath());
	}

	/**
	 * Show the application login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin()
	{
		return view('auth.login');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'username' => 'required', 'password' => 'required',
		],
			[
				'username.required' => 'O campo nome de usuário é obrigatório.',
				'password.required' => 'O campo senha é obrigatório.'
			]);

		$credentials = $request->only('username', 'password');



		if ($this->auth->attempt($credentials, $request->has('remember_token')))
		{

			if($this->auth->user()->statu_id == 1) {
				return redirect()->intended($this->redirectPath());
			}else{
				$this->auth->logout();
				return redirect($this->loginPath())
					->withInput($request->only('username', 'remember_token'))
					->withErrors([
						'username' => $this->getFailedStatusMessage(),
					]);
			}
		}

		return redirect($this->loginPath())
					->withInput($request->only('username', 'remember_token'))
					->withErrors([
						'username' => $this->getFailedLoginMessage(),
					]);
	}

	/**
	 * Get the failed login message.
	 *
	 * @return string
	 */
	protected function getFailedLoginMessage()
	{
		return 'Usuário ou senha inválido';
	}

	protected function getFailedStatusMessage()
	{
		return 'Usuário inativo no sistema.';
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{
		$this->auth->logout();

		return redirect('/auth/login');
	}

	/**
	 * Get the post register / login redirect path.
	 *
	 * @return string
	 */
	public function redirectPath()
	{

		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
	}

	/**
	 * Get the path to the login route.
	 *
	 * @return string
	 */
	public function loginPath()
	{
		return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
	}

}
