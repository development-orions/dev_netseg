<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Venda extends Model {

    protected $table = 'vendas';
    protected $dates = ['created_at'];

    public function itens(){
        return $this-> hasMany('App\Itens', 'venda_id');
    }

    public function servicos()
    {
        return $this->belongsToMany('App\Servico', 'servico_venda');
    }

    public function clientes(){
        return $this->belongsTo('App\Cliente', 'cliente_id');
    }

    public function formapagamentos(){
        return $this->belongsTo('App\Formapagamento', 'formapagamento_id');
    }

    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }

    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function parcelas(){
        return $this->hasMany('App\Parcela', 'venda_id');
    }

    public function medicosvenda(){
        return $this->hasMany('App\Medicosvenda', 'venda_id');
    }
    public function tecnicosvenda(){
        return $this->hasMany('App\Tecnicosvenda', 'venda_id');
    }

    public function gerarorcamento(){

    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
    public function getUpdatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

}
