<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Observacoe extends Model {

    protected $table = 'observacoes';

    public function cronogramas(){
        return $this->belongsTo('App\Cronograma', 'cronograma_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

}
