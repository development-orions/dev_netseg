<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Medico extends Model {

    protected $table = 'medicos';

    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }

    public function jqcalendar(){
        return $this->hasMany('App/Jqcalendar', 'medico_id');
    }

    public function medicosvenda(){
        return $this->hasMany('App\Medicosvenda', 'medico_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

}
