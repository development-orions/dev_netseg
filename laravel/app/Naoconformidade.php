<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Naoconformidade extends Model {

    protected $table = 'naoconformidades';

    public function cronogramas(){
        return $this->hasOne('App\Naoconformidade', 'cronograma_id');
    }
}
