<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Exame extends Model {

    protected $table = 'exames';
    protected $fillable = ['nome'];

 
    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function getValorRAttribute()
    {
        return "R$ ".number_format($this->valor, 2, ",", ".");
    }
}
