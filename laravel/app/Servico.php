<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model {
    protected $table = 'servicos';
    public $timestamps = false;
	//
    protected $fillable = ['nome', 'valor'];

    public function itens(){
        return $this-> hasMany('App/Itens', 'servico_id');
    }

    public function vendas()
    {
        return $this->belongsToMany('App\Venda', 'servico_venda');
    }
}
