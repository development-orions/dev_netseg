<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Jqcalendar extends Model {


    protected $table = 'jqcalendar';
    public $timestamps = false;
    protected $primaryKey = 'Id';

    protected $fillable = ['Subject', 'Location', 'Description', 'StartTime', 'EndTime', 'IsAllDayEvent', 'Color', 'RecurringRule', 'exame', 'observacoes', 'medico_id', 'paciente_id'];

    public function pacientes(){
        return $this->belongsTo('App\Paciente', 'paciente_id');
    }
    public function medicos(){
        return $this->belongsTo('App\Medico', 'medico_id');
    }


}
