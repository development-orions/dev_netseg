<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Statu extends Model {
    protected $table = 'status';
    public $timestamps = false;
    public function clientes(){
        return $this->hasMany('App/Cliente', 'statu_id');
    }
    public function users(){
        return $this->hasMany('App/User', 'statu_id');
    }
    public function tecnicos(){
        return $this->hasMany('App/Tecnico', 'statu_id');
    }
    public function atividades(){
        return $this->hasMany('App/Atividade', 'statu_id');
    }
    public function contas(){
        return $this->hasMany('App\Conta', 'statu_id');
    }
    public function produtos(){
        return $this->hasMany('App/Produto', 'statu_id');
    }
    public function pacientes(){
        return $this->hasMany('App/Paciente', 'statu_id');
    }
    public function vendas(){
        return $this->hasMany('App/Venda', 'statu_id');
    }
    public function cronogramas(){
        return $this->hasMany('App\Cronograma', 'statu_id');
    }
    public function medicos(){
        return $this->hasMany('App\Medico', 'statu_id');
    }
}
