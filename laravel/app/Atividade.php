<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model {
    protected $table = 'atividades';

    public $timestamps = false;
    //
    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }

    public function cronogramas(){
        return $this->hasMany('App\Cronograma', 'atividade_id');
    }
}
