<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Recebimento extends Model {

    protected $table = 'recebimentos';
    public $timestamps = false;

    protected $fillable = ['cliente_id', 'conta_id'];

    public function contas(){
        return $this->belongsTo('App\Conta', 'conta_id');
    }
    public function clientes(){
        return $this->belongsTo('App\Cliente', 'cliente_id');
    }

}
