<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 28/01/2016
 * Time: 08:16
 */

namespace app\Repositories;


use App\Cliente;
use App\Cronograma;
use App\Naoconformidade;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class NaoconformidadeRepository {

    private $PAGINATE = 20;
    /**
     * @var Naoconformidade
     */
    private $naoconformidade;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var Cliente
     */
    private $cliente;
    /**
     * @var Auxiliar
     */
    private $tecnico;
    /**
     * @var Cronograma
     */
    private $cronograma;

    public function __construct(Naoconformidade $naoconformidade, AuxiliarService $auxiliarService, Cliente $cliente, Cronograma $cronograma)
    {
        $this->naoconformidade = $naoconformidade;
        $this->auxiliarService = $auxiliarService;
        $this->cliente = $cliente;
        $this->cronograma = $cronograma;
    }

    public function store($data){

        $this->naoconformidade->codigo = $data['codigo'];
        $this->naoconformidade->revisao = $data['revisao'];
        $this->naoconformidade->vigencia = $this->auxiliarService->data_banco($data['vigencia']);
        $this->naoconformidade->ano = date("Y");
        //$this->naoconformidade->numero = $data['numero'];

        $this->naoconformidade->realpotencial = $data['realpotencial'];
        $this->naoconformidade->materialsistema = $data['materialsistema'];

        $this->naoconformidade->ocorrencia = $data['ocorrencia'];
        if($data['ocorrencia_responsavel']!= null ) {
            $this->naoconformidade->ocorrencia_responsavel = $data['ocorrencia_responsavel'];
        }
        if($data['ocorrencia_data']!= null ) {
            $this->naoconformidade->ocorrencia_data = $this->auxiliarService->data_banco($data['ocorrencia_data']);
        }

        if($data['acaoimediata']!= null ) {
            $this->naoconformidade->acaoimediata = $data['acaoimediata'];
        }
        if($data['acaoimediata_responsavel']!= null ) {
            $this->naoconformidade->acaoimediata_responsavel = $data['acaoimediata_responsavel'];
        }
        if($data['acaoimediata_data']!= null ) {
            $this->naoconformidade->acaoimediata_data = $this->auxiliarService->data_banco($data['acaoimediata_data']);
        }

        $this->naoconformidade->causaprovavel_op = $data['causaprovavel_op'];
        if($data['causaprovavel']!= null ) {
            $this->naoconformidade->causaprovavel = $data['causaprovavel'];
        }
        if($data['causaprovavel_responsavel']!= null ) {
            $this->naoconformidade->causaprovavel_responsavel = $data['causaprovavel_responsavel'];
        }
        if($data['causaprovavel_data']!= null ) {
            $this->naoconformidade->causaprovavel_data = $this->auxiliarService->data_banco($data['causaprovavel_data']);
        }

        if($data['acaocorretiva']!= null ) {
            $this->naoconformidade->acaocorretiva = $data['acaocorretiva'];
        }
        if($data['acaocorretiva_responsavel']!= null ) {
            $this->naoconformidade->acaocorretiva_responsavel = $data['acaocorretiva_responsavel'];
        }
        if($data['acaocorretiva_prazo']!= null ) {
            $this->naoconformidade->acaocorretiva_prazo = $this->auxiliarService->data_banco($data['acaocorretiva_prazo']);
        }
        $this->naoconformidade->foiimplementada = $data['foiimplementada'];


        $this->naoconformidade->eeficaz = $data['eeficaz'];

        if($data['eficaz_observacoes']!= null ) {
            $this->naoconformidade->eficaz_observacoes = $data['eficaz_observacoes'];
        }
        if($data['eficaz_responsavel']!= null ) {
            $this->naoconformidade->eficaz_responsavel = $data['eficaz_responsavel'];
        }
        if($data['eficaz_data'] != null ) {
            $this->naoconformidade->eficaz_data = $this->auxiliarService->data_banco($data['eficaz_data']);
        }

        $this->naoconformidade->cliente_id = $data['cliente_id'];
        $this->naoconformidade->tecnico_id = $data['tecnico_id'];
        $this->naoconformidade->cronograma_id = $data['cronograma_id'];

        if($this->naoconformidade->save()) {
            //conta quantos registros existem no ano cadastrado
            $i = DB::table('naoconformidades')->where('ano', '=', date("Y"))->count();
            //guarda em dígito o valor do count (Sempre retornará ao menos 1)
            $this->naoconformidade->digito = $i;

            //montando código usando digito e ano - ex: 1/2016
            $zero=($this->naoconformidade->digito >9) ? '' : '0';
            $this->naoconformidade->numero = $zero.$this->naoconformidade->digito."/".$this->naoconformidade->ano;

            /* //cóidgo dinâmico
            $naoconformidade = Naoconformidade::find($this->naoconformidade->id);
            $naoconformidade->codigo = 'RG-SGQ-00'.$naoconformidade->id;
            $naoconformidade->save();*/
            $this->naoconformidade->save();
            //Session::forget('nc');
            Session::put('nc', $this->naoconformidade->id);
            return true;
        }
        return false;
    }

    public function update($id, $data){
        $naoconformidade = Naoconformidade::find($id);
        $naoconformidade->id = $id;


        $naoconformidade->codigo = $data['codigo'];
        $naoconformidade->revisao = $data['revisao'];
        $naoconformidade->vigencia = $this->auxiliarService->data_banco($data['vigencia']);


        $naoconformidade->realpotencial = $data['realpotencial'];
        $naoconformidade->materialsistema = $data['materialsistema'];

        $naoconformidade->ocorrencia = $data['ocorrencia'];
        if($data['ocorrencia_responsavel']!= null ) {
            $naoconformidade->ocorrencia_responsavel = $data['ocorrencia_responsavel'];
        }
        if($data['ocorrencia_data']!= null ) {
            $naoconformidade->ocorrencia_data = $this->auxiliarService->data_banco($data['ocorrencia_data']);
        }

        if($data['acaoimediata']!= null ) {
            $naoconformidade->acaoimediata = $data['acaoimediata'];
        }
        if($data['acaoimediata_responsavel']!= null ) {
            $naoconformidade->acaoimediata_responsavel = $data['acaoimediata_responsavel'];
        }
        if($data['acaoimediata_data']!= null ) {
            $naoconformidade->acaoimediata_data = $this->auxiliarService->data_banco($data['acaoimediata_data']);
        }

        $naoconformidade->causaprovavel_op = $data['causaprovavel_op'];
        if($data['causaprovavel']!= null ) {
            $naoconformidade->causaprovavel = $data['causaprovavel'];
        }
        if($data['causaprovavel_responsavel']!= null ) {
            $naoconformidade->causaprovavel_responsavel = $data['causaprovavel_responsavel'];
        }
        if($data['causaprovavel_data']!= null ) {
            $naoconformidade->causaprovavel_data = $this->auxiliarService->data_banco($data['causaprovavel_data']);
        }

        if($data['acaocorretiva']!= null ) {
            $naoconformidade->acaocorretiva = $data['acaocorretiva'];
        }
        if($data['acaocorretiva_responsavel']!= null ) {
            $naoconformidade->acaocorretiva_responsavel = $data['acaocorretiva_responsavel'];
        }
        if($data['acaocorretiva_prazo']!= null ) {
            $naoconformidade->acaocorretiva_prazo = $this->auxiliarService->data_banco($data['acaocorretiva_prazo']);
        }
        $naoconformidade->foiimplementada = $data['foiimplementada'];


        $naoconformidade->eeficaz = $data['eeficaz'];

        if($data['eficaz_observacoes']!= null ) {
            $this->naoconformidade->eficaz_observacoes = $data['eficaz_observacoes'];
        }
        if($data['eficaz_responsavel']!= null ) {
            $this->naoconformidade->eficaz_responsavel = $data['eficaz_responsavel'];
        }
        if($data['eficaz_data'] != null ) {
            $this->naoconformidade->eficaz_data = $this->auxiliarService->data_banco($data['eficaz_data']);
        }


        try {
            if($naoconformidade->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }


    public function listar(){
        return $this->cronograma
            ->join('naoconformidades','naoconformidades.cronograma_id' , '=','cronogramas.id')
            ->select('naoconformidades.*', 'cronogramas.tecnico_id', 'cronogramas.cliente_id', 'cronogramas.atividade_id', 'cronogramas.mes')
            ->paginate($this->PAGINATE);
    }

    public function listarPdf(){
        return $this->cronograma
            ->join('naoconformidades','naoconformidades.cronograma_id' , '=','cronogramas.id')
            ->select('naoconformidades.*', 'cronogramas.tecnico_id', 'cronogramas.cliente_id', 'cronogramas.atividade_id', 'cronogramas.mes')
            ->get();
    }

    public function buscar($data){
        $resultado = Cronograma::join('naoconformidades','naoconformidades.cronograma_id' , '=','cronogramas.id')
            ->select('naoconformidades.*', 'cronogramas.tecnico_id', 'cronogramas.cliente_id', 'cronogramas.atividade_id', 'cronogramas.mes');

        if($data['cliente_id'] != '') $resultado->where('cronogramas.cliente_id', '=', $data['cliente_id']);
        if($data['tecnico_id'] != '') $resultado->where('cronogramas.tecnico_id', '=', $data['tecnico_id']);
        if($data['mes'] != '') $resultado->where('cronogramas.mes', '=', $data['mes']);
        if($data['ano'] != '') $resultado->where('cronogramas.ano', '=', $data['ano']);

        return $resultado->paginate($this->PAGINATE);
    }

    public function buscarPorId($id){
        return $this->naoconformidade->find($id);
    }

    public function verificaCodigo($codigo){
        $conta = Naoconformidade::where('codigo', '=', $codigo)->count();
        if($conta)
            return false;
        return true;
    }
    public function buscarPorCronogramaId($id){
        return $this->naoconformidade
            ->where('cronograma_id', '=', $id)
            ->get();
    }

    public function remove($id){
        if(Naoconformidade::destroy($id))
            return true;

        return false;
    }
}