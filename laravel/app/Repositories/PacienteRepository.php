<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 08:43
 */

namespace App\Repositories;


use App\Paciente;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\DB;

class PacienteRepository
{
    private $paciente;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * PacienteRepository constructor.
     */
    public function __construct(Paciente $paciente, AuxiliarService $auxiliarService)
    {
        $this->paciente = $paciente;
        $this->auxiliarService = $auxiliarService;
    }

    public function data_banco($data){
        $vencimento = explode(' ', $data);
        $dia  = $vencimento[0];
        $ano = $vencimento[2];
        $vencimento = explode(',', $vencimento[1]);

        $meses = array('Janeiro' => 01, 'Fevereiro' => 02, 'Março' => 03, 'Abril' => 04, 'Maio' => 05, 'Junho' => 06, 'Julho' => 07, 'Agosto' => 8, 'Setembro' => 9, 'Outubro' => 10, 'Novembro' => 11, 'Dezembro' => 12);
        $mes = $meses[$vencimento[0]];
        $dataformatada = $ano.'-'.$mes.'-'.$dia;

        return $dataformatada;
    }

    public function store($data){
        $this->paciente->nome = $data['nome'];
        $this->paciente->rg = $data['rg'];
        $this->paciente->cpf = $data['cpf'];
        $this->paciente->datanascimento = $this->auxiliarService->data_banco($data['datanascimento']);
        $this->paciente->sexo = $data['sexo'];
        $this->paciente->cep = $data['cep'];
        $this->paciente->rua = $data['rua'];
        $this->paciente->numero = $data['numero'];
        $this->paciente->complemento = $data['complemento'];
        $this->paciente->bairro = $data['bairro'];
        $this->paciente->cidade = $data['cidade'];
        $this->paciente->estado = $data['estado'];
        $this->paciente->estadocivil = $data['estadocivil'];
        $this->paciente->naturalidade = $data['naturalidade'];
        $this->paciente->grau_instrucao = $data['grau_instrucao'];
        $this->paciente->funcao = $data['funcao'];
        $this->paciente->setor = $data['setor'];
        $this->paciente->email = $data['email'];
        $this->paciente->telefone = $data['telefone'];
        $this->paciente->statu_id = $data['statu_id'];
        $this->paciente->cliente_id = $data['cliente_id'];
        if($this->paciente->save())
            return $this->paciente;

        return false;
    }

    public function update($id, $data){

        $paciente = Paciente::find($id);
        $paciente->id = $id;
        $paciente->nome = $data['nome'];
        $paciente->rg = $data['rg'];
        $paciente->cpf = $data['cpf'];
        $paciente->datanascimento = $this->auxiliarService->data_banco($data['datanascimento']);
        $paciente->sexo = $data['sexo'];
        $paciente->cep = $data['cep'];
        $paciente->rua = $data['rua'];
        $paciente->numero = $data['numero'];
        $paciente->complemento = $data['complemento'];
        $paciente->bairro = $data['bairro'];
        $paciente->cidade = $data['cidade'];
        $paciente->estado = $data['estado'];
        $paciente->estadocivil = $data['estadocivil'];
        $paciente->naturalidade = $data['naturalidade'];
        $paciente->grau_instrucao = $data['grau_instrucao'];
        $paciente->funcao = $data['funcao'];
        $paciente->setor = $data['setor'];
        $paciente->email = $data['email'];
        $paciente->telefone = $data['telefone'];
        $paciente->statu_id = $data['statu_id'];
        $paciente->cliente_id = $data['cliente_id'];
        try {
            if($paciente->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return false;
    }

    public function inativar($id){
        $paciente = Paciente::findOrFail($id);
        $paciente->statu_id = 2;
        if ($paciente->save())
            return true;
    }
    public function ativar($id){
        $paciente = Paciente::findOrFail($id);
        $paciente->statu_id = 1;
        if ($paciente->save())
            return true;
    }

    public function listar($status){
        return $this->paciente->where('statu_id', $status)->paginate($this->PAGINATE);
    }

    public function listar_drop($status,$term,$page){

        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $breeds = $this->paciente->select('id', DB::raw("CONCAT(nome, ' - ', cpf) as text"))->where('statu_id', $status)->where(function ($query) use ($term) {
            $query->where('nome', 'LIKE',  '%' .$term. '%')
                   ->orWhere('cpf', 'LIKE',  '%' .$term. '%');})->orderBy('text', 'asc')->skip($offset)->take($resultCount)->get(['id','text']);

        $count = $this->paciente->select('id')->where('statu_id', $status)->where(function ($query) use ($term) {
            $query->where('nome', 'LIKE',  '%' .$term. '%')
                ->orWhere('cpf', 'LIKE',  '%' .$term. '%');})->skip($offset)->take($resultCount)->count();
        $endCount = $offset + $resultCount;
        $morePages = $endCount < $count;

        $results = array(
            "results" => $breeds,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);

    }


    public function buscar($status, $data){
        $resultado = Paciente::where('pacientes.statu_id', '=', $status)->join('clientes as clientes', 'pacientes.cliente_id', '=', 'clientes.id');
        if($data['q'] != ''){
            $resultado->where('pacientes.nome', 'like', '%'.$data['q'].'%');

        }
        if($data['empresa'] != ''){
            $resultado->where('clientes.nome', 'like', '%'.$data['empresa'].'%');
        }
        $resultado->selectRaw('clientes.nome as cliente, pacientes.nome as nome, pacientes.funcao as funcao, pacientes.setor as setor, pacientes.id as id, pacientes.statu_id as statu_id');

        return $resultado->paginate($this->PAGINATE);
    }

    public function buscarPorId($id){
        return $this->paciente->find($id);
    }

    public function buscar_paciente($busca)
    {
        $paciente = Paciente::where('pacientes.statu_id', '=', 1)->join('clientes as clientes', 'pacientes.cliente_id', '=', 'clientes.id');
        $paciente->where('pacientes.nome', 'LIKE', '%'. $busca .'%');
        $paciente->selectRaw('clientes.nome as cliente, pacientes.nome as nome, pacientes.id as id, pacientes.cpf as cpf');

        $paciente->orderBy('nome', 'asc');
            $paciente->take(4);
        return $paciente->get();
    }
}