<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 10:27
 */

namespace App\Repositories;


use App\Centrocusto;
use App\Cliente;
use App\Conta;
use App\Fornecedore;
use App\Pagamento;
use App\Recebimento;
use App\Services\AuxiliarService;
use Collective\Html\HtmlFacade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class ContaRepository
{
    private $conta;
    private $recebimento;
    private $pagamento;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * ContaRepository constructor.
     */
    public function __construct(Conta $conta, Recebimento $recebimento, Pagamento $pagamento, AuxiliarService $auxiliarService)
    {
        $this->conta = $conta;
        $this->recebimento = $recebimento;
        $this->pagamento = $pagamento;
        $this->auxiliarService = $auxiliarService;
    }

    public function cancelar($id){
        $conta = Conta::findOrFail($id);
        $conta->statu_id = 3;
        if ($conta->save())
            return true;
    }

    public function store($data){
        $conta = new Conta;
        $conta->nome = $data['nome'];
        $conta->vencimento = $data['vencimento'];
        if(!empty($data['baixa'])) {
            $conta->baixa = $this->auxiliarService->data_banco($data['baixa']);
        }
        $conta->valor = $data['valor'];
        $conta->portador = $data['portador'];
        $conta->observacao = $data['observacao'];
        $conta->nf = $data['nf'];
        $conta->num_boleto = $data['num_boleto'];
        $conta->statu_id = $data['statu_id'];
        $conta->centrocusto_id = $data['centrocusto_id'];
        $conta->formapagamento_id = $data['formapagamento_id'];

        if(isset($data['tipov']) && isset($data['valor2'])) {
            if ($data['tipov'] == 1) { // Acrescimo
                $conta->acrescimo = $data['valor2'];
                $conta->total = $data['valor'] + $data['valor2'];
            } else { // desconto
                $conta->desconto = $data['valor2'];
                $conta->total = $data['valor'] - $data['valor2'];
            }
        }else{
            $conta->total = $data['valor'];
        }

        if($conta->save()){
            return $conta;
        }

        return false;
    }

    public function update($tipo, $id, $data){

        $conta = Conta::find($id);
        $conta->id = $id;
        $conta->nome = $data['nome'];
        $conta->vencimento = $this->auxiliarService->data_banco($data['vencimento']);
        if(!empty($data['baixa'])) {
            $conta->baixa = $this->auxiliarService->data_banco($data['baixa']);
        }
        $conta->valor = $data['valor'];
        $conta->portador = $data['portador'];
        $conta->observacao = $data['observacao'];
        $conta->nf = $data['nf'];
        $conta->num_boleto = $data['num_boleto'];
        $conta->statu_id = $data['statu_id'];
        $conta->centrocusto_id = $data['centrocusto_id'];
        $conta->formapagamento_id = $data['formapagamento_id'];

        if(isset($data['tipov']) && isset($data['valor2'])) {
            if ($data['tipov'] == 1) { // Acrescimo
                $conta->acrescimo = $data['valor2'];
                $conta->total = $data['valor'] + $data['valor2'];
            } else { // desconto
                $conta->desconto = $data['valor2'];
                $conta->total = $data['valor'] - $data['valor2'];
            }
        }else{
            $conta->total = $data['valor'];
        }
        try {
            if($conta->save()){
                return $conta;
            }
        }catch (\Exception $e){

            return $e->getMessage();
        }
        return false;
    }

    public function buscarPorId($id){
        $conta_edit = Conta::find($id);
        return $conta_edit;
    }

    public function listar($tipo){
        if($tipo == 1){
            return $this->recebimento->paginate($this->PAGINATE);
        }else{
            return $this->pagamento->paginate($this->PAGINATE);
        }
    }

    public function listar_relatorio($tipo){
        if($tipo != 1){
            return $this->recebimento->get();
        }else{
            return $this->pagamento->get();
        }
    }

    public function delete($data){
        if(Conta::destroy($data->id))
            return true;

        return false;
    }

    public function relatorio_conta_pagar($data){
        $resultado = Pagamento::join('contas', 'contas.id', '=', 'pagamentos.conta_id')->join('fornecedores', 'fornecedores.id', '=', 'pagamentos.fornecedore_id');
        if($data['statu_id'] != ''){
            $resultado->where('contas.statu_id', '=', $data['statu_id']);
        }
        if($data['centrocusto_id'] != '') {
            $resultado->where('contas.centrocusto_id', '=', $data['centrocusto_id']);
        }
        if($data['q'] != ''){
            $resultado->where('fornecedores.nome', 'like', '%'.$data['q'].'%');
        }
        if($data['inicio'] != '' && $data['fim'] != ''){
            $resultado->whereBetween('contas.vencimento', [$data['inicio'], $data['fim']]);
        }
        $resultado->orderBy(DB::raw('DAY(contas.created_at)'), 'asc');

        return $resultado->get();
    }

    public function relatorio_conta_receber($data){
        $resultado = Recebimento::join('contas', 'contas.id', '=', 'recebimentos.conta_id')->join('clientes', 'clientes.id', '=', 'recebimentos.cliente_id');
        if($data['statu_id'] != ''){
            $resultado->where('contas.statu_id', '=', $data['statu_id']);

        }
        if($data['centrocusto_id'] != '') {
            $resultado->where('contas.centrocusto_id', '=', $data['centrocusto_id']);
        }
        if($data['q'] != ''){
            $resultado->where('clientes.nome', 'like', '%'.$data['q'].'%');
        }
        if($data['inicio'] != '' && $data['fim'] != ''){
            $resultado->whereBetween('contas.vencimento', [$data['inicio'], $data['fim']]);
        }
        $resultado->orderBy(DB::raw('DAY(contas.created_at)'), 'asc');

        return $resultado->get();
    }

    public function update_status_atrasadas(){
        $contas = Conta::where('contas.statu_id', '=', 5)
        ->where('contas.vencimento', '<', date('Y-m-d'))
        ->get();

        foreach($contas as $conta){
            $conta->statu_id = 14;
            $conta->save();
        }


    }

    /**
     * @param $data
     * @return array
     */
    public function grafico($data){
        $recebimento = Recebimento::join('contas', 'contas.id', '=', 'recebimentos.conta_id');
        $recebimento->whereBetween('contas.vencimento', [$data['inicio'], $data['fim']]);
        if(isset($data['contas']) && $data['contas'] != ''){
            $recebimento->where('contas.statu_id', '=', 4);
        }else{
            $recebimento->where('contas.statu_id', '!=', 3);
        }
        if(isset($data['portador']) && $data['portador'] != ''){
            $recebimento->where('contas.portador', 'like', $data['portador'].'%');
        }
        if(isset($data['custo']) && $data['custo'] != ''){
            $recebimento->where('contas.centrocusto_id', '=', $data['custo'])->join('centrocustos', 'centrocustos.id', '=', 'contas.centrocusto_id');
            $recebimento->select('centrocustos.nome as centrocusto',DB::raw('sum(valor) as soma'), DB::raw('MONTH(contas.vencimento) month'));
        }else{
            $recebimento->select(DB::raw('sum(valor) as soma'), DB::raw('MONTH(contas.vencimento) month'));
        }

        $recebimento->groupBy('month');
        $recebimentos = $recebimento->get();

        $pagamento = Pagamento::join('contas', 'contas.id', '=', 'pagamentos.conta_id');
        $pagamento->whereBetween('contas.vencimento', [$data['inicio'], $data['fim']]);
        if(isset($data['contas']) && $data['contas'] != ''){
            $pagamento->where('contas.statu_id', '=', 4);
        }else{
            $pagamento->where('contas.statu_id', '!=', 3);
        }
        if(isset($data['portador']) && $data['portador'] != ''){
            $pagamento->where('contas.portador', 'like', $data['portador'].'%');
        }
        if(isset($data['custo']) && $data['custo'] != ''){
            $pagamento->where('contas.centrocusto_id', '=', $data['custo'])->join('centrocustos', 'centrocustos.id', '=', 'contas.centrocusto_id');
            $pagamento->select('centrocustos.nome as centrocusto', DB::raw('sum(valor) as soma'), DB::raw('MONTH(contas.vencimento) month'));
        }else{
            $pagamento->select(DB::raw('sum(valor) as soma'), DB::raw('MONTH(contas.vencimento) month'));
        }

        $pagamento->groupBy('month');
        $pagamentos = $pagamento->get();

        $resultado = [];

        $receb = json_decode(json_encode($recebimentos), true);
        $pag = json_decode(json_encode($pagamentos), true);

        $resultado['recebimento'] = $recebimentos;
        $resultado['totalentradas'] = number_format(array_sum(array_column($receb, 'soma')),2, ',', '.');
        $resultado['totalsaidas'] = number_format(array_sum(array_column($pag, 'soma')), 2, ',', '.');
        $resultado['saldo'] = number_format(array_sum(array_column($receb, 'soma')) - array_sum(array_column($pag, 'soma')), 2, ',', '.');
        $resultado['pagamento'] = $pagamentos;

        return $resultado;
    }

    public function graficoReseultado($data){
        $recebimento = Recebimento::join('contas', 'contas.id', '=', 'recebimentos.conta_id');
        $recebimento->whereBetween('contas.vencimento', [$data['inicio'], $data['fim']]);
        if(isset($data['contas']) && $data['contas'] != ''){
            $recebimento->where('contas.statu_id', '=', 4);
        }else{
            $recebimento->where('contas.statu_id', '!=', 3);
        }
        if(isset($data['custo']) && $data['custo'] != ''){
            $recebimento->where('contas.centrocusto_id', '=', $data['custo'])->join('centrocustos', 'centrocustos.id', '=', 'contas.centrocusto_id');
            $recebimento->select('centrocustos.nome as centrocusto',DB::raw('sum(valor) as soma'), DB::raw('MONTH(contas.vencimento) month'));
        }else{
            $recebimento->select(DB::raw('sum(valor) as soma'), DB::raw('contas.portador portador'));
        }

        $recebimento->groupBy('portador');
        $recebimentos = $recebimento->get();


        $pagamento = Pagamento::join('contas', 'contas.id', '=', 'pagamentos.conta_id');
        $pagamento->whereBetween('contas.vencimento', [$data['inicio'], $data['fim']]);
        if(isset($data['contas']) && $data['contas'] != ''){
            $pagamento->where('contas.statu_id', '=', 4);
        }else{
            $pagamento->where('contas.statu_id', '!=', 3);
        }
        if(isset($data['portador']) && $data['portador'] != ''){
            $pagamento->where('contas.portador', 'like', $data['portador'].'%');
        }
        if(isset($data['custo']) && $data['custo'] != ''){
            $pagamento->where('contas.centrocusto_id', '=', $data['custo'])->join('centrocustos', 'centrocustos.id', '=', 'contas.centrocusto_id');
            $pagamento->select('centrocustos.nome as centrocusto', DB::raw('sum(valor) as soma'), DB::raw('MONTH(contas.vencimento) month'));
        }else{
            $pagamento->select(DB::raw('sum(valor) as soma'), DB::raw('contas.portador portador'));
        }

        $pagamento->groupBy('portador');
        $pagamentos = $pagamento->get();

        $resultado = [];

        $receb = json_decode(json_encode($recebimentos), true);
        $pag = json_decode(json_encode($pagamentos), true);



        foreach ($recebimentos as $recebe){
            if($recebe['portador'] == 'Egydio'){
                $resultado['entradaEgydio'] = $recebe['soma'];
            }
            else if($recebe['portador'] == 'Medicina'){
                $resultado['entradaMedicina'] = $recebe['soma'];
            }
            else{
                $resultado['entradaNetseg'] = $recebe['soma'];
            }
        }

        foreach ($pagamentos as $paga){
            if($paga['portador'] == 'Egydio'){
                $resultado['saidaEgydio'] = $paga['soma'];
            }
            else if($paga['portador'] == 'Medicina'){
                $resultado['saidaMedicina'] = $paga['soma'];
            }
            else{
                $resultado['saidaNetseg'] = $paga['soma'];
            }
        }

        if(isset($resultado['entradaEgydio'])){
            if(isset($resultado['saidaEgydio'])){
                $resultado['saldoEgydio'] = $resultado['entradaEgydio'] - $resultado['saidaEgydio'];
            }else{
                $resultado['saldoEgydio'] = $resultado['entradaEgydio'];
            }
        }else{
            if(isset($resultado['saidaEgydio'])){
                $resultado['saldoEgydio'] = 0 - $resultado['saidaEgydio'];
            }else {
                $resultado['saldoEgydio'] = 0;
            }
        }

        if(isset($resultado['entradaNetseg'])){
            if(isset($resultado['saidaNetseg'])){
                $resultado['saldoNetseg'] = $resultado['entradaNetseg'] - $resultado['saidaNetseg'];
            }else{
                $resultado['saldoNetseg'] = $resultado['entradaNetseg'];
            }
        }else{
            if(isset($resultado['saidaNetseg'])){
                $resultado['saldoNetseg'] = 0 - $resultado['saidaNetseg'];
            }else{
                $resultado['saldoNetseg'] = 0;
            }
        }

        if(isset($resultado['entradaMedicina'])){
            if(isset($resultado['saidaMedicina'])){
                $resultado['saldoMedicina'] = $resultado['entradaMedicina'] - $resultado['saidaMedicina'];
            }else{
                $resultado['saldoMedicina'] = $resultado['entradaMedicina'];
            }
        }else{
            if(isset($resultado['saidaMedicina'])){
                $resultado['saldoMedicina'] = 0 - $resultado['saidaMedicina'];
            }else{
                $resultado['saldoMedicina'] = 0;
            }
        }

        $resultado['saldoEgydiof'] = number_format($resultado['saldoEgydio'], 2, ',', '.');
        $resultado['saldoNetsegf'] = number_format($resultado['saldoNetseg'], 2, ',', '.');
        $resultado['saldoMedicinaf'] = number_format($resultado['saldoMedicina'], 2, ',', '.');

        $resultado['totalentradas'] = number_format(array_sum(array_column($receb, 'soma')),2, ',', '.');
        $resultado['totalsaidas'] = number_format(array_sum(array_column($pag, 'soma')), 2, ',', '.');
        $resultado['saldo'] = number_format(array_sum(array_column($receb, 'soma')) - array_sum(array_column($pag, 'soma')), 2, ',', '.');


        return $resultado;
    }
}