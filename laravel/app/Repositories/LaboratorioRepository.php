<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Repositories;


use App\Laboratorio;

class LaboratorioRepository
{
    private $laboratorio;
    /**
     * LaboratorioRepository constructor.
     */
    public function __construct(Laboratorio $laboratorio)
    {
        $this->laboratorio = $laboratorio;
    }

    public function store($data){
        $this->laboratorio->nome = $data['nome'];
        //$this->laboratorio->valor = $data['valor'];

        if($this->laboratorio->save())
            return true;

        return false;
    }

    public function buscarPorId($id){
        return $this->laboratorio->find($id);
    }

    public function update($id, $data){

        $laboratorio = Laboratorio::find($id);
        $laboratorio->id = $id;
        $laboratorio->nome = $data['nome'];

        try {
            if($laboratorio->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function listar(){
        return $this->laboratorio->all();
    }

    public function listar_drop(){
        return $this->laboratorio->orderBy('nome', 'asc')->lists('nome','id')->all();
    }

    public function remove($id){
        if(Laboratorio::destroy($id))
            return true;

        return false;
    }

    public function laboratorioSelectbox(){
        return Laboratorio::orderBy('nome')->lists('nome', 'id')->toarray();
    }

}