<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 12/11/2015
 * Time: 10:13
 */

namespace App\Repositories;


use App\Cliente;
use App\Fornecedore;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\Session;
use Validator;

class FornecedorRepository {

    /**
     * @var Fornecedore
     */
    private $fornecedor;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(Fornecedore $fornecedor, AuxiliarService $auxiliarService){

        $this->fornecedor = $fornecedor;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($data){
        $this->fornecedor->nome = $data['nome'];
        $this->fornecedor->razaosocial = $data['razaosocial'];
        $this->fornecedor->cnpj = $data['cnpj'];
        $this->fornecedor->cpf = $data['cpf'];
        $this->fornecedor->ie = $data['ie'];
        $this->fornecedor->cep = $data['cep'];
        $this->fornecedor->rua = $data['rua'];
        $this->fornecedor->numero = $data['numero'];
        $this->fornecedor->complemento = $data['complemento'];
        $this->fornecedor->bairro = $data['bairro'];
        $this->fornecedor->cidade = $data['cidade'];
        $this->fornecedor->estado = $data['estado'];
        $this->fornecedor->responsavel = $data['responsavel'];
        $this->fornecedor->funcao = $data['funcao'];
        $this->fornecedor->email_responsavel = $data['email_responsavel'];
        $this->fornecedor->telefone_responsavel = $data['telefone_responsavel'];
        $this->fornecedor->statu_id = $data['statu_id'];
        $this->fornecedor->cei_autonomo = $data['cei_autonomo'];
        $this->fornecedor->datainicio_servico = $this->auxiliarService->data_banco($data['datainicio_servico']);
        if($this->fornecedor->save())
            return true;

        return false;
    }

    public function store_para_funcoes($data){
        $this->fornecedor->nome = $data['nome'];
        $this->fornecedor->razaosocial = $data['razaosocial'];
        $this->fornecedor->cnpj = $data['cnpj'];
        $this->fornecedor->cpf = $data['cpf'];
        $this->fornecedor->ie = $data['ie'];
        $this->fornecedor->cep = $data['cep'];
        $this->fornecedor->rua = $data['rua'];
        $this->fornecedor->numero = $data['numero'];
        $this->fornecedor->complemento = $data['complemento'];
        $this->fornecedor->bairro = $data['bairro'];
        $this->fornecedor->cidade = $data['cidade'];
        $this->fornecedor->estado = $data['estado'];
        $this->fornecedor->responsavel = $data['responsavel'];
        $this->fornecedor->funcao = $data['funcao'];
        $this->fornecedor->email_responsavel = $data['email_responsavel'];
        $this->fornecedor->telefone_responsavel = $data['telefone_responsavel'];
        $this->fornecedor->statu_id = $data['statu_id'];
        $this->fornecedor->cei_autonomo = $data['cei_autonomo'];
        $this->fornecedor->datainicio_servico = $this->auxiliarService->data_banco($data['datainicio_servico']);

        try{
            if($this->fornecedor->save()){
                return $this->fornecedor;
            }
        }catch (\Exception $e){

            return $e->getMessage();
        }

        return false;
    }
    public function adicionar($id){
        $dados = Fornecedore::find($id);
        return $dados;
    }



    public function listar($status){
        return $this->fornecedor->where('statu_id', $status)->paginate($this->PAGINATE);
    }

    public function buscar($status, $texto){
        if(isset($texto)){
            return $this->fornecedor->where('statu_id', $status)
                ->where('nome', 'LIKE', '%'. $texto['q'] .'%')
                ->orWhere('razaosocial', 'LIKE', '%'. $texto['q'] .'%')
                ->paginate($this->PAGINATE);
        }else{
            return $this->fornecedor->where('statu_id', $status)->paginate($this->PAGINATE);
        }
    }

    public function buscarPorId($id){
        return $this->fornecedor->find($id);
    }

    public function update($id, $data){

        //verifica se os campos foram validados

            $fornecedor = Fornecedore::find($id);
            $fornecedor->id = $id;
            $fornecedor->nome = $data['nome'];
            $fornecedor->razaosocial = $data['razaosocial'];
            $fornecedor->cnpj = $data['cnpj'];
            $fornecedor->cpf = $data['cpf'];
            $fornecedor->ie = $data['ie'];
            $fornecedor->cep = $data['cep'];
            $fornecedor->rua = $data['rua'];
            $fornecedor->numero = $data['numero'];
            $fornecedor->complemento = $data['complemento'];
            $fornecedor->bairro = $data['bairro'];
            $fornecedor->cidade = $data['cidade'];
            $fornecedor->estado = $data['estado'];
            $fornecedor->responsavel = $data['responsavel'];
            $fornecedor->funcao = $data['funcao'];
            $fornecedor->email_responsavel = $data['email_responsavel'];
            $fornecedor->telefone_responsavel = $data['telefone_responsavel'];
            $fornecedor->statu_id = $data['statu_id'];
            $fornecedor->cei_autonomo = $data['cei_autonomo'];
            $fornecedor->datainicio_servico = $this->auxiliarService->data_banco($data['datainicio_servico']);
            try {
                if($fornecedor->save())
                    return true;
            }catch (\Exception $e){
                return $e->getMessage();
            }

        return false;
    }
    
    public function ativar($id)
    {
        $fornecedor = Fornecedore::findOrFail($id);
        $fornecedor->statu_id = 1;
        if ($fornecedor->save())
            return true;
        /*return DB::table('fornecedor')
            ->where('id', $id)
            ->update(['statu_id' => 1]);*/
    }

    public function inativar($id)
    {
        $fornecedor = Fornecedore::findOrFail($id);
        $fornecedor->statu_id = 2;
        if ($fornecedor->save())
            return true;
        /*return DB::table('fornecedor')
            ->where('id', $id)
            ->update(['statu_id' => 2]);*/
    }

    public function buscarFornecedor($conta){
        return Fornecedore::find($conta->pagamentos[0]->fornecedore_id);
    }

    /**
     * verifica o cpf se já está cadastrado
     * @author Silvio Luis
     *
     * @param string $cpf
     * @return boolean
     */
    public function verificaCpf($cpf){
        $conta = Fornecedore::where('cpf', '=', $cpf)->count();
        if($conta){
            return false;
        }
        return true;
    }

    /**
     * Verifica o cnpj se já está cadastrado no sistema
     *
     * @author Silvio Luis
     *
     * @param string $cnpj
     * @return bool
     */
    public function verificaCnpj($cnpj){
        $conta = Fornecedore::where('cnpj', '=', $cnpj)->count();
        if($conta){
            return false;
        }
        return true;
    }

    /**
     * Verifica se a razão social já está cadastrada no sistema
     *
     * @author Silvio Luis
     * @param string $razaosocial
     * @return bool
     */
    public function verificaRazaosocial($razaosocial){
        $conta = Fornecedore::where('razaosocial', '=', $razaosocial)->count();
        if($conta){
            return false;
        }
        return true;
    }
    /**
     * Verifica cnpj para edição
     * @author Silvio Luis
     * @param $id
     * @param $cnpj
     * @return bool
     */
    public function verificaCnpjEdicao($id, $cnpj){
        $conta = Cliente::where('id', '!=', $id)->where('cnpj', '=', $cnpj)->count();
        if($conta){
            return false;
        }
        return true;
    }

    /**
     * Busca Ajax por fornecedores que estão ativos
     * @param $busca
     * @return mixed
     */
    public function buscar_fornecedor($busca)
    {
        return Fornecedore::where('razaosocial', 'LIKE', '%'. $busca .'%')
            ->orWhere('nome', 'LIKE', '%'. $busca .'%')
            ->where('statu_id', '=', 1)
            ->orderBy('razaosocial', 'asc')->take(4)->get();
    }
}