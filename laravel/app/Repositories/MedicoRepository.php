<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 03/02/2016
 * Time: 08:24
 */

namespace App\Repositories;
use App\Medico;
use App\Services\AuxiliarService;
use Illuminate\Validation\Validator;

class MedicoRepository
{
    /**
     * @var Medico
     */
    private $medico;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * MedicoRepository constructor.
     */
    public function __construct(Medico $medico, AuxiliarService $auxiliarService)
    {
        $this->medico = $medico;
        $this->auxiliarService = $auxiliarService;
    }

    public function inativar($id){
        $medico = Medico::findOrFail($id);
        $medico->statu_id = 2;
        if ($medico->save())
            return true;
    }
    public function ativar($id){
        $medico = Medico::findOrFail($id);
        $medico->statu_id = 1;
        if ($medico->save())
            return true;
    }

    public function store($data){
        $this->medico->nome = $data['nome'];
        $this->medico->crm = $data['crm'];
        $this->medico->nit = $data['nit'];
        $this->medico->datacontrato = $this->auxiliarService->data_banco($data['datacontrato']);
        $this->medico->cep = $data['cep'];
        $this->medico->rua = $data['rua'];
        $this->medico->numero = $data['numero'];
        $this->medico->complemento = $data['complemento'];
        $this->medico->bairro = $data['bairro'];
        $this->medico->cidade = $data['cidade'];
        $this->medico->estado = $data['estado'];
        $this->medico->telefone = $data['telefone'];
        $this->medico->statu_id = $data['statu_id'];
        $this->medico->observacao = $data['observacao'];
        if($this->medico->save())
            return $this->medico;

        return false;
    }

    public function verificaCrm($crm){
        $conta = Medico::where('crm', '=', $crm)->count();
        if($conta){
            return false;
        }
        return true;
    }
    public function verificaCrmEdicao($id, $crm){
        $conta = Medico::where('id', '!=', $id)->where('crm', '=', $crm)->count();
        if($conta){
            return false;
        }
        return true;
    }

    public function listar($status){
        return $this->medico->where('statu_id', $status)->orderBy('nome', 'asc')->paginate($this->PAGINATE);
    }

    public function buscar($status, $texto){
        if(isset($texto)){
            return $this->medico->where('statu_id', $status)
                ->where(function($query) use ($texto)
                {
                    $query->where('nome', 'LIKE', '%'. $texto['q'] .'%')
                        ->orWhere('crm', 'LIKE', '%'. $texto['q'] .'%');
                })
                ->orderBy('nome', 'asc')
                ->paginate($this->PAGINATE);
        }else{
            return $this->medico->where('statu_id', $status)->paginate($this->PAGINATE);
        }
    }

    public function listar_drop($status){
        return $this->medico->where('statu_id', $status)->orderBy('nome', 'asc')->lists('nome','id')->all();
    }

    public function buscarPorId($id){
        return $this->medico->find($id);
    }

    public function update($id, $data){

            $medico = Medico::find($id);
            $medico->id = $id;
            $medico->nome = $data['nome'];
            $medico->crm = $data['crm'];
            $medico->nit = $data['nit'];
            $medico->cep = $data['cep'];
            $medico->rua = $data['rua'];
            $medico->numero = $data['numero'];
            $medico->complemento = $data['complemento'];
            $medico->bairro = $data['bairro'];
            $medico->cidade = $data['cidade'];
            $medico->estado = $data['estado'];
            $medico->telefone = $data['telefone'];
            $medico->statu_id = $data['statu_id'];
            $medico->observacao = $data['observacao'];
            $medico->datacontrato = $this->auxiliarService->data_banco($data['datacontrato']);

            try {
                if($medico->save())
                    return true;
            }catch (\Exception $e){
                return $e->getMessage();
            }

        return false;
    }

    public function buscar_medico($busca)
    {
        return Medico::where('nome', 'LIKE', '%'. $busca .'%')->orderBy('nome', 'asc')->take(4)->get();
    }

    public function medicoVenda(){
        return Medico::where('statu_id', '=', 1)->lists('nome', 'id')->all();
    }
}