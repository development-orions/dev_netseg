<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 10/12/2015
 * Time: 16:31
 */

namespace App\Repositories;


use App\Role;

class RoleRepository
{
    /**
     * @var Statu
     */
    private $role;

    public function __construct(Role $role){

        $this->role = $role;
    }

    public function roleUser(){
        return Role::where('id', '!=', 11)->orderBy('nome', 'asc')->lists('nome', 'id')->all();
    }
}