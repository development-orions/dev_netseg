<?php

namespace App\Repositories;

use App\AtendimentoExame;
use App\Services\AuxiliarService;

/**
 * Class MenuRepository
 * @package App\Repositories\Admin
 * @version September 25, 2018, 5:06 pm -03
 *
 * @method AtendimentoExame findWithoutFail($id, $columns = ['*'])
 * @method AtendimentoExame find($id, $columns = ['*'])
 * @method AtendimentoExame first($columns = ['*'])
 */
class AtendimentoExameRepository
{

    public function __construct(AuxiliarService $auxiliarService)
    {
        $this->auxiliarService = $auxiliarService;
    }
    /**
     * Configure the Model
     **/
    public function model()
    {
        return AtendimentoExame::class;
    }

    public function create($id,$data)
    {
        foreach ($data['exame_id'] as $key => $value){
            $atendimento = new AtendimentoExame;
            $atendimento->atendimento_id = $id;
            $atendimento->exame_id = $value;
            $atendimento->laboratorio_id = $data['laboratorio_id'][$key];
            $atendimento->valor = $this->auxiliarService->moeda($data['valor'][$key]);
            $atendimento->valor_medico = $this->auxiliarService->moeda($data['valor_medico'][$key]);
            if($data['medico_id'][$key] != '')
            $atendimento->medico_id =  $data['medico_id'][$key];
            $atendimento->save();
        }

        return true;
    }

    public function update($id,$data)
    {

        AtendimentoExame::where('atendimento_id', $id)->delete();

        foreach ($data['exame_id'] as $key => $value){
            $atendimento = new AtendimentoExame;
            $atendimento->atendimento_id = $id;
            $atendimento->exame_id = $value;
            $atendimento->laboratorio_id = $data['laboratorio_id'][$key];
            $atendimento->valor = $this->auxiliarService->moeda($data['valor'][$key]);
            if($data['medico_id'][$key] != '')
            $atendimento->medico_id =  $data['medico_id'][$key]; 
            $atendimento->save();
        }

        return true;
    }

    public function list_atendimento($id)
    {
        return AtendimentoExame::where('atendimento_id',$id)->get()->toarray();
    }

}
