<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 13:43
 */

namespace App\Repositories;

use Carbon\Carbon;
use App\Cliente;
use App\Clientestecnico;
use App\Services\AuxiliarService;
use App\Tecnico;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class TecnicoRepository
{

    /**
     * @var Tecnico
     */
    private $tecnico;
    private $auxiliarService;
    private $PAGINATE = 20;

    public function __construct(Tecnico $tecnico, AuxiliarService $auxiliarService){

        $this->tecnico = $tecnico;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($data){
        $this->tecnico->nome = $data['nome'];
        $this->tecnico->num_registro = $data['num_registro'];
        $this->tecnico->cep = $data['cep'];
        $this->tecnico->rua = $data['rua'];
        $this->tecnico->numero = $data['numero'];
        $this->tecnico->complemento = $data['complemento'];
        $this->tecnico->bairro = $data['bairro'];
        $this->tecnico->cidade = $data['cidade'];
        $this->tecnico->estado = $data['estado'];
        $this->tecnico->descricao = $data['descricao'];
        $this->tecnico->email = $data['email'];
        $this->tecnico->telefone = $data['telefone'];
        $this->tecnico->statu_id = $data['statu_id'];

        if($this->tecnico->save()) {

            $user = new User();
            $user->nome = $this->tecnico->nome;
            $user->funcao = 'Técnico';
            $user->email = $this->tecnico->email;
            $user->username = mb_substr($this->tecnico->nome, 0, 8).$this->tecnico->id;
            $user->password = bcrypt(mb_substr($this->tecnico->nome, 0, 4).'@'.$this->tecnico->created_at->format('Y'));
            $user->role_id = 11;
            $user->statu_id = $this->tecnico->statu_id;
            $user->tecnico_id = $this->tecnico->id;
            $user->save();

            return true;
        }

        return false;
    }

    public function listar($status){
        return $this->tecnico->where('statu_id', $status)->paginate($this->PAGINATE);
    }

    public function buscar($status, $texto){
        if(isset($texto)){
            return $this->tecnico->where('statu_id', $status)
                ->where(function($query) use ($texto)
                {
                    $query->where('nome', 'LIKE', '%'. $texto['q'] .'%')
                        ->orWhere('num_registro', 'LIKE', '%'. $texto['q'] .'%');
                })

                ->paginate($this->PAGINATE);
        }else{
            return $this->tecnico->where('statu_id', $status)->paginate($this->PAGINATE);
        }
    }

    public function buscarPorId($id){
        return $this->tecnico->find($id);
    }

    public function update($id, $data){

        $v = Validator::make($data,
            [
                'num_registro' => 'required|unique:tecnicos,num_registro,'.$data['num_registro'].',num_registro'
            ]
        );
        //verifica se os campos foram validados
        if (!$v->fails())
        {
            $tecnico = Tecnico::find($id);
            $tecnico->nome = $data['nome'];
            $tecnico->num_registro = $data['num_registro'];
            $tecnico->cep = $data['cep'];
            $tecnico->rua = $data['rua'];
            $tecnico->numero = $data['numero'];
            $tecnico->complemento = $data['complemento'];
            $tecnico->bairro = $data['bairro'];
            $tecnico->cidade = $data['cidade'];
            $tecnico->estado = $data['estado'];
            $tecnico->descricao = $data['descricao'];
            $tecnico->email = $data['email'];
            $tecnico->telefone = $data['telefone'];
            $tecnico->statu_id = $data['statu_id'];


            try {
                if($tecnico->save()){

                    $usuario = User::where('tecnico_id', $tecnico->id)->first();

                    if(sizeof($usuario) == 0){
                        $user = new User();
                        $user->nome = $tecnico->nome;
                        $user->funcao = 'Técnico';
                        $user->email = $tecnico->email;
                        $user->username = mb_substr(str_replace(' ', '', $tecnico->nome), 0, 8);
                        $user->password = bcrypt(mb_substr($tecnico->nome, 0, 4).'@'.$tecnico->created_at->format('Y'));
                        $user->role_id = 11;
                        $user->statu_id = $tecnico->statu_id;
                        $user->tecnico_id = $tecnico->id;
                        $user->save();
                    }else{
                        $usuario->nome = $tecnico->nome;
                        $usuario->funcao = 'Técnico';
                        $usuario->email = $tecnico->email;
                        $usuario->username = mb_substr(str_replace(' ', '', $tecnico->nome), 0, 8);
                        $usuario->password = bcrypt(mb_substr($tecnico->nome, 0, 4).'@'.$tecnico->created_at->format('Y'));
                        $usuario->role_id = 11;
                        $usuario->statu_id = $tecnico->statu_id;
                        $usuario->tecnico_id = $tecnico->id;
                       // $usuario->id = $usuario->id;
                        $usuario->save();
                    }

                    return true;
                }

            }catch (\Exception $e){
                return $e->getMessage();
            }
        }

        return false;
    }

    public function inativar($id){
        $tecnico = Tecnico::findOrFail($id);
        $tecnico->statu_id = 2;
        if ($tecnico->save())
            return true;
    }
    public function ativar($id){
        $tecnico = Tecnico::findOrFail($id);
        $tecnico->statu_id = 1;
        if ($tecnico->save())
            return true;
    }

    public function tecnicoCronograma(){
        if(Auth::user()->role_id != 11){
            return Tecnico::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        }else{
            return Tecnico::where('id', Auth::user()->tecnico_id)->lists('nome', 'id')->all();
        }
    }
    public function tecnicoVenda(){
        return Tecnico::where('statu_id', '=', 1)->orderBy('nome', 'asc')->lists('nome', 'id')->all();
    }

    public function relatorio_atividades($data){
        $resultado = Tecnico::with(['clientes' => function($query) use ($data){
            $query->select('clientes.nome', 'clientes.razaosocial', 'clientes.id', 'clientes.statu_id', 'clientes.tecnico_id')->where('clientes.statu_id', '=', 1);
            if($data['cliente_id'] != '')
                $query->where('id', '=', $data['cliente_id']);

            $query->with(['cronogramas' => function($query2) use ($data){
                $query2->select('cronogramas.*', 'atividades.nome as atividade', 'status.nome as statu');
                $query2->join('atividades', 'cronogramas.atividade_id', '=', 'atividades.id')
                    ->join('status', 'cronogramas.statu_id', '=', 'status.id');
                if($data['atividade_id'] != '')
                    $query2->where('atividades.id', '=', $data['atividade_id']);
                if($data['statu_id'] != '')
                    $query2->where('status.id', '=', $data['statu_id']);

                if($data['inicio'] != '' && $data['fim'] != ''){
                    $inicio = $this->auxiliarService->data_banco($data['inicio']);
                    $fim = $this->auxiliarService->data_banco($data['fim']);
                    $query2->whereBetween('vencimento', [$inicio, $fim]);
                }
            }]);
        }]);
        if($data['tecnico_id'] != '')
            $resultado->where('id', '=', $data['tecnico_id']);

        return $resultado->get();
    }

    public function relatorio_pagamento($data){

        return Clientestecnico::join('clientes', 'clientes.id', '=', 'clientestecnicos.cliente_id')
            ->where('tecnico_id',  $data['tecnico_id'])
            ->where('clientes.tipo', 'Assessoria')
            ->select('clientestecnicos.*', 'clientes.*', DB::raw('((clientestecnicos.valor_tecnico / clientes.valor_contrato) * 100) as total'))
            ->orderBy('clientes.razaosocial', 'asc')
            ->get();

    }

}