<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 08:02
 */

namespace App\Repositories;


use App\Centrocusto;
use App\Recebimento;
use Illuminate\Support\Facades\DB;

class CentrocustoRepository
{
    private $centrocusto;
    /**
     * @var Recebimento
     */
    private $recebimento;

    /**
     * CentrocustoRepository constructor.
     */
    public function __construct(Centrocusto $centrocusto)
    {
        $this->centrocusto = $centrocusto;
    }

    public function store($data){
        $this->centrocusto->nome = $data['nome'];

        if($this->centrocusto->save())
            return true;

        return false;
    }

    public function buscarPorId($id){
        return $this->centrocusto->find($id);
    }

    public function update($id, $data){

        $centro = Centrocusto::find($id);
        $centro->id = $id;
        $centro->nome = $data['nome'];

        try {
            if($centro->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function listar(){
        return $this->centrocusto->orderBy('nome', 'asc')->get();
    }

    public function centroCustoSelectBox(){
        return Centrocusto::orderBy('nome', 'asc')->lists('nome', 'id')->all();
    }

    public function cronograma($data){
        $resultado = $this->centrocusto
            ->with(['contas' => function($query) use ($data){
                $query->select('contas.*', DB::raw('sum(total) as totalvalor'));
                if(isset($data['contas']) && !empty($data['contas'])){
                    $query->where('contas.statu_id', '=', 4);
                }else{
                    $query->where('contas.statu_id', '!=', 3);
                }
                if(isset($data['portador']) && $data['portador'] != ''){
                    $query->where('contas.portador', 'like', $data['portador'].'%');
                }
                $query->where('contas.statu_id', '!=', 3);
                $query->where(DB::raw('MONTH(contas.vencimento)'), $data['mes']);
                $query->where(DB::raw('YEAR(contas.vencimento)'), $data['ano']);
                $query->groupBy('contas.centrocusto_id', DB::raw('DAY(contas.vencimento)'));
                $query->orderBy(DB::raw('DAY(contas.vencimento)'), 'asc');
                $query->whereExists(function($query2)
                {
                    $query2->select('*')
                        ->from('recebimentos')
                        ->whereRaw('recebimentos.conta_id = contas.id');
                });
            }]);
        return $resultado->get();
    }

    public function cronograma_pagamento($data){

        $resultado = $this->centrocusto
            ->with(['contas' => function($query) use ($data){
                $query->select('contas.*', DB::raw('sum(total) as totalvalor'));
                if(isset($data['contas']) && !empty($data['contas'])){
                    $query->where('contas.statu_id', '=', 4);
                }else{
                    $query->where('contas.statu_id', '!=', 3);
                }
                if(isset($data['portador']) && $data['portador'] != ''){
                    $query->where('contas.portador', 'like', $data['portador'].'%');
                }
                $query->where(DB::raw('MONTH(contas.vencimento)'), $data['mes']);
                $query->where(DB::raw('YEAR(contas.vencimento)'), $data['ano']);
                $query->groupBy('contas.centrocusto_id', DB::raw('DAY(contas.vencimento)'));
                $query->orderBy(DB::raw('DAY(contas.vencimento)'), 'asc');
                $query->whereExists(function($query2)
                {
                    $query2->select('*')
                        ->from('pagamentos')
                        ->whereRaw('pagamentos.conta_id = contas.id');
                });
            }]);

        return $resultado->get();
    }


}