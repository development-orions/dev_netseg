<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 23/03/2016
 * Time: 10:41
 */

namespace App\Repositories;


use App\Medicosvenda;
use Illuminate\Support\Facades\DB;

class MedicosvendaRepository
{
    /**
     * @var Medicosvenda
     */
    private $medicosvenda;

    /**
     * MedicosvendaRepository constructor.
     */
    public function __construct(Medicosvenda $medicosvenda)
    {
        $this->medicosvenda = $medicosvenda;
    }

    public function store($venda_id, $data){
        for($x=0; $x<3; $x++) {
            if (!empty($data['valor_medico'][$x]) && !empty($data['medico_id'][$x])) {
                $cadastro = new Medicosvenda();
                $cadastro->valor = $data['valor_medico'][$x];
                $cadastro->medico_id = $data['medico_id'][$x];
                $cadastro->venda_id = $venda_id;
                $cadastro->save();
            }
        }
    }

    public function listar($venda_id){
        return $this->medicosvenda->where('venda_id', '=', $venda_id)->get();
    }

    public function relatorio_venda($data){
        return Medicosvenda::join('vendas', 'vendas.id', '=', 'medicosvendas.venda_id')->join('medicos', 'medicos.id', '=', 'medicosvendas.medico_id')
            ->where('medicos.id', '=', $data['medico_id'])
            ->where('vendas.statu_id', '!=', 3)
            ->whereBetween('vendas.created_at', [$data['inicio'], $data['fim']])
            ->select('medicosvendas.valor as valor_medico', 'vendas.*', DB::raw('((medicosvendas.valor / vendas.valor) * 100) as total'))
            ->orderBy('vendas.created_at', 'asc')
            ->get();
    }
}