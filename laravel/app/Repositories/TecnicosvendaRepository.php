<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 23/03/2016
 * Time: 10:40
 */

namespace App\Repositories;


use App\Tecnicosvenda;
use App\Venda;
use Illuminate\Support\Facades\DB;

class TecnicosvendaRepository
{
    /**
     * @var Tecnicosvenda
     */
    private $tecnicosvenda;

    /**
     * TecnicosvendaRepository constructor.
     */
    public function __construct(Tecnicosvenda $tecnicosvenda)
    {
        $this->tecnicosvenda = $tecnicosvenda;
    }

    public function store($venda_id, $data){
        for($x=0; $x<3; $x++) {
            if (!empty($data['valor_tecnico'][$x]) && !empty($data['tecnico_id'][$x])) {
                $cadastro = new Tecnicosvenda();
                $cadastro->valor = $data['valor_tecnico'][$x];
                $cadastro->tecnico_id = $data['tecnico_id'][$x];
                $cadastro->venda_id = $venda_id;
                $cadastro->save();
            }
        }
    }

    public function listar($venda_id){
        return $this->tecnicosvenda->where('venda_id', '=', $venda_id)->get();
    }

    public function relatorio_venda($data){
        return Venda::join('tecnicosvendas', 'tecnicosvendas.venda_id', '=', 'vendas.id')->join('tecnicos', 'tecnicos.id', '=', 'tecnicosvendas.tecnico_id')
            ->where('tecnicos.id', '=', $data['tecnico_id'])
            ->where('vendas.statu_id', '!=', 3)
            ->where('vendas.orcamento', '=', 0)
            ->whereBetween('vendas.created_at', [$data['inicio'], $data['fim']])
            ->select('tecnicosvendas.valor as valor_medico', 'vendas.*', DB::raw('((tecnicosvendas.valor / vendas.valor) * 100) as total'))
            ->orderBy('vendas.created_at', 'asc')
            ->get();
    }
}