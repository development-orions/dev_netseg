<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 09/12/2015
 * Time: 15:59
 */

namespace App\Repositories;


use App\Conta;
use App\Recebimento;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class RecebimentoRepository
{

    /**
     * @var Recebimento
     */
    private $recebimento;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(Recebimento $recebimento, AuxiliarService $auxiliarService){
        $this->recebimento = $recebimento;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($dados, $data){
        $recebimento = new Recebimento();
        $recebimento->conta_id = $dados->id;
        $recebimento->cliente_id = $data['cliente_id'];
        if($recebimento->save())
            return true;

        return false;
    }

    public function update($dados, $data){

            $recebimento = Recebimento::where('conta_id', $dados)->first();
            $recebimento->cliente_id = $data['cliente_id'];

            if($recebimento->save()){
                return true;
            }else{
                return false;
            }

    }

    public function buscar($data){
        $resultado = Recebimento::join('contas', 'contas.id', '=', 'recebimentos.conta_id')->join('clientes', 'clientes.id', '=', 'recebimentos.cliente_id');
        if($data['conta'] != ''){
            $resultado->where('contas.nome', 'like', '%'.$data['conta'].'%');

        }
        if($data['statu_id'] != ''){
            $resultado->where('contas.statu_id', '=', $data['statu_id']);

        }
        if($data['q'] != ''){
            $resultado->where('clientes.nome', 'like', '%'.$data['q'].'%');
        }
        if($data['nf'] != ''){
            $resultado->where('contas.nf', 'like', $data['nf'].'%');

        }
        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('contas.vencimento', [$inicio, $fim]);
        }
        return $resultado->paginate($this->PAGINATE);
    }

    public function conta_cronograma($ano, $mes, $dia, $centrocusto){
        return Recebimento::join('contas', 'contas.id', '=', 'recebimentos.conta_id')
            ->where('contas.centrocusto_id', '=', $centrocusto)
            ->where(DB::raw('MONTH(contas.vencimento)'), $mes)
            ->where(DB::raw('YEAR(contas.vencimento)'), $ano)
            ->where(DB::raw('DAY(contas.vencimento)'), $dia)
            ->orderBy('contas.nome', 'asc')
            ->get();
    }

}