<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 28/01/2016
 * Time: 17:05
 */

namespace App\Repositories;


use App\Observacoe;

class ObservacaoRepository
{
    /**
     * @var Observacoe
     */
    private $observacoe;

    /**
     * ObservacaoRepository constructor.
     */
    public function __construct(Observacoe $observacoe)
    {
        $this->observacoe = $observacoe;
    }

    public function store($id, $data){

        $this->observacoe->cronograma_id = $id;
        $this->observacoe->observacao = $data['observacao'];

        if($this->observacoe->save()) {
            return $this->observacoe;
        }
        return false;
    }

    public function buscarPorCronogramaId($id){
        return $this->observacoe
            ->where('cronograma_id', '=', $id)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function remove($id){
        if(Observacoe::destroy($id))
            return true;

        return false;
    }
}