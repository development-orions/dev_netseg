<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 10/12/2015
 * Time: 16:31
 */

namespace App\Repositories;


use App\Statu;

class StatuRepository
{
    /**
     * @var Statu
     */
    private $statu;

    public function __construct(Statu $statu){

        $this->statu = $statu;
    }

    public function statusConta(){
        return Statu::find([3, 4, 5, 14])->lists('nome', 'id')->all();
    }
    public function statusOrcamento(){
        return Statu::find([6, 7, 2])->lists('nome', 'id')->all();
    }
    public function statusVenda(){
        return Statu::find([7, 3])->lists('nome', 'id')->all();
    }
    public function statusAll(){
        return $this->statu->all();
    }
    public function statusCronograma(){
        return Statu::find([8, 9, 10, 11, 12, 13])->lists('nome', 'id')->all();
    }
}