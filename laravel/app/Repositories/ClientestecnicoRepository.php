<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 26/02/2016
 * Time: 09:54
 */

namespace App\Repositories;


use App\Clientestecnico;

class ClientestecnicoRepository
{
    /**
     * @var Clientestecnico
     */
    private $clientestecnico;

    /**
     * ClientestecnicoRepository constructor.
     */
    public function __construct(Clientestecnico $clientestecnico)
    {
        $this->clientestecnico = $clientestecnico;
    }

    public function store($cliente_id, $data){

        for($x=0; $x<3; $x++){
            if (!empty($data['valor_tecnico'][$x]) && !empty($data['tecnico_id'][$x]) && !empty($data['dia_tecnico'][$x])) {
                $cadastro = new Clientestecnico();
                $cadastro->valor_tecnico = $data['valor_tecnico'][$x];
                $cadastro->tecnico_id = $data['tecnico_id'][$x];
                $cadastro->dia_tecnico = $data['dia_tecnico'][$x];
                $cadastro->cliente_id = $cliente_id;
                $cadastro->save();
            }
        }
    }
    public function buscaPorClienteId($id){
        return Clientestecnico::where('cliente_id', $id)->get();
    }

    public function deletePorClienteId($id){
        $tecnicos = Clientestecnico::where('cliente_id', $id)->get();

        foreach($tecnicos as $tec){
            Clientestecnico::destroy($tec->id);
        }
    }
}