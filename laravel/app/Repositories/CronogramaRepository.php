<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 26/01/2016
 * Time: 07:56
 */

namespace App\Repositories;


use App\Cliente;
use App\Cronograma;
use App\Services\AuxiliarService;
use App\Tecnico;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Validator;

class CronogramaRepository
{
    private $cronograma;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * CronogramaRepository constructor.
     */
    public function __construct(Cronograma $cronograma, AuxiliarService $auxiliarService)
    {

        $this->cronograma = $cronograma;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($cliente_id, $tecnico_id, $atividade_id, $ano, $mes, $data){


        if(!empty($data['arquivo'])) {
            $filename = (md5($data['arquivo']->getClientOriginalName() . time())) . '.' . $data['arquivo']->getClientOriginalExtension();
            $this->cronograma->nome = $data['nome'];
            $this->cronograma->file = $filename;
        }

        $this->cronograma->cliente_id = $cliente_id;
        $this->cronograma->tecnico_id = $tecnico_id;
        $this->cronograma->atividade_id = $atividade_id;
        $this->cronograma->ano = $ano;
        $this->cronograma->mes = date("n", strtotime($data['vencimento']));
        $this->cronograma->vencimento = $data['vencimento'];
        $this->cronograma->statu_id = $data['statu_id'];

        if($this->cronograma->save()) {
            if(!empty($data['arquivo'])) {
                $destinationPath = 'uploads/cronograma';
                $data['arquivo']->move($destinationPath, $filename);
            }
            return $this->cronograma;
        }
        return false;
    }

    public function verifyCronograma($cliente_id, $tecnico_id, $atividade_id, $ano, $mes){
        $check = $this->cronograma
            ->where('atividade_id', '=', $atividade_id)
            ->where('cliente_id', '=', $cliente_id)
            ->where('tecnico_id', '=', $tecnico_id)
            ->where('ano', '=', $ano)
            ->where('mes', '=', $mes)
            ->get();
        if($check){
            return sizeof($check);
        }
        return false;
    }

    public function findByCliente($data){
        return $this->cronograma
            ->where('cliente_id', '=', $data['cliente_id'])
            ->where('tecnico_id', '=', $data['tecnico_id'])
            ->where('ano', '=', $data['ano'])
            ->get();
    }

    public function buscarPorId($id){
        return $this->cronograma->find($id);
    }

    public function update($id, $data){
        $cronograma = Cronograma::find($id);
        $cronograma->id = $id;

        if(Auth::user()->role_id != 11){
            $cronograma->mes = date("n", strtotime($data['vencimento']));
            $cronograma->vencimento = $data['vencimento'];
            $cronograma->statu_id = $data['statu_id'];
        }

        $cronograma->nome = $data['nome'];

        if(!empty($data['arquivo'])) {
            File::delete('uploads/cronograma/'.$cronograma->file);
            $filename = (md5($data['arquivo']->getClientOriginalName().time())).'.'.$data['arquivo']->getClientOriginalExtension();
            $cronograma->file = $filename;
        }


        try {
            if($cronograma->save()) {
                if (!empty($data['arquivo'])) {
                    $destinationPath = 'uploads/cronograma';
                    $data['arquivo']->move($destinationPath, $filename);
                }
                return true;
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function remove($id){
        if(Cronograma::destroy($id))
            return true;

        return false;
    }

    public function deletar_arquivo($id){
        $id = base64_decode($id);
        $cronograma = Cronograma::find($id);

        try {
            if(File::delete('uploads/cronograma/'.$cronograma->file)){
                $cronograma->id = $id;
                $cronograma->nome = null;
                $cronograma->file = null;
                $cronograma->save();
                return true;
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return false;
    }


    public function buscar($data){
        $resultado = Cliente::with(['cronogramas' => function($query) use ($data){
            $query->select('cronogramas.*', 'atividades.nome as atividade', 'status.nome as statu');

            $query
                ->join('atividades', 'cronogramas.atividade_id', '=', 'atividades.id')
                ->join('status', 'cronogramas.statu_id', '=', 'status.id');
            $query->where('cronogramas.tecnico_id', '=', $data['tecnico_id']);
            if($data['atividade_id'] != '')
                $query->where('atividades.id', '=', $data['atividade_id']);
            if($data['statu_id'] != '')
                $query->where('status.id', '=', $data['statu_id']);

            if($data['inicio'] != '' && $data['fim'] != ''){
                $inicio = $this->auxiliarService->data_banco($data['inicio']);
                $fim = $this->auxiliarService->data_banco($data['fim']);
                $query->whereBetween('vencimento', [$inicio, $fim]);
            }
        }]);

        if($data['cliente_id'] != '')  {
            $resultado->where('id', '=', $data['cliente_id']);
        }

        return $resultado->where('statu_id', '=', 1)->get();
    }
    public function buscar_tecnico($data){
        $resultado = Tecnico::orderBy('id');
        if($data['tecnico_id'] != '')
            $resultado->where('tecnicos.id', '=', $data['tecnico_id']);
        return $resultado->get();
    }
    public function listar(){
        return $this->cronograma->groupBy('cliente_id')->get();
    }

    public function listar_por_id_entre_datas($atividade_id, $tecnico_id, $datas)
    {
        return $this->cronograma->where("atividade_id", "=", $atividade_id)->where("tecnico_id", "=", $tecnico_id)->whereBetween("ano", [$datas["inicio"], $datas["fim"]])->get();
    }

}