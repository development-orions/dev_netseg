<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 13:43
 */

namespace App\Repositories;


use App\Aso;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class AsoRepository
{

    /**
     * @var Aso
     */
    private $aso;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var AsostecnicoRepository
     */
    private $asostecnicoRepository;

    public function __construct(Aso $aso, AuxiliarService $auxiliarService){

        $this->aso = $aso;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($data){

        $this->aso->valor = $data['valor'];
        $this->aso->medico_id = $data['medico'];
        $this->aso->paciente_id = $data['paciente'];



        if($this->aso->save()) {
            return base64_encode($this->aso->medico_id. '-' .$this->aso->paciente_id);
        }

        return false;
    }


    public function buscar($medico,$data){
        $resultado = Aso::join('pacientes','pacientes.id', '=', 'asos.paciente_id')->select('asos.*','pacientes.nome')->where('asos.medico_id', '=', $medico);

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('asos.created_at', [$inicio, $fim.' 23:59:59']);
        }
        $resultado->orderBy('created_at', 'desc');
        return $resultado->paginate($this->PAGINATE);
    }

    public function listar($medico){
        return $this->aso->join('pacientes','pacientes.id', '=', 'asos.paciente_id')->select('asos.*','pacientes.nome')->where('asos.medico_id', '=', $medico)
            ->orderBy('created_at', 'desc')
            ->paginate($this->PAGINATE);
    }


    public function buscarPorId($id){
        return $this->aso->find($id);
    }


}