<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 16/11/2015
 * Time: 10:13
 */

namespace App\Repositories;


use App\Atividade;
use App\Cronograma;
use Illuminate\Support\Facades\Auth;

class AtividadeRepository
{
    /**
     * @var Atividade
     */
    private $atividade;
    private $PAGINATE = 20;

    public function __construct(Atividade $atividade){

        $this->atividade = $atividade;
    }

    public function store($data){
        $this->atividade->nome = $data['nome'];
        $this->atividade->statu_id = $data['statu_id'];

        if($this->atividade->save())
            return true;

        return false;
    }

    public function listarTodos()
    {
        return $this->atividade->all();
    }

    public function listar($status){
        return $this->atividade->where('statu_id', $status)->paginate($this->PAGINATE);
    }

    public function buscarPorId($id){
        return $this->atividade->find($id);
    }

    public function update($id, $data){

        $atividade = Atividade::find($id);
        $atividade->id = $id;
        $atividade->nome = $data['nome'];
        $atividade->statu_id = $data['statu_id'];
        try {
            if($atividade->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function inativar($id){
        $atividade = Atividade::findOrFail($id);
        $atividade->statu_id = 2;
        if ($atividade->save())
            return true;
    }
    public function ativar($id){
        $atividade = Atividade::findOrFail($id);
        $atividade->statu_id = 1;
        if ($atividade->save())
            return true;
    }

    public function listar_cronograma($status, $data){

            $resultado = $this->atividade
                ->with(['cronogramas' => function ($query) use ($data) {
                    $query->where('cliente_id', '=', $data['cliente_id']);
                    $query->where('tecnico_id', '=', $data['tecnico_id']);
                    $query->where('ano', '=', $data['ano']);
                }])
                ->where('atividades.statu_id', '=', $status);

            return $resultado->get();

    }

    public function listar_cronograma_por_tecnico_entre_datas($data)
    {
        $data = $this->atividade
                ->with(['cronogramas' => function ($query) use ($data) {
                    $query->where('tecnico_id', '=', $data['tecnico_id']);
                    $query->where('ano', '>=', $data['ano_inicio']);
                    $query->where('ano', '<=', $data['ano_fim']);
                }]);

        return $data;
    }


    public function atividadeCronograma(){
        return Atividade::orderBy('nome', 'asc')->lists('nome', 'id')->all();
    }
}