<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 29/02/2016
 * Time: 09:04
 */

namespace app\Repositories;


use App\Jqcalendar;

class JqcalendarRepository {
    private $jqcalendar;

    public function __construct(Jqcalendar $jqcalendar){
        $this->jqcalendar = $jqcalendar;
    }

    public function store($data, $paciente_id, $medico_id)
    {
        $this->jqcalendar->Subject = $data['Subject'];
        $this->jqcalendar->Location = $data['Location'];
        $this->jqcalendar->Description = $data['Description'];
        $this->jqcalendar->StartTime = $data['StartTime'];
        $this->jqcalendar->EndTime = $data['EndTime'];
        $this->jqcalendar->IsAllDayEvent = $data['IsAllDayEvent'];
        $this->jqcalendar->Color = $data['Color'];
        $this->jqcalendar->RecurringRule = $data['RecurringRule'];

        $this->jqcalendar->exame = $data['exame'];
        $this->jqcalendar->observacoes = $data['observacoes'];
        $this->jqcalendar->medico_id = $medico_id;
        $this->jqcalendar->paciente_id = $paciente_id;

        if($this->jqcalendar->save()) {
            return $this->jqcalendar;
        }
        return false;

    }
    public function update($id, $st, $et)
    {
        $jqcalendar = Jqcalendar::find($id);
        $jqcalendar->StartTime = $st;
        $jqcalendar->EndTime = $et;
        try {
            if($jqcalendar->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }
    public function updateDetails($id, $st, $et, $sub, $ade, $dscr, $loc, $color, $medico_id, $paciente_id, $exame)
    {
        $jqcalendar = Jqcalendar::find($id);
        $jqcalendar->StartTime = $st;
        $jqcalendar->EndTime = $et;

        $jqcalendar->Subject = $sub;
        $jqcalendar->IsAllDayEvent = $ade;
        $jqcalendar->Description = $dscr;
        $jqcalendar->Location = $loc;
        $jqcalendar->Color = $color;

        $jqcalendar->exame = $exame;
        $jqcalendar->medico_id = $medico_id;
        $jqcalendar->paciente_id = $paciente_id;
        try {
            if($jqcalendar->save()) {
                return true;
            }
        }catch (\Exception $e){
            return false;
        }
        return false;
    }

    public function remove($id)
    {
        if($this->jqcalendar->where('Id', '=', $id)->delete())
            return true;

        return false;
    }
    public function lista($sd, $ed){
        return $this->jqcalendar->whereBetween('starttime', [$sd, $ed])->get();
    }

    public function buscarPorId($id){
        return $this->jqcalendar->find($id);
    }

    public function add($st, $et, $sub, $ade, $medico, $paciente, $exame)
    {
        $this->jqcalendar->Subject = $sub;

        $this->jqcalendar->StartTime = $st;
        $this->jqcalendar->EndTime = $et;
        $this->jqcalendar->IsAllDayEvent = $ade;

        $this->jqcalendar->medico_id = $medico;
        $this->jqcalendar->paciente_id = $paciente;
        $this->jqcalendar->exame = $exame;

        if($this->jqcalendar->save()) {
            return $this->jqcalendar;
        }
        return false;
    }

    public function addDetailed($st, $et, $sub, $ade, $desc, $loc, $color, $medico, $paciente, $exame)
    {
        $this->jqcalendar->Subject = $sub;

        $this->jqcalendar->StartTime = $st;
        $this->jqcalendar->EndTime = $et;
        $this->jqcalendar->IsAllDayEvent = $ade;
        $this->jqcalendar->Description = $desc;
        $this->jqcalendar->Location = $loc;
        $this->jqcalendar->Color = $color;

        $this->jqcalendar->medico_id = $medico;
        $this->jqcalendar->paciente_id = $paciente;
        $this->jqcalendar->exame = $exame;

        if($this->jqcalendar->save()) {
            return $this->jqcalendar;
        }
        return false;
    }

    public function getCalendarById($data)
    {
        return $this->jqcalendar->find($data['id']);
    }
}