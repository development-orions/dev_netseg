<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Repositories;


use App\Exame;

class ExameRepository
{
    private $exame;
    /**
     * ExameRepository constructor.
     */
    public function __construct(Exame $exame)
    {
        $this->exame = $exame;
    }

    public function store($data){
        $this->exame->nome = $data['nome'];
        $this->exame->valor = $data['valor'];

        if($this->exame->save())
            return true;

        return false;
    }

    public function buscarPorId($id){
        return $this->exame->find($id);
    }

    public function update($id, $data){

        $exame = Exame::find($id);
        $exame->id = $id;
        $exame->nome = $data['nome'];
        $exame->valor = $data['valor'];

        try {
            if($exame->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function listar(){
        return $this->exame->orderBy('nome', 'asc')->get();
    }

    public function remove($id){
        if(Exame::destroy($id))
            return true;

        return false;
    }

    public function exameSelectbox(){
        return Exame::orderBy('nome')->lists('nome', 'id')->toarray();
    }
}