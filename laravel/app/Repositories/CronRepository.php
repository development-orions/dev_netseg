<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 10/03/2016
 * Time: 11:40
 */

namespace App\Repositories;


use App\Cron;

class CronRepository
{
    /**
     * @var Cron
     */
    private $cron;

    /**
     * CronRepository constructor.
     */
    public function __construct(Cron $cron)
    {
        $this->cron = $cron;
    }

    public function buscar(){
        return Cron::first();
    }

    public function update(){
        if($cron = Cron::find(1)) {
            $cron->data = date('Y-m-d');

            if($cron->save())
                return true;

        }else{
            $this->cron->data = date('Y-m-d');
            if($this->cron->save())
                return true;
        }
    }
}