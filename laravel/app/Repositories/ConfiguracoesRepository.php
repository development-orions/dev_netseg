<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 09/12/2015
 * Time: 08:16
 */

namespace app\Repositories;


use App\Configuracoes;

class ConfiguracoesRepository
{
    private $configuracoes;

    /**
     * ConfiguracoesRepository constructor.
     */
    public function __construct(Configuracoes $configuracoes)
    {
        $this->configuracoes = $configuracoes;
    }

    public function buscar(){
        return Configuracoes::first();
    }

    public function update($data){


        if($configuracoes = Configuracoes::find(1)){
            //caso exista o registro no banco
            $configuracoes->nomefantasia = $data['nomefantasia'];
            $configuracoes->razaosocial = $data['razaosocial'];
            $configuracoes->cnpj = $data['cnpj'];
            $configuracoes->ie = $data['ie'];
            $configuracoes->cep = $data['cep'];
            $configuracoes->rua = $data['rua'];
            $configuracoes->numero = $data['numero'];
            $configuracoes->complemento = $data['complemento'];
            $configuracoes->bairro = $data['bairro'];
            $configuracoes->cidade = $data['cidade'];
            $configuracoes->estado = $data['estado'];
            $configuracoes->formajuridica = $data['formajuridica'];
            $configuracoes->site = $data['site'];
            $configuracoes->email = $data['email'];
            $configuracoes->telefone = $data['telefone'];
            $configuracoes->cnae = $data['cnae'];

            try {
                if($configuracoes->save())
                    return true;
            }catch (\Exception $e){
                return $e->getMessage();
            }
        }else {
            $this->configuracoes->nomefantasia = $data['nomefantasia'];
            $this->configuracoes->razaosocial = $data['razaosocial'];
            $this->configuracoes->cnpj = $data['cnpj'];
            $this->configuracoes->ie = $data['ie'];
            $this->configuracoes->cep = $data['cep'];
            $this->configuracoes->rua = $data['rua'];
            $this->configuracoes->numero = $data['numero'];
            $this->configuracoes->complemento = $data['complemento'];
            $this->configuracoes->bairro = $data['bairro'];
            $this->configuracoes->cidade = $data['cidade'];
            $this->configuracoes->estado = $data['estado'];
            $this->configuracoes->formajuridica = $data['formajuridica'];
            $this->configuracoes->site = $data['site'];
            $this->configuracoes->email = $data['email'];
            $this->configuracoes->telefone = $data['telefone'];
            $this->configuracoes->cnae = $data['cnae'];

            try {
                if($this->configuracoes->save())
                    return true;
            }catch (\Exception $e){
                return $e->getMessage();
            }
        }

    }
}