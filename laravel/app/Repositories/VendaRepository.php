<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 17/12/2015
 * Time: 07:45
 */

namespace App\Repositories;


use App\Cliente;
use App\Itens;
use App\Services\AuxiliarService;
use App\Venda;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class VendaRepository
{

    /**
     * VendaRepository constructor.
     */
    private $venda;
    private $PAGINATE = 20;
    /**
     * @var Itens
     */
    private $itens;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * @var Cliente
     */
    private $cliente;
    /**
     * @var ParcelaRepository
     */
    private $parcelaRepository;

    public function __construct(Venda $venda, Itens $itens, AuxiliarService $auxiliarService, Cliente $cliente, ParcelaRepository $parcelaRepository)
    {
        $this->venda = $venda;
        $this->itens = $itens;
        $this->auxiliarService = $auxiliarService;
        $this->cliente = $cliente;
        $this->parcelaRepository = $parcelaRepository;
    }

    public function store($data){
        $now = date("Y-m-d");
        $now = date('Y-m-d', strtotime($now. ' + 15 days'));

        $this->venda->validade = $now;
        $this->venda->orcamento = $data['orcamento'];
        $this->venda->parcela = $data['parcela'];
        $this->venda->aprovado = $data['aprovado'];
        $this->venda->statu_id = $data['statu'];

        if($data['orcamento'] == 0) { // venda
            $this->venda->fechamento = $data['fechamento'];
            $this->venda->entrega = $data['entrega'];
            $this->venda->num_boleto = $data['num_boleto'];
            $this->venda->nf = $data['nf'];
            $this->venda->valoradministrativo = $data['valoradministrativo'];
        }


        if(isset($data['temnota'])) {
            $this->venda->temnota = 1;
        }else{
            $this->venda->temnota = 0;
        }
        $this->venda->formapagamento_id = $data['formapagamento_id'];
        $this->venda->cliente_id = $data['cliente_id'];
        $this->venda->user_id = Auth::user()->id;

        $total = 0;

        if($this->venda->save()) {


            if(Session::has('servico')){

                foreach(Session::get('servico') as $servicos){
                    $combo = '';
                    if(isset($servicos['servicos_combo'])) {
                        foreach ($servicos['servicos_combo'] as $sv_combo) {
                            $combo = $combo . ',' . $sv_combo;
                        }
                    }

                    $servicos['valor'] = $this->auxiliarService->moeda($servicos['valor']);

                    $this->venda->servicos()->attach($servicos['servico_id']->id, array('descricao'=>$servicos['descricao'], 'valor'=>$servicos['valor'], 'cargahoraria'=>$servicos['cargahoraria'], 'participantes'=>$servicos['participantes'], 'periodo'=>$servicos['periodo'], 'horario'=> $servicos['horario'], 'servico'=> $combo));

                    $total = $total + $servicos['valor'];
                }
            }

            Session::forget('servico');
            Session::forget('cliente_orcamento');

            $this->venda->valor = $total;



            $this->venda->save();

            return $this->venda;
        }

        return false;
    }

    public function buscarPorId($id){
        return $this->venda->find($id);
    }

    public function verificaTipo($id){

        $tipoids = Itens::where('servico_venda.venda_id', $id)->lists('servico_id')->all();

        $medicina = 0;
        $treinamento = 0;
        $combo = 0;
        $normal = 0;

        // VERIFICANDO OS TIPOS DE SERVIÇO DO ORÇAMENTO
        foreach($tipoids as $id){
            if($id == 1){
                $treinamento++;
            }elseif($id == 2){
                $medicina++;
            }elseif($id == 3){
                $combo++;
            }else{
                $normal++;
            }
        }

        if($combo != 0){
            $template = "Combo";
        }elseif($combo == 0 && $medicina != 0 && $treinamento == 0 && $normal == 0){
            $template = "Medicina";
        }elseif($combo == 0 && $treinamento != 0 && $medicina == 0 && $normal == 0){
            $template = "Treinamento";
        }elseif($combo == 0 && $treinamento == 0 && $medicina == 0 && $normal != 0){
            $template = "Normal";
        }else{
            $template = "Combo";
        }

         return $template;

    }

    public function listar(){
        return $this->cliente
            ->join('vendas','vendas.cliente_id' , '=','clientes.id')
            ->join('formapagamentos', 'formapagamentos.id', '=', 'vendas.formapagamento_id')
            ->select('vendas.*', 'clientes.razaosocial', 'formapagamentos.nome')
            ->where('vendas.statu_id', '!=', 2)
            ->where('vendas.convertidoemvenda', '=', null)
            ->where('vendas.orcamento', '=', 1)
            ->orderBy('id', 'desc')
            ->paginate($this->PAGINATE);
    }

    public function buscar($data){
        $resultado = Cliente::join('vendas','vendas.cliente_id' , '=','clientes.id' )
            ->join('formapagamentos', 'formapagamentos.id', '=', 'vendas.formapagamento_id')
            ->select('vendas.*', 'clientes.razaosocial', 'formapagamentos.nome')
            ->where('vendas.convertidoemvenda', '=', null)
            ->where('vendas.orcamento', '=', 1);

        if($data['venda'] != ''){
            $resultado->where('vendas.id', '=', $data['venda']);

        }
        if($data['statu_id'] != ''){
            $resultado->where('vendas.statu_id', '=', $data['statu_id']);

        }
        if($data['q'] != ''){
            $resultado
                ->where('clientes.razaosocial', 'like', '%'.$data['q'].'%');
        }
        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('vendas.created_at', [$inicio, $fim]);
        }
        $resultado->orderBy('id', 'desc');
        return $resultado->paginate($this->PAGINATE);
    }

    public function converter_orcamento($id, $data){
        $orcamento = Venda::find($id);

        $this->venda->validade = $orcamento->validade;
        $this->venda->orcamento = 0;
        $this->venda->aprovado = 1;
        $this->venda->fechamento = $this->auxiliarService->data_banco($data['fechamento']);
        $this->venda->entrega = $this->auxiliarService->data_banco($data['entrega']);

        if(isset($data['num_boleto'])) {
            $this->venda->num_boleto = $data['num_boleto'];
        }
        $this->venda->temnota = $orcamento->temnota;

        if($orcamento->temnota == 1){
            $this->venda->nf = $data['nf'];
        }

        $this->venda->valoradministrativo = $data['valoradministrativo'];

        $this->venda->statu_id = 7;
        $this->venda->formapagamento_id = $orcamento->formapagamento_id;
        $this->venda->cliente_id = $orcamento->cliente_id;
        $this->venda->user_id = Auth::user()->id;
        $this->venda->venda_id = $orcamento->id;
        $this->venda->valor = $orcamento->valor;



        try {
            if($this->venda->save()) {

                if(Session::has('servico')){
                    foreach(Session::get('servico') as $servicos){
                        $combo = '';
                        if(isset($servicos['servicos_combo'])) {
                            foreach ($servicos['servicos_combo'] as $sv_combo) {
                                $combo = $combo . ',' . $sv_combo;
                            }
                        }
                        $servicos['valor'] = $this->auxiliarService->moeda($servicos['valor']);
                        $this->venda->servicos()->attach($servicos['servico_id']->id, array('descricao'=>$servicos['descricao'], 'valor'=>$servicos['valor'], 'cargahoraria'=>$servicos['cargahoraria'], 'participantes'=>$servicos['participantes'], 'periodo'=>$servicos['periodo'], 'horario'=> $servicos['horario'], 'servico'=> $combo));

                    }
                }
                Session::forget('servico');
                Session::forget('cliente_orcamento');

                $this->venda->save();

                $orcamento->id = $id;
                $orcamento->convertidoemvenda = 1;
                $orcamento->save();

                $this->parcelaRepository->copia($this->venda->id, $orcamento);

                return $this->venda;
            }
        }catch (\Exception $e){

            return $e->getMessage();
        }
        return false;
    }

    public function update($id, $data){

        $now = date("Y-m-d");
        $now = date('Y-m-d', strtotime($now. ' + 15 days'));

        $orcamento = Venda::find($id);

        $orcamento->validade = $now;
        $orcamento->orcamento = $data['orcamento'];

        $orcamento->parcela = $data['parcela'];
        $orcamento->aprovado = $data['aprovado'];
        $orcamento->statu_id = 6;


        if(isset($data['temnota'])) {
            $orcamento->temnota = 1;
        }else{
            $orcamento->temnota = 0;
        }
        $orcamento->formapagamento_id = $data['formapagamento_id'];
        $orcamento->cliente_id = $data['cliente_id'];
        $orcamento->user_id = Auth::user()->id;

        $total = 0;

        if($orcamento->save()) {

            if(Session::has('servico')){
                $orcamento->servicos()->detach();

                foreach(Session::get('servico') as $servicos){
                    $combo = '';
                    if(isset($servicos['servicos_combo'])) {
                        foreach ($servicos['servicos_combo'] as $sv_combo) {
                            $combo = $combo . ',' . $sv_combo;
                        }
                    }

                    $servicos['valor'] = $this->auxiliarService->moeda($servicos['valor']);

                    $orcamento->servicos()->attach($servicos['servico_id']->id, array('descricao'=>$servicos['descricao'], 'valor'=>$servicos['valor'], 'cargahoraria'=>$servicos['cargahoraria'], 'participantes'=>$servicos['participantes'], 'periodo'=>$servicos['periodo'], 'horario'=> $servicos['horario'], 'servico'=> $combo));

                    $total = $total + $servicos['valor'];
                }
            }
            Session::forget('servico');
            Session::forget('cliente_orcamento');

            $orcamento->valor = $total;

            $orcamento->save();

            return $orcamento;
        }
        return false;

    }

    //VENDAS

    public function listar_venda(){
        return $this->cliente
            ->join('vendas','vendas.cliente_id' , '=','clientes.id')
            ->join('formapagamentos', 'formapagamentos.id', '=', 'vendas.formapagamento_id')
            ->select('vendas.*', 'clientes.razaosocial', 'formapagamentos.nome')
            ->where('vendas.orcamento', '=', 0)
            ->orderBy('vendas.id', 'desc')
            ->paginate($this->PAGINATE);
    }

    public function buscar_venda($data){
        $resultado = Cliente::join('vendas','vendas.cliente_id' , '=','clientes.id' )
            ->join('formapagamentos', 'formapagamentos.id', '=', 'vendas.formapagamento_id')
            ->select('vendas.*', 'clientes.razaosocial', 'formapagamentos.nome')
            ->where('vendas.orcamento', '=', 0);

        if($data['venda'] != ''){
            $resultado->where('vendas.id', '=', $data['venda']);

        }
        if($data['statu_id'] != ''){
            $resultado->where('vendas.statu_id', '=', $data['statu_id']);

        }
        if($data['q'] != ''){
            $resultado
                ->where('clientes.razaosocial', 'like', '%'.$data['q'].'%');
        }
        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('vendas.created_at', [$inicio, $fim]);
        }
        return $resultado->orderBy('vendas.id', 'desc')->paginate($this->PAGINATE);
    }

    //CONVERTIDOS

    public function listar_convertidos(){
        return $this->cliente
            ->join('vendas','vendas.cliente_id' , '=','clientes.id')
            ->join('formapagamentos', 'formapagamentos.id', '=', 'vendas.formapagamento_id')
            ->select('vendas.*', 'clientes.razaosocial', 'formapagamentos.nome')
            ->where('vendas.convertidoemvenda', '=', 1)
            ->orderBy('vendas.id', 'desc')
            ->where('vendas.orcamento', '=', 1)->paginate($this->PAGINATE);
    }

    public function buscar_convertidos($data){
        $resultado = Cliente::join('vendas', 'vendas.cliente_id', '=', 'clientes.id')
            ->join('formapagamentos', 'formapagamentos.id', '=', 'vendas.formapagamento_id')
            ->select('vendas.*', 'clientes.razaosocial', 'formapagamentos.nome')
            ->where('vendas.convertidoemvenda', '=', 1)
            ->where('vendas.orcamento', '=', 1);

        if ($data['venda'] != '') {
            $resultado->where('vendas.id', '=', $data['venda']);

        }

        if ($data['q'] != '') {
            $resultado
                ->where('clientes.razaosocial', 'like', '%' . $data['q'] . '%');
        }
        if ($data['inicio'] != '' && $data['fim'] != '') {
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('vendas.created_at', [$inicio, $fim]);
        }
        $resultado->orderBy('vendas.id', 'desc');
        return $resultado->paginate($this->PAGINATE);
    }
    public function cancelar($id, $data){
        $venda = Venda::findOrFail($id);
        $venda->statu_id = 3;
        $venda->motivo = $data['motivo'];
        if ($venda->save())
            return true;
    }

    public function aprovar($id){
        $venda = Venda::findOrFail($id);
        $venda->statu_id = 7;
        if ($venda->save())
            return true;
    }
    public function inativar($id){
        $venda = Venda::findOrFail($id);
        $venda->statu_id = 2;
        if ($venda->save())
            return true;
    }

    public function ativar($id){
        $venda = Venda::findOrFail($id);
        $venda->statu_id = 6;
        if ($venda->save())
            return true;
    }
}