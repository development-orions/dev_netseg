<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 13:43
 */

namespace App\Repositories;


use App\Arquivo;
use App\Arquivoscliente;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\Auth;

class ArquivoRepository
{

    /**
     * @var Arquivo
     */
    private $arquivo;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var ArquivostecnicoRepository
     */
    private $arquivostecnicoRepository;

    public function __construct(Arquivo $arquivo, AuxiliarService $auxiliarService, Arquivoscliente $arquivoscliente){

        $this->arquivo = $arquivo;
        $this->arquivoscliente = $arquivoscliente;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($data){

        $filename = (md5($data['arquivo']->getClientOriginalName().time())).'.'.$data['arquivo']->getClientOriginalExtension();

        $this->arquivo->nome = $data['nome'];
        $this->arquivo->file = $filename;



        if($this->arquivo->save()) {

            $destinationPath = 'uploads/arquivo';
            $data['arquivo']->move($destinationPath,$filename);


            if($data['todos'] == 1) {
                $this->arquivoscliente->arquivo_id = $this->arquivo->id;
                $this->arquivoscliente->todos = $data['todos'];
                $this->arquivoscliente->save();
            }else{
                foreach ($data['ids'] as $id){
                    $arquivoscliente = new Arquivoscliente();
                    $arquivoscliente->arquivo_id = $this->arquivo->id;
                    $arquivoscliente->cliente_id = $id;
                    $arquivoscliente->todos = $data['todos'];
                    $arquivoscliente->save();
                }
            }

            return $this->arquivo;
        }

        return false;
    }


    public function listar_arquivo(){
        return $this->arquivo
            ->orderBy('arquivos.created_at', 'desc')
            ->paginate($this->PAGINATE);
    }

    public function arquivos_cliente(){

        $id = Auth::user()->cliente_id;

        return $this->arquivo
            ->join('arquivosclientes','arquivosclientes.arquivo_id' , '=','arquivos.id')
            ->where('arquivosclientes.cliente_id',$id)
            ->orWhere('arquivosclientes.todos',1)
            ->orderBy('arquivos.created_at', 'desc')
            ->paginate($this->PAGINATE);
    }

    public function buscar_arquivo($data){
        $resultado = Arquivo::select('*');

        if($data['arquivo'] != ''){
            $resultado->where('arquivos.nome', '=', $data['arquivo']);

        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('arquivos.created_at', [$inicio, $fim]);
        }
        return $resultado->orderBy('arquivos.id', 'desc')->paginate($this->PAGINATE);
    }


    public function listar_clientes($id){
        $clientes = $this->arquivoscliente
            ->join('clientes','clientes.id' , '=','arquivosclientes.cliente_id')
            ->where('arquivosclientes.arquivo_id',$id)
            ->orderBy('clientes.nome')
            ->lists('clientes.nome')->all();

        $clientes = implode("<br>", $clientes);

        return $clientes;
    }

   

    public function buscarPorId($id){
        return $this->arquivo->find($id);
    }


}