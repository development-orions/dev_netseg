<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Repositories;


use App\Servico;

class ServicoRepository
{
    private $servico;
    /**
     * ServicoRepository constructor.
     */
    public function __construct(Servico $servico)
    {
        $this->servico = $servico;
    }

    public function store($data){
        $this->servico->nome = $data['nome'];
        //$this->servico->valor = $data['valor'];

        if($this->servico->save())
            return true;

        return false;
    }

    public function buscarPorId($id){
        return $this->servico->find($id);
    }

    public function update($id, $data){

        $servico = Servico::find($id);
        $servico->id = $id;
        $servico->nome = $data['nome'];
        //$servico->valor = $data['valor'];

        try {
            if($servico->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function listar(){
        return $this->servico->all();
    }
}