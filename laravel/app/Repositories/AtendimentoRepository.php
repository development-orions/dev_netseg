<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Repositories;


use App\Atendimento;
use App\Repositories\AtendimentoExameRepository;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\DB;

class AtendimentoRepository
{
    private $atendimento;
    private $PAGINATE = 20;
    /**
     * AtendimentoRepository constructor.
     */
    public function __construct(Atendimento $atendimento, AtendimentoExameRepository $atendimentoExameRepository, AuxiliarService $auxiliarService)
    {
        $this->atendimento = $atendimento;
        $this->atendimentoExameRepository = $atendimentoExameRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($data){
        $this->atendimento->descricao = $data['descricao'];
        $this->atendimento->data = $this->auxiliarService->data_banco($data['data']);
        $this->atendimento->observacao = $data['observacao'];
        $this->atendimento->paciente_id = $data['paciente'];
        if($data['medico'] != '')
        $this->atendimento->medico_id = $data['medico'];
        $this->atendimento->statu_id = 1;
        if($this->atendimento->save()) {
            $this->atendimentoExameRepository->create($this->atendimento->id,$data);
            return true;
        }

        return false;
    }

    public function buscarPorId($id){
        return $this->atendimento->select('atendimentos.*', DB::raw("CONCAT(pacientes.nome, ' - ', pacientes.cpf) as paciente_nome"))->where('atendimentos.id',$id)->join('pacientes', 'atendimentos.paciente_id', '=', 'pacientes.id')->first();
    }

    public function update($id, $data){

        $atendimento = Atendimento::find($id);
        $atendimento->id = $id;
        $atendimento->descricao = $data['descricao'];
        $atendimento->data = $this->auxiliarService->data_banco($data['data']);
        $atendimento->observacao = $data['observacao'];
        $atendimento->paciente_id = $data['paciente'];
        if($data['medico'] != '')
        $atendimento->medico_id = $data['medico'];
        $atendimento->statu_id = 1;

        try {
            if($atendimento->save()) {
                $this->atendimentoExameRepository->update($id,$data);
                return true;
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return false;
    }

    public function listar(){
        return $this->atendimento->with(['paciente','medico','statu','atendimentoExames'])->paginate($this->PAGINATE);
    }

    public function buscar($data){
        $resultado =  $this->atendimento->with(['paciente','medico','statu','atendimentoExames']);

        if($data['statu_id'] != ''){
            $resultado->where('atendimentos.statu_id', '=', $data['statu_id']);
        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('atendimentos.data', [$inicio, $fim]);
        }
        return $resultado->paginate($this->PAGINATE);
    }

    public function medico_exame($data){
        $resultado =  $this->atendimento->select('exames.nome as exame', 'atendimento_exames.valor', 'atendimento_exames.valor_medico','pacientes.nome', 'pacientes.funcao', 'atendimentos.data', 'clientes.nome as empresa')
            ->join('atendimento_exames','atendimento_exames.atendimento_id' , '=','atendimentos.id')
            ->join('pacientes','pacientes.id' , '=','atendimentos.paciente_id')
            ->join('clientes','clientes.id' , '=','pacientes.cliente_id')
            ->join('exames','atendimento_exames.exame_id' , '=','exames.id')
            ->where('atendimentos.statu_id', '=', 1)
            ->orderby('atendimentos.data');

        if($data['medico_id'] != ''){
            $resultado->where('atendimentos.medico_id', '=', $data['medico_id']);
            $resultado->where('atendimento_exames.medico_id', '=', null);
        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('atendimentos.data', [$inicio, $fim]);
        }


        return $resultado->get();
    }

    public function med_exame($data){
        $resultado =  $this->atendimento->select('exames.nome as exame', 'atendimento_exames.valor', 'atendimento_exames.valor_medico','pacientes.nome', 'pacientes.funcao', 'atendimentos.data', 'clientes.nome as empresa')
            ->join('atendimento_exames','atendimento_exames.atendimento_id' , '=','atendimentos.id')
            ->join('pacientes','pacientes.id' , '=','atendimentos.paciente_id')
            ->join('clientes','clientes.id' , '=','pacientes.cliente_id')
            ->join('exames','atendimento_exames.exame_id' , '=','exames.id')
            ->where('atendimentos.statu_id', '=', 1)
            ->orderby('atendimentos.data');

        if($data['medico_id'] != ''){
            $resultado->where('atendimento_exames.medico_id', '=', $data['medico_id']);
        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('atendimentos.data', [$inicio, $fim]);
        }


        return $resultado->get();
    }

    public function atendimento_medico($data){
        $resultado =  $this->atendimento->select('atendimentos.*','pacientes.nome')
            ->join('pacientes','atendimentos.paciente_id' , '=','pacientes.id')
            ->where('atendimentos.statu_id', '=', 1);

        if($data['medico_id'] != ''){
            $resultado->where('atendimentos.medico_id', '=', $data['medico_id']);
        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('atendimentos.data', [$inicio, $fim]);
        }

        return $resultado->get();
    }

    public function laboratorio_exame($data){
        $resultado =  $this->atendimento->select('exames.nome as exame', 'atendimento_exames.valor', 'atendimento_exames.valor_medico','pacientes.nome')
            ->join('atendimento_exames','atendimento_exames.atendimento_id' , '=','atendimentos.id')
            ->join('pacientes','pacientes.id' , '=','atendimentos.paciente_id')
            ->join('exames','atendimento_exames.exame_id' , '=','exames.id')
            ->where('atendimentos.statu_id', '=', 1);

        if($data['laboratorio_id'] != ''){
            $resultado->where('atendimento_exames.laboratorio_id', '=', $data['laboratorio_id']);
        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('atendimentos.data', [$inicio, $fim]);
        }

        return $resultado->get();
    }

    public function empresa_exame($data){
        $resultado =  $this->atendimento->select('exames.nome as exame', 'atendimento_exames.valor', 'atendimento_exames.valor_medico','pacientes.nome', 'pacientes.funcao', 'atendimentos.data', 'atendimentos.descricao')
            ->join('atendimento_exames','atendimento_exames.atendimento_id' , '=','atendimentos.id')
            ->join('exames','atendimento_exames.exame_id' , '=','exames.id')
            ->join('pacientes','atendimentos.paciente_id' , '=','pacientes.id')
            ->where('atendimentos.statu_id', '=', 1)
            ->orderby('atendimentos.data');

        if($data['empresa_id'] != ''){
            $resultado->where('pacientes.cliente_id', '=', $data['empresa_id']);
        }

        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('atendimentos.data', [$inicio, $fim]);
        }
        
       // dd($resultado->get());

        return $resultado->get();
    }

    public function remove($id){
        if(Atendimento::destroy($id))
            return true;

        return false;
    }
}