<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 13:43
 */

namespace App\Repositories;


use App\Cliente;
use App\User;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\DB;

class ClienteRepository
{

    /**
     * @var Cliente
     */
    private $cliente;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var ClientestecnicoRepository
     */
    private $clientestecnicoRepository;

    public function __construct(Cliente $cliente, AuxiliarService $auxiliarService, ClientestecnicoRepository $clientestecnicoRepository){

        $this->cliente = $cliente;
        $this->auxiliarService = $auxiliarService;
        $this->clientestecnicoRepository = $clientestecnicoRepository;
    }

    public function store($data){
        $this->cliente->nome = $data['nome'];
        $this->cliente->razaosocial = $data['razaosocial'];
        $this->cliente->cnpj = $data['cnpj'];
        $this->cliente->ie = $data['ie'];
        $this->cliente->cpf = $data['cpf'];
        $this->cliente->cei_autonomo = $data['cei_autonomo'];
        $this->cliente->cep = $data['cep'];
        $this->cliente->rua = $data['rua'];
        $this->cliente->numero = $data['numero'];
        $this->cliente->complemento = $data['complemento'];
        $this->cliente->bairro = $data['bairro'];
        $this->cliente->cidade = $data['cidade'];
        $this->cliente->estado = $data['estado'];
        $this->cliente->formajuridica = $data['formajuridica'];
        $this->cliente->responsavel = $data['responsavel'];
        $this->cliente->funcao = $data['funcao'];
        $this->cliente->email_responsavel = $data['email_responsavel'];
        $this->cliente->telefone_responsavel = $data['telefone_responsavel'];
        $this->cliente->statu_id = $data['statu_id'];
        $this->cliente->tipo = $data['tipo'];

        if(isset($data['valor_contrato'])) {
            $this->cliente->valor_contrato = $data['valor_contrato'];
        }
        if($data['data_contrato'] != null) {
            $this->cliente->data_contrato = $this->auxiliarService->data_banco($data['data_contrato']);
        }
        if($this->cliente->save()) {

            if($data['tipo'] == 'Assessoria') {
                $this->clientestecnicoRepository->store($this->cliente->id, $data);
            }


            $razao = utf8_decode($this->cliente->razaosocial);
            $razao = utf8_encode(strtolower($razao));
            $razao = $this->auxiliarService->slug($razao, '');

            $this->cliente->usuario = mb_substr($razao, 0, 8).$this->cliente->id;
            $this->cliente->senha = mb_substr($razao, 0, 4).'@'.$this->cliente->created_at->format('Y');

            if($this->cliente->save()) {

                $user = new User();
                $user->nome =  $this->cliente->razaosocial;
                $user->funcao = 'Cliente';
                $user->email = '';
                $user->username = $this->cliente->usuario;
                $user->password = bcrypt($this->cliente->senha);
                $user->role_id = 9;
                $user->statu_id = $this->cliente->statu_id;
                $user->cliente_id = $this->cliente->id;
                $user->save();
            }


            return $this->cliente;
        }

        return false;
    }

    public function store_para_funcoes($data){
        $this->cliente->nome = $data['nome'];
        $this->cliente->razaosocial = $data['razaosocial'];
        $this->cliente->cnpj = $data['cnpj'];
        $this->cliente->ie = $data['ie'];
        $this->cliente->cep = $data['cep'];
        $this->cliente->rua = $data['rua'];
        $this->cliente->numero = $data['numero'];
        $this->cliente->complemento = $data['complemento'];
        $this->cliente->bairro = $data['bairro'];
        $this->cliente->cidade = $data['cidade'];
        $this->cliente->estado = $data['estado'];
        $this->cliente->formajuridica = $data['formajuridica'];
        $this->cliente->responsavel = $data['responsavel'];
        $this->cliente->funcao = $data['funcao'];
        $this->cliente->email_responsavel = $data['email_responsavel'];
        $this->cliente->telefone_responsavel = $data['telefone_responsavel'];
        $this->cliente->statu_id = $data['statu_id'];
        $this->cliente->tipo = $data['tipo'];
        $this->cliente->cpf = $data['cpf'];
        $this->cliente->cei_autonomo = $data['cei_autonomo'];

        if(isset($data['valor_contrato'])) {
            $this->cliente->valor_contrato = $data['valor_contrato'];
        }
        if($data['data_contrato'] != null) {
            $this->cliente->data_contrato = $this->auxiliarService->data_banco($data['data_contrato']);
        }

        if($this->cliente->save()) {

            if($data['tipo'] == 'Assessoria') {
                $this->clientestecnicoRepository->store($this->cliente->id, $data);
            }

            return $this->cliente;
        }
        return false;
    }

    public function listar($status){
        return $this->cliente->where('statu_id', $status)->orderBy('razaosocial', 'asc')->paginate($this->PAGINATE);
    }

    public function buscar($status, $texto){
        if(isset($texto)){
            return $this->cliente->where('statu_id', $status)
                ->where(function($query) use ($texto)
                {
                    $query->where('nome', 'LIKE', '%'. $texto['q'] .'%')
                        ->orWhere('razaosocial', 'LIKE', '%'. $texto['q'] .'%');
                })
                ->orderBy('razaosocial', 'asc')
                ->paginate($this->PAGINATE);
        }else{
            return $this->cliente->where('statu_id', $status)->paginate($this->PAGINATE);
        }
    }

    public function buscarPorId($id){
        return $this->cliente->find($id);
    }

    public function buscarPorIdPdf($id){
        return Cliente::select('id','nome','cnpj')->where('id', $id)->get();
    }


    public function adicionar($id){
        $cliente = Cliente::find($id);
        return $cliente;
    }

    public function update($id, $data){

        $v = Validator::make($data,
            [
                'cnpj' => 'required|unique:clientes,cnpj,'.$data['cnpj'].',cnpj'
            ]
        );
        //verifica se os campos foram validados
        if (!$v->fails())
        {
            $cliente = Cliente::find($id);
            $cliente->id = $id;
            $cliente->nome = $data['nome'];
            $cliente->razaosocial = $data['razaosocial'];
            $cliente->cnpj = $data['cnpj'];
            $cliente->ie = $data['ie'];
            $cliente->cep = $data['cep'];
            $cliente->rua = $data['rua'];
            $cliente->numero = $data['numero'];
            $cliente->complemento = $data['complemento'];
            $cliente->bairro = $data['bairro'];
            $cliente->cidade = $data['cidade'];
            $cliente->estado = $data['estado'];
            $cliente->formajuridica = $data['formajuridica'];
            $cliente->responsavel = $data['responsavel'];
            $cliente->funcao = $data['funcao'];
            $cliente->email_responsavel = $data['email_responsavel'];
            $cliente->telefone_responsavel = $data['telefone_responsavel'];
            $cliente->statu_id = $data['statu_id'];
            $cliente->tipo = $data['tipo'];
            $cliente->valor_contrato = $data['valor_contrato'];
            $cliente->cpf = $data['cpf'];
            $cliente->cei_autonomo = $data['cei_autonomo'];

            if($data['data_contrato'] != null) {
                $cliente->data_contrato = $this->data_banco($data['data_contrato']);
            }
            try {
                if($cliente->save()) {

                    if($data['tipo'] == 'Assessoria') {
                        $this->clientestecnicoRepository->deletePorClienteId($cliente->id);
                        $this->clientestecnicoRepository->store($cliente->id, $data);
                    }

                    return true;
                }
            }catch (\Exception $e){
                return $e->getMessage();
            }
        }

        return false;
    }

    public function data_banco($data){
        $data_contrato = explode(' ', $data);
        $dia  = $data_contrato[0];
        $ano = $data_contrato[2];
        $data_contrato = explode(',', $data_contrato[1]);

        $meses = array('Janeiro' => 01, 'Fevereiro' => 02, 'Março' => 03, 'Abril' => 04, 'Maio' => 05, 'Junho' => 06, 'Julho' => 07, 'Agosto' => 8, 'Setembro' => 9, 'Outubro' => 10, 'Novembro' => 11, 'Dezembro' => 12);
        $mes = $meses[$data_contrato[0]];
        $dataformatada = $ano.'-'.$mes.'-'.$dia;

        return $dataformatada;
    }
    public function inativar($id){
        $cliente = Cliente::findOrFail($id);
        $cliente->statu_id = 2;
        if ($cliente->save())
            return true;
    }
    public function ativar($id){
        $cliente = Cliente::findOrFail($id);
        $cliente->statu_id = 1;
        if ($cliente->save())
            return true;
    }

    public function buscarCliente($conta){
        return Cliente::find($conta->recebimentos[0]->cliente_id);;
    }


    /**
     * Verifica se o cnpj já está cadastrado no sistema
     * @author Silvio Luis
     * @param string $cnpj
     * @return bool
     */
    public function verificaCnpj($cnpj){
        $conta = Cliente::where('cnpj', '=', $cnpj)->count();
        if($conta){
            return false;
        }
        return true;
    }
    /**
     * Verifica cnpj para edição
     * @author Silvio Luis
     * @param $id
     * @param $cnpj
     * @return bool
     */
    public function verificaCnpjEdicao($id, $cnpj){
        $conta = Cliente::where('id', '!=', $id)->where('cnpj', '=', $cnpj)->count();
        if($conta){
            return false;
        }
        return true;
    }

    /**
     * Busca Ajax por clientes que estão ativos
     * @param $busca
     * @return mixed
     */
    public function buscar_cliente($busca)
    {
        return Cliente::where('statu_id', '=', 1)
            ->where(function ($q) use ($busca){
                $q->where('razaosocial', 'LIKE', '%'. $busca .'%')
                    ->orWhere('nome', 'LIKE', '%'. $busca .'%');
            })
            ->orderBy('razaosocial', 'asc')->take(4)->get();
    }


    public function listarBusca(){

        $clientes = collect();

        Cliente::select('id','nome')->where('statu_id',1)->chunk(100, function ($users) use ($clientes) {
            foreach ($users as $user) {
                $clientes->push($user->id.' - '.$user->nome);
            }
        });


        return(json_encode($clientes));
    }

    public function listar_drop($status,$term,$page){

        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $breeds = $this->cliente->select('id', DB::raw("CONCAT(nome, ' - ', cnpj) as text"))->where('statu_id', $status)->where(function ($query) use ($term) {
            $query->where('nome', 'LIKE',  '%' .$term. '%')
                ->orWhere('cnpj', 'LIKE',  '%' .$term. '%');})->orderBy('text', 'asc')->skip($offset)->take($resultCount)->get(['id','text']);

        $count = $this->cliente->select('id')->where('statu_id', $status)->where(function ($query) use ($term) {
            $query->where('nome', 'LIKE',  '%' .$term. '%')
                ->orWhere('cnpj', 'LIKE',  '%' .$term. '%');})->skip($offset)->take($resultCount)->count();
        $endCount = $offset + $resultCount;
        $morePages = $endCount < $count;

        $results = array(
            "results" => $breeds,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);

    }


}