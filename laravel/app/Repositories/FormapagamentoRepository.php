<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 16/11/2015
 * Time: 10:13
 */

namespace App\Repositories;


use App\Formapagamento;

class FormapagamentoRepository
{
    /**
     * @var Formapagamento
     */
    private $formapagamento;

    public function __construct(Formapagamento $formapagamento){

        $this->formapagamento = $formapagamento;
    }

    public function formaPagamentoSelectBox(){
        return Formapagamento::orderBy('nome', 'asc')->lists('nome', 'id')->all();
    }
    public function formaPagamentoVenda(){
        return Formapagamento::find([1, 2, 3, 4])->lists('nome', 'id')->all();
    }
}