<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 06/01/2016
 * Time: 09:27
 */

namespace App\Repositories;


use App\Parcela;
use App\Services\AuxiliarService;

class ParcelaRepository
{
    private $parcela;
    private $auxiliarService;
    /**
     * ParcelaRepository constructor.
     */
    public function __construct(Parcela $parcela, AuxiliarService $auxiliarService)
    {
        $this->parcela = $parcela;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($id, $data){
        foreach($data['data']['Parcela'] as $parcelas){

            $parcelas['data'] = $this->auxiliarService->converteDataBanco( $parcelas['data']);

            $parcela = Parcela::create(['numero' => $parcelas['numero'], 'data' => $parcelas['data'], 'venda_id' => $id]);
            $parcela->save();
        }
    }

    public function copia($id, $data){
        foreach($data->parcelas as $parcelas){

            $parcela = Parcela::create(['numero' => $parcelas['numero'], 'data' => $parcelas['data'], 'venda_id' => $id]);
            $parcela->save();

        }
    }

    public function delete($id){
        return $this->parcela->where('venda_id', '=', $id)->delete();
    }
}