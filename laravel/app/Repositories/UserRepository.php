<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 30/11/2015
 * Time: 09:02
 */

namespace app\Repositories;

use App\User;
use App\Cliente;

class UserRepository {

    /**
     * @var User
     */
    private $user;
    private $PAGINATE = 20;

    public function __construct(User $user, Cliente $cliente){
        $this->user = $user;
        $this->cliente = $cliente;
    }

    public function listar(){
        return $this->user->where('role_id','!=',9)->paginate($this->PAGINATE);
    }

    public function buscar($texto){
        if(isset($texto)){
            return $this->user
                ->where('funcao', 'LIKE', '%'. $texto['q'] .'%')
                ->paginate($this->PAGINATE);
        }else{
            return $this->user->paginate($this->PAGINATE);
        }

    }
    public function store($data){
        $this->user->nome = $data['nome'];
        $this->user->funcao = $data['funcao'];
        $this->user->email = $data['email'];
        $this->user->username = $data['username'];
        $this->user->password = bcrypt($data['password']);
        $this->user->role_id = $data['role_id'];
        $this->user->statu_id = $data['statu_id'];
        if($this->user->save())
            return true;

        return false;
    }


    public function usermigrate(){

        $clientes = $this->cliente->get();

        foreach ($clientes as $cli) {

            $user = new User();
            $user->nome = $cli['razaosocial'];
            $user->funcao = 'Cliente';
            $user->email = '';
            $user->username = $cli['usuario'];
            $user->password = bcrypt($cli['senha']);
            $user->role_id = 9;
            $user->statu_id = $cli['statu_id'];
            $user->cliente_id = $cli['id'];
            $user->save();
//                return true;
        }
        return true;
    }



    public function update($id, $data){
        $user = User::find($id);
        $user->nome = $data['nome'];
        $user->funcao = $data['funcao'];
        $user->email = $data['email'];
        $user->role_id = $data['role_id'];

        try {
            if($user->save())

                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return false;
    }

    public function resetpassaword($id, $data){
        $user = User::find($id);
        $user->password = bcrypt($data['password']);
        try {
            if($user->save())
                return true;
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return false;
    }

    public function buscarPorId($id){
        return $this->user->find($id);
    }

    public function ativar($id)
    {
        $user = User::findOrFail($id);
        $user->statu_id = 2;
        if ($user->save())
            return true;
        /*return DB::table('user')
            ->where('id', $id)
            ->update(['statu_id' => 1]);*/
    }

    public function inativar($id)
    {
        $user = User::findOrFail($id);
        $user->statu_id = 1;
        if ($user->save())
            return true;
        /*return DB::table('user')
            ->where('id', $id)
            ->update(['statu_id' => 2]);*/
    }
}