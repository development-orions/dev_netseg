<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 05/01/2016
 * Time: 10:33
 */

namespace App\Repositories;


use App\Log;

class LogRepository {

    private $log;
    private $PAGINATE = 30;

    public function __construct(Log $log){
        $this->log = $log;
    }

    public function store($data){
        $this->log->log = $data;

        if($this->log->save())
            return true;

        return false;
    }

    public function remove($id){
        if(Log::destroy($id))
            return true;

        return false;
    }

    public function listar(){
        return $this->log->orderBy('created_at', 'desc')->paginate($this->PAGINATE);
    }

    public function buscar($data){
        return $resultado = Log::whereBetween('created_at', [$data['inicio'], $data['fim']])->orderBy('created_at', 'desc')->paginate($this->PAGINATE);
    }

}