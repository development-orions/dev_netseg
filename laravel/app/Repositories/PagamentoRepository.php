<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 09/12/2015
 * Time: 16:07
 */

namespace App\Repositories;


use App\Pagamento;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PagamentoRepository
{

    /**
     * @var Pagamento
     */
    private $pagamento;
    private $PAGINATE = 20;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(Pagamento $pagamento, AuxiliarService $auxiliarService){
        $this->pagamento = $pagamento;
        $this->auxiliarService = $auxiliarService;
    }

    public function store($dados, $data){
        $pagamento = new Pagamento();
        $pagamento->conta_id = $dados->id;
        $pagamento->fornecedore_id = $data['fornecedor_id'];
        if($pagamento->save())
            return true;

        return false;
    }
    public function update($dados, $data){

            $pagamento = Pagamento::where('conta_id', $dados)->first();
            $pagamento->fornecedore_id = $data['fornecedor_id'];
            if($pagamento->save()){
                return true;
            }else{
                return false;
            }
    }

    public function buscar($data){
        $resultado = Pagamento::join('contas', 'contas.id', '=', 'pagamentos.conta_id')->join('fornecedores', 'fornecedores.id', '=', 'pagamentos.fornecedore_id');
        if($data['conta'] != ''){
            $resultado->where('contas.nome', 'like', '%'.$data['conta'].'%');

        }
        if($data['statu_id'] != ''){
            $resultado->where('contas.statu_id', '=', $data['statu_id']);

        }
        if($data['q'] != ''){
            $resultado->where('fornecedores.nome', 'like', '%'.$data['q'].'%');
        }
        if($data['inicio'] != '' && $data['fim'] != ''){
            $inicio = $this->auxiliarService->data_banco($data['inicio']);
            $fim = $this->auxiliarService->data_banco($data['fim']);
            $resultado->whereBetween('contas.vencimento', [$inicio, $fim]);
        }
        return $resultado->paginate($this->PAGINATE);
    }

    public function conta_cronograma($ano, $mes, $dia, $centrocusto){
        return Pagamento::join('contas', 'contas.id', '=', 'pagamentos.conta_id')
            ->where('contas.centrocusto_id', '=', $centrocusto)
            ->where(DB::raw('MONTH(contas.vencimento)'), $mes)
            ->where(DB::raw('YEAR(contas.vencimento)'), $ano)
            ->where(DB::raw('DAY(contas.vencimento)'), $dia)
            ->orderBy('contas.nome', 'asc')
            ->get();
    }


}