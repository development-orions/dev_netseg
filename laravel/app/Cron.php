<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model {

    protected $table = 'crons';
    public $timestamps = false;

}
