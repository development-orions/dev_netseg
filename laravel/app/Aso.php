<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Aso extends Model {

    protected $table = 'asos';


    public function paciente(){
        return $this->belongsTo('App\Paciente', 'paciente_id');
    }
    public function medico(){
        return $this->belongsTo('App\Medico', 'medico_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

}
