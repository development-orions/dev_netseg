<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Atendimento
 * @package App\Models\Admin
 * @version September 25, 2018, 5:06 pm -03
 *
 * @property \Illuminate\Database\Eloquent\Collection clientes
 * @property \Illuminate\Database\Eloquent\Collection cursos
 * @property \Illuminate\Database\Eloquent\Collection AtendimentoExame
 * @property \Illuminate\Database\Eloquent\Collection pagamentos
 * @property \Illuminate\Database\Eloquent\Collection videos
 * @property string nome
 * @property integer drop
 * @property integer ontop
 * @property integer onfooter
 * @property integer ordem
 */
class AtendimentoExame extends Model
{

    public $table = 'atendimento_exames';
    public $timestamps = false;


    public function exame()
    {
        return $this->belongsTo(\App\Exame::class, 'exame_id');
    }

    public function laboratorio()
    {
        return $this->belongsTo(\App\Laboratorio::class, 'laboratorio_id');
    }

    public function getValorRAttribute()
    {
        return "R$ ".number_format($this->valor, 2, ",", ".");
    }

    public function getValorMedAttribute()
    {
        return "R$ ".number_format($this->valor_medico, 2, ",", ".");
    }

}
