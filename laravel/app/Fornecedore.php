<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedore extends Model {
    protected $table = 'fornecedores';
    protected $fillable = ['nome', 'email_responsavel', 'razaosocial', 'statu_id', 'cnpj', 'ie', 'telefone_responsavel', 'rua', 'numero', 'bairro', 'complemento', 'estado', 'cidade', 'cep', 'responsavel', 'funcao', 'valor_contrato', 'data_contrato'];
    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }
    public function pagamentos(){
        return $this->hasMany('App\Pagamento', 'fornecedore_id');
    }
    public function produtos(){
        return $this->hasMany('App\Produto', 'fornecedore_id');
    }
}
