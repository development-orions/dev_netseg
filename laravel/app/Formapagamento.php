<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Formapagamento extends Model {
    protected $table = 'formapagamentos';
    public $timestamps = false;
    public function contas(){
        return $this->hasMany('App/Conta', 'formapagamento_id');
    }
    public function vendas(){
        return $this->hasMany('App/Venda', 'formapagamento_id');
    }

}
