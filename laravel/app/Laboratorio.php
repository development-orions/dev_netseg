<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model {

    protected $table = 'laboratorios';
    protected $fillable = ['nome'];

 
    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
}
