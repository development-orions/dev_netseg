<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Arquivoscliente extends Model {
    protected $table = 'arquivosclientes';
    public $timestamps = false;
    protected $fillable = ['arquivo_id', 'cliente_id', 'id'];
    public function arquivos()
    {
        return $this->belongsTo('App\Arquivo', 'arquivo_id');
    }
    public function clientes()
    {
        return $this->belongsTo('App\Cliente', 'cliente_id');
    }

}
