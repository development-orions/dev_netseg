<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model {

    protected $table = 'pagamentos';
    public $timestamps = false;

    protected $fillable = ['fornecedore_id', 'conta_id'];

    public function contas(){
        return $this->belongsTo('App\Conta', 'conta_id');
    }
    public function fornecedores(){
        return $this->belongsTo('App\Fornecedore', 'fornecedore_id');
    }

}
