<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

use App\Role;
use App\Statu;
use App\Repositories\UserRepository;
use App\User;

use Illuminate\Support\Facades\Route;
use Validator;

class UserController extends Controller {

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository){

        $this->userRepository= $userRepository;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        return view('users');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param UserService $userService
     * @return Response
     */
	public function store(UserService $userService)
	{
		$input = Request::all();
        return $userService->create($input);
	}

    public function usermigrate(UserService $userService)
    {
        return $userService->usermigrate();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param UserService $userService
     * @return Response
     */
	public function show($id, UserService $userService)
	{
        $id = base64_decode($id);
        $user = $userService->buscarPorId($id);
        return view('visualizar.user_view', compact('user'));
        //return redirect('visualizar/cliente_view/'.base64_encode($id), compact('cliente'));
	}
	public function show2(UserService $userService)
	{
        $user = $userService->buscarPorId(Auth::user()->id);
        return view('visualizar.user_viewapp', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    /**
     * @param UserService $userService
     * @return \Illuminate\View\View
     */
    public function listar_user(UserService $userService)
    {
        $users = $userService->listar();
        return view('users', compact('users', 'status'));
    }

    /**
     * @param UserService $userService
     * @return \Illuminate\View\
     */
    public function buscar_user(UserService $userService)
    {
        $texto = Request::all();
        $users = $userService->buscar($texto);

        return view('users', compact('users', 'texto'));
    }

    public function user(){
        return view('users');
    }

    public function update($id, UserService $userService)
    {
        $data = Request::all();
        return $userService->update($id, $data);
    }

    public function resetpassword($id, UserService $userService)
    {
        $data = Request::all();
        return $userService->resetpassword($id, $data);
    }

    public function ativar_user($id, UserService $userService)
    {
        $id = base64_decode($id);
        $userService->ativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/user_lista/');
    }

    public function inativar_user($id, UserService $userService)
    {
        $id = base64_decode($id);
        $userService->inativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/user_lista/');
    }

}
