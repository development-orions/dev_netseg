<?php namespace App\Http\Controllers;

use App\Fornecedore;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\FornecedorRepository;
use App\Services\FornecedorService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;


class FornecedorController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(FornecedorService $fornecedorService)
	{
		//
        $input = Request::all();
        return $fornecedorService->create($input);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, FornecedorService $fornecedorService)
	{
		$id = base64_decode($id);
        $fornecedor = $fornecedorService->buscarPorId($id);
        return view('visualizar.fornecedor_view', compact('fornecedor'));
   //     return redirect('visualizar/cliente_view/'.base64_encode($id), compact('cliente'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, FornecedorService $fornecedorService)
	{
		$data = Request::all();
        return $fornecedorService->update($id, $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function ativar_fornecedor($id, FornecedorService $fornecedorService)
    {
        $id = base64_decode($id);
        $fornecedorService->ativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/fornecedor_lista/2');
    }

    public function inativar_fornecedor($id, FornecedorService $fornecedorService)
    {
        $id = base64_decode($id);
        $fornecedorService->inativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/fornecedor_lista/1');
    }

	// ajax
	public function buscar_fornecedor($busca, FornecedorRepository $fornecedorRepository)
	{
		$fornecedor = $fornecedorRepository->buscar_fornecedor($busca);
		return response()->json($fornecedor);
	}
}
