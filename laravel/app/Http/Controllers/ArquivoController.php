<?php namespace App\Http\Controllers;

use App\Arquivo;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\ArquivoRepository;
use App\Services\ArquivoService;
use App\Services\ClienteService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class ArquivoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

    public function arquivos(ClienteService $clienteService){
        $clientes = $clienteService->listarBusca();
        return view('arquivos.arquivo_add', compact('clientes'));
    }

	public function store(ArquivoService $arquivoService){
		$input = Request::all();
		return $arquivoService->create($input);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
//	public function show($id, ArquivoService $arquivoService, TecnicoService $tecnicoService)
//	{
//		$id = base64_decode($id);
//		$arquivo = $arquivoService->buscarPorId($id);
//		return view('visualizar.arquivo_view', compact('arquivo'));
//		//return redirect('visualizar/arquivo_view/'.base64_encode($id), compact('arquivo'));
//	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, ArquivoService $arquivoService)
	{
		$data = Request::all();
		return $arquivoService->update($id, $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function listar_arquivo(ArquivoService $arquivoService)
    {
        $arquivos = $arquivoService->listar_arquivo();
        return view('arquivos.arquivos', compact('arquivos'));
    }

    public function arquivos_cliente(ArquivoService $arquivoService)
    {
        $arquivos = $arquivoService->arquivos_cliente();
        return view('arquivos.arquivos_cliente', compact('arquivos'));
    }

    public function buscar_arquivo(ArquivoService $arquivoService)
    {
        $data = Request::all();
        $arquivos = $arquivoService->buscar_arquivo($data);
        return view('arquivos.arquivos', compact('arquivos', 'data'));
    }

}
