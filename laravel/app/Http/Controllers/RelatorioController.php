<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\CentrocustoRepository;
use App\Repositories\StatuRepository;
use App\Services\AuxiliarService;
use App\Services\ConfiguracoesService;
use App\Services\MedicoService;
use App\Services\RelatorioService;
use App\Services\TecnicoService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Barryvdh\Snappy\Facades\SnappyPdf as SnappyPDF;

class RelatorioController extends Controller {
	/**
	 * @var RelatorioService
	 */
	private $relatorioService;
	/**
	 * @var StatuRepository
	 */
	private $statuRepository;
	/**
	 * @var CentrocustoRepository
	 */
	private $centrocustoRepository;
	/**
	 * @var ConfiguracoesService
	 */
	private $configuracoesService;
	/**
	 * @var AuxiliarService
	 */
	private $auxiliarService;
	/**
	 * @var TecnicoService
	 */
	private $tecnicoService;
	/**
	 * @var MedicoService
	 */
	private $medicoService;


	/**
	 * RelatorioController constructor.
	 */
	public function __construct(RelatorioService $relatorioService, StatuRepository $statuRepository, CentrocustoRepository $centrocustoRepository, ConfiguracoesService $configuracoesService, AuxiliarService $auxiliarService, TecnicoService $tecnicoService, MedicoService $medicoService)
	{
		$this->relatorioService = $relatorioService;
		$this->statuRepository = $statuRepository;
		$this->centrocustoRepository = $centrocustoRepository;
		$this->configuracoesService = $configuracoesService;
		$this->auxiliarService = $auxiliarService;
		$this->tecnicoService = $tecnicoService;
		$this->medicoService = $medicoService;
	}

	public function conta($id){
		$status = $this->statuRepository->statusConta();
		$centrocusto = $this->centrocustoRepository->centroCustoSelectBox();

		if(Session::has('message')){
			$mensagem = Session::get('message');
		}

		if($id == 1){
			return view('relatorio.conta_pagar', compact('status', 'centrocusto', 'mensagem'));
		}else{
			return view('relatorio.conta_receber', compact('status', 'centrocusto', 'mensagem'));
		}
	}

	public function buscar_conta($tipo)
	{
		$data = Request::all();
		$resultado = $this->relatorioService->conta($tipo, $data);


		if(sizeof($resultado) != 0){
			$total = 0;
			foreach($resultado as $conta){
				$total += $conta->contas->total;
				$conta->contas->total = $this->auxiliarService->format($conta->contas->total);
			}
			$total = $this->auxiliarService->format($total);

			$configuracoes = $this->configuracoesService->buscar();

			$template = 'relatorio_conta';

			$view =  \View::make('pdf.'.$template, compact('configuracoes','resultado', 'tipo', 'total'))->render();

            $pdf = SnappyPDF::loadHTML($view)->setPaper('a4', 'landscape');

            return $pdf->inline($template.'.pdf');
		}else{
			return redirect(Route::getCurrentRoute()->getPrefix().'/relatorio_conta/' . $tipo)->with('message', 'Nenhum resultado encontrado!');
		}
	}

	public function pagamentotecnico(){

		Session::forget('total_contrato');
		Session::forget('total_tecnico');
		Session::forget('total_p');

		$tecnicos = $this->tecnicoService->tecnico_list();
		return view('relatorio.pagamentotecnico', compact('tecnicos'));
	}

	public function buscar_pagamentotecnico(){
		$data = Request::all();
		$resultado = $this->relatorioService->pagamentotecnico($data);
		$tecnico = $this->tecnicoService->buscarPorId($data['tecnico_id']);
		$configuracoes = $this->configuracoesService->buscar();
		$template = 'relatorio_pagamentotecnico';
		$view =  \View::make('pdf.'.$template, compact('configuracoes','resultado', 'tecnico'))->render();
		$pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
		$pdf->loadHTML($view);
		return $pdf->stream($template);
	}

	public function relatorio_venda($tipo){
		$tecnicos = $this->tecnicoService->tecnico_list();
		$medicos = $this->medicoService->medicoVenda();
		return view('relatorio.vendas', compact('tipo', 'tecnicos', 'medicos'));
	}

	public function relatorio_venda_post($tipo){
		$data = Request::all();
		$resultado = $this->relatorioService->relatorio_vendas($tipo, $data);


		if($tipo == 1){
			$medico = $this->medicoService->buscarPorId($data['medico_id']);
		}else{
			$tecnico = $this->tecnicoService->buscarPorId($data['tecnico_id']);
		}

		$configuracoes = $this->configuracoesService->buscar();
		$template = 'relatorio_vendas';
		$view =  \View::make('pdf.'.$template, compact('configuracoes','resultado', 'tipo', 'medico', 'tecnico'))->render();
		$pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
		$pdf->loadHTML($view);
		return $pdf->stream($template);
	}

}
