<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\CentrocustoRepository;
use App\Services\GraficoService;
use Illuminate\Http\Request;

class GraficoController extends Controller {

    /**
     * @var GraficoService
     */
    private $graficoService;
    /**
     * @var CentrocustoRepository
     */
    private $centrocustoRepository;

    public function __construct(GraficoService $graficoService, CentrocustoRepository $centrocustoRepository)
    {
        $this->graficoService = $graficoService;
        $this->centrocustoRepository = $centrocustoRepository;
    }


    public function carrega_grafico(Requests\GraficoPost $request)
	{
        $input = $request->all();
		return response()->json($this->graficoService->gerarGrafico($input));
	}

	public function centrodecustos(){
        return response()->json($this->centrocustoRepository->listar());
    }

    public function carrega_graficoResultado(Requests\GraficoPost $request)
    {
        $input = $request->all();
        return response()->json($this->graficoService->gerarGraficoResultado($input));
    }

}
