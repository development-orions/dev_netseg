<?php namespace App\Http\Controllers;

use App\Tecnico;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\TecnicoService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class TecnicoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(TecnicoService $tecnicoService)
	{
		$input = Request::all();
        return $tecnicoService->create($input);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, TecnicoService $tecnicoService)
	{
		$id = base64_decode($id);
        $tecnico = $tecnicoService->buscarPorId($id);
        return view('visualizar.tecnico_view', compact('tecnico'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, TecnicoService $tecnicoService)
	{
        $data = Request::all();
        return $tecnicoService->update($id, $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}



    public function inativar_tecnico($id, TecnicoService $tecnicoService){
        $id = base64_decode($id);
        $tecnicoService->inativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/tecnico_lista/1');
    }

    public function ativar_tecnico($id, TecnicoService $tecnicoService){
        $id = base64_decode($id);
        $tecnicoService->ativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/tecnico_lista/2');
    }

}
