<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AtendimentoExameRepository;
use App\Repositories\ClienteRepository;
use App\Repositories\ExameRepository;
use App\Repositories\LaboratorioRepository;
use App\Repositories\MedicoRepository;
use App\Repositories\PacienteRepository;
use App\Services\AtendimentoService;
use App\Services\AuxiliarService;
use App\Services\ConfiguracoesService;
use App\Services\ExameService;
use App\Services\LaboratorioService;
use App\Services\JqcalendarService;
use App\Services\MedicoService;
use App\Services\PacienteService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;

class MedicinaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PacienteService $pacienteService)
    {
        $input = Request::all();
        return $pacienteService->create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, PacienteService $pacienteService)
    {
        $id = base64_decode($id);
        $paciente = $pacienteService->buscarPorId($id);
        return view('visualizar.paciente_view', compact('paciente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param PacienteService $pacienteService
     * @return Response
     */

    public function update($id, PacienteService $pacienteService)
    {
        $data = Request::all();
        return $pacienteService->update($id, $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function inativar_paciente($id, PacienteService $pacienteService){
        $id = base64_decode($id);
        $pacienteService->inativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/paciente_lista/1');
    }
    public function ativar_paciente($id, PacienteService $pacienteService){
        $id = base64_decode($id);
        $pacienteService->ativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/paciente_lista/2');
    }

    public function paciente(){
        return view('paciente.paciente_add');
    }

    public function listar_paciente($status, PacienteService $pacienteService)
    {
        $pacientes = $pacienteService->listar($status);
        return view('paciente.pacientes', compact('pacientes', 'status'));
    }

    public function buscar_paciente($status, PacienteService $pacienteService)
    {
        $texto = Request::all();
        $pacientes = $pacienteService->buscar($status, $texto);

        return view('paciente.pacientes', compact('pacientes', 'texto', 'status'));
    }

    public function editar_paciente($id, PacienteService $pacienteService){
        $id = base64_decode($id);
        $paciente_edit = $pacienteService->buscarPorId($id);
        return view('paciente.paciente_add', compact('paciente_edit'));
    }

    public function buscar_medico($busca, MedicoRepository $medicoRepository)
    {
        $medico = $medicoRepository->buscar_medico($busca);
        return response()->json($medico);
    }

    public function buscar_pacienteagenda($busca, PacienteRepository $pacienteRepository)
    {
        $paciente = $pacienteRepository->buscar_paciente($busca);
        return response()->json($paciente);
    }


    /******************
     **  LABORATORIO **
     ******************/

    public function laboratorio(){
        return view('laboratorio.laboratorio_add');
    }
    public function laboratorio_store(LaboratorioService $laboratorioService){
        $input = Request::all();
        return $laboratorioService->create($input);
    }
    public function editar_laboratorio($id, LaboratorioService $laboratorioService){
        $id = base64_decode($id);
        $laboratorio_edit = $laboratorioService->buscarPorId($id);
        return view('laboratorio.laboratorio_add', compact('laboratorio_edit'));
    }
    public function update_laboratorio($id, LaboratorioService $laboratorioService)
    {
        $data = Request::all();
        return $laboratorioService->update($id, $data);
    }
    public function listar_laboratorio(LaboratorioService $laboratorioService)
    {
        $laboratorios = $laboratorioService->listar();
        return view('laboratorio.laboratorios', compact('laboratorios'));
    }
    public function laboratorio_delete($id,LaboratorioService $laboratorioService)
    {
        $id = base64_decode($id);
        return $laboratorioService->deletar($id);
    }


    /****************
     **  EXAMES **
     ****************/
    public function exame(){
        return view('exame.exame_add');
    }
    public function exame_store(ExameService $exameService){
        $input = Request::all();
        return $exameService->create($input);
    }
    public function editar_exame($id, ExameService $exameService){
        $id = base64_decode($id);
        $exame_edit = $exameService->buscarPorId($id);
        return view('exame.exame_add', compact('exame_edit'));
    }
    public function update_exame($id, ExameService $exameService)
    {
        $data = Request::all();
        return $exameService->update($id, $data);
    }
    public function listar_exame(ExameService $exameService)
    {
        $exames = $exameService->listar();
        return view('exame.exames', compact('exames'));
    }
    public function exame_delete($id,ExameService $exameService)
    {
        $id = base64_decode($id);
        return $exameService->deletar($id);
    }

    /*******************
     **  ATENDIMENTO **
     ****************/
    public function atendimento(MedicoService $medicoService, ExameRepository $exameRepository, LaboratorioRepository $laboratorioRepository){
        $medicos = $medicoService->listar_drop(1);
        $exames = $exameRepository->exameSelectbox();
        $laboratorios = $laboratorioRepository->laboratorioSelectbox();
        return view('atendimento.atendimento_add',compact('medicos','exames', 'laboratorios'));
    }
    public function atendimento_store(AtendimentoService $atendimentoService){
        $input = Request::all();
        return $atendimentoService->create($input);
    }

    public function editar_atendimento($id, AtendimentoService $atendimentoService,MedicoService $medicoService, ExameRepository $exameRepository, LaboratorioRepository $laboratorioRepository, AtendimentoExameRepository $atendimentoExameRepository){
        $id = base64_decode($id);
        $medicos = $medicoService->listar_drop(1);
        $exames = $exameRepository->exameSelectbox();
        $laboratorios = $laboratorioRepository->laboratorioSelectbox();
        $atendimento_edit = $atendimentoService->buscarPorId($id);
        $atendimento_exames= $atendimentoExameRepository->list_atendimento($id);
        return view('atendimento.atendimento_add', compact('atendimento_edit','medicos','exames', 'laboratorios','atendimento_exames'));
    }

    public function update_atendimento($id, AtendimentoService $atendimentoService)
    {
        $data = Request::all();
        return $atendimentoService->update($id, $data);
    }

    public function listar_atendimento(AtendimentoService $atendimentoService)
    {
        $data = Request::all();
        $atendimentos = $atendimentoService->listar($data);
        return view('atendimento.atendimentos', compact('atendimentos','data'));
    }

    public function atendimento_cancela($id,AtendimentoService $atendimentoService)
    {
        $id = base64_decode($id);
        return $atendimentoService->deletar($id);
    }

    public function medico_exame(AtendimentoService $atendimentoService, MedicoService $medicoService, AuxiliarService $auxiliarService)
    {
        $data = Request::all();
        $medicos = $medicoService->listar_drop(1);
        if(!empty($data)) {
            $atendimentos = $atendimentoService->medico_exame($data);



            if($atendimentos){
                $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
                $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));
                return view('atendimento.medico_exame', compact('atendimentos','medicos','data','valor_total','valor_medico_total'));
            }else{
                return view('atendimento.medico_exame',compact('medicos'));
            }
        }else{
            return view('atendimento.medico_exame',compact('medicos'));
        }
    }

    public function laboratorio_exame(AtendimentoService $atendimentoService, LaboratorioService $laboratorioService, AuxiliarService $auxiliarService)
    {
        $data = Request::all();
        $laboratorios = $laboratorioService->listar_drop();
        if(!empty($data)) {
            $atendimentos = $atendimentoService->laboratorio_exame($data);

            if($atendimentos){
                $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
                $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));
                return view('atendimento.laboratorio_exame', compact('atendimentos','laboratorios','data','valor_total','valor_medico_total'));
            }else{
                return view('atendimento.laboratorio_exame',compact('laboratorios'));
            }
        }else{
            return view('atendimento.laboratorio_exame',compact('laboratorios'));
        }
    }


    public function atendimento_medico(AtendimentoService $atendimentoService, MedicoService $medicoService)
    {
        $data = Request::all();
        $medicos = $medicoService->listar_drop(1);
        if(!empty($data)) {
            $atendimentos = $atendimentoService->atendimento_medico($data);
            if($atendimentos){
                return view('atendimento.atendimento_medico', compact('atendimentos','medicos','data'));
            }else{
                return view('atendimento.atendimento_medico',compact('medicos'));
            }
        }else{
            return view('atendimento.atendimento_medico',compact('medicos'));
        }
    }


    public function empresa_exame(AtendimentoService $atendimentoService, AuxiliarService $auxiliarService, ClienteRepository $clienteRepository)
    {
        $data = Request::all();
        if(!empty($data)) {
            $atendimentos = $atendimentoService->empresa_exame($data);

            if($atendimentos){
                $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
                $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));
                $cliente= $clienteRepository->buscarPorId($data['empresa_id']);

                return view('atendimento.empresa_exame', compact('atendimentos','data','valor_total','cliente','valor_medico_total'));
            }else{
                return view('atendimento.empresa_exame');
            }
        }else{
            return view('atendimento.empresa_exame');
        }
    }


    public function med_exame(AtendimentoService $atendimentoService, MedicoService $medicoService, AuxiliarService $auxiliarService)
    {
        $data = Request::all();
        $medicos = $medicoService->listar_drop(1);
        if(!empty($data)) {
            $atendimentos = $atendimentoService->med_exame($data);
            if($atendimentos){
                $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
                $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));
                return view('atendimento.med_exame', compact('atendimentos','medicos','data','valor_total','valor_medico_total'));
            }else{
                return view('atendimento.med_exame',compact('medicos'));
            }
        }else{
            return view('atendimento.med_exame',compact('medicos'));
        }
    }


    public function medico_exame_pdf(AtendimentoService $atendimentoService, MedicoService $medicoService, AuxiliarService $auxiliarService, ConfiguracoesService $configuracoesService)
    {
        $data = Request::all();
        if(!empty($data)) {
            $medico = $medicoService->buscarPorId($data['medico_id']);
            $atendimentos = $atendimentoService->medico_exame($data);
            $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
            $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));

            $configuracoes = $configuracoesService->buscar();
            $template = 'medico_exame';
            $view = \View::make('pdf.' . $template, compact('configuracoes', 'atendimentos', 'medico', 'valor_total', 'valor_medico_total', 'data'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option('enable_html5_parser', true);
            $pdf->getDomPDF()->get_option('enable_html5_parser');
            $pdf->loadHTML($view);
            return $pdf->stream($template);
        }


    }

    public function med_exame_pdf(AtendimentoService $atendimentoService, MedicoService $medicoService, AuxiliarService $auxiliarService, ConfiguracoesService $configuracoesService)
    {
        $data = Request::all();
        if(!empty($data)) {
            $medico = $medicoService->buscarPorId($data['medico_id']);
            $atendimentos = $atendimentoService->med_exame($data);
            $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
            $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));

            $configuracoes = $configuracoesService->buscar();
            $template = 'med_exame';
            $view = \View::make('pdf.' . $template, compact('configuracoes', 'atendimentos', 'medico', 'valor_total', 'valor_medico_total', 'data'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option('enable_html5_parser', true);
            $pdf->getDomPDF()->get_option('enable_html5_parser');
            $pdf->loadHTML($view);
            return $pdf->stream($template);
        }

    }

    public function laboratorio_exame_pdf(AtendimentoService $atendimentoService, LaboratorioService $laboratorioService, AuxiliarService $auxiliarService, ConfiguracoesService $configuracoesService)
    {
        $data = Request::all();

        if(!empty($data)) {
            $atendimentos = $atendimentoService->laboratorio_exame($data);
            $laboratorio = $laboratorioService->buscarPorId($data['laboratorio_id']);

            $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
            $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));

            $configuracoes = $configuracoesService->buscar();
            $template = 'laboratorio_exame';
            $view = \View::make('pdf.' . $template, compact('configuracoes', 'atendimentos', 'laboratorio', 'valor_total', 'valor_medico_total', 'data'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option('enable_html5_parser', true);
            $pdf->getDomPDF()->get_option('enable_html5_parser');
            $pdf->loadHTML($view);
            return $pdf->stream($template);
        }
    }


    public function atendimento_medico_pdf(AtendimentoService $atendimentoService, MedicoService $medicoService, ConfiguracoesService $configuracoesService)
    {
        $data = Request::all();

        if(!empty($data)) {
            $atendimentos = $atendimentoService->atendimento_medico($data);
            $medico = $medicoService->buscarPorId($data['medico_id']);


            $configuracoes = $configuracoesService->buscar();
            $template = 'atendimento_medico';
            $view =  \View::make('pdf.'.$template, compact('configuracoes','atendimentos','medico','data'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option('enable_html5_parser', true);
            $pdf->getDomPDF()->get_option('enable_html5_parser');
            $pdf->loadHTML($view);
            return $pdf->stream($template);
        }
    }


    public function empresa_exame_pdf(AtendimentoService $atendimentoService, AuxiliarService $auxiliarService, ClienteRepository $clienteRepository, ConfiguracoesService $configuracoesService)
    {
        $data = Request::all();
        if(!empty($data)) {
            $atendimentos = $atendimentoService->empresa_exame($data);

            $valor_total = $auxiliarService->formatRs($atendimentos->sum('valor'));
            $valor_medico_total = $auxiliarService->formatRs($atendimentos->sum('valor_medico'));
            $cliente= $clienteRepository->buscarPorIdPdf($data['empresa_id']);


            $configuracoes = $configuracoesService->buscar();
            $template = 'empresa_exame';
            $view =  \View::make('pdf.'.$template, compact('configuracoes','atendimentos', 'valor_total', 'cliente', 'valor_medico_total','data'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option('enable_html5_parser', true);
            $pdf->getDomPDF()->get_option('enable_html5_parser');
            $pdf->loadHTML($view);
            return $pdf->stream($template);
        }

    }

    public function exames_pdf(ExameRepository $exameRepository, ConfiguracoesService $configuracoesService)
    {
            $exames = $exameRepository->listar();
            $configuracoes = $configuracoesService->buscar();
            $template = 'exame_lista';
            $view =  \View::make('pdf.'.$template, compact('configuracoes','exames'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->getDomPDF()->set_option('enable_html5_parser', true);
            $pdf->getDomPDF()->get_option('enable_html5_parser');
            $pdf->loadHTML($view);
            return $pdf->stream($template);


    }

}

