<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AtividadeRepository;
use App\Repositories\ClienteRepository;
use App\Repositories\TecnicoRepository;
use App\Repositories\StatuRepository;
use App\Services\AtividadeService;
use App\Services\ConfiguracoesService;
use App\Services\CronogramaService;
use App\Services\NaoconformidadeService;
use App\Services\TecnicoService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class QualidadeController extends Controller {

    private $clienteRepository;
    private $tecnicoRepository;
    private $statuRepository;
    private $atividadeRepository;

    public function __construct(ClienteRepository $clienteRepository, TecnicoRepository $tecnicoRepository, StatuRepository $statuRepository, AtividadeRepository $atividadeRepository){

        $this->clienteRepository = $clienteRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->statuRepository = $statuRepository;
        $this->atividadeRepository = $atividadeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(AtividadeService $atividadeService){
        $input = Request::all();

        return $atividadeService->create($input);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,  AtividadeService $atividadeService)
    {
        $data = Request::all();
        return $atividadeService->update($id, $data);
    }

    public function editar_atividade($id, AtividadeService $atividadeService){
        $id = base64_decode($id);
        $atividade_edit = $atividadeService->buscarPorId($id);
        return view('atividade.atividade_add', compact('atividade_edit'));
    }

    public function atividade(){
        return view('atividade.atividade_add');
    }

    public function listar_atividade($status, AtividadeService $atividadeService)
    {
        $atividades = $atividadeService->listar($status);
        return view('atividade.atividades', compact('atividades', 'status'));
    }
    public function inativar_atividade($id, AtividadeService $atividadeService){
        $id = base64_decode($id);
        $atividadeService->inativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/atividade_lista/1');
    }

    public function ativar_atividade($id, AtividadeService $atividadeService){
        $id = base64_decode($id);
        $atividadeService->ativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix().'/atividade_lista/2');
    }

    /************
     ** TECNICO **
     ************/

    public function tecnico(){
        return view('tecnico.tecnico_add');
    }

    public function listar_tecnico($status, TecnicoService $tecnicoService)
    {
        $tecnicos = $tecnicoService->listar($status);
        return view('tecnico.tecnicos', compact('tecnicos', 'status'));
    }

    public function buscar_tecnico($status, TecnicoService $tecnicoService)
    {
        $texto = Request::all();
        $tecnicos = $tecnicoService->buscar($status, $texto);

        return view('tecnico.tecnicos', compact('tecnicos', 'texto', 'status'));
    }

    public function editar_tecnico($id, TecnicoService $tecnicoService){
        $id = base64_decode($id);
        $tecnico_edit = $tecnicoService->buscarPorId($id);
        return view('tecnico.tecnico_add', compact('tecnico_edit'));
    }

    /*********************
     ** NÃO CONFORMIDADE **
     *********************/

    public function store_naoconformidade(NaoconformidadeService $naoconformidadeService){
        $input = Request::all();
        return $naoconformidadeService->create($input);
    }

    public function naoconformidade($id, CronogramaService $cronogramaService){
        $cronograma = $cronogramaService->buscarPorId($id);

        $cliente = $this->clienteRepository->buscarPorId($cronograma->cliente_id);
        $tecnico = $this->tecnicoRepository->buscarPorId($cronograma->tecnico_id);

        return view('naoconformidade.naoconformidade_add_iframe', compact('cronograma', 'cliente', 'tecnico'));
    }


    public function show($id, NaoconformidadeService $naoconformidadeService, CronogramaService $cronogramaService)
    {
        $id = base64_decode($id);

        $naoconformidade = $naoconformidadeService->buscarPorId($id);
        $cronograma = $cronogramaService->buscarPorId($naoconformidade->cronograma_id);

        $cliente = $this->clienteRepository->buscarPorId($naoconformidade->cliente_id);
        $tecnico = $this->tecnicoRepository->buscarPorId($naoconformidade->tecnico_id);
        $atividade = $this->atividadeRepository->buscarPorId($cronograma->atividade_id);
        $mes = $cronograma->mes;
        $ano = $cronograma->ano;

        return view('naoconformidade.naoconformidade_view_iframe', compact('naoconformidade', 'cronograma','cliente', 'tecnico', 'atividade', 'mes', 'ano'));
    }
    public function listar_naoconformidade(NaoconformidadeService $naoconformidadeService, TecnicoRepository $tecnicoRepository)
    {
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        $naoconformidades = $naoconformidadeService->listar();
        return view('naoconformidade.naoconformidades', compact('naoconformidades', 'tecnicos'));
    }
    public function listar_naoconformidadePdf(NaoconformidadeService $naoconformidadeService, TecnicoRepository $tecnicoRepository)
    {
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        $naoconformidades = $naoconformidadeService->listar();
        return view('naoconformidade.naoconformidades_relatorio', compact('naoconformidades', 'tecnicos'));
    }
    public function buscar_naoconformidade(TecnicoRepository $tecnicoRepository, NaoconformidadeService $naoconformidadeService){
        $input = Request::all();
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        $naoconformidades = $naoconformidadeService->buscar($input);
        if(isset($input['mes']) || isset($input['ano']) || isset($input['tecnico_id'])){
            $mes = $input['mes'];
            $ano = $input['ano'];
            $tecnico = $input['tecnico_id'];
        }
        return view('naoconformidade.naoconformidades', compact('naoconformidades', 'tecnicos', 'ano', 'mes', 'tecnico'));

    }
    public function buscarPdf(TecnicoRepository $tecnicoRepository, NaoconformidadeService $naoconformidadeService, ConfiguracoesService $configuracoesService){
        $input = Request::all();
        $configuracoes = $configuracoesService->buscar();
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        $naoconformidades = $naoconformidadeService->buscar($input);
        $mes = $input['mes'];
        $ano = $input['ano'];
        $tecnico = $input['tecnico_id'];

        $template = 'naoconformidade_list';
        $view =  \View::make('pdf.'.$template, compact('configuracoes', 'naoconformidades', 'tecnicos', 'mes', 'ano', 'tecnico'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
        $pdf->loadHTML($view);
        return $pdf->stream($template);

    }

    public function editar_naoconformidade($id, NaoconformidadeService $naoconformidadeService, CronogramaService $cronogramaService){
        $id = base64_decode($id);
        $naoconformidade_edit = $naoconformidadeService->buscarPorId($id);
        $cronograma = $cronogramaService->buscarPorId($naoconformidade_edit->cronograma_id);
        $cliente = $this->clienteRepository->buscarPorId($cronograma->cliente_id);
        $tecnico = $this->tecnicoRepository->buscarPorId($cronograma->tecnico_id);

        return view('naoconformidade.naoconformidade_add', compact('naoconformidade_edit', 'cronograma', 'cliente', 'tecnico'));
    }

    public function naoconformidade_update($id, NaoconformidadeService $naoconformidadeService){
        $data = Request::all();
        return $naoconformidadeService->update($id, $data);
    }
}
