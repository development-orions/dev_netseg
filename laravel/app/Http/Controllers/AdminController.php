<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\ContaService;
use App\Services\VendaService;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;


class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function cancelar_conta($tipo, $id, ContaService $contaService){
		$id = base64_decode($id);
		$contaService->cancelar($id);
		return redirect(Route::getCurrentRoute()->getPrefix().'/conta_lista/'.$tipo);
	}

	public function cancelar_venda($id){
		return view('venda.cancelar', compact('id'));
	}

	public function cancelar_venda_post($id, VendaService $vendaService){
		$id = base64_decode($id);
		$data = Request::all();
		return $vendaService->cancelar($id, $data);
		//return redirect(Route::getCurrentRoute()->getPrefix().'/venda_lista/0');
	}

}
