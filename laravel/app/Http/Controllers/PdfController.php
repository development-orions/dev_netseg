<?php namespace App\Http\Controllers;

use App\Cronograma;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\AtividadeRepository;
use App\Repositories\ClienteRepository;
use App\Repositories\TecnicoRepository;
use App\Services\AuxiliarService;
use App\Services\ConfiguracoesService;
use App\Services\CronogramaService;
use App\Services\NaoconformidadeService;
use App\Services\VendaService;
use Illuminate\Http\Request;
use Barryvdh\Snappy\Facades\SnappyPdf as SnappyPDF;

class PdfController extends Controller {

	/**
	 * @var AuxiliarService
	 */
	private $auxiliarService;
    /**
     * @var ClienteRepository
     */
    private $clienteRepository;
    /**
     * @var CronogramaService
     */
    private $cronogramaService;
    /**
     * @var TecnicoRepository
     */
    private $tecnicoRepository;
    /**
     * @var AtividadeRepository
     */
    private $atividadeRepository;

    public function __construct(AuxiliarService $auxiliarService, ClienteRepository $clienteRepository, TecnicoRepository $tecnicoRepository, AtividadeRepository $atividadeRepository){

		$this->auxiliarService = $auxiliarService;
        $this->clienteRepository = $clienteRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->atividadeRepository = $atividadeRepository;
    }

	public function gerarpdf($id = null, ConfiguracoesService $configuracoesService, VendaService $vendaService)
	{
		$id = base64_decode($id);
		$configuracoes = $configuracoesService->buscar();
		$orcamento = $vendaService->buscarPorId($id);
		$orcamento->validade = $this->auxiliarService->converteDataBrasil($orcamento->validade);
		$orcamento->valor = $this->auxiliarService->format($orcamento->valor);

		foreach($orcamento->parcelas as $parcela){
			$parcela['data'] = $this->auxiliarService->converteDataBrasil($parcela['data']);
		}

		foreach($orcamento->itens as $sv){
			$sv['valor'] = $this->auxiliarService->format($sv['valor']);
		}

		$medicina = 0;
		$treinamento = 0;
		$combo = 0;
		$normal = 0;

		// VERIFICANDO OS TIPOS DE SERVIÇO DO ORÇAMENTO
		foreach($orcamento->itens as $servico){
			if($servico['servico_id'] == 1){
				$treinamento++;
			}elseif($servico['servico_id'] == 2){
				$medicina++;
			}elseif($servico['servico_id'] == 3){
				$combo++;
			}else{
				$normal++;
			}
		}

		// VERIFICANDO QUAL TEMPLATE DE PDF USAR
		$template = "";

		if($combo != 0){
			$template = "invoice";
		}elseif($combo == 0 && $medicina != 0 && $treinamento == 0 && $normal == 0){
			$template = "orcamento_medicina";
		}elseif($combo == 0 && $treinamento != 0 && $medicina == 0 && $normal == 0){
			$template = "orcamento_treinamento";
		}elseif($combo == 0 && $treinamento == 0 && $medicina == 0 && $normal != 0){
			$template = "orcamento_servico";
		}else{
			$template = "invoice";
		}

		$view =  \View::make('pdf.'.$template, compact('configuracoes', 'orcamento'))->render();

//		return $view;

        $pdf = SnappyPDF::loadHTML($view)->setPaper('a4', 'portrait');
        return $pdf->inline($template.'.pdf');
	}

	public function pdfNaoConformidadeView($id = null, ConfiguracoesService $configuracoesService, NaoconformidadeService $naoconformidadeService, CronogramaService $cronogramaService)
	{
		$id = base64_decode($id);
		$configuracoes = $configuracoesService->buscar();
		$naoconformidade = $naoconformidadeService->buscarPorId($id);

        $cronograma = $cronogramaService->buscarPorId($naoconformidade->cronograma_id);
        $cliente = $this->clienteRepository->buscarPorId($naoconformidade->cliente_id);
        $tecnico = $this->tecnicoRepository->buscarPorId($naoconformidade->tecnico_id);
        $atividade = $this->atividadeRepository->buscarPorId($cronograma->atividade_id);
        $mes = $cronograma->mes;
        $ano = $cronograma->ano;

        $template = 'naoconformidade_view';
		$view =  \View::make('pdf.'.$template, compact('configuracoes', 'naoconformidade', 'cronograma','cliente', 'tecnico', 'atividade', 'mes', 'ano'))->render();
		$pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
		$pdf->loadHTML($view);
		return $pdf->stream($template);
	}

    public function pdfNaoConformidadeList($id = null, NaoconformidadeService $naoconformidadeService, TecnicoRepository $tecnicoRepository, ConfiguracoesService $configuracoesService)
    {
        $configuracoes = $configuracoesService->buscar();
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        $naoconformidades = $naoconformidadeService->listarPdf();

        $template = 'naoconformidade_list';
        $view =  \View::make('pdf.'.$template, compact('configuracoes', 'naoconformidades', 'tecnicos'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
        $pdf->loadHTML($view);
        return $pdf->stream($template);
    }
}
