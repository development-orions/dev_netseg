<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\JqcalendarRepository;
use App\Services\JqcalendarService;
use Illuminate\Http\Request;

class AgendaController extends Controller {

	/**
	 * @var JqcalendarRepository
	 */
	private $jqcalendarRepository;

	public function __construct(JqcalendarRepository $jqcalendarRepository){

		$this->jqcalendarRepository = $jqcalendarRepository;
	}
	public function agendas(){
		return view('agenda.jqcalendar_add');
	}

	public function agenda(Request $request){
		$method = $request->get('method');
		$ret = array();
		switch ($method) {
			case "add":
				$ret = $this->addCalendar($request->get("CalendarStartTime"), $request->get("CalendarEndTime"), $request->get("CalendarTitle"), $request->get("IsAllDayEvent"), $request->get("medico_id"), $request->get("paciente_id"), $request->get("exame"));
				break;
			case "list":
				$ret = $this->listCalendar($request->get("showdate"), $request->get("viewtype"));
				break;
			case "update":
				$ret = $this->updateCalendar($request->get("calendarId"), $request->get("CalendarStartTime"), $request->get("CalendarEndTime"));
				break;
			case "remove":
				$ret = $this->removeCalendar($request->get("calendarId"));
				break;
			case "adddetails":
				$st = $request->get("stpartdate").' '.$request->get('stparttime');
				$et = $request->get("etpartdate").' '.$request->get('etparttime');
				$ade = $request->get("IsAllDayEvent");
				$desc = $request->get("Description");
				$id = $request->get("id");
				if (isset($id)) {
					$ret = $this->updateDetailedCalendar($id, $st, $et, $request->get("Subject"), isset($ade) ? 1 : 0, $desc, $request->get("Location"), $request->get("colorvalue"), $request->get("timezone"), $request->get("medico_id"), $request->get("paciente_id"), $request->get("exame"));
				} else {
					$ret = $this->addDetailedCalendar($st, $et,
						$request->get("Subject"), $request->get('IsAllDayEvent') ? 1 : 0, $desc,
						$request->get("Location"), $request->get("colorvalue"), $request->get("timezone"), $request->get("medico_id"), $request->get("paciente_id"), $request->get("exame"));
				}
		}
		return response()->json($ret);
	}

	function addCalendar($st, $et, $sub, $ade, $medico, $paciente, $exame){
		$ret = array();
		try{
			$sql = $this->jqcalendarRepository->add($this->php2MySqlTime($this->js2PhpTime($st)), $this->php2MySqlTime($this->js2PhpTime($et)), $sub, $ade, $medico, $paciente, $exame);
			if($sql==false){
				$ret['IsSuccess'] = false;
				$ret['Msg'] = 'Erro ao tentar inserir evento. Tente novamente.';
			}else{
				$ret['IsSuccess'] = true;
				$ret['Msg'] = 'add success';
				$ret['Data'] = $sql->id;
			}
		}catch(Exception $e){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = $e->getMessage();
		}
		return $ret;
	}


	function addDetailedCalendar($st, $et, $sub, $ade, $dscr, $loc, $color, $tz, $medico, $paciente, $exame){
		$ret = array();
		try{
			$sql = $this->jqcalendarRepository->addDetailed($this->php2MySqlTime($this->js2PhpTime($st)), $this->php2MySqlTime($this->js2PhpTime($et)), $sub, $ade, $dscr, $loc, $color, $medico, $paciente, $exame);
			if($sql==false){
				$ret['IsSuccess'] = false;
				$ret['Msg'] = 'Erro ao tentar inserir evento. Tente novamente.';
			}else{
				$ret['IsSuccess'] = true;
				$ret['Msg'] = 'add success';
				$ret['Data'] = $sql->id;
			}
		}catch(Exception $e){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = $e->getMessage();
		}
		return $ret;
	}

	function listCalendarByRange($sd, $ed){
		$ret = array();
		$ret['events'] = array();
		$ret["issort"] =true;
		$ret["start"] = $this->php2JsTime($sd);
		$ret["end"] = $this->php2JsTime($ed);
		$ret['error'] = null;
		try{
			$rows = $this->jqcalendarRepository->lista($this->php2MySqlTime($sd), $this->php2MySqlTime($ed));
			foreach ($rows as $row) {
				$ret['events'][] = array(
					$row->Id,
					'Exame: '.$row->exame.' <br>| Empresa: '.$row->pacientes->clientes->nome.' <br>| Paciente: '.$row->pacientes->nome.' <br>| Medico: '.$row->medicos->nome,
					$this->php2JsTime($this->mySql2PhpTime($row->StartTime)),
					$this->php2JsTime($this->mySql2PhpTime($row->EndTime)),
					$row->IsAllDayEvent,
					0,
					0,
					$row->Color,
					1,
					$row->Location,
					''
				);
			}
		}catch(Exception $e){
			$ret['error'] = $e->getMessage();
		}
		return $ret;
	}

	private function listCalendar($day, $type){
		$phpTime = $this->js2PhpTime($day);
		switch($type){
			case "month":
				$st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime)+1, 1, date("Y", $phpTime));
				break;
			case "week":
				//suppose first day of a week is monday
				$monday  =  date("d", $phpTime) - date('N', $phpTime) + 1;
				//echo date('N', $phpTime);
				$st = mktime(0,0,0,date("m", $phpTime), $monday, date("Y", $phpTime));
				$et = mktime(0,0,-1,date("m", $phpTime), $monday+7, date("Y", $phpTime));
				break;
			case "day":
				$st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime)+1, date("Y", $phpTime));
				break;
		}
		//echo $st . "--" . $et;
		return $this->listCalendarByRange($st, $et);
	}

	function updateCalendar($id, $st, $et){
		$ret = array();
		try{
			$sql = $this->jqcalendarRepository->update($id, $this->php2MySqlTime($this->js2PhpTime($st)), $this->php2MySqlTime($this->js2PhpTime($et)));
			if(!$sql){
				$ret['IsSuccess'] = false;
				$ret['Msg'] = 'Erro ao editar';
			}else{
				$ret['IsSuccess'] = true;
				$ret['Msg'] = 'Sucesso';
			}
		}catch(Exception $e){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = $e->getMessage();
		}
		return $ret;
	}

	function updateDetailedCalendar($id, $st, $et, $sub, $ade, $dscr, $loc, $color, $tz, $medico, $paciente, $exame){
		$ret = array();
		try{
			$sql = $this->jqcalendarRepository->updateDetails($id, $this->php2MySqlTime($this->js2PhpTime($st)), $this->php2MySqlTime($this->js2PhpTime($et)), $sub, $ade, $dscr, $loc, $color, $medico, $paciente, $exame);
			if(!$sql){
				$ret['IsSuccess'] = false;
				$ret['Msg'] = 'Erro ao editar';
			}else{
				$ret['IsSuccess'] = true;
				$ret['Msg'] = 'Editado com sucesso';
			}
		}catch(Exception $e){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = $e->getMessage();
		}
		return $ret;
	}

	function removeCalendar($id){
		$ret = array();
		try{
			$sql = $this->jqcalendarRepository->remove($id);
			if(!$sql){
				$ret['IsSuccess'] = false;
				$ret['Msg'] = 'Erro ao deletar esse registro, tente novamente.';
			}else{
				$ret['IsSuccess'] = true;
				$ret['Msg'] = 'Registro deletado com sucesso.';
			}
		}catch(Exception $e){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = $e->getMessage();
		}
		return $ret;
	}
	function js2PhpTime($jsdate){
		if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
			$ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
			//echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
		}else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
			$ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
			//echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
		}
		return $ret;
	}

	function php2JsTime($phpDate){
		//echo $phpDate;
		//return "/Date(" . $phpDate*1000 . ")/";
		return date("m/d/Y H:i", $phpDate);
	}

	function php2MySqlTime($phpDate){
		return date("Y-m-d H:i:s", $phpDate);
	}

	function mySql2PhpTime($sqlDate){
		$arr = date_parse($sqlDate);
		return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);

	}

	public function edit(Request $request, JqcalendarRepository $jqcalendarRepository)
	{
		$id = $request->get('id');
		if (isset($id)){
			$event = $jqcalendarRepository->getCalendarById($request->all());
			return view('agenda.edit', compact('event'));
		}
		return view('agenda.edit');
	}


}
