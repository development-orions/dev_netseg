<?php namespace App\Http\Controllers;

use App\Cliente;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\ClienteRepository;
use App\Services\ClienteService;
use App\Services\TecnicoService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class ClienteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ClienteService $clienteService){
		$input = Request::all();
		return $clienteService->create($input);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, ClienteService $clienteService, TecnicoService $tecnicoService)
	{
		$id = base64_decode($id);
		$cliente = $clienteService->buscarPorId($id);
		return view('visualizar.cliente_view', compact('cliente'));
		//return redirect('visualizar/cliente_view/'.base64_encode($id), compact('cliente'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, ClienteService $clienteService)
	{
		$data = Request::all();
		return $clienteService->update($id, $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function inativar_cliente($id, ClienteService $clienteService){
		$id = base64_decode($id);
		$clienteService->inativar($id);
		return redirect(Route::getCurrentRoute()->getPrefix().'/cliente_lista/1');
	}

	public function ativar_cliente($id, ClienteService $clienteService){
		$id = base64_decode($id);
		$clienteService->ativar($id);
		return redirect(Route::getCurrentRoute()->getPrefix().'/cliente_lista/2');
	}

	// ajax
	public function buscar_cliente($busca, ClienteRepository $clienteRepository)
	{
		$cliente = $clienteRepository->buscar_cliente($busca);
		return response()->json($cliente);
	}

}
