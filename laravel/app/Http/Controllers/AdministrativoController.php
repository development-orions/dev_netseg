<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Formapagamento;
use App\Fornecedore;
use App\Http\Requests;
use App\Recebimento;
use App\Repositories\AtividadeRepository;
use App\Repositories\CentrocustoRepository;
use App\Repositories\ClienteRepository;
use App\Repositories\ContaRepository;
use App\Repositories\CronogramaRepository;
use App\Repositories\CronRepository;
use App\Repositories\FormapagamentoRepository;
use App\Repositories\FornecedorRepository;
use App\Repositories\RoleRepository;
use App\Repositories\ServicoRepository;
use App\Repositories\StatuRepository;
use App\Repositories\TecnicoRepository;
use App\Role;
use App\Services\AtividadeService;
use App\Services\AuxiliarService;
use App\Services\CentrocustoService;
use App\Services\ClienteService;
use App\Services\ClientestecnicoService;
use App\Services\ConfiguracoesService;
use App\Services\ContaService;
use App\Services\CronogramaService;
use App\Services\FornecedorService;
use App\Services\LogService;
use App\Services\MailService;
use App\Services\MedicoService;
use App\Services\NaoconformidadeService;
use App\Services\ObservacaoService;
use App\Services\PacienteService;
use App\Services\ServicoService;
use App\Services\UserService;
use App\Services\VendaService;
use App\Servico;
use App\Statu;
use App\Tecnico;
use App\Venda;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Services\AsoService;
use Illuminate\Http\Request as Ajax;
use Illuminate\Support\Facades\Input;

class AdministrativoController extends Controller
{

    /**
     * @var ClienteRepository
     * @var FornecedorRepository
     */
    private $clienteRepository;
    private $fornecedorRepository;
    /**
     * @var StatuRepository
     */
    private $statuRepository;
    /**
     * @var CentrocustoRepository
     */
    private $centrocustoRepository;
    /**
     * @var FormapagamentoRepository
     */
    private $formapagamentoRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var MailService
     */
    private $mailService;
    /**
     * @var ServicoService
     */
    private $servicoService;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var VendaService
     */
    private $vendaService;
    /**
     * @var TecnicoRepository
     */
    private $tecnicoRepository;
    /**
     * @var AtividadeRepository
     */
    private $atividadeRepository;
    /**
     * @var MedicoService
     */
    private $medicoService;
    /**
     * @var CronogramaRepository
     */
    private $cronogramaRepository;
    /**
     * @var CronRepository
     */
    private $cronRepository;
    /**
     * @var ContaRepository
     */
    private $contaRepository;
    /**
     * @var ContaService
     */
    private $contaService;


    /**
     * @param ClienteRepository $clienteRepository
     * @param FornecedorRepository $fornecedorRepository
     * @param StatuRepository $statuRepository
     * @param CentrocustoRepository $centrocustoRepository
     * @param FormapagamentoRepository $formapagamentoRepository
     * @param RoleRepository $roleRepository
     * @param ServicoRepository $servicoRepository
     * @param MailService $mailService
     * @param ServicoService $servicoService
     */
    public function __construct(
        ClienteRepository $clienteRepository,
        FornecedorRepository $fornecedorRepository,
        StatuRepository $statuRepository,
        CentrocustoRepository $centrocustoRepository,
        FormapagamentoRepository $formapagamentoRepository,
        RoleRepository $roleRepository,
        ServicoRepository $servicoRepository,
        MailService $mailService,
        ServicoService $servicoService,
        AuxiliarService $auxiliarService,
        VendaService $vendaService,
        TecnicoRepository $tecnicoRepository,
        AtividadeRepository $atividadeRepository,
        MedicoService $medicoService,
        CronogramaRepository $cronogramaRepository,
        CronRepository $cronRepository,
        ContaRepository $contaRepository,
        ContaService $contaService,
        PacienteService $pacienteService,
        ClienteService $clienteService,
        ConfiguracoesService $configuracoesService
    ) {

        $this->clienteRepository = $clienteRepository;
        $this->fornecedorRepository = $fornecedorRepository;
        $this->statuRepository = $statuRepository;
        $this->centrocustoRepository = $centrocustoRepository;
        $this->formapagamentoRepository = $formapagamentoRepository;
        $this->roleRepository = $roleRepository;
        $this->servicoRepository = $servicoRepository;
        $this->mailService = $mailService;
        $this->servicoService = $servicoService;
        $this->auxiliarService = $auxiliarService;
        $this->vendaService = $vendaService;
        $this->auxiliarService = $auxiliarService;
        $this->vendaService = $vendaService;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->atividadeRepository = $atividadeRepository;
        $this->medicoService = $medicoService;
        $this->cronogramaRepository = $cronogramaRepository;
        $this->cronRepository = $cronRepository;
        $this->contaRepository = $contaRepository;
        $this->contaService = $contaService;
        $this->pacienteService = $pacienteService;
        $this->clienteService = $clienteService;
        $this->configuracoesService = $configuracoesService;
    }
    /**
     * Tela inicial do Sistema
     */
    public function index()
    {
        $cron = $this->cronRepository->buscar();

        if ($cron) {
            if (strtotime($cron->data) < strtotime(date('Y-m-d'))) {
                //chama a função para alterar a conta
                $this->contaRepository->update_status_atrasadas();
                //update no cron
                $this->cronRepository->update();
            }
        } else { // PRIMEIRA VEZ
            //chama a função para alterar a conta
            $this->contaRepository->update_status_atrasadas();
            //update no cron
            $this->cronRepository->update();
        }

        return view('inicio');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    /****************
     **  CLIENTE **
     ****************/

    public function listar_cliente($status, ClienteService $clienteService)
    {
        $clientes = $clienteService->listar($status);
        return view('cliente.clientes', compact('clientes', 'status'));
    }
    public function buscar_cliente($status, ClienteService $clienteService)
    {
        $texto = Request::all();
        $clientes = $clienteService->buscar($status, $texto);

        return view('cliente.clientes', compact('clientes', 'texto', 'status'));
    }

    public function cliente()
    {
        $tecnicos = Tecnico::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        return view('cliente.cliente_add', compact('tecnicos'));
    }

    public function editar_cliente($id, ClienteService $clienteService, ClientestecnicoService $clientestecnicoService)
    {
        $id = base64_decode($id);
        $tecnicos = Tecnico::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        $cliente_edit = $clienteService->buscarPorId($id);
        $tecnico_edit = $clientestecnicoService->buscaPorClienteId($id);
        return view('cliente.cliente_add', compact('cliente_edit', 'tecnicos', 'tecnico_edit'));
    }


    /****************
     **  FORNECEDOR **
     ****************/

    public function fornecedor()
    {
        return view('fornecedor.fornecedor_add');
    }

    public function listar_fornecedor($status, FornecedorService $fornecedorService)
    {
        $fornecedor = $fornecedorService->listar($status);
        return view('fornecedor.fornecedor', compact('fornecedor', 'status'));
    }
    public function buscar_fornecedor($status, FornecedorService $fornecedorService)
    {
        $texto = Request::all();
        $fornecedor = $fornecedorService->buscar($status, $texto);

        return view('fornecedor.fornecedor', compact('fornecedor', 'texto', 'status'));
    }
    public function editar_fornecedor($id, FornecedorService $fornecedorService)
    {
        $id = base64_decode($id);
        $fornecedor_edit = $fornecedorService->buscarPorId($id);
        return view('fornecedor.fornecedor_add', compact('fornecedor_edit'));
    }

    /****************
     **  CENTRO DE CUSTO **
     ****************/
    public function centrocusto()
    {
        return view('centrocusto.centrocusto_add');
    }
    public function store(CentrocustoService $centrocustoService)
    {
        $input = Request::all();
        return $centrocustoService->create($input);
    }
    public function editar_centrocusto($id, CentrocustoService $centrocustoService)
    {
        $id = base64_decode($id);
        $centro_edit = $centrocustoService->buscarPorId($id);
        return view('centrocusto.centrocusto_add', compact('centro_edit'));
    }
    public function update($id, CentrocustoService $centrocustoService)
    {
        $data = Request::all();
        return $centrocustoService->update($id, $data);
    }
    public function listar_centrocusto(CentrocustoService $centrocustoService)
    {
        $centros = $centrocustoService->listar();
        return view('centrocusto.centrocustos', compact('centros'));
    }

    /****************
     **  SERVICO **
     ****************/
    public function servico()
    {
        return view('servico.servico_add');
    }
    public function servico_store(ServicoService $servicoService)
    {
        $input = Request::all();
        return $servicoService->create($input);
    }
    public function editar_servico($id, ServicoService $servicoService)
    {
        $id = base64_decode($id);
        $servico_edit = $servicoService->buscarPorId($id);
        return view('servico.servico_add', compact('servico_edit'));
    }
    public function update_servico($id, ServicoService $servicoService)
    {
        $data = Request::all();
        return $servicoService->update($id, $data);
    }
    public function listar_servico(ServicoService $servicoService)
    {
        $servicos = $servicoService->listar();
        return view('servico.servicos', compact('servicos'));
    }

    /****************
     **  CONTAS **
     ****************/
    public function conta($tipo)
    {
        Session::forget('fornecedor');
        Session::forget('cliente_orcamento');
        $status = $this->statuRepository->statusConta();
        $centrocustos = $this->centrocustoRepository->centroCustoSelectBox();
        $formapagamento = Formapagamento::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        return view('conta.conta_add', compact('tipo', 'status', 'centrocustos', 'formapagamento'));
    }

    public function editar_conta($tipo, $id, ContaService $contaService)
    {
        Session::forget('fornecedor');
        $id = base64_decode($id);
        $conta_edit = $contaService->buscarPorId($tipo, $id);
        $status = $this->statuRepository->statusConta();
        $centrocustos = $this->centrocustoRepository->centroCustoSelectBox();
        $formapagamento = $this->formapagamentoRepository->formaPagamentoSelectBox();
        return view('conta.conta_add', compact('conta_edit', 'tipo', 'status', 'centrocustos', 'formapagamento'));
    }

    public function conta_pagar($tipo)
    {
        Session::forget('id');
        $status = $this->statuRepository->statusConta();
        $centrocustos = $this->centrocustoRepository->centroCustoSelectBox();
        $formapagamento = $this->formapagamentoRepository->formaPagamentoSelectBox();
        return view('conta.conta_pagar', compact('tipo', 'status', 'centrocustos', 'formapagamento'));
    }

    public function editar_conta_pagar($tipo, $id, ContaService $contaService)
    {
        Session::forget('id');
        $id = base64_decode($id);
        $conta_edit = $contaService->buscarPorId($tipo, $id);
        $status = $this->statuRepository->statusConta();
        $centrocustos = $this->centrocustoRepository->centroCustoSelectBox();
        $formapagamento = $this->formapagamentoRepository->formaPagamentoSelectBox();
        return view('conta.conta_pagar', compact('conta_edit', 'tipo', 'status', 'centrocustos', 'formapagamento'));
    }

    public function conta_update($tipo, $id, ContaService $contaService)
    {
        $data = Request::all();
        return $contaService->update($tipo, $id, $data);
    }

    public function buscar($status, $tipo)
    {
        return view('administrativo.buscar', compact('tipo'));
    }
    public function buscar_post($status, $tipo, ClienteService $clienteService, FornecedorService $fornecedorService)
    {
        $texto = Request::all();
        if ($tipo == 1) {
            $clientes = $clienteService->buscar($status, $texto);
        } else {
            $fornecedores = $fornecedorService->buscar($status, $texto);
        }
        return view('administrativo.buscar', compact('clientes', 'texto', 'fornecedores', 'tipo'));
    }
    public function adicionar($id, $tipo, ContaService $contaService)
    {
        $contaService->adicionar($id, $tipo);
        if ($tipo == 1) {
            return redirect(Route::getCurrentRoute()->getPrefix() . '/conta/' . $tipo);
        } else {
            return redirect(Route::getCurrentRoute()->getPrefix() . '/conta_pagar/' . $tipo);
        }
    }

    public function conta_store(ContaService $contaService)
    {
        $input = Request::all();
        return $contaService->create($input);
    }
    public function cliente_iframe()
    {
        $tecnicos = Tecnico::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        return view('cliente.cliente_add_iframe', compact('tecnicos'));
    }

    public function cliente_store(ClienteService $clienteService)
    {
        $input = Request::all();
        return $clienteService->create_para_funcoes($input);
    }

    public function fornecedor_conta()
    {
        return view('fornecedor.fornecedor_add_conta');
    }

    public function fornecedor_store(FornecedorService $fornecedorService)
    {
        $input = Request::all();
        return $fornecedorService->create_para_funcoes($input);
    }

    /*LISTAR*/
    public function listar_conta($tipo, ContaService $contaService)
    {
        $contas = $contaService->listar($tipo);
        $status = $this->statuRepository->statusConta();
        //$dados = ['mensagem' => 'olá'];
        //$this->mailService->emailOrcamento($dados);

        return view('conta.contas', compact('contas', 'tipo', 'status'));
    }

    public function buscar_conta($tipo, ContaService $contaService)
    {
        $data = Request::all();
        $contas = $contaService->buscar($tipo, $data);
        $status = $this->statuRepository->statusConta();

        return view('conta.contas', compact('contas', 'data', 'tipo', 'status'));
    }

    public function listar_conta_pagar($tipo, ContaService $contaService)
    {
        $contas = $contaService->listar($tipo);
        $status = $this->statuRepository->statusConta();

        return view('conta.contas', compact('contas', 'tipo', 'status'));
    }

    public function buscar_conta_pagar($tipo, ContaService $contaService)
    {
        $data = Request::all();
        $contas = $contaService->buscar($tipo, $data);
        $status = $this->statuRepository->statusConta();
        return view('conta.contas', compact('contas', 'data', 'tipo', 'status'));
    }

    public function show($tipo, $id, ContaService $contaService)
    {
        $id = base64_decode($id);
        $conta = $contaService->buscarPorId($tipo, $id);


        $conta->total = $this->format($conta->total);

        return view('visualizar.conta_view', compact('conta', 'tipo'));
    }

    /*************
     **  ASO **
     *************/

    public function aso(AsoService $asoService)
    {
        $medicos = $this->medicoService->listar_drop(1);
        return view('aso.aso_add', compact('medicos', 'pacientes'));
    }

    public function pacientes_ajax(Ajax $request)
    {

        if ($request->ajax()) {
            $page = Input::get('page');
            $term = Input::get("term");

            $pacientes = $this->pacienteService->listar_drop(1, $term, $page);

            return $pacientes;
        }
    }

    public function aso_post(AsoService $asoService)
    {
        $input = Request::all();
        return $asoService->create($input);
    }

    public function aso_pdf($data)
    {
        $data = base64_decode($data);

        $data = explode("-", $data);
        $medico = $data[0];
        $paciente = $data[1];

        $paciente = $this->pacienteService->buscarPorId($paciente);
        $cliente = $this->clienteService->buscarPorId($paciente->cliente_id);
        $configuracoes = $this->configuracoesService->buscar();

        $template = 'aso';
        $view = \View::make('pdf.' . $template, compact('cliente', 'paciente', 'configuracoes'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function ficha_pdf($data)
    {
        $data = base64_decode($data);

        $data = explode("-", $data);
        $paciente = $data[1];

        $paciente = $this->pacienteService->buscarPorId($paciente);
        $cliente = $this->clienteService->buscarPorId($paciente->cliente_id);
        $configuracoes = $this->configuracoesService->buscar();

        $template = 'ficha';
        $view = \View::make('pdf.' . $template, compact('cliente', 'paciente', 'configuracoes'))->render();

        //        return $view;

        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function medico_aso($medico, AsoService $asoService)
    {
        $total = 0;
        $data = Request::all();
        $medico =  base64_decode($medico);
        $asos = $asoService->buscar($medico, $data);

        foreach ($asos as $aso) {
            if ($data != []) {
                $total += $aso->valor;
            }
            $aso->valor = $this->auxiliarService->format($aso->valor);
        }

        if ($data != []) {
            $total = $this->auxiliarService->format($total);
        }
        $medico = $this->medicoService->buscarPorId($medico);
        return view('aso.medico_aso', compact('data', 'medico', 'asos', 'total'));
    }



    /*************
     **  USUARIO **
     *************/

    /**
     * @return \Illuminate\View\
     * @internal param UserService $userService
     */

    public function user()
    {
        $roles = $this->roleRepository->roleUser();
        return view('user.user_add', compact('roles'));
    }
    public function listar_user(UserService $userService)
    {
        $users = $userService->listar();
        return view('user.users', compact('users'));
    }

    public function buscar_user(UserService $userService)
    {
        $texto = Request::all();
        $users = $userService->buscar($texto);

        return view('user.users', compact('users', 'texto'));
    }

    public function editar_user($id, UserService $userService)
    {
        $id = base64_decode($id);
        $roles = $this->roleRepository->roleUser();
        $user_edit = $userService->buscarPorId($id);
        return view('user.update_register', compact('user_edit', 'roles'));
    }

    public function editar_user2(UserService $userService)
    {
        $roles = $this->roleRepository->roleUser();
        $user_edit = $userService->buscarPorId(Auth::user()->id);
        return view('user.update_register', compact('user_edit', 'roles'));
    }

    public function user_update($id, UserService $userService)
    {
        $usuario = Request::all();
        return $userService->update($id, $usuario);
    }

    public function resetsenha($id, UserService $userService)
    {
        $id = base64_decode($id);
        $user_edit = $userService->buscarPorId($id);
        return view('user.resetpassword', compact('user_edit'));
    }

    public function reset_password($id, UserService $userService)
    {
        $usuario = Request::all();
        return $userService->resetpassword($id, $usuario);
    }


    public function format($valor)
    {
        return "R$ " . number_format($valor, 2, ",", ".");
    }

    /****************
     **  CONFIGURAÇÕES **
     ****************/
    public function configuracoes(ConfiguracoesService $configuracoesService)
    {
        $configuracoes = $configuracoesService->buscar();
        return view('configuracoes.configuracoes', compact('configuracoes'));
    }
    public function configuracoes_add(ConfiguracoesService $configuracoesService)
    {
        $configuracoes = Request::all();
        return $configuracoesService->update($configuracoes);
    }

    /**************
     ** ORÇAMENTO **
     **************/
    public function orcamento()
    {
        Session::forget('id');
        $formapagamento = Formapagamento::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        return view('orcamento.orcamento_add', compact('formapagamento'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function adicionar_servico()
    {
        $servicos = Servico::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        $servicos_combo = Servico::where('id', '!=', 3)->get();
        return view('servico.adicionar_servico', compact('servicos', 'servicos_combo'));
    }

    public function servico_valor($id)
    {
        return $this->servicoRepository->buscarPorId($id);
    }

    /**
     * @param ServicoService $servicoService
     * @return array
     */
    public function servico_orcamento(ServicoService $servicoService)
    {
        $servico = Request::all();

        return $servicoService->adicionar($servico);
    }

    public function mail(ConfiguracoesService $configuracoesService)
    {
        $configuracoes = $configuracoesService->buscar();
        return view('mail.orcamento', compact('configuracoes'));
    }

    public function orcamento_store(VendaService $vendaService)
    {
        $input = Request::all();
        return $vendaService->create($input);
    }

    public function buscar_orcamento()
    {
        return view('orcamento.buscar_orcamento');
    }

    public function buscar_post_orcamento(ClienteService $clienteService)
    {
        $texto = Request::all();
        $status = 1;

        $clientes = $clienteService->buscar($status, $texto);

        return view('orcamento.buscar_orcamento', compact('clientes', 'texto'));
    }


    public function adicionar_orcamento($id, VendaService $vendaService)
    {
        $vendaService->adicionar($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento');
    }

    public function editar_orcamento($id, VendaService $vendaService, $edita = null)
    {
        $codId = $id;
        $id = base64_decode($id);
        $formapagamento = Formapagamento::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        $orcamento_edit = $vendaService->buscarPorId($id);
        if (!isset($edita)) {
            Session::forget('servico');
            $sv = [];
            foreach ($orcamento_edit->itens as $servico) :

                $dados['valor'] = $servico->valor;
                $dados['descricao'] = $servico->descricao;
                $dados['cargahoraria'] = $servico->cargahoraria;
                $dados['participantes'] = $servico->participantes;
                $dados['periodo'] = $servico->periodo;
                $dados['horario'] = $servico->horario;

                $dados['tecnico_id'] = $servico->tecnicos;
                $dados['servico_id'] = $servico->servicos;

                if (isset($servico->servico)) {
                    $data = explode(',', $servico->servico);
                    foreach ($data as $d) {
                        $dados['servicos_combo'][] = $d;
                    }
                }

                $sv[] = $dados;
            endforeach;

            if (!Session::has('servico'))
                Session::put('servico', $sv);

            return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento_edita/' . $codId . '/edita');
        }

        if (!Session::has('cliente_orcamento'))
            Session::put('cliente_orcamento', $orcamento_edit->clientes);

        foreach ($orcamento_edit->parcelas as $parcelas) {
            $parcelas['data'] = $this->auxiliarService->converteDataBrasil2($parcelas['data']);
        }

        return view('orcamento.orcamento_add', compact('orcamento_edit', 'formapagamento'));
    }

    public function remover_servico($id)
    {
        $servico = Session::get('servico');
        unset($servico[$id]);
        Session::forget('servico');
        Session::put('servico', $servico);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento');
    }

    public function remover_servico_edit($orcamento, $id, VendaService $vendaService)
    {
        $servico = Session::get('servico');
        unset($servico[$id]);
        Session::forget('servico');
        Session::put('servico', $servico);

        return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento_edita/' . base64_encode($orcamento) . '/edita');
    }

    public function limpar_filtro()
    {
        Session::forget('cliente_cronograma');
        return redirect()->back();
    }
    public function remover_cliente()
    {
        Session::forget('cliente_cronograma');
        return redirect()->back()->withInput();
    }
    public function orcamento_update($id, VendaService $vendaService)
    {
        $data = Request::all();
        return $vendaService->update($id, $data);
    }

    public function converter_orcamento($id)
    {
        $id = base64_decode($id);
        $orcamento = $this->vendaService->buscarPorId($id);
        $orcamento->validade = $this->auxiliarService->converteDataBrasil($orcamento->validade);
        $orcamento->valor = $this->auxiliarService->format($orcamento->valor);

        $medicina = 0;
        $treinamento = 0;
        $combo = 0;
        $normal = 0;


        foreach ($orcamento->itens as $servico) :

            $dados['valor'] = $servico->valor;
            $dados['descricao'] = $servico->descricao;
            $dados['cargahoraria'] = $servico->cargahoraria;
            $dados['participantes'] = $servico->participantes;
            $dados['periodo'] = $servico->periodo;
            $dados['horario'] = $servico->horario;

            $dados['tecnico_id'] = $servico->tecnicos;
            $dados['servico_id'] = $servico->servicos;
            $sv[] = $dados;

            // VERIFICANDO OS TIPOS DE SERVIÇO DO ORÇAMENTO
            if ($servico['servico_id'] == 1) {
                $treinamento++;
            } elseif ($servico['servico_id'] == 2) {
                $medicina++;
            } elseif ($servico['servico_id'] == 3) {
                $combo++;
            } else {
                $normal++;
            }

        endforeach;

        if (!Session::has('cliente_orcamento'))
            Session::put('cliente_orcamento', $orcamento->clientes);


        if (!Session::has('servico'))
            Session::put('servico', $sv);

        $tecnicos = $this->tecnicoRepository->tecnicoVenda();
        $medicos = $this->medicoService->medicoVenda();


        // VERIFICANDO QUAL TEMPLATE DE EMAIL USAR
        $tipo = "";

        if ($combo != 0) {
            $tipo = "ambos";
        } elseif ($combo == 0 && $medicina != 0 && $treinamento == 0 && $normal == 0) {
            $tipo = "medicina";
        } elseif ($combo == 0 && $treinamento != 0 && $medicina == 0 && $normal == 0) {
            $tipo = "treinamento";
        } elseif ($combo == 0 && $treinamento == 0 && $medicina == 0 && $normal != 0) {
            $tipo = "treinamento";
        } else {
            $tipo = "treinamento";
        }


        return view('orcamento.converter', compact('orcamento', 'tecnicos', 'tipo', 'medicos'));
    }

    public function converter_orcamento_post($id)
    {
        $id = base64_decode($id);
        $data = Request::all();
        return $this->vendaService->converter_orcamento($id, $data);
    }

    public function aprovar_orcamento($id)
    {
        $id = base64_decode($id);
        $this->vendaService->aprovarOrcamento($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento_lista/1');
    }

    public function inativar_orcamento($id)
    {
        $id = base64_decode($id);
        $this->vendaService->inativarOrcamento($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento_lista/1');
    }

    public function ativar_orcamento($id)
    {
        $id = base64_decode($id);
        $this->vendaService->ativarOrcamento($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/orcamento_lista/1');
    }

    public function finaliza_orcamento($id)
    {
        $orcamento = $this->vendaService->buscarPorId($id);
        $orcamento->validade = $this->auxiliarService->converteDataBrasil($orcamento->validade);
        return view('orcamento.finalizado', compact('orcamento'));
    }

    public function email_orcamento($id)
    {
        return view('orcamento.email', compact('id'));
    }

    public function email_orcamento_post($id, ConfiguracoesService $configuracoesService)
    {
        $id = base64_decode($id);
        $orcamento = $this->vendaService->buscarPorId($id);
        $orcamento->validade = $this->auxiliarService->converteDataBrasil($orcamento->validade);
        $orcamento->valor = $this->auxiliarService->format($orcamento->valor);

        foreach ($orcamento->parcelas as $parcela) {
            $parcela['data'] = $this->auxiliarService->converteDataBrasil($parcela['data']);
        }

        foreach ($orcamento->itens as $sv) {
            $sv['valor'] = $this->auxiliarService->format($sv['valor']);
        }

        $configuracoes = $configuracoesService->buscar();
        $destinatarios = Request::all();
        $destinatarios = str_replace(' ', '', $destinatarios['destinatarios']);
        $destinatarios = strtolower($destinatarios);
        $destinatarios = explode(';', $destinatarios);


        $user = $orcamento->users['email'];


        $medicina = 0;
        $treinamento = 0;
        $combo = 0;
        $normal = 0;

        // VERIFICANDO OS TIPOS DE SERVIÇO DO ORÇAMENTO
        foreach ($orcamento->itens as $servico) {
            if ($servico['servico_id'] == 1) {
                $treinamento++;
            } elseif ($servico['servico_id'] == 2) {
                $medicina++;
            } elseif ($servico['servico_id'] == 3) {
                $combo++;
            } else {
                $normal++;
            }
        }

        // VERIFICANDO QUAL TEMPLATE DE EMAIL USAR
        $template = "";

        if ($combo != 0) {
            $template = "orcamento";
        } elseif ($combo == 0 && $medicina != 0 && $treinamento == 0 && $normal == 0) {
            $template = "orcamento_medicina";
        } elseif ($combo == 0 && $treinamento != 0 && $medicina == 0 && $normal == 0) {
            $template = "orcamento_treinamento";
        } elseif ($combo == 0 && $treinamento == 0 && $medicina == 0 && $normal != 0) {
            $template = "orcamento_servico";
        } else {
            $template = "orcamento";
        }

        $this->mailService->emailOrcamento($template, $orcamento, $configuracoes, $destinatarios, $user);

        $resposta = array();
        $resposta['msg'] = 'Orçamento enviado com sucesso.';
        $resposta['erro'] = false;

        return $resposta;
    }

    /* NÃO CONVERTIDOS EM VENDA */
    public function listar_orcamento($orcamento, VendaService $vendaService)
    {
        $vendas = $vendaService->listar($orcamento);
        $status = $this->statuRepository->statusOrcamento();
        return view('orcamento.orcamentos', compact('vendas', 'status', 'orcamento'));
    }

    public function buscar_listaorcamento(VendaService $vendaService)
    {
        $data = Request::all();
        $vendas = $vendaService->buscar($data);
        $status = $this->statuRepository->statusOrcamento();

        return view('orcamento.orcamentos', compact('vendas', 'data', 'status'));
    }

    /* CONVERTIDOS EM VENDA */
    public function listar_convertidos($orcamento, VendaService $vendaService)
    {
        $vendas = $vendaService->listar_convertidos($orcamento);
        $status = $this->statuRepository->statusOrcamento();
        return view('orcamento.orcamentosconvertidos', compact('vendas', 'status', 'orcamento'));
    }

    public function buscar_convertidos(VendaService $vendaService)
    {
        $data = Request::all();
        $vendas = $vendaService->buscar_convertidos($data);
        $status = $this->statuRepository->statusOrcamento();

        return view('orcamento.orcamentosconvertidos', compact('vendas', 'data', 'status'));
    }


    public function show_orcamento($id, VendaService $vendaService)
    {
        $id = base64_decode($id);
        $orcamento = $vendaService->buscarPorId($id);
        $orcamento->validade = $this->auxiliarService->converteDataBrasil($orcamento->validade);
        $orcamento->valor = $this->auxiliarService->format($orcamento->valor);


        foreach ($orcamento->parcelas as $parcela) {
            $parcela['data'] = $this->auxiliarService->converteDataBrasil($parcela['data']);
        }

        foreach ($orcamento->itens as $sv) {
            $sv['valor'] = $this->auxiliarService->format($sv['valor']);
        }

        $medicina = 0;
        $treinamento = 0;
        $combo = 0;
        $normal = 0;

        // VERIFICANDO OS TIPOS DE SERVIÇO DO ORÇAMENTO
        foreach ($orcamento->itens as $servico) {
            if ($servico['servico_id'] == 1) {
                $treinamento++;
            } elseif ($servico['servico_id'] == 2) {
                $medicina++;
            } elseif ($servico['servico_id'] == 3) {
                $combo++;
            } else {
                $normal++;
            }
        }

        // VERIFICANDO QUAL TEMPLATE DE PDF USAR
        $template = "";

        if ($combo != 0) {
            $template = "invoice";
        } elseif ($combo == 0 && $medicina != 0 && $treinamento == 0 && $normal == 0) {
            $template = "medicina";
        } elseif ($combo == 0 && $treinamento != 0 && $medicina == 0 && $normal == 0) {
            $template = "treinamento";
        } elseif ($combo == 0 && $treinamento == 0 && $medicina == 0 && $normal != 0) {
            $template = "servico";
        } else {
            $template = "invoice";
        }

        return view('visualizar.orcamento_' . $template . '_view', compact('orcamento'));
    }

    /*************
     **  VENDA **
     *************/

    public function venda()
    {
        Session::forget('id');
        $formapagamento = Formapagamento::orderBy('nome', 'asc')->lists('nome', 'id')->all();
        $tecnicos = $this->tecnicoRepository->tecnicoVenda();
        $medicos = $this->medicoService->medicoVenda();
        return view('venda.venda_add', compact('formapagamento', 'tecnicos', 'medicos'));
    }

    public function venda_store(VendaService $vendaService)
    {
        $input = Request::all();
        return $vendaService->create($input);
    }

    public function listar_venda($orcamento, VendaService $vendaService)
    {
        $vendas = $vendaService->listar_venda($orcamento);
        $status = $this->statuRepository->statusVenda();
        return view('venda.vendas', compact('vendas', 'status', 'orcamento'));
    }

    public function buscar_venda(VendaService $vendaService)
    {
        $data = Request::all();
        $vendas = $vendaService->buscar_venda($data);
        $status = $this->statuRepository->statusVenda();
        return view('venda.vendas', compact('vendas', 'data', 'status'));
    }

    public function show_venda($id, VendaService $vendaService)
    {
        $id = base64_decode($id);
        $venda = $vendaService->buscarPorId($id);
        $venda->validade = $this->auxiliarService->converteDataBrasil($venda->validade);
        $venda->fechamento = $this->auxiliarService->converteDataBrasil($venda->fechamento);
        $venda->entrega = $this->auxiliarService->converteDataBrasil($venda->entrega);
        $venda->valor = $this->auxiliarService->format($venda->valor);
        $venda->valoradministrativo = $this->auxiliarService->format($venda->valoradministrativo);


        foreach ($venda->parcelas as $parcela) {
            $parcela['data'] = $this->auxiliarService->converteDataBrasil($parcela['data']);
        }

        foreach ($venda->itens as $sv) {
            $sv['valor'] = $this->auxiliarService->format($sv['valor']);
        }

        $tecnicos = $this->vendaService->listar_tecnico_venda($id);
        $medicos = $this->vendaService->listar_medico_venda($id);

        $medicina = 0;
        $treinamento = 0;
        $combo = 0;
        $normal = 0;

        // VERIFICANDO OS TIPOS DE SERVIÇO DO ORÇAMENTO
        foreach ($venda->itens as $servico) {
            if ($servico['servico_id'] == 1) {
                $treinamento++;
            } elseif ($servico['servico_id'] == 2) {
                $medicina++;
            } elseif ($servico['servico_id'] == 3) {
                $combo++;
            } else {
                $normal++;
            }
        }

        // VERIFICANDO QUAL TEMPLATE DE PDF USAR
        $template = "";

        if ($combo != 0) {
            $template = "geral";
        } elseif ($combo == 0 && $medicina != 0 && $treinamento == 0 && $normal == 0) {
            $template = "medicina";
        } elseif ($combo == 0 && $treinamento != 0 && $medicina == 0 && $normal == 0) {
            $template = "treinamento";
        } elseif ($combo == 0 && $treinamento == 0 && $medicina == 0 && $normal != 0) {
            $template = "servico";
        } else {
            $template = "geral";
        }

        return view('visualizar.venda_' . $template . '_view', compact('venda', 'tecnicos', 'medicos'));
    }


    /*****************
     **  CRONOGRAMA **
     *****************/

    public function cronograma(TecnicoRepository $tecnicoRepository)
    {
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        return view('cronograma.cronogramas', compact('tecnicos'));
    }

    public function buscar_cliente_cronograma()
    {
        return view('cronograma.buscar_cliente');
    }

    public function buscar_post_cliente_cronograma(ClienteService $clienteService)
    {
        $texto = Request::all();
        $status = 1;
        $clientes = $clienteService->buscar($status, $texto);
        return view('cronograma.buscar_cliente', compact('clientes', 'texto'));
    }

    public function adicionar_cronograma($id, CronogramaService $cronogramaService)
    {
        $cronogramaService->adicionar($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/cronograma');
    }

    public function cronograma_post(TecnicoRepository $tecnicoRepository, AtividadeService $atividadeService)
    {
        $input = Request::all();
        $atividades = $atividadeService->listar_cronograma(1, $input);
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        $ano = $input['ano'];
        $tecnico = $input['tecnico_id'];
        $cliente = $this->clienteRepository->buscarPorId($input['cliente_id']);

        return view('cronograma.cronogramas', compact('atividades', 'tecnicos', 'ano', 'tecnico', 'cliente'));
    }

    public function cronograma_grafico(TecnicoRepository $tecnicoRepository)
    {
        $tecnicos = $tecnicoRepository->tecnicoCronograma();
        return view("cronograma.cronograma_grafico", ["tecnicos" => $tecnicos, "atividades" => json_encode(array())]);
    }

    public function cronograma_grafico_post(AtividadeService $atividadeService, CronogramaService $cronogramaService, TecnicoRepository $tecnicoRepository)
    {
        $data = Request::all();
        unset($data["_token"]);
        $tecnicos = $tecnicoRepository->tecnicoCronograma();

        $atividades = [];
        foreach ($atividadeService->listarTodos() as $key => $ativ) $atividades[str_slug($ativ["nome"])] = count($cronogramaService->listar_por_id_entre_datas($ativ["id"], $data["tecnico_id"], ["inicio" => $data["ano_inicio"], "fim" => $data["ano_fim"]]));

        return view("cronograma.cronograma_grafico", ["tecnicos" => $tecnicos, "atividades" => json_encode($atividades)]);
    }

    public function adicionar_atividade_cronograma($cliente_id, $tecnico_id, $atividade_id, $ano, $mes)
    {
        $status = $this->statuRepository->statusCronograma();
        $cliente = $this->clienteRepository->buscarPorId($cliente_id);
        $tecnico = $this->tecnicoRepository->buscarPorId($tecnico_id);
        $atividade = $this->atividadeRepository->buscarPorId($atividade_id);
        return view('cronograma.adicionar_atividade', compact('status', 'ano', 'cliente', 'tecnico', 'mes', 'atividade'));
    }

    public function adicionar_atividade_cronograma_post($cliente_id, $tecnico_id, $atividade_id, $ano, $mes, CronogramaService $cronogramaService)
    {
        $input = Request::all();
        return $cronogramaService->store($cliente_id, $tecnico_id, $atividade_id, $ano, $mes, $input);
    }

    public function editar_cronograma($id, CronogramaService $cronogramaService, ObservacaoService $observacaoService, NaoconformidadeService $naoconformidadeService)
    {
        $cronograma_edit = $cronogramaService->buscarPorId($id);

        $status = $this->statuRepository->statusCronograma();
        $cliente = $this->clienteRepository->buscarPorId($cronograma_edit->cliente_id);
        $tecnico = $this->tecnicoRepository->buscarPorId($cronograma_edit->tecnico_id);
        $atividade = $this->atividadeRepository->buscarPorId($cronograma_edit->atividade_id);
        $mes = $cronograma_edit->mes;
        $ano = $cronograma_edit->ano;
        $observacoes = $observacaoService->buscarPorCronogramaId($id);
        $naoconformidade = $naoconformidadeService->buscarPorCronogramaId($id);



        return view('cronograma.adicionar_atividade', compact('cronograma_edit', 'cliente', 'tecnico', 'mes', 'atividade', 'ano', 'status', 'observacoes', 'naoconformidade'));
    }

    public function editar_cronograma_post($id, CronogramaService $cronogramaService, ObservacaoService $observacaoService)
    {

        $data = Request::all();
        if (!empty($data['observacao'])) {
            $observacaoService->store($id, $data);
        }
        return $cronogramaService->update($id, $data);
    }

    public function remover_cronograma($id, CronogramaService $cronogramaService)
    {
        $cronogramaService->remove($id);

        return redirect(Route::getCurrentRoute()->getPrefix() . '/cronograma');
    }

    public function deletar_arquivo($id, CronogramaService $cronogramaService)
    {
        $cronogramaService->deletar_arquivo($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/cronograma');
    }

    public function listar_atividades(CronogramaService $cronogramaService)
    {
        $atividades = $cronogramaService->listar();
        $status = $this->statuRepository->statusCronograma();
        $tecnicos = $this->tecnicoRepository->tecnicoCronograma();
        $atividads = $this->atividadeRepository->atividadeCronograma();

        return view('atividade.atividades_relatorio', compact('atividades', 'atividads', 'data', 'status', 'tecnicos', 'ano', 'mes', 'tecnico'));
    }

    public function buscar_atividades(CronogramaService $cronogramaService, ConfiguracoesService $configuracoesService)
    {
        ob_start();
        $data = Request::all();
        $configuracoes = $configuracoesService->buscar();
        $atividades = $cronogramaService->buscar($data);
        $status = $this->statuRepository->statusCronograma();
        $tecnicos_list = $this->cronogramaRepository->buscar_tecnico($data);
        $tecnicos = $this->tecnicoRepository->tecnicoCronograma();
        $atividads = $this->atividadeRepository->atividadeCronograma();
        $tecnico = $data['tecnico_id'];
        $statu = $data['statu_id'];
        $ativ = $data['atividade_id'];
        $inicio = $data['inicio'];
        $fim = $data['fim'];

        //*/
        $template = 'atividades_relatorio';
        $view =  \View::make('pdf.' . $template, compact('configuracoes', 'atividades', 'atividads', 'data', 'status', 'tecnicos_list', 'tecnicos', 'tecnico', 'statu', 'ativ', 'inicio', 'fim'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_html5_parser', true);
        $pdf->getDomPDF()->get_option('enable_html5_parser');
        $pdf->loadHTML($view);
        return $pdf->stream($template);
        //*/
        /*/
        return view('pdf.atividades_relatorio', compact('configuracoes', 'atividades', 'atividads', 'data', 'status','tecnicos_list', 'tecnicos', 'tecnico', 'statu', 'ativ', 'inicio', 'fim'));
        //*/
    }
    /****************************
     **  CRONOGRAMA FINANCEIRO **
     ****************************/

    public function cronograma_financeiro()
    {
        return view('cronograma.cronograma_financeiro');
    }

    public function cronograma_financeiro_post(CentrocustoService $centrocustoService)
    {
        $input = Request::all();

        $cronograma = $centrocustoService->cronograma($input);
        $cronograma_pagamento = $centrocustoService->cronograma_pagamento($input);

        $ano = $input['ano'];
        $mes = $input['mes'];
        $contas = $input['contas'];
        $portador = $input['portador'];

        $num = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

        if (sizeof($mes) == 2) {
            $abr =  substr($mes, 1) - 1;
        } else {
            $abr = $mes - 1;
        }

        $meses = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
        $nomemes = $meses[$abr];


        return view('cronograma.cronograma_financeiro', compact('cronograma', 'ano', 'mes', 'num', 'nomemes', 'cronograma_pagamento', 'portador', 'contas'));
    }

    public function contas_cronograma($tipo, $ano, $mes, $dia, $centrocusto)
    {
        $centrocusto = base64_decode($centrocusto);
        $centro_nome = $this->centrocustoRepository->buscarPorId($centrocusto);
        $contas = $this->contaService->conta_cronograma($tipo, $ano, $mes, $dia, $centrocusto);
        return view('cronograma.lista_contas', compact('contas', 'dia', 'centro_nome', 'tipo'));
    }


    /*************
     **  MEDICO **
     *************/
    public function inativar_medico($id)
    {
        $id = base64_decode($id);
        $this->medicoService->inativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/medico_lista/1');
    }

    public function ativar_medico($id)
    {
        $id = base64_decode($id);
        $this->medicoService->ativar($id);
        return redirect(Route::getCurrentRoute()->getPrefix() . '/medico_lista/2');
    }

    public function medico()
    {
        return view('medico.medico_add');
    }

    public function medico_store()
    {
        $input = Request::all();
        return $this->medicoService->create($input);
    }

    public function listar_medico($status)
    {
        $medicos = $this->medicoService->listar($status);
        return view('medico.medicos', compact('medicos', 'status'));
    }
    public function buscar_medico($status)
    {
        $texto = Request::all();
        $medicos = $this->medicoService->buscar($status, $texto);

        return view('medico.medicos', compact('medicos', 'texto', 'status'));
    }

    public function editar_medico($id)
    {
        $id = base64_decode($id);
        $medico_edit = $this->medicoService->buscarPorId($id);
        return view('medico.medico_add', compact('medico_edit'));
    }

    public function medico_update($id)
    {
        $data = Request::all();
        return $this->medicoService->update($id, $data);
    }

    public function show_medico($id)
    {
        $id = base64_decode($id);
        $medico = $this->medicoService->buscarPorId($id);
        return view('visualizar.medico_view', compact('medico'));
    }

    public function logs(LogService $logService)
    {
        $logs = $logService->listar();
        return view('log.logs', compact('logs'));
    }

    public function busca_logs(LogService $logService)
    {
        $data = Request::all();
        $logs = $logService->buscar($data);
        return view('log.logs', compact('logs', 'data'));
    }

    public function idRedirect()
    {
        return redirect("/pdf/naoconformidade/" . base64_encode(Session::get('nc')));
    }

    public function redirect()
    {
        return redirect()->back();
    }

    public function grafico()
    {
        $centrocusto = $this->centrocustoRepository->centroCustoSelectBox();
        return view('graficos.grafico', compact('centrocusto'));
    }

    public function grafico_resultado()
    {
        $centrocusto = $this->centrocustoRepository->centroCustoSelectBox();
        return view('graficos.graficoresultado', compact('centrocusto'));
    }


    public function empresas_ajax(Ajax $request)
    {

        if ($request->ajax()) {
            $page = Input::get('page');
            $term = Input::get("term");

            $empresas = $this->clienteService->listar_drop(1, $term, $page);

            return $empresas;
        }
    }
}
