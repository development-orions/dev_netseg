<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Ti {

    protected $auth;
    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        switch($this->auth->user()->role_id){
            case 1:
                return redirect()->to('administrador');
                break;
            case 2:
                return redirect()->to('administrativo');
                break;
            case 3:
                return redirect()->to('qualidade');
                break;
            case 4:
                return redirect()->to('medicina');
                break;
            case 5:
                return redirect()->to('vendas');
                break;
            case 6:
                return redirect()->to('desenvolvedor');
                break;
            case 7:

                break;
            case 8:
                return redirect()->to('auxiliaradm');
                break;
            case 9:
                return redirect()->to('cliente');
                break;
            case 11:
                return redirect()->to('qualidade');
                break;
        }
		return $next($request);
	}

}
