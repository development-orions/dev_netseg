<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
        'Illuminate\Cookie\Middleware\EncryptCookies',
        'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
        'Illuminate\Session\Middleware\StartSession',
        'Illuminate\View\Middleware\ShareErrorsFromSession',
        'App\Http\Middleware\VerifyCsrfToken',
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => 'App\Http\Middleware\Authenticate',
        'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
        'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',

        //MIDDLEWARE
        'administrador' => 'App\Http\Middleware\Administrador',
        'administrativo' => 'App\Http\Middleware\Administrativo',
        'medicina' => 'App\Http\Middleware\Medicina',
        'desenvolvedor' => 'App\Http\Middleware\Desenvolvedor',
        'qualidade' => 'App\Http\Middleware\Qualidade',
        'vendas' => 'App\Http\Middleware\Vendas',
        'tecnico' => 'App\Http\Middleware\Auxiliaradm',
        'ti' => 'App\Http\Middleware\Ti',
        'nivel' => 'App\Http\Middleware\Nivel',
        'auxiliaradm' => 'App\Http\Middleware\Auxiliaradm',
        'cliente' => 'App\Http\Middleware\Cliente',
    ];

}
