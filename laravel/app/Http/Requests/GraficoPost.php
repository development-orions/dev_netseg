<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class GraficoPost extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules()
    {
        return [
            'inicio' => 'required',
            'fim' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'inicio.required' => 'Informe uma data inicial!',
            'fim.required' => 'Informe uma data final!'
        ];
    }
    public function response(array $errors)
    {
        return \Response::json($errors, 422);
    }

}
