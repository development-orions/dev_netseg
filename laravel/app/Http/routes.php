<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['middleware' => ['auth', 'nivel'], function(){}]);

Route::group(['prefix'=>'visualizar'], function(){
    Route::get('/cliente_view/{id}', 'ClienteController@show');
    Route::get('/fornecedor_view/{id}', 'FornecedorController@show');
    Route::get('/tecnico_view/{id}', 'TecnicoController@show');
    Route::get('/user_view/{id}', 'UserController@show');
    Route::get('/conta_view/{tipo}/{id}', 'AdministrativoController@show');
    Route::get('/paciente_view/{id}', 'MedicinaController@show');
    Route::get('/orcamento_view/{id}', 'AdministrativoController@show_orcamento');
    Route::get('/venda_view/{id}', 'AdministrativoController@show_venda');
    Route::get('/orcamento_view/{id}', 'AdministrativoController@show_orcamento');
    Route::get('/naoconformidade_view/{id}', 'QualidadeController@show');
    Route::get('/medico_view/{id}', 'AdministrativoController@show_medico');
});

Route::group(['prefix'=>'administrador'], function(){
    Route::group(['middleware' => ['auth','administrador']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/grafico/', 'AdministrativoController@grafico');
        Route::get('/grafico_resultado/', 'AdministrativoController@grafico_resultado');

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');
        Route::get('/user_inativa/{id}', 'UserController@inativar_user');
        Route::get('/user_ativa/{id}', 'UserController@ativar_user');

        Route::get('/configuracoes', 'AdministrativoController@configuracoes');
        Route::post('/configuracoes', 'AdministrativoController@configuracoes_add');

        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/fornecedor_inativa/{id}', 'FornecedorController@inativar_fornecedor');
        Route::get('/fornecedor_ativa/{id}', 'FornecedorController@ativar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@listar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@buscar_fornecedor');
        Route::get('/fornecedor', 'AdministrativoController@fornecedor');
        Route::post('/fornecedor', 'FornecedorController@store');
        Route::get('/fornecedor_edita/{id}', 'AdministrativoController@editar_fornecedor');
        Route::post('/fornecedor_edita/{id}', 'FornecedorController@update');

        Route::get('/conta/{tipo}', 'AdministrativoController@conta');
        Route::get('/conta_pagar/{tipo}', 'AdministrativoController@conta_pagar');
        Route::post('/conta', 'AdministrativoController@conta_store');
        Route::get('/buscar/{status}/{tipo}', 'AdministrativoController@buscar');
        Route::post('/buscar/{status}/{tipo}', 'AdministrativoController@buscar_post');
        Route::get('/adicionar/{id}/{tipo}', 'AdministrativoController@adicionar');
        Route::get('/cliente_iframe', 'AdministrativoController@cliente_iframe');
        Route::post('/cliente_conta', 'AdministrativoController@cliente_store');
        Route::get('/fornecedor_conta', 'AdministrativoController@fornecedor_conta');
        Route::post('/fornecedor_conta', 'AdministrativoController@fornecedor_store');

        Route::get('/conta_edita/{tipo}/{id}', 'AdministrativoController@editar_conta');
        Route::get('/conta_edita_pagar/{tipo}/{id}', 'AdministrativoController@editar_conta_pagar');
        Route::post('/conta_edita/{tipo}/{id}', 'AdministrativoController@conta_update');
        Route::get('/conta_cancela/{tipo}/{id}', 'AdminController@cancelar_conta');

        Route::get('/centrocusto', 'AdministrativoController@centrocusto');
        Route::post('/centrocusto', 'AdministrativoController@store');
        Route::get('/centrocusto_edita/{id}', 'AdministrativoController@editar_centrocusto');
        Route::post('/centrocusto_edita/{id}', 'AdministrativoController@update');
        Route::get('/centrocusto_lista', 'AdministrativoController@listar_centrocusto');

        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');

        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');


        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@listar_venda');
        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@buscar_venda');
        Route::get('/cancelar_venda/{id}', 'AdminController@cancelar_venda');
        Route::post('/cancelar_venda_post/{id}', 'AdminController@cancelar_venda_post');

        Route::get('/conta_lista/{tipo}', 'AdministrativoController@listar_conta');
        Route::get('/conta_lista/{tipo}', 'AdministrativoController@buscar_conta');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@listar_conta_pagar');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@buscar_conta_pagar');

        Route::get('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro');
        Route::post('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro_post');
        Route::get('/deletar_arquivo/{id}', 'AdministrativoController@deletar_arquivo');

        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');

        Route::get('/relatorio_conta/{tipo}', 'RelatorioController@conta');
        Route::post('/relatorio_conta/{tipo}', 'RelatorioController@buscar_conta');
        Route::get('/relatorio_pagamentotecnico', 'RelatorioController@pagamentotecnico');
        Route::post('/relatorio_pagamentotecnico', 'RelatorioController@buscar_pagamentotecnico');
        Route::get('/contas_cronograma/{tipo}/{ano}/{mes}/{dia}/{centrocusto}', 'AdministrativoController@contas_cronograma');

        Route::get('/relatorio_venda/{tipo}', 'RelatorioController@relatorio_venda');
        Route::post('/relatorio_venda/{tipo}', 'RelatorioController@relatorio_venda_post');
    });

});  //1

Route::group(['prefix'=>'administrativo'], function(){
    Route::group(['middleware' => ['auth','administrativo']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/grafico/', 'AdministrativoController@grafico');
        Route::get('/grafico_resultado/', 'AdministrativoController@grafico_resultado');

        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/fornecedor_inativa/{id}', 'FornecedorController@inativar_fornecedor');
        Route::get('/fornecedor_ativa/{id}', 'FornecedorController@ativar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@listar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@buscar_fornecedor');
        Route::get('/fornecedor', 'AdministrativoController@fornecedor');
        Route::post('/fornecedor', 'FornecedorController@store');
        Route::get('/fornecedor_edita/{id}', 'AdministrativoController@editar_fornecedor');
        Route::post('/fornecedor_edita/{id}', 'FornecedorController@update');

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');
        Route::get('/user_inativa/{id}', 'UserController@inativar_user');
        Route::get('/user_ativa/{id}', 'UserController@ativar_user');

        Route::get('/configuracoes', 'AdministrativoController@configuracoes');
        Route::post('/configuracoes', 'AdministrativoController@configuracoes_add');

        Route::get('/tecnico', 'QualidadeController@tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@listar_tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@buscar_tecnico');
        Route::get('/tecnico_edita/{id}', 'QualidadeController@editar_tecnico');
        Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
        Route::post('/tecnico', 'TecnicoController@store');
        Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
        Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');

        Route::get('/servico', 'AdministrativoController@servico');
        Route::post('/servico', 'AdministrativoController@servico_store');
        Route::get('/servico_edita/{id}', 'AdministrativoController@editar_servico');
        Route::post('/servico_edita/{id}', 'AdministrativoController@update_servico');
        Route::get('/servico_lista', 'AdministrativoController@listar_servico');

        Route::get('/venda', 'AdministrativoController@venda');
        Route::post('/venda', 'AdministrativoController@venda_store');
        Route::get('/converter_orcamento/{orcamento}', 'AdministrativoController@converter_orcamento');
        Route::post('/converter_orcamento_post/{orcamento}', 'AdministrativoController@converter_orcamento_post');
        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@listar_venda');
        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@buscar_venda');
        Route::get('/cancelar_venda/{id}', 'AdminController@cancelar_venda');
        Route::post('/cancelar_venda_post/{id}', 'AdminController@cancelar_venda_post');

        Route::get('/conta_lista/{tipo}', 'AdministrativoController@listar_conta');
        Route::get('/conta_lista/{tipo}', 'AdministrativoController@buscar_conta');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@listar_conta_pagar');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@buscar_conta_pagar');


        Route::get('/orcamento', 'AdministrativoController@orcamento');
        Route::post('/orcamento', 'AdministrativoController@orcamento_store');
        Route::get('/adicionar_servico', 'AdministrativoController@adicionar_servico');
        Route::get('/servico_valor/{id}', 'AdministrativoController@servico_valor');
        Route::get('/remover_servico/{serv}', 'AdministrativoController@remover_servico');
        Route::post('/servico_orcamento', 'AdministrativoController@servico_orcamento');
        Route::get('/buscar_orcamento', 'AdministrativoController@buscar_orcamento');
        Route::post('/buscar_orcamento', 'AdministrativoController@buscar_post_orcamento');
        Route::get('/adicionar_orcamento/{id}', 'AdministrativoController@adicionar_orcamento');
        Route::get('/finalizado/{id}', 'AdministrativoController@finaliza_orcamento');

        Route::get('/orcamento_edita/{id}/{edita?}', 'AdministrativoController@editar_orcamento');
        Route::post('/orcamento_edita/{id}', 'AdministrativoController@orcamento_update');

        Route::get('/email_orcamento/{orcamento}', 'AdministrativoController@email_orcamento');
        Route::post('/email_orcamento_post/{orcamento}', 'AdministrativoController@email_orcamento_post');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');
        Route::get('/orcamento_aprova/{id}', 'AdministrativoController@aprovar_orcamento');

        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');

        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');

        Route::get('/conta/{tipo}', 'AdministrativoController@conta');
        Route::get('/conta_pagar/{tipo}', 'AdministrativoController@conta_pagar');
        Route::post('/conta', 'AdministrativoController@conta_store');
        Route::get('/buscar/{status}/{tipo}', 'AdministrativoController@buscar');
        Route::post('/buscar/{status}/{tipo}', 'AdministrativoController@buscar_post');
        Route::get('/adicionar/{id}/{tipo}', 'AdministrativoController@adicionar');
        Route::get('/cliente_iframe', 'AdministrativoController@cliente_iframe');
        Route::post('/cliente_conta', 'AdministrativoController@cliente_store');
        Route::get('/fornecedor_conta', 'AdministrativoController@fornecedor_conta');
        Route::post('/fornecedor_conta', 'AdministrativoController@fornecedor_store');

        Route::get('/conta_edita/{tipo}/{id}', 'AdministrativoController@editar_conta');
        Route::get('/conta_edita_pagar/{tipo}/{id}', 'AdministrativoController@editar_conta_pagar');
        Route::post('/conta_edita/{tipo}/{id}', 'AdministrativoController@conta_update');
        Route::get('/conta_cancela/{tipo}/{id}', 'AdminController@cancelar_conta');

        Route::get('/centrocusto', 'AdministrativoController@centrocusto');
        Route::post('/centrocusto', 'AdministrativoController@store');
        Route::get('/centrocusto_edita/{id}', 'AdministrativoController@editar_centrocusto');
        Route::post('/centrocusto_edita/{id}', 'AdministrativoController@update');
        Route::get('/centrocusto_lista', 'AdministrativoController@listar_centrocusto');

        Route::get('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro');
        Route::post('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro_post');

        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');

        Route::get('/relatorio_conta/{tipo}', 'RelatorioController@conta');
        Route::post('/relatorio_conta/{tipo}', 'RelatorioController@buscar_conta');


        Route::get('/contas_cronograma/{tipo}/{ano}/{mes}/{dia}/{centrocusto}', 'AdministrativoController@contas_cronograma');

        Route::get('/relatorio_venda/{tipo}', 'RelatorioController@relatorio_venda');
        Route::post('/relatorio_venda/{tipo}', 'RelatorioController@relatorio_venda_post');

        Route::get('/arquivos/', 'ArquivoController@arquivos');
        Route::post('/arquivos/', 'ArquivoController@store');
        Route::get('/arquivo_lista', 'ArquivoController@listar_arquivo');
        Route::get('/arquivo_lista', 'ArquivoController@buscar_arquivo');

        Route::get('/exame', 'MedicinaController@exame');
        Route::post('/exame', 'MedicinaController@exame_store');
        Route::get('/exame_lista', 'MedicinaController@listar_exame');
        Route::get('/exame_edita/{id}', 'MedicinaController@editar_exame');
        Route::post('/exame_edita/{id}', 'MedicinaController@update_exame');
        Route::get('/exame_delete/{id}', 'MedicinaController@exame_delete');

        Route::get('/laboratorio', 'MedicinaController@laboratorio');
        Route::post('/laboratorio', 'MedicinaController@laboratorio_store');
        Route::get('/laboratorio_lista', 'MedicinaController@listar_laboratorio');
        Route::get('/laboratorio_edita/{id}', 'MedicinaController@editar_laboratorio');
        Route::post('/laboratorio_edita/{id}', 'MedicinaController@update_laboratorio');
        Route::get('/laboratorio_delete/{id}', 'MedicinaController@laboratorio_delete');

        Route::get('/atendimento', 'MedicinaController@atendimento');
        Route::post('/atendimento', 'MedicinaController@atendimento_store');
        Route::get('/atendimento_lista', 'MedicinaController@listar_atendimento');
        Route::get('/atendimento_edita/{id}', 'MedicinaController@editar_atendimento');
        Route::post('/atendimento_edita/{id}', 'MedicinaController@update_atendimento');
        Route::get('/atendimento_cancela/{id}', 'MedicinaController@atendimento_cancela');
        Route::get('/medico_exame', 'MedicinaController@medico_exame');
        Route::get('/med_exame', 'MedicinaController@med_exame');
        Route::get('/laboratorio_exame', 'MedicinaController@laboratorio_exame');
        Route::get('/atendimento_medico', 'MedicinaController@atendimento_medico');
        Route::get('/empresa_exame', 'MedicinaController@empresa_exame');

        Route::get('/medico_exame_pdf', 'MedicinaController@medico_exame_pdf');
        Route::get('/med_exame_pdf', 'MedicinaController@med_exame_pdf');
        Route::get('/laboratorio_exame_pdf', 'MedicinaController@laboratorio_exame_pdf');
        Route::get('/atendimento_medico_pdf', 'MedicinaController@atendimento_medico_pdf');
        Route::get('/empresa_exame_pdf', 'MedicinaController@empresa_exame_pdf');

        Route::get('/aso/', 'AdministrativoController@aso');
        Route::post('/aso/', 'AdministrativoController@aso_post');
        Route::get('/pacientes_ajax/', 'AdministrativoController@pacientes_ajax');
        Route::get('/empresas_ajax/', 'AdministrativoController@empresas_ajax');
        Route::get('/medico_aso/{medico}', 'AdministrativoController@medico_aso');
        Route::get('/gerar_aso/{data}', 'AdministrativoController@aso_pdf');
        Route::get('/gerar_fichaclinica/{data}', 'AdministrativoController@ficha_pdf');
    });
});//2

Route::group(['prefix'=>'qualidade'], function(){
    Route::group(['middleware' => ['auth','qualidade']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');
        Route::get('/user_inativa/{id}', 'UserController@inativar_user');
        Route::get('/user_ativa/{id}', 'UserController@ativar_user');

        Route::get('/tecnico', 'QualidadeController@tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@listar_tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@buscar_tecnico');
        Route::get('/tecnico_edita/{id}', 'QualidadeController@editar_tecnico');
        Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
        Route::post('/tecnico', 'TecnicoController@store');
        Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
        Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');


        Route::get('/servico', 'AdministrativoController@servico');
        Route::post('/servico', 'AdministrativoController@servico_store');
        Route::get('/servico_edita/{id}', 'AdministrativoController@editar_servico');
        Route::post('/servico_edita/{id}', 'AdministrativoController@update_servico');
        Route::get('/servico_lista', 'AdministrativoController@listar_servico');

        Route::get('/orcamento', 'AdministrativoController@orcamento');
        Route::post('/orcamento', 'AdministrativoController@orcamento_store');
        Route::get('/adicionar_servico', 'AdministrativoController@adicionar_servico');
        Route::get('/servico_valor/{id}', 'AdministrativoController@servico_valor');
        Route::get('/remover_servico/{serv}', 'AdministrativoController@remover_servico');
        Route::post('/servico_orcamento', 'AdministrativoController@servico_orcamento');
        Route::get('/buscar_orcamento', 'AdministrativoController@buscar_orcamento');
        Route::post('/buscar_orcamento', 'AdministrativoController@buscar_post_orcamento');
        Route::get('/adicionar_orcamento/{id}', 'AdministrativoController@adicionar_orcamento');
        Route::get('/finalizado/{id}', 'AdministrativoController@finaliza_orcamento');

        Route::get('/orcamento_edita/{id}/{edita?}', 'AdministrativoController@editar_orcamento');
        Route::post('/orcamento_edita/{id}', 'AdministrativoController@orcamento_update');

        Route::get('/email_orcamento/{orcamento}', 'AdministrativoController@email_orcamento');
        Route::post('/email_orcamento_post/{orcamento}', 'AdministrativoController@email_orcamento_post');

        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');
        Route::get('/orcamento_aprova/{id}', 'AdministrativoController@aprovar_orcamento');

        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');

        Route::get('/atividade', 'QualidadeController@atividade');
        Route::post('/atividade', 'QualidadeController@store');
        Route::get('/atividade_lista/{status}', 'QualidadeController@listar_atividade');
        Route::get('/atividade_edita/{id}', 'QualidadeController@editar_atividade');
        Route::post('/atividade_edita/{id}', 'QualidadeController@update');
        Route::get('/atividade_inativa/{id}', 'QualidadeController@inativar_atividade');
        Route::get('/atividade_ativa/{id}', 'QualidadeController@ativar_atividade');

        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');

        Route::get('/cronograma', 'AdministrativoController@cronograma');
        Route::post('/cronograma', 'AdministrativoController@cronograma_post');
        Route::get('/buscar_cliente_cronograma', 'AdministrativoController@buscar_cliente_cronograma');
        Route::post('/buscar_cliente_cronograma', 'AdministrativoController@buscar_post_cliente_cronograma');
        Route::get('/adicionar_cronograma/{id}', 'AdministrativoController@adicionar_cronograma');
        Route::get('/adicionar_atividade/{cliente}/{tecnico}/{atividade}/{ano}/{mes}', 'AdministrativoController@adicionar_atividade_cronograma');
        Route::post('/adicionar_atividade/{cliente}/{tecnico}/{atividade}/{ano}/{mes}', 'AdministrativoController@adicionar_atividade_cronograma_post');
        Route::post('/cronograma_edita_post/{id}', 'AdministrativoController@editar_cronograma_post');
        Route::get('/cronograma_edita/{id}', 'AdministrativoController@editar_cronograma');
        Route::get('/remover_cronograma/{id}', 'AdministrativoController@remover_cronograma');
        Route::get('/observacao/{id}', 'AdministrativoController@observacao');
        Route::get('/deletar_arquivo/{id}', 'AdministrativoController@deletar_arquivo');

        Route::post('/naoconformidade/', 'QualidadeController@store_naoconformidade');
        Route::get('/naoconformidade/{id}', 'QualidadeController@naoconformidade');

        Route::get('/naoconformidade_lista/', 'QualidadeController@listar_naoconformidade');
        Route::get('/naoconformidade_lista/', 'QualidadeController@buscar_naoconformidade');
        Route::get('/naoconformidade_edita/{id}', 'QualidadeController@editar_naoconformidade');
        Route::post('/naoconformidade_edita/{id}', 'QualidadeController@naoconformidade_update');

        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');


        Route::get('/pdf_naoconformidade_lista/', 'QualidadeController@listar_naoconformidadePdf');
        Route::post('/pdf_naoconformidade_lista/', 'QualidadeController@buscarPdf');

        Route::get('/buscar_atividades/', 'AdministrativoController@listar_atividades');
        Route::post('/buscar_atividades/', 'AdministrativoController@buscar_atividades');

        Route::get('/limpar_filtro/', 'AdministrativoController@limpar_filtro');
        Route::get('/remover_cliente/', 'AdministrativoController@remover_cliente');
        Route::get('/remover_cliente_naoconformidade/', 'AdministrativoController@remover_cliente_naoconformidade');

        Route::get('/agendas/', 'AgendaController@agendas');
        Route::post('/agendas/', 'AgendaController@agenda');

        Route::get('/arquivos/', 'ArquivoController@arquivos');
        Route::post('/arquivos/', 'ArquivoController@store');
        Route::get('/arquivo_lista', 'ArquivoController@listar_arquivo');
        Route::get('/arquivo_lista', 'ArquivoController@buscar_arquivo');


    });

}); //3

Route::group(['prefix'=>'medicina'], function(){
    Route::group(['middleware' => ['auth','medicina']], function() {
        Route::get('/', 'AdministrativoController@index');


        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');

        Route::get('/tecnico', 'QualidadeController@tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@listar_tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@buscar_tecnico');
        Route::get('/tecnico_edita/{id}', 'QualidadeController@editar_tecnico');
        Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
        Route::post('/tecnico', 'TecnicoController@store');
        Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
        Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');

        Route::get('/servico', 'AdministrativoController@servico');
        Route::post('/servico', 'AdministrativoController@servico_store');
        Route::get('/servico_edita/{id}', 'AdministrativoController@editar_servico');
        Route::post('/servico_edita/{id}', 'AdministrativoController@update_servico');
        Route::get('/servico_lista', 'AdministrativoController@listar_servico');

        Route::get('/paciente_inativa/{id}', 'MedicinaController@inativar_paciente');
        Route::get('/paciente_ativa/{id}', 'MedicinaController@ativar_paciente');
        Route::get('/paciente', 'MedicinaController@paciente');
        Route::post('/paciente', 'MedicinaController@store');
        Route::get('/paciente_lista/{status}', 'MedicinaController@listar_paciente');
        Route::get('/paciente_lista/{status}', 'MedicinaController@buscar_paciente');
        Route::get('/paciente_edita/{id}', 'MedicinaController@editar_paciente');
        Route::post('/paciente_edita/{id}', 'MedicinaController@update');

        Route::get('/orcamento', 'AdministrativoController@orcamento');
        Route::post('/orcamento', 'AdministrativoController@orcamento_store');
        Route::get('/adicionar_servico', 'AdministrativoController@adicionar_servico');
        Route::get('/servico_valor/{id}', 'AdministrativoController@servico_valor');
        Route::get('/remover_servico/{serv}', 'AdministrativoController@remover_servico');
        Route::post('/servico_orcamento', 'AdministrativoController@servico_orcamento');
        Route::get('/buscar_orcamento', 'AdministrativoController@buscar_orcamento');
        Route::post('/buscar_orcamento', 'AdministrativoController@buscar_post_orcamento');
        Route::get('/adicionar_orcamento/{id}', 'AdministrativoController@adicionar_orcamento');
        Route::get('/finalizado/{id}', 'AdministrativoController@finaliza_orcamento');

        Route::get('/orcamento_edita/{id}/{edita?}', 'AdministrativoController@editar_orcamento');
        Route::post('/orcamento_edita/{id}', 'AdministrativoController@orcamento_update');
        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');

        Route::get('/email_orcamento/{orcamento}', 'AdministrativoController@email_orcamento');
        Route::post('/email_orcamento_post/{orcamento}', 'AdministrativoController@email_orcamento_post');

        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');
        Route::get('/orcamento_aprova/{id}', 'AdministrativoController@aprovar_orcamento');

        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');

        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');

        Route::get('/agendas/', 'AgendaController@agendas');
        Route::post('/agendas/', 'AgendaController@agenda');

        Route::get('/exame', 'MedicinaController@exame');
        Route::post('/exame', 'MedicinaController@exame_store');
        Route::get('/exame_lista', 'MedicinaController@listar_exame');
        Route::get('/exame_edita/{id}', 'MedicinaController@editar_exame');
        Route::post('/exame_edita/{id}', 'MedicinaController@update_exame');
        Route::get('/exame_delete/{id}', 'MedicinaController@exame_delete');

        Route::get('/laboratorio', 'MedicinaController@laboratorio');
        Route::post('/laboratorio', 'MedicinaController@laboratorio_store');
        Route::get('/laboratorio_lista', 'MedicinaController@listar_laboratorio');
        Route::get('/laboratorio_edita/{id}', 'MedicinaController@editar_laboratorio');
        Route::post('/laboratorio_edita/{id}', 'MedicinaController@update_laboratorio');
        Route::get('/laboratorio_delete/{id}', 'MedicinaController@laboratorio_delete');

        Route::get('/atendimento', 'MedicinaController@atendimento');
        Route::post('/atendimento', 'MedicinaController@atendimento_store');
        Route::get('/atendimento_lista', 'MedicinaController@listar_atendimento');
        Route::get('/atendimento_edita/{id}', 'MedicinaController@editar_atendimento');
        Route::post('/atendimento_edita/{id}', 'MedicinaController@update_atendimento');
        Route::get('/atendimento_cancela/{id}', 'MedicinaController@atendimento_cancela');
        Route::get('/medico_exame', 'MedicinaController@medico_exame');
        Route::get('/med_exame', 'MedicinaController@med_exame');
        Route::get('/laboratorio_exame', 'MedicinaController@laboratorio_exame');
        Route::get('/atendimento_medico', 'MedicinaController@atendimento_medico');
        Route::get('/empresa_exame', 'MedicinaController@empresa_exame');

        Route::get('/medico_exame_pdf', 'MedicinaController@medico_exame_pdf');
        Route::get('/med_exame_pdf', 'MedicinaController@med_exame_pdf');
        Route::get('/laboratorio_exame_pdf', 'MedicinaController@laboratorio_exame_pdf');
        Route::get('/atendimento_medico_pdf', 'MedicinaController@atendimento_medico_pdf');
        Route::get('/empresa_exame_pdf', 'MedicinaController@empresa_exame_pdf');
        Route::get('/exames_pdf', 'MedicinaController@exames_pdf');

        Route::get('/aso/', 'AdministrativoController@aso');
        Route::post('/aso/', 'AdministrativoController@aso_post');
        Route::get('/pacientes_ajax/', 'AdministrativoController@pacientes_ajax');
        Route::get('/empresas_ajax/', 'AdministrativoController@empresas_ajax');
        Route::get('/medico_aso/{medico}', 'AdministrativoController@medico_aso');
        Route::get('/gerar_aso/{data}', 'AdministrativoController@aso_pdf');
        Route::get('/gerar_fichaclinica/{data}', 'AdministrativoController@ficha_pdf');

    });
}); //4

Route::group(['prefix'=>'vendas'], function(){
    Route::group(['middleware' => ['auth','vendas']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');

        Route::get('/tecnico', 'QualidadeController@tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@listar_tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@buscar_tecnico');
        Route::get('/tecnico_edita/{id}', 'QualidadeController@editar_tecnico');
        Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
        Route::post('/tecnico', 'TecnicoController@store');
        Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
        Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');

        Route::get('/servico', 'AdministrativoController@servico');
        Route::post('/servico', 'AdministrativoController@servico_store');
        Route::get('/servico_edita/{id}', 'AdministrativoController@editar_servico');
        Route::post('/servico_edita/{id}', 'AdministrativoController@update_servico');
        Route::get('/servico_lista', 'AdministrativoController@listar_servico');

        Route::get('/orcamento', 'AdministrativoController@orcamento');
        Route::post('/orcamento', 'AdministrativoController@orcamento_store');
        Route::get('/adicionar_servico', 'AdministrativoController@adicionar_servico');
        Route::get('/servico_valor/{id}', 'AdministrativoController@servico_valor');
        Route::get('/remover_servico/{serv}', 'AdministrativoController@remover_servico');
        Route::post('/servico_orcamento', 'AdministrativoController@servico_orcamento');
        Route::get('/buscar_orcamento', 'AdministrativoController@buscar_orcamento');
        Route::post('/buscar_orcamento', 'AdministrativoController@buscar_post_orcamento');
        Route::get('/adicionar_orcamento/{id}', 'AdministrativoController@adicionar_orcamento');
        Route::get('/finalizado/{id}', 'AdministrativoController@finaliza_orcamento');

        Route::get('/orcamento_edita/{id}/{edita?}', 'AdministrativoController@editar_orcamento');
        Route::post('/orcamento_edita/{id}', 'AdministrativoController@orcamento_update');

        Route::get('/email_orcamento/{orcamento}', 'AdministrativoController@email_orcamento');
        Route::post('/email_orcamento_post/{orcamento}', 'AdministrativoController@email_orcamento_post');

        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');
        Route::get('/orcamento_aprova/{id}', 'AdministrativoController@aprovar_orcamento');

        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');

        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');

        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');
        Route::get('/medico_aso/{medico}', 'AdministrativoController@medico_aso');
    });
});//5

Route::group(['prefix'=>'ti'], function(){
    Route::group(['middleware' => ['auth','ti']], function() {
        Route::get('/', 'AdministrativoController@index');


        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');
        Route::get('/user_inativa/{id}', 'UserController@inativar_user');
        Route::get('/user_ativa/{id}', 'UserController@ativar_user');

        Route::get('/configuracoes', 'AdministrativoController@configuracoes');
        Route::post('/configuracoes', 'AdministrativoController@configuracoes_add');

        Route::get('/logs', 'AdministrativoController@logs');
        Route::get('/logs', 'AdministrativoController@busca_logs');
    });
});  //7

Route::group(['prefix'=>'tecnico'], function(){
    Route::group(['middleware' => ['auth','tecnico']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/user_edita', 'AdministrativoController@editar_user2');
        Route::get('/user_view2', 'UserController@show2');
    });
});  //8

Route::group(['prefix'=>'desenvolvedor'], function(){
    Route::group(['middleware' => ['auth','desenvolvedor']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/fornecedor_inativa/{id}', 'FornecedorController@inativar_fornecedor');
        Route::get('/fornecedor_ativa/{id}', 'FornecedorController@ativar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@listar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@buscar_fornecedor');
        Route::get('/fornecedor', 'AdministrativoController@fornecedor');
        Route::post('/fornecedor', 'FornecedorController@store');
        Route::get('/fornecedor_edita/{id}', 'AdministrativoController@editar_fornecedor');
        Route::post('/fornecedor_edita/{id}', 'FornecedorController@update');
        

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');
        Route::get('/user_inativa/{id}', 'UserController@inativar_user');
        Route::get('/user_ativa/{id}', 'UserController@ativar_user');

        Route::get('/configuracoes', 'AdministrativoController@configuracoes');
        Route::post('/configuracoes', 'AdministrativoController@configuracoes_add');

        Route::get('/tecnico', 'QualidadeController@tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@listar_tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@buscar_tecnico');
        Route::get('/tecnico_edita/{id}', 'QualidadeController@editar_tecnico');
        Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
        Route::post('/tecnico', 'TecnicoController@store');
        Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
        Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');

        Route::get('/servico', 'AdministrativoController@servico');
        Route::post('/servico', 'AdministrativoController@servico_store');
        Route::get('/servico_edita/{id}', 'AdministrativoController@editar_servico');
        Route::post('/servico_edita/{id}', 'AdministrativoController@update_servico');
        Route::get('/servico_lista', 'AdministrativoController@listar_servico');

        Route::get('/paciente_inativa/{id}', 'MedicinaController@inativar_paciente');
        Route::get('/paciente_ativa/{id}', 'MedicinaController@ativar_paciente');
        Route::get('/paciente', 'MedicinaController@paciente');
        Route::post('/paciente', 'MedicinaController@store');
        Route::get('/paciente_lista/{status}', 'MedicinaController@listar_paciente');
        Route::get('/paciente_lista/{status}', 'MedicinaController@buscar_paciente');
        Route::get('/paciente_edita/{id}', 'MedicinaController@editar_paciente');
        Route::post('/paciente_edita/{id}', 'MedicinaController@update');


        Route::get('/venda', 'AdministrativoController@venda');
        Route::post('/venda', 'AdministrativoController@venda_store');
        Route::get('/converter_orcamento/{orcamento}', 'AdministrativoController@converter_orcamento');
        Route::post('/converter_orcamento_post/{orcamento}', 'AdministrativoController@converter_orcamento_post');

        Route::get('/orcamento', 'AdministrativoController@orcamento');
        Route::post('/orcamento', 'AdministrativoController@orcamento_store');
        Route::get('/adicionar_servico', 'AdministrativoController@adicionar_servico');
        Route::get('/servico_valor/{id}', 'AdministrativoController@servico_valor');
        Route::get('/remover_servico/{serv}', 'AdministrativoController@remover_servico');
        Route::post('/servico_orcamento', 'AdministrativoController@servico_orcamento');
        Route::get('/buscar_orcamento', 'AdministrativoController@buscar_orcamento');
        Route::post('/buscar_orcamento', 'AdministrativoController@buscar_post_orcamento');
        Route::get('/adicionar_orcamento/{id}', 'AdministrativoController@adicionar_orcamento');
        Route::get('/finalizado/{id}', 'AdministrativoController@finaliza_orcamento');

        Route::get('/orcamento_edita/{id}/{edita?}', 'AdministrativoController@editar_orcamento');
        Route::post('/orcamento_edita/{id}', 'AdministrativoController@orcamento_update');

        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');

        Route::get('/email_orcamento/{orcamento}', 'AdministrativoController@email_orcamento');
        Route::post('/email_orcamento_post/{orcamento}', 'AdministrativoController@email_orcamento_post');

        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');
        Route::get('/orcamento_aprova/{id}', 'AdministrativoController@aprovar_orcamento');

        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');


        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');

        Route::get('/conta/{tipo}', 'AdministrativoController@conta');
        Route::get('/conta_pagar/{tipo}', 'AdministrativoController@conta_pagar');
        Route::post('/conta', 'AdministrativoController@conta_store');
        Route::get('/buscar/{status}/{tipo}', 'AdministrativoController@buscar');
        Route::post('/buscar/{status}/{tipo}', 'AdministrativoController@buscar_post');
        Route::get('/adicionar/{id}/{tipo}', 'AdministrativoController@adicionar');
        Route::get('/cliente_iframe', 'AdministrativoController@cliente_iframe');
        Route::post('/cliente_conta', 'AdministrativoController@cliente_store');
        Route::get('/fornecedor_conta', 'AdministrativoController@fornecedor_conta');
        Route::post('/fornecedor_conta', 'AdministrativoController@fornecedor_store');

        Route::get('/conta_edita/{tipo}/{id}', 'AdministrativoController@editar_conta');
        Route::get('/conta_edita_pagar/{tipo}/{id}', 'AdministrativoController@editar_conta_pagar');
        Route::post('/conta_edita/{tipo}/{id}', 'AdministrativoController@conta_update');
        Route::get('/conta_cancela/{tipo}/{id}', 'AdminController@cancelar_conta');


        Route::get('/atividade', 'QualidadeController@atividade');
        Route::post('/atividade', 'QualidadeController@store');
        Route::get('/atividade_lista/{status}', 'QualidadeController@listar_atividade');
        Route::get('/atividade_edita/{id}', 'QualidadeController@editar_atividade');
        Route::post('/atividade_edita/{id}', 'QualidadeController@update');
        Route::get('/atividade_inativa/{id}', 'QualidadeController@inativar_atividade');
        Route::get('/atividade_ativa/{id}', 'QualidadeController@ativar_atividade');

        



        Route::get('/centrocusto', 'AdministrativoController@centrocusto');
        Route::post('/centrocusto', 'AdministrativoController@store');
        Route::get('/centrocusto_edita/{id}', 'AdministrativoController@editar_centrocusto');
        Route::post('/centrocusto_edita/{id}', 'AdministrativoController@update');
        Route::get('/centrocusto_lista', 'AdministrativoController@listar_centrocusto');

        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@listar_venda');
        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@buscar_venda');
        Route::get('/cancelar_venda/{id}', 'AdminController@cancelar_venda');
        Route::post('/cancelar_venda_post/{id}', 'AdminController@cancelar_venda_post');

        Route::get('/conta_lista/{tipo}', 'AdministrativoController@listar_conta');
        Route::get('/conta_lista/{tipo}', 'AdministrativoController@buscar_conta');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@listar_conta_pagar');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@buscar_conta_pagar');


        Route::get('/cronograma', 'AdministrativoController@cronograma');
        Route::post('/cronograma', 'AdministrativoController@cronograma_post');
        Route::get("/cronograma/graficos", "AdministrativoController@cronograma_grafico");
        Route::post("/cronograma/graficos", "AdministrativoController@cronograma_grafico_post");
        Route::get('/buscar_cliente_cronograma', 'AdministrativoController@buscar_cliente_cronograma');
        Route::post('/buscar_cliente_cronograma', 'AdministrativoController@buscar_post_cliente_cronograma');
        Route::get('/adicionar_cronograma/{id}', 'AdministrativoController@adicionar_cronograma');
        Route::get('/adicionar_atividade/{cliente}/{tecnico}/{atividade}/{ano}/{mes}', 'AdministrativoController@adicionar_atividade_cronograma');
        Route::post('/adicionar_atividade/{cliente}/{tecnico}/{atividade}/{ano}/{mes}', 'AdministrativoController@adicionar_atividade_cronograma_post');
        Route::post('/cronograma_edita_post/{id}', 'AdministrativoController@editar_cronograma_post');
        Route::get('/cronograma_edita/{id}', 'AdministrativoController@editar_cronograma');
        Route::get('/remover_cronograma/{id}', 'AdministrativoController@remover_cronograma');
        Route::get('/observacao/{id}', 'AdministrativoController@observacao');
        Route::get('/deletar_arquivo/{id}', 'AdministrativoController@deletar_arquivo');

        Route::post('/naoconformidade/', 'QualidadeController@store_naoconformidade');
        Route::get('/naoconformidade/{id}', 'QualidadeController@naoconformidade');
        Route::get('/naoconformidade_lista/', 'QualidadeController@listar_naoconformidade');
        Route::get('/naoconformidade_lista/', 'QualidadeController@buscar_naoconformidade');
        Route::get('/naoconformidade_edita/{id}', 'QualidadeController@editar_naoconformidade');
        Route::post('/naoconformidade_edita/{id}', 'QualidadeController@naoconformidade_update');

        Route::get('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro');
        Route::post('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro_post');


        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');



        Route::get('/relatorio_conta/{tipo}', 'RelatorioController@conta');
        Route::post('/relatorio_conta/{tipo}', 'RelatorioController@buscar_conta');
        Route::get('/relatorio_pagamentotecnico', 'RelatorioController@pagamentotecnico');
        Route::post('/relatorio_pagamentotecnico', 'RelatorioController@buscar_pagamentotecnico');

        Route::get('/pdf_naoconformidade_lista/', 'QualidadeController@listar_naoconformidadePdf');
        Route::post('/pdf_naoconformidade_lista/', 'QualidadeController@buscarPdf');

        Route::get('/buscar_atividades/', 'AdministrativoController@listar_atividades');
        Route::post('/buscar_atividades/', 'AdministrativoController@buscar_atividades');

        Route::get('/limpar_filtro/', 'AdministrativoController@limpar_filtro');
        Route::get('/remover_cliente/', 'AdministrativoController@remover_cliente');
        Route::get('/remover_cliente_naoconformidade/', 'AdministrativoController@remover_cliente_naoconformidade');

        Route::get('/logs', 'AdministrativoController@logs');
        Route::get('/logs', 'AdministrativoController@busca_logs');

        Route::get('/agendas/', 'AgendaController@agendas');
        Route::post('/agendas/', 'AgendaController@agenda');

        Route::get('/contas_cronograma/{tipo}/{ano}/{mes}/{dia}/{centrocusto}', 'AdministrativoController@contas_cronograma');

        Route::get('/relatorio_venda/{tipo}', 'RelatorioController@relatorio_venda');
        Route::post('/relatorio_venda/{tipo}', 'RelatorioController@relatorio_venda_post');

        Route::get('/grafico/', 'AdministrativoController@grafico');
        Route::get('/grafico_resultado/', 'AdministrativoController@grafico_resultado');


        //new
        Route::get('/aso/', 'AdministrativoController@aso');
        Route::post('/aso/', 'AdministrativoController@aso_post');
        Route::get('/pacientes_ajax/', 'AdministrativoController@pacientes_ajax');
        Route::get('/empresas_ajax/', 'AdministrativoController@empresas_ajax');
        Route::get('/medico_aso/{medico}', 'AdministrativoController@medico_aso');
        Route::get('/gerar_aso/{data}', 'AdministrativoController@aso_pdf');
        Route::get('/gerar_fichaclinica/{data}', 'AdministrativoController@ficha_pdf');

        Route::get('/arquivos/', 'ArquivoController@arquivos');
        Route::post('/arquivos/', 'ArquivoController@store');
        Route::get('/arquivo_lista', 'ArquivoController@listar_arquivo');
        Route::get('/arquivo_lista', 'ArquivoController@buscar_arquivo');

//        Route::get('/usermigrate', 'UserController@usermigrate');

        Route::get('/exame', 'MedicinaController@exame');
        Route::post('/exame', 'MedicinaController@exame_store');
        Route::get('/exame_lista', 'MedicinaController@listar_exame');
        Route::get('/exame_edita/{id}', 'MedicinaController@editar_exame');
        Route::post('/exame_edita/{id}', 'MedicinaController@update_exame');
        Route::get('/exame_delete/{id}', 'MedicinaController@exame_delete');

        Route::get('/laboratorio', 'MedicinaController@laboratorio');
        Route::post('/laboratorio', 'MedicinaController@laboratorio_store');
        Route::get('/laboratorio_lista', 'MedicinaController@listar_laboratorio');
        Route::get('/laboratorio_edita/{id}', 'MedicinaController@editar_laboratorio');
        Route::post('/laboratorio_edita/{id}', 'MedicinaController@update_laboratorio');
        Route::get('/laboratorio_delete/{id}', 'MedicinaController@laboratorio_delete');

        Route::get('/atendimento', 'MedicinaController@atendimento');
        Route::post('/atendimento', 'MedicinaController@atendimento_store');
        Route::get('/atendimento_lista', 'MedicinaController@listar_atendimento');
        Route::get('/atendimento_edita/{id}', 'MedicinaController@editar_atendimento');
        Route::post('/atendimento_edita/{id}', 'MedicinaController@update_atendimento');
        Route::get('/atendimento_cancela/{id}', 'MedicinaController@atendimento_cancela');
        Route::get('/medico_exame', 'MedicinaController@medico_exame');
        Route::get('/med_exame', 'MedicinaController@med_exame');
        Route::get('/laboratorio_exame', 'MedicinaController@laboratorio_exame');
        Route::get('/atendimento_medico', 'MedicinaController@atendimento_medico');
        Route::get('/empresa_exame', 'MedicinaController@empresa_exame');

        Route::get('/medico_exame_pdf', 'MedicinaController@medico_exame_pdf');
        Route::get('/med_exame_pdf', 'MedicinaController@med_exame_pdf');
        Route::get('/laboratorio_exame_pdf', 'MedicinaController@laboratorio_exame_pdf');
        Route::get('/atendimento_medico_pdf', 'MedicinaController@atendimento_medico_pdf');
        Route::get('/empresa_exame_pdf', 'MedicinaController@empresa_exame_pdf');

        Route::get('/exames_pdf', 'MedicinaController@exames_pdf');

    });
}); // 6

Route::group(['prefix'=>'auxiliaradm'], function(){
    Route::group(['middleware' => ['auth','auxiliaradm']], function() {
        Route::get('/', 'AdministrativoController@index');


        Route::get('/cliente_ativa/{id}', 'ClienteController@ativar_cliente');
        Route::get('/cliente_inativa/{id}', 'ClienteController@inativar_cliente');
        Route::post('/cliente_edita/{id}', 'ClienteController@update');
        Route::post('/cliente', 'ClienteController@store');
        Route::get('/cliente', 'AdministrativoController@cliente');
        Route::get('/cliente_edita/{id}', 'AdministrativoController@editar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@listar_cliente');
        Route::get('/cliente_lista/{status}', 'AdministrativoController@buscar_cliente');

        Route::get('/fornecedor_inativa/{id}', 'FornecedorController@inativar_fornecedor');
        Route::get('/fornecedor_ativa/{id}', 'FornecedorController@ativar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@listar_fornecedor');
        Route::get('/fornecedor_lista/{status}', 'AdministrativoController@buscar_fornecedor');
        Route::get('/fornecedor', 'AdministrativoController@fornecedor');
        Route::post('/fornecedor', 'FornecedorController@store');
        Route::get('/fornecedor_edita/{id}', 'AdministrativoController@editar_fornecedor');
        Route::post('/fornecedor_edita/{id}', 'FornecedorController@update');

        Route::get('/user_view2', 'UserController@show2');
        Route::get('/user', 'AdministrativoController@user');
        Route::get('/user_edita/{id}', 'AdministrativoController@editar_user');
        Route::get('/user_resetarsenha/{id}', 'AdministrativoController@resetsenha');
        Route::get('/user_lista/', 'AdministrativoController@listar_user');
        Route::get('/user_lista/', 'AdministrativoController@buscar_user');
        Route::get('/user_inativa/{id}', 'UserController@inativar_user');
        Route::get('/user_ativa/{id}', 'UserController@ativar_user');

        Route::get('/configuracoes', 'AdministrativoController@configuracoes');
        Route::post('/configuracoes', 'AdministrativoController@configuracoes_add');

        Route::get('/tecnico', 'QualidadeController@tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@listar_tecnico');
        Route::get('/tecnico_lista/{status}', 'QualidadeController@buscar_tecnico');
        Route::get('/tecnico_edita/{id}', 'QualidadeController@editar_tecnico');
        Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
        Route::post('/tecnico', 'TecnicoController@store');
        Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
        Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');

        Route::get('/servico', 'AdministrativoController@servico');
        Route::post('/servico', 'AdministrativoController@servico_store');
        Route::get('/servico_edita/{id}', 'AdministrativoController@editar_servico');
        Route::post('/servico_edita/{id}', 'AdministrativoController@update_servico');
        Route::get('/servico_lista', 'AdministrativoController@listar_servico');

        Route::get('/venda', 'AdministrativoController@venda');
        Route::post('/venda', 'AdministrativoController@venda_store');
        Route::get('/converter_orcamento/{orcamento}', 'AdministrativoController@converter_orcamento');
        Route::post('/converter_orcamento_post/{orcamento}', 'AdministrativoController@converter_orcamento_post');
        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@listar_venda');
        Route::get('/venda_lista/{orcamento}', 'AdministrativoController@buscar_venda');
        Route::get('/cancelar_venda/{id}', 'AdminController@cancelar_venda');
        Route::post('/cancelar_venda_post/{id}', 'AdminController@cancelar_venda_post');

        Route::get('/conta_lista/{tipo}', 'AdministrativoController@listar_conta');
        Route::get('/conta_lista/{tipo}', 'AdministrativoController@buscar_conta');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@listar_conta_pagar');
        Route::get('/conta_lista_pagar/{tipo}', 'AdministrativoController@buscar_conta_pagar');


        Route::get('/orcamento', 'AdministrativoController@orcamento');
        Route::post('/orcamento', 'AdministrativoController@orcamento_store');
        Route::get('/adicionar_servico', 'AdministrativoController@adicionar_servico');
        Route::get('/servico_valor/{id}', 'AdministrativoController@servico_valor');
        Route::get('/remover_servico/{serv}', 'AdministrativoController@remover_servico');
        Route::post('/servico_orcamento', 'AdministrativoController@servico_orcamento');
        Route::get('/buscar_orcamento', 'AdministrativoController@buscar_orcamento');
        Route::post('/buscar_orcamento', 'AdministrativoController@buscar_post_orcamento');
        Route::get('/adicionar_orcamento/{id}', 'AdministrativoController@adicionar_orcamento');
        Route::get('/finalizado/{id}', 'AdministrativoController@finaliza_orcamento');

        Route::get('/orcamento_edita/{id}/{edita?}', 'AdministrativoController@editar_orcamento');
        Route::post('/orcamento_edita/{id}', 'AdministrativoController@orcamento_update');

        Route::get('/email_orcamento/{orcamento}', 'AdministrativoController@email_orcamento');
        Route::post('/email_orcamento_post/{orcamento}', 'AdministrativoController@email_orcamento_post');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@listar_orcamento');
        Route::get('/orcamento_lista/{orcamento}', 'AdministrativoController@buscar_listaorcamento');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@listar_convertidos');
        Route::get('/orcamento_convertidos_lista/{orcamento}', 'AdministrativoController@buscar_convertidos');
        Route::get('/orcamento_aprova/{id}', 'AdministrativoController@aprovar_orcamento');

        Route::get('/orcamento_inativa/{id}', 'AdministrativoController@inativar_orcamento');
        Route::get('/orcamento_ativa/{id}', 'AdministrativoController@ativar_orcamento');

        Route::get('/remover_servico_edit/{orcamento}/{serv}', 'AdministrativoController@remover_servico_edit');

        Route::get('/conta/{tipo}', 'AdministrativoController@conta');
        Route::get('/conta_pagar/{tipo}', 'AdministrativoController@conta_pagar');
        Route::post('/conta', 'AdministrativoController@conta_store');
        Route::get('/buscar/{status}/{tipo}', 'AdministrativoController@buscar');
        Route::post('/buscar/{status}/{tipo}', 'AdministrativoController@buscar_post');
        Route::get('/adicionar/{id}/{tipo}', 'AdministrativoController@adicionar');
        Route::get('/cliente_iframe', 'AdministrativoController@cliente_iframe');
        Route::post('/cliente_conta', 'AdministrativoController@cliente_store');
        Route::get('/fornecedor_conta', 'AdministrativoController@fornecedor_conta');
        Route::post('/fornecedor_conta', 'AdministrativoController@fornecedor_store');

        Route::get('/conta_edita/{tipo}/{id}', 'AdministrativoController@editar_conta');
        Route::get('/conta_edita_pagar/{tipo}/{id}', 'AdministrativoController@editar_conta_pagar');
        Route::post('/conta_edita/{tipo}/{id}', 'AdministrativoController@conta_update');
        Route::get('/conta_cancela/{tipo}/{id}', 'AdminController@cancelar_conta');

        Route::get('/centrocusto', 'AdministrativoController@centrocusto');
        Route::post('/centrocusto', 'AdministrativoController@store');
        Route::get('/centrocusto_edita/{id}', 'AdministrativoController@editar_centrocusto');
        Route::post('/centrocusto_edita/{id}', 'AdministrativoController@update');
        Route::get('/centrocusto_lista', 'AdministrativoController@listar_centrocusto');

        Route::get('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro');
        Route::post('/cronograma_financeiro', 'AdministrativoController@cronograma_financeiro_post');

        Route::get('/medico_inativa/{id}', 'AdministrativoController@inativar_medico');
        Route::get('/medico_ativa/{id}', 'AdministrativoController@ativar_medico');
        Route::get('/medico', 'AdministrativoController@medico');
        Route::post('/medico', 'AdministrativoController@medico_store');
        Route::get('/medico_lista/{status}', 'AdministrativoController@listar_medico');
        Route::get('/medico_lista/{status}', 'AdministrativoController@buscar_medico');
        Route::post('/medico_edita/{id}', 'AdministrativoController@medico_update');
        Route::get('/medico_edita/{id}', 'AdministrativoController@editar_medico');






    });
});//8



Route::group(['prefix'=>'cliente'], function(){
    Route::group(['middleware' => ['auth','cliente']], function() {
        Route::get('/', 'AdministrativoController@index');

        Route::get('/arquivos_cliente', 'ArquivoController@arquivos_cliente');


    });
});//9

Route::any('/agenda/{method?}', 'AgendaController@agenda');

Route::post('/fornecedor_edita/{id}', 'FornecedorController@update');
Route::post('/fornecedor', 'FornecedorController@store');
Route::get('/fornecedor_inativa/{id}', 'FornecedorController@ativar_fornecedor');
Route::get('/fornecedor_ativa/{id}', 'FornecedorController@inativar_fornecedor');

Route::post('/tecnico_edita/{id}', 'TecnicoController@update');
Route::post('/tecnico', 'TecnicoController@store');
Route::get('/tecnico_inativa/{id}', 'TecnicoController@inativar_tecnico');
Route::get('/tecnico_ativa/{id}', 'TecnicoController@ativar_tecnico');

Route::post('/user_edita/{id}', 'UserController@update');
Route::post('/user_resetarsenha/{id}', 'UserController@resetpassword');
Route::post('/user', 'UserController@store');

Route::get('/mail', 'AdministrativoController@mail');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/pdf/{id}', 'PdfController@gerarpdf');
Route::get('/pdf/naoconformidade/{id}', 'PdfController@pdfNaoConformidadeView');
Route::get('/retorna/', 'AdministrativoController@idRedirect');
Route::get('/redirect', 'AdministrativoController@redirect');
Route::get('/pdf/naoconformidades/{id}', 'PdfController@pdfNaoConformidadeList');
Route::get('/buscarmedico/{busca}', 'MedicinaController@buscar_medico');
Route::get('/buscarpaciente/{busca}', 'MedicinaController@buscar_pacienteagenda');
Route::get('/buscarcliente/{busca}', 'ClienteController@buscar_cliente');
Route::get('/buscarfornecedor/{busca}', 'FornecedorController@buscar_fornecedor');
Route::any('/edit', 'AgendaController@edit');

Route::post('/grafico', 'GraficoController@carrega_grafico');
Route::get('/centrocusto', 'GraficoController@centrodecustos');
Route::post('/grafico_resultado', 'GraficoController@carrega_graficoResultado');
