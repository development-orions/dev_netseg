<?php
/**
 * Created by PhpStorm.
 * User: SilvioLeite
 * Date: 09/05/2017
 * Time: 18:27
 */

namespace App\Services;



use App\Repositories\ContaRepository;

class GraficoService
{

    /**
     * @var ContaRepository
     */
    private $contaRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(ContaRepository $contaRepository, AuxiliarService $auxiliarService)
    {
        $this->contaRepository = $contaRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function gerarGrafico($data){
        $data['inicio'] = $this->auxiliarService->converteDataBancocomSlash($data['inicio']);
        $data['fim'] = $this->auxiliarService->converteDataBancocomSlash($data['fim']);
        return $this->contaRepository->grafico($data);
    }

    public function gerarGraficoResultado($data){
        $data['inicio'] = $this->auxiliarService->converteDataBancocomSlash($data['inicio']);
        $data['fim'] = $this->auxiliarService->converteDataBancocomSlash($data['fim']);
        return $this->contaRepository->graficoReseultado($data);
    }
}