<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 14:37
 */

namespace App\Services;


use App\Repositories\ArquivoRepository;
use App\Repositories\LogRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ArquivoService
{
    protected $arquivos;
    /**
     * @var ArquivoRepository
     */
    private $arquivoRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(ArquivoRepository $arquivoRepository, LogRepository $logRepository, AuxiliarService $auxiliarService){

        $this->arquivoRepository = $arquivoRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function create($data){

        if (isset($data['todos']) && $data['todos'] == 'on'){
            $data['todos'] = 1;
        }else{

            $clientes = explode(', ', $data['busca']);
            $ids="";
            foreach ($clientes as $cli){
                $cli = explode(' - ', $cli);
                $ids[]= (int)$cli[0];
            }

            if (end($ids) == ""){
                array_pop($ids);
            }
            $data['ids']= $ids;
            $data['todos'] = 0;
        }

        $resposta = array();
        try {
            if ($this->arquivoRepository->store($data)) {
                $resposta['msg'] = 'Arquivo salvo com sucesso.';

                $this->logRepository->store("CADASTRAR ARQUIVO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o arquivo, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }


    public function buscar($status, $texto){
        if(is_numeric($status)){
            if(!empty($texto['q']))
                return $this->arquivoRepository->buscar($status, $texto);
            else
                return $this->listar($status);
        }
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $arquivo = $this->arquivoRepository->buscarPorId($id);

            if(isset($arquivo->data_contrato)) {
                $arquivo->data_contrato = $this->auxiliarService->converteData($arquivo->data_contrato);
            }
            $arquivo->valor_contrato = $this->format($arquivo->valor_contrato);
            if(isset($arquivo->valor_tecnico)) {
                $arquivo->valor_tecnico = $this->format($arquivo->valor_tecnico);
            }
            return $arquivo;
        }
    }


    public function listar_arquivo(){
        $arquivos = $this->arquivoRepository->listar_arquivo();

        foreach ($arquivos as $arquivo) {
            $clientes = $this->arquivoRepository->listar_clientes($arquivo->id);
            $arquivo->clientes = !empty($clientes) ? $clientes : 'Todos';
        }

        return  $arquivos;
    }

    public function arquivos_cliente(){
        $arquivos = $this->arquivoRepository->arquivos_cliente();

        return  $arquivos;
    }

    public function buscar_arquivo($data){
        if(!empty($data['arquivo']) || !empty($data['inicio']) || !empty($data['fim'])) {
            $arquivos = $this->arquivoRepository->buscar_arquivo($data);

            foreach ($arquivos as $arquivo) {
                $clientes = $this->arquivoRepository->listar_clientes($arquivo->id);
                $arquivo->clientes = !empty($clientes) ? $clientes : 'Todos';
            }

            return $arquivos;


        }else{
            return $this->listar_arquivo();
        }
    }

}