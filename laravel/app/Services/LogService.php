<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/02/2016
 * Time: 11:26
 */

namespace App\Services;


use App\Repositories\LogRepository;

class LogService
{
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * LogService constructor.
     */
    public function __construct(LogRepository $logRepository, AuxiliarService $auxiliarService)
    {
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function listar(){
        return $this->logRepository->listar();
    }

    public function buscar($data){
        if(!empty($data['inicio']) && !empty($data['fim'])) {
            $data['inicio'] = $this->auxiliarService->data_banco($data['inicio']);
            $data['fim'] = $this->auxiliarService->data_banco($data['fim']);
            return $this->logRepository->buscar($data);
        }else{
            return $this->listar();
        }
    }
}