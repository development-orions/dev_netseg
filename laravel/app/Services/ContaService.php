<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 10:28
 */

namespace App\Services;


use App\Repositories\ClienteRepository;
use App\Repositories\ContaRepository;
use App\Repositories\FornecedorRepository;
use App\Repositories\PagamentoRepository;
use App\Repositories\RecebimentoRepository;
use Illuminate\Support\Facades\Session;

class ContaService
{

    /**
     * ContaService constructor.
     */
    private $contaRepository;
    /**
     * @var PagamentoRepository
     */
    private $pagamentoRepository;
    /**
     * @var RecebimentoRepository
     */
    private $recebimentoRepository;
    /**
     * @var ClienteRepository
     */
    private $clienteRepository;
    /**
     * @var FornecedorRepository
     */
    private $fornecedorRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(ContaRepository $contaRepository, PagamentoRepository $pagamentoRepository, RecebimentoRepository $recebimentoRepository, ClienteRepository $clienteRepository, FornecedorRepository $fornecedorRepository, AuxiliarService $auxiliarService)
    {
        $this->contaRepository = $contaRepository;
        $this->pagamentoRepository = $pagamentoRepository;
        $this->recebimentoRepository = $recebimentoRepository;
        $this->clienteRepository = $clienteRepository;
        $this->fornecedorRepository = $fornecedorRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function cancelar($id){
        if(is_numeric($id)){
            return $this->contaRepository->cancelar($id);
        }
    }

    public function adicionar($id, $tipo){
        if(is_numeric($id)){
            if($tipo == 1) {
                $cliente = $this->clienteRepository->adicionar($id);
                Session::forget('id');
                Session::put('id', $cliente);
            }else {
                $fornecedor = $this->fornecedorRepository->adicionar($id);
                Session::forget('fornecedor');
                Session::put('fornecedor', $fornecedor);
            }
            return true;
        }
    }

    public function create($data){
        $data['valor'] = $this->auxiliarService->moeda($data['valor']);
        if(isset($data['valor2'])){
            $data['valor2'] = $this->auxiliarService->moeda($data['valor2']);
        }
        $resposta = array();
        try {
            $vencimento = $this->auxiliarService->data_banco($data['vencimento']);
            $operacoes = false;
            $erro = false;
            for($i = 0; $i <= (int) $data['replica']; $i++){
                $data['vencimento'] = date('Y-m-d', strtotime("+".$i." month",strtotime($vencimento)));
                if ($dados = $this->contaRepository->store($data)) {                    
                    if($data['tipo'] == 1){ //conta a receber
                        $operacoes = $this->recebimentoRepository->store($dados, $data);
                    }else{ // conta a pagar
                        $operacoes = $this->pagamentoRepository->store($dados, $data);
                    }                    
                }else {
                    $erro = true;
                    break;
                }              
            }
            if($erro){
                $resposta['msg'] = 'Erro C002 - Erro ao salvar esta conta, tente novamente.';
                $resposta['erro'] = true;                
            }else {
                if($operacoes){
                    Session::forget('id');
                    Session::forget('fornecedor');
                    $resposta['msg'] = 'Conta salva com sucesso.';
                    $resposta['erro'] = false;
                }else{
                    $this->contaRepository->delete($dados);
                    $resposta['msg'] = 'Erro C001 - Erro ao salvar esta conta, tente novamente.';
                    $resposta['erro'] = true;
                }
            }   
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($tipo, $id){
        if(is_numeric($id)){
            $conta = $this->contaRepository->buscarPorId($id);
            $conta->vencimento = $this->auxiliarService->converteData($conta->vencimento);
            if(isset($conta->baixa) && $conta->baixa != ''){
                $conta->baixa = $this->auxiliarService->converteData($conta->baixa);
            }
            $conta->valor = $this->auxiliarService->format($conta->valor);
            $conta->acrescimo = $this->auxiliarService->format($conta->acrescimo);
            $conta->desconto = $this->auxiliarService->format($conta->desconto);

            if ($tipo == 1) {
                if(!Session::has('id'))
                    Session::put('id', $conta->recebimentos[0]->clientes);
            } else {
                if(!Session::has('fornecedor'))
                    Session::put('fornecedor', $conta->pagamentos[0]->fornecedores);
            }

            return $conta;
        }
    }

    public function update($tipo, $id, $data){
        $data['valor'] = $this->auxiliarService->moeda($data['valor']);
        if(isset($data['valor2'])){
            $data['valor2'] = $this->auxiliarService->moeda($data['valor2']);
        }
        $resposta = array();
        try {
            if ($dados = $this->contaRepository->update($tipo, $id,$data)) {
                $operacoes = false;
                if($data['tipo'] == 1){ //conta a receber
                    $operacoes = $this->recebimentoRepository->update($dados->id, $data);
                }else{ // conta a pagar
                    $operacoes = $this->pagamentoRepository->update($dados->id, $data);
                }
                if($operacoes){
                    if(Session::has('id'))
                        Session::forget('id');
                    if(Session::has('fornecedor'))
                        Session::forget('fornecedor');
                    $resposta['msg'] = 'Conta editado com sucesso!';
                    $resposta['erro'] = false;
                }else{
                    $resposta['msg'] = 'Erro C003 - Erro ao salvar esta conta, tente novamente.';
                    $resposta['erro'] = true;
                }
            } else {
                $resposta['msg'] = 'Erro C004 - Erro ao tentar editar esta conta, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }



    public function listar($tipo){
        if(is_numeric($tipo)){
            $conta = $this->contaRepository->listar($tipo);
            foreach($conta as $contas){
                $contas->contas->vencimento = $this->auxiliarService->converteDataBrasil($contas->contas->vencimento);
                if(isset($contas->contas->baixa) && $contas->contas->baixa != ''){
                    $contas->contas->baixa = $this->auxiliarService->converteDataBrasil($contas->contas->baixa);
                }
                $contas->contas->valor = $this->auxiliarService->format($contas->contas->valor);
                $contas->contas->total = $this->auxiliarService->format($contas->contas->total);
            }
            return  $conta;

        }

    }


    public function buscar($tipo, $data){
        if(!empty($data['conta']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id']) || !empty($data['nf'])) {
            if($tipo == 1) {
                //receber
                $conta = $this->recebimentoRepository->buscar($data);
            }else {
                //pagar
                $conta = $this->pagamentoRepository->buscar($data);
            }
            foreach($conta as $contas){
                $contas->contas->vencimento = $this->auxiliarService->converteDataBrasil($contas->contas->vencimento);
                if(isset($contas->contas->baixa) && $contas->contas->baixa != ''){
                    $contas->contas->baixa = $this->auxiliarService->converteDataBrasil($contas->contas->baixa);
                }
                $contas->contas->valor = $this->auxiliarService->format($contas->contas->valor);
                $contas->contas->total = $this->auxiliarService->format($contas->contas->total);
            }
            return  $conta;

        }else{
            return $this->listar($tipo);
        }

    }

    public function conta_cronograma($tipo, $ano, $mes, $dia, $centrocusto){
        if($tipo == 1) {
            //receber
            $conta = $this->recebimentoRepository->conta_cronograma($ano, $mes, $dia, $centrocusto);
        }else {
            //pagar
            $conta = $this->pagamentoRepository->conta_cronograma($ano, $mes, $dia, $centrocusto);
        }
        foreach($conta as $ct){
            $ct->contas['total'] = $this->auxiliarService->format($ct->contas['total']);
        }
        return $conta;
    }

}