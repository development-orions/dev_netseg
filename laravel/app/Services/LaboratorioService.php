<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Services;


use App\Repositories\LogRepository;
use App\Repositories\LaboratorioRepository;
use App\Repositories\TecnicoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LaboratorioService
{

    /**
     * LaboratorioService constructor.
     */
    private $laboratorioRepository;
    private $tecnicoRepository;
    private $auxiliarService;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(LaboratorioRepository $laboratorioRepository, TecnicoRepository $tecnicoRepository, AuxiliarService $auxiliarService, LogRepository $logRepository)
    {
        $this->laboratorioRepository = $laboratorioRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->logRepository = $logRepository;
    }

    public function create($data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->laboratorioRepository->store($data)) {
                $resposta['msg'] = 'Laboratorio salvo com sucesso.';

                $this->logRepository->store("CADASTRAR LABORATORIO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            return $this->laboratorioRepository->buscarPorId($id);
        }
    }

    public function update($id, $data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->laboratorioRepository->update($id,$data)) {
                $resposta['msg'] = 'Laboratorio editado com sucesso!';

                $this->logRepository->store("EDITAR LABORATORIO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar(){
        return $this->laboratorioRepository->listar();
    }

    public function listar_drop(){
        return $this->laboratorioRepository->listar_drop();
    }

    public function adicionar($laboratorio){
        $resposta = array();
        $laboratorioobj = $this->laboratorioRepository->buscarPorId(\Illuminate\Support\Facades\Request::get('laboratorio_id'));
        $laboratorio['laboratorio_id'] = $laboratorioobj;


        if(Session::has('laboratorio')) {
            $sv = Session::get('laboratorio');
            $sv[] = $laboratorio;
            Session::put('laboratorio', $sv);

        }else{
            $serv[0] = $laboratorio;

            Session::put('laboratorio', $serv);
        }

        $resposta['msg'] = 'Laboratorio adicionado com sucesso!';
        $resposta['erro'] = false;

        return $resposta;

    }

    public function deletar($id)
    {
        if(is_numeric($id)){
            $resposta = array();

            $data = $this->laboratorioRepository->buscarPorId($id);
            try {
                if ($this->laboratorioRepository->remove($id)) {

                    $this->logRepository->store("EXCLUIR LABORATORIO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                    $resposta['msg'] = 'Laboratório deletado com sucesso.';

                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao deletar Laboratório.';
                    $resposta['erro'] = true;
                }

            }catch (\Exception $e){
                $resposta['msg'] = $e->getMessage();
                $resposta['erro'] = true;
            };

            return $resposta;
        }
        return false;
    }
}