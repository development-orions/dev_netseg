<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 30/11/2015
 * Time: 09:00
 */

namespace app\Services;


use App\Repositories\LogRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class UserService
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(UserRepository $userRepository, LogRepository $logRepository)
    {
        $this->userRepository = $userRepository;
        $this->logRepository = $logRepository;
    }

    public function listar()
    {
        return $this->userRepository->listar();
    }

    public function buscar($texto)
    {
        if(!empty($texto['q']))
            return $this->userRepository->buscar($texto);
        else
            return $this->listar();

    }

    public function buscarPorId($id)
    {
        if (is_numeric($id)) {
            return $this->userRepository->buscarPorId($id);
        }
    }

    public function create($data){
        $data['statu_id'] = 1;
        $resposta = array();
        try {
            if ($this->userRepository->store($data)) {
                $resposta['msg'] = 'Fornecedor salvo com sucesso.';

                $this->logRepository->store("CADASTRAR CLIENTE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email']." - NOME DE USUARIO: ".$data['username']." - STATUS: ".$data['statu_id']." - NÍVEL DE USUÁRIO: ".$data['role_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o fornecedor, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function usermigrate(){

            $this->userRepository->usermigrate();
    }

    public function update($id, $data)
    {
        $resposta = array();
        try {
            if ($this->userRepository->update($id, $data)) {
                $resposta['msg'] = 'Usuário editado com sucesso!';

                $this->logRepository->store("CADASTRAR USUÁRIO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email']." - NÍVEL DE USUÁRIO: ".$data['role_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este usuário, tente novamente.';
                $resposta['erro'] = true;
            }
        } catch (\Exception $e) {
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function resetpassword($id, $data)
    {
        $resposta = array();
        try {
            if ($this->userRepository->resetpassaword($id, $data)) {
                $resposta['msg'] = 'Senha resetada com sucesso!';

                $this->logRepository->store("EDITAR USUÁRIO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email']." - NOME DE USUARIO: ".$data['username']." - STATUS: ".$data['statu_id']." - NÍVEL DE USUÁRIO: ".$data['role_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar resetar essa senha, tente novamente.';
                $resposta['erro'] = true;
            }
        } catch (\Exception $e) {
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function ativar($id)
    {
        if(is_numeric($id)){
            return $this->userRepository->ativar($id);
        }
    }

    public function inativar($id)
    {
        if(is_numeric($id)){
            return $this->userRepository->inativar($id);
        }
    }
}