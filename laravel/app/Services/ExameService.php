<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Services;


use App\Repositories\LogRepository;
use App\Repositories\ExameRepository;
use App\Repositories\TecnicoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ExameService
{

    /**
     * ExameService constructor.
     */
    private $exameRepository;
    private $tecnicoRepository;
    private $auxiliarService;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(ExameRepository $exameRepository, TecnicoRepository $tecnicoRepository, AuxiliarService $auxiliarService, LogRepository $logRepository)
    {
        $this->exameRepository = $exameRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->logRepository = $logRepository;
    }

    public function create($data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->exameRepository->store($data)) {
                $resposta['msg'] = 'Exame salvo com sucesso.';

                $this->logRepository->store("CADASTRAR EXAME - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            return $this->exameRepository->buscarPorId($id);
        }
    }

    public function update($id, $data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->exameRepository->update($id,$data)) {
                $resposta['msg'] = 'Exame editado com sucesso!';

                $this->logRepository->store("EDITAR EXAME - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar(){
        return $this->exameRepository->listar();
    }

    public function adicionar($exame){
        $resposta = array();
        $exameobj = $this->exameRepository->buscarPorId(\Illuminate\Support\Facades\Request::get('exame_id'));
        $exame['exame_id'] = $exameobj;


        if(Session::has('exame')) {
            $sv = Session::get('exame');
            $sv[] = $exame;
            Session::put('exame', $sv);

        }else{
            $serv[0] = $exame;

            Session::put('exame', $serv);
        }

        $resposta['msg'] = 'Exame adicionado com sucesso!';
        $resposta['erro'] = false;

        return $resposta;

    }

    public function deletar($id)
    {
        if(is_numeric($id)){
            $resposta = array();

            $data = $this->exameRepository->buscarPorId($id);
            try {
                if ($this->exameRepository->remove($id)) {

                    $this->logRepository->store("EXCLUIR EXAME - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                    $resposta['msg'] = 'Exame deletado com sucesso.';

                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao deletar Exame.';
                    $resposta['erro'] = true;
                }

            }catch (\Exception $e){
                $resposta['msg'] = $e->getMessage();
                $resposta['erro'] = true;
            };

            return $resposta;
        }
        return false;
    }
}