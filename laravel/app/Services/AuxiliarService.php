<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 15/12/2015
 * Time: 15:26
 */

namespace App\Services;

use DateTime;

class AuxiliarService
{

    protected static $_transliteration = array(
        '/ä|æ|ǽ/' => 'ae',
        '/ö|œ/' => 'oe',
        '/ü/' => 'ue',
        '/Ä/' => 'Ae',
        '/Ü/' => 'Ue',
        '/Ö/' => 'Oe',
        '/À|Á|Â|Ã|Å|Ǻ|Ā|Ă|Ą|Ǎ/' => 'A',
        '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª/' => 'a',
        '/Ç|Ć|Ĉ|Ċ|Č/' => 'C',
        '/ç|ć|ĉ|ċ|č/' => 'c',
        '/Ð|Ď|Đ/' => 'D',
        '/ð|ď|đ/' => 'd',
        '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/' => 'E',
        '/è|é|ê|ë|ē|ĕ|ė|ę|ě/' => 'e',
        '/Ĝ|Ğ|Ġ|Ģ/' => 'G',
        '/ĝ|ğ|ġ|ģ/' => 'g',
        '/Ĥ|Ħ/' => 'H',
        '/ĥ|ħ/' => 'h',
        '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ/' => 'I',
        '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı/' => 'i',
        '/Ĵ/' => 'J',
        '/ĵ/' => 'j',
        '/Ķ/' => 'K',
        '/ķ/' => 'k',
        '/Ĺ|Ļ|Ľ|Ŀ|Ł/' => 'L',
        '/ĺ|ļ|ľ|ŀ|ł/' => 'l',
        '/Ñ|Ń|Ņ|Ň/' => 'N',
        '/ñ|ń|ņ|ň|ŉ/' => 'n',
        '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ/' => 'O',
        '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º/' => 'o',
        '/Ŕ|Ŗ|Ř/' => 'R',
        '/ŕ|ŗ|ř/' => 'r',
        '/Ś|Ŝ|Ş|Š/' => 'S',
        '/ś|ŝ|ş|š|ſ/' => 's',
        '/Ţ|Ť|Ŧ/' => 'T',
        '/ţ|ť|ŧ/' => 't',
        '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ/' => 'U',
        '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ/' => 'u',
        '/Ý|Ÿ|Ŷ/' => 'Y',
        '/ý|ÿ|ŷ/' => 'y',
        '/Ŵ/' => 'W',
        '/ŵ/' => 'w',
        '/Ź|Ż|Ž/' => 'Z',
        '/ź|ż|ž/' => 'z',
        '/Æ|Ǽ/' => 'AE',
        '/ß/' => 'ss',
        '/Ĳ/' => 'IJ',
        '/ĳ/' => 'ij',
        '/Œ/' => 'OE',
        '/ƒ/' => 'f'
    );


    public function converteData($data){
        /*
         * ENTRADA: aaaa-mm-dd
         * SAÍDA: dd Mês, aaaa
         *
         * */

        $data = explode('-', $data);
        $ano = $data[0];
        $dia = $data[2];

        $meses = array('01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');

        $mes = $meses[$data[1]];
        $dataformatada = $dia.' '.$mes.', '.$ano;

        return $dataformatada;
    }

    function calc_idade($data_nasc) {

        $data_nasc = new DateTime($data_nasc);

        $data_nasc = $data_nasc->format('d/m/Y');

        $data_nasc=explode('/',$data_nasc);

        $data=date('d/m/Y');

        $data=explode('/',$data);

        if($data_nasc[2] == '0000'){
            return " ";
        }

        $anos=$data[2]-$data_nasc[2];

        if($data_nasc[1] > $data[1])

            return $anos-1;

        if($data_nasc[1] == $data[1])
            if($data_nasc[0] <= $data[0]) {
                return $anos;
            }
            else{
                return $anos-1;
            }

        if ($data_nasc[1] < $data[1])
            return $anos;
    }

    public function converteDataBrasil($data){
        /*
         * ENTRADA: aaaa-mm-dd
         * SAÍDA: dd/mm/aaaa
         *
         * */

        $data = explode('-', $data);
        $ano = $data[0];
        $dia = $data[2];
        $mes = $data[1];

        $dataformatada = $dia.'/'.$mes.'/'.$ano;

        return $dataformatada;
    }

    public function converteDataBrasil2($data){
        /*
         * ENTRADA: aaaa-mm-dd
         * SAÍDA: dd-mm-aaaa
         *
         * */

        $data = explode('-', $data);
        $ano = $data[0];
        $dia = $data[2];
        $mes = $data[1];

        $dataformatada = $dia.'-'.$mes.'-'.$ano;

        return $dataformatada;
    }

    public function format($valor){
        return number_format($valor, 2, ",", ".");
    }

    public function formatRs($valor)
    {
        return "R$ ".number_format($valor, 2, ",", ".");
    }

    public function porcentagem($valor){
        return number_format($valor, 2, ",", ".");
    }

    public function data_banco($data){
        /*
         * ENTRADA: dd Mês, aaaa
         * SAÍDA: aaaa-mm-dd
         *
         * */

        $vencimento = explode(' ', $data);
        $dia  = $vencimento[0];
        $ano = $vencimento[2];
        $vencimento = explode(',', $vencimento[1]);

        $meses = array('Janeiro' => '01', 'Fevereiro' => '02', 'Março' => '03', 'Abril' => '04', 'Maio' => '05', 'Junho' => '06', 'Julho' => '07', 'Agosto' => '08', 'Setembro' => '09', 'Outubro' => '10', 'Novembro' => '11', 'Dezembro' => '12');
        $mes = $meses[$vencimento[0]];
        $dataformatada = $ano.'-'.$mes.'-'.$dia;

        return $dataformatada;
    }

    public function converteDataBanco($data){
        /*
         * ENTRADA: dd-mm-aaaa
         * SAÍDA: aaaa-mm-dd
         * */

        $data = explode('-', $data);
        $dataformatada = $data[2].'-'.$data[1].'-'.$data[0];

        return $dataformatada;
    }

    public function converteDataBancocomSlash($data){
        /*
         * ENTRADA: dd/mm/aaaa
         * SAÍDA: aaaa-mm-dd
         * */

        $data = explode('/', $data);
        $dataformatada = $data[2].'-'.$data[1].'-'.$data[0];

        return $dataformatada;
    }

    public function moeda($get_valor) {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        return $valor; //retorna o valor formatado para gravar no banco
    }

    public function comparaData($data){

        $now = date('Y/m/d');
        $time_inicial = strtotime($data);
        $time_final = strtotime($now);

        if($time_final > $time_inicial){
            return false;
        }else{
            return true;
        }
    }

    public static function slug($string, $replacement = '_') {
        $quotedReplacement = preg_quote($replacement, '/');

        $merge = array(
            '/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
            '/\\s+/' => $replacement,
            sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
        );

        $map = self::$_transliteration + $merge;
        return preg_replace(array_keys($map), array_values($map), $string);
    }
}