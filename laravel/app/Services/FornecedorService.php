<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 12/11/2015
 * Time: 10:15
 */

namespace App\Services;


use App\Repositories\FornecedorRepository;
use App\Repositories\LogRepository;
use App\Services\AuxiliarService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FornecedorService {

    /**
     * @var FornecedorRepository
     */
    private $fornecedorRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    private $auxiliarService;

    public function __construct(FornecedorRepository $fornecedorRepository, LogRepository $logRepository, AuxiliarService $auxiliarService){

        $this->fornecedorRepository = $fornecedorRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function create($data){
        $data['statu_id'] = 1;
        $resposta = array();
        if(!empty($data['cnpj'])) {
            if (!$this->fornecedorRepository->verificaCnpj($data['cnpj'])) {
                $resposta['msg'] = 'CNPJ já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        if(!empty($data['cpf'])) {
            if (!$this->fornecedorRepository->verificaCpf($data['cpf'])) {
                $resposta['msg'] = 'CPF já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        if(!$this->fornecedorRepository->verificaRazaosocial($data['razaosocial'])) {
            $resposta['msg'] = 'Razão Social já cadastrada no sistema.';
            $resposta['erro'] = true;
            return $resposta;
        }
        try {
            if ($this->fornecedorRepository->store($data)) {
                $resposta['msg'] = 'Fornecedor salvo com sucesso.';

                $this->logRepository->store("CADASTRAR FORNECEDOR - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - CPF: ".$data['cpf']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - TELEFONE: ".$data['telefone_responsavel']." - CEI/AUTONOMO: ".$data['cei_autonomo']." - STATUS: ".$data['statu_id']." - DATA DE INIVIO DE SERVIÇO: ".$data['datainicio_servico']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o fornecedor, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function create_para_funcoes($data){
        $data['statu_id'] = 1;
        $resposta = array();

        if(!empty($data['cnpj'])) {
            if (!$this->fornecedorRepository->verificaCnpj($data['cnpj'])) {
                $resposta['msg'] = 'CNPJ já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        if(!empty($data['cpf'])) {
            if (!$this->fornecedorRepository->verificaCpf($data['cpf'])) {
                $resposta['msg'] = 'CPF já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        if(!$this->fornecedorRepository->verificaRazaosocial($data['razaosocial'])) {
            $resposta['msg'] = 'Razão Social já cadastrada no sistema.';
            $resposta['erro'] = true;
            return $resposta;
        }
        try {
            if ($fornecedor = $this->fornecedorRepository->store_para_funcoes($data)) {
                Session::forget('fornecedor');
                Session::put('fornecedor', $fornecedor);
                $resposta['msg'] = 'Fornecedor salvo com sucesso.';

                $this->logRepository->store("CADASTRAR FORNECEDOR - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - CPF: ".$data['cpf']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - TELEFONE: ".$data['telefone_responsavel']." - CEI/AUTONOMO: ".$data['cei_autonomo']." - STATUS: ".$data['statu_id']." - DATA DE INIVIO DE SERVIÇO: ".$data['datainicio_servico']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o fornecedor, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar($status){
        if(is_numeric($status)){
            return $this->fornecedorRepository->listar($status);

        }

    }
    public function buscar($status, $texto){
        if(is_numeric($status)){
            if(!empty($texto['q']))
                return $this->fornecedorRepository->buscar($status, $texto);
            else
                return $this->listar($status);
        }
    }



    public function format($valor){
        return number_format($valor, 2, ",", ".");
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function buscarPorId($id){
        if(is_numeric($id)){
            $fornecedor = $this->fornecedorRepository->buscarPorId($id);
            $fornecedor->datainicio_servico = $this->auxiliarService->converteData($fornecedor->datainicio_servico);
            return $fornecedor;
        }
    }

    public function update($id, $data){
        $resposta = array();
        if(!empty($data['cnpj'])) {
            if (!$this->fornecedorRepository->verificaCnpjEdicao($id, $data['cnpj'])) {
                $resposta['msg'] = 'CNPJ já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        try {
            if ($this->fornecedorRepository->update($id,$data)) {
                $resposta['msg'] = 'Fornecedor editado com sucesso!';

                $this->logRepository->store("EDITAR FORNECEDOR - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - CPF: ".$data['cpf']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - TELEFONE: ".$data['telefone_responsavel']." - CEI/AUTONOMO: ".$data['cei_autonomo']." - STATUS: ".$data['statu_id']." - DATA DE INICIO DE SERVIÇO: ".$data['datainicio_servico']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este fornecedor, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function ativar($id)
    {
        if(is_numeric($id)){
            return $this->fornecedorRepository->ativar($id);
        }
    }

    public function inativar($id)
    {
        if(is_numeric($id)){
            return $this->fornecedorRepository->inativar($id);
        }
    }



}