<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 16/12/2015
 * Time: 23:51
 */

namespace App\Services;


use Illuminate\Support\Facades\Mail;

class MailService
{
    public function emailOrcamento($template, $orcamento, $configuracoes, $destinatarios, $user){
        $dados = array('configuracoes' => $configuracoes, 'orcamento' => serialize($orcamento));
        $destinatarios = array_values($destinatarios);
        Mail::queue('mail.'.$template, $dados, function($message) use($user, $destinatarios)
        {
            $message
                ->subject('Orçamento Netseg')
                ->to($destinatarios)
                ->replyTo($user);
                //->cc('');
        });
    }
}