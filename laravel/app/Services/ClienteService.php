<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 14:37
 */

namespace App\Services;


use App\Repositories\ClienteRepository;
use App\Repositories\LogRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ClienteService
{
    protected $clientes;
    /**
     * @var ClienteRepository
     */
    private $clienteRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(ClienteRepository $clienteRepository, LogRepository $logRepository, AuxiliarService $auxiliarService){

        $this->clienteRepository = $clienteRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function create($data){
        $data['statu_id'] = 1;
        if($data['valor_contrato'] != null) {
            $data['valor_contrato'] = $this->auxiliarService->moeda($data['valor_contrato']);
        }

        for($x=0; $x<3; $x++){
            if($data['valor_tecnico'][$x] != null) {
                $data['valor_tecnico'][$x] = $this->auxiliarService->moeda($data['valor_tecnico'][$x]);
            }
        }


        $resposta = array();
        try {
            if ($this->clienteRepository->store($data)) {
                $resposta['msg'] = 'Cliente salvo com sucesso.';

                $this->logRepository->store("CADASTRAR CLIENTE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - FORMA JURIDICA: ".$data['formajuridica']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - TELEFONE: ".$data['telefone_responsavel']." - STATUS: ".$data['statu_id']." - VALOR CONTRATO: ".$data['valor_contrato']." - DATA DE CONTRATO: ".$data['data_contrato']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o cliente, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function create_para_funcoes($data){
        $data['statu_id'] = 1;
        if($data['valor_contrato'] != null) {
            $data['valor_contrato'] = $this->auxiliarService->moeda($data['valor_contrato']);
        }
        for($x=0; $x<3; $x++){
            if($data['valor_tecnico'][$x] != null) {
                $data['valor_tecnico'][$x] = $this->auxiliarService->moeda($data['valor_tecnico'][$x]);
            }
        }
        $resposta = array();
        try {
            if ($cliente = $this->clienteRepository->store_para_funcoes($data)) {
                Session::forget('id');
                Session::put('id', $cliente);
                Session::forget('cliente_orcamento');
                Session::put('cliente_orcamento', $cliente);
                $resposta['msg'] = 'Cliente salvo com sucesso.';
                $this->logRepository->store("CADASTRAR CLIENTE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - FORMA JURIDICA: ".$data['formajuridica']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - TELEFONE: ".$data['telefone_responsavel']." - STATUS: ".$data['statu_id']." - VALOR CONTRATO: ".$data['valor_contrato']." - DATA DE CONTRATO: ".$data['data_contrato']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o cliente, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }


    public function buscar($status, $texto){
        if(is_numeric($status)){
            if(!empty($texto['q']))
                return $this->clienteRepository->buscar($status, $texto);
            else
                return $this->listar($status);
        }
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $cliente = $this->clienteRepository->buscarPorId($id);

            if(isset($cliente->data_contrato)) {
                $cliente->data_contrato = $this->auxiliarService->converteData($cliente->data_contrato);
            }
            $cliente->valor_contrato = $this->format($cliente->valor_contrato);
            if(isset($cliente->valor_tecnico)) {
                $cliente->valor_tecnico = $this->format($cliente->valor_tecnico);
            }
            return $cliente;
        }
    }

    public function update($id, $data){
        if($data['valor_contrato'] != null) {
            $data['valor_contrato'] = $this->auxiliarService->moeda($data['valor_contrato']);
        }
        for($x=0; $x<3; $x++){
            if($data['valor_tecnico'][$x] != null) {
                $data['valor_tecnico'][$x] = $this->auxiliarService->moeda($data['valor_tecnico'][$x]);
            }
        }
        $user = Auth::user()->nome;
        $resposta = array();
        try {
            if ($this->clienteRepository->update($id,$data)) {
                $resposta['msg'] = 'Cliente editado com sucesso!';

                $this->logRepository->store("EDITAR CLIENTE - USUARIO: ".$user." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - FORMA JURIDICA: ".$data['formajuridica']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - STATUS: ".$data['statu_id']." - VALOR CONTRATO: ".$data['valor_contrato']." - DATA DE CONTRATO: ".$data['data_contrato']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este cliente, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function converteData($data){
        $data = explode('-', $data);
        $ano = $data[0];
        $dia = $data[2];

        $meses = array(01 => 'Janeiro', 02 => 'Fevereiro', 03 => 'Março', 04 => 'Abril', 05 => 'Maio', 06 => 'Junho', 07 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro');

        $mes = $meses[$data[1]];
        $dataformatada = $dia.' '.$mes.', '.$ano;

        return $dataformatada;
    }

    public function converteDataBrasil($data){
        $data = explode('-', $data);
        $ano = $data[0];
        $dia = $data[2];
        $mes = $data[1];

        $dataformatada = $dia.'/'.$mes.'/'.$ano;

        return $dataformatada;
    }

    public function format($valor){
        return number_format($valor, 2, ",", ".");
    }

    public function listar($status){
        if(is_numeric($status)){
            return $this->clienteRepository->listar($status);

        }
    }

    public function inativar($id){
        if(is_numeric($id)){
            return $this->clienteRepository->inativar($id);
        }
    }
    public function ativar($id){
        if(is_numeric($id)){
            return $this->clienteRepository->ativar($id);
        }
    }

    public function listarBusca(){
        return $this->clienteRepository->listarBusca();
    }

    public function listar_drop($status,$term,$page){
        if(is_numeric($status)){
            return $this->clienteRepository->listar_drop($status,$term,$page);
        }
    }

}