<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 26/02/2016
 * Time: 11:05
 */

namespace App\Services;


use App\Repositories\ClientestecnicoRepository;

class ClientestecnicoService
{
    /**
     * @var ClientestecnicoRepository
     */
    private $clientestecnicoRepository;

    /**
     * ClientestecnicoService constructor.
     */
    public function __construct(ClientestecnicoRepository $clientestecnicoRepository)
    {
        $this->clientestecnicoRepository = $clientestecnicoRepository;
    }

    public function buscaPorClienteId($id){
        $tecnicos = $this->clientestecnicoRepository->buscaPorClienteId($id);

       /* foreach($tecnicos as $tec){
            $tec->valor_tecnico = $this->format($tec->valor_tecnico);
        }*/

        return $tecnicos;
    }
}