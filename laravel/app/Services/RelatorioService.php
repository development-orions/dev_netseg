<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 15/02/2016
 * Time: 07:46
 */

namespace App\Services;


use App\Repositories\ContaRepository;
use App\Repositories\MedicosvendaRepository;
use App\Repositories\TecnicoRepository;
use App\Repositories\TecnicosvendaRepository;
use Illuminate\Support\Facades\Session;

class RelatorioService
{
    /**
     * @var ContaRepository
     */
    private $contaRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var TecnicoRepository
     */
    private $tecnicoRepository;
    /**
     * @var MedicosvendaRepository
     */
    private $medicosvendaRepository;
    /**
     * @var TecnicosvendaRepository
     */
    private $tecnicosvendaRepository;

    /**
     * RelatorioService constructor.
     */
    public function __construct(ContaRepository $contaRepository, AuxiliarService $auxiliarService, TecnicoRepository $tecnicoRepository, MedicosvendaRepository $medicosvendaRepository, TecnicosvendaRepository $tecnicosvendaRepository)
    {
        $this->contaRepository = $contaRepository;
        $this->auxiliarService = $auxiliarService;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->medicosvendaRepository = $medicosvendaRepository;
        $this->tecnicosvendaRepository = $tecnicosvendaRepository;
    }

    public function conta($tipo, $data){
        if(!empty($data['conta']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id']) || !empty($data['centrocusto_id'])) {

            if(!empty($data['inicio'])){
                $data['inicio'] = $this->auxiliarService->data_banco($data['inicio']);
            }
            if(!empty($data['fim'])){
                $data['fim'] = $this->auxiliarService->data_banco($data['fim']);
            }

            if($tipo == 1) {
                $resultado = $this->contaRepository->relatorio_conta_pagar($data);
            }else {
                $resultado = $this->contaRepository->relatorio_conta_receber($data);
            }

            foreach($resultado as $contas){
                $contas->contas->vencimento = $this->auxiliarService->converteDataBrasil($contas->contas->vencimento);
                if(isset($contas->contas->baixa) && $contas->contas->baixa != ''){
                    $contas->contas->baixa = $this->auxiliarService->converteDataBrasil($contas->contas->baixa);
                }
                $contas->contas->valor = $this->auxiliarService->format($contas->contas->valor);
                //$contas->contas->total = $this->auxiliarService->format($contas->contas->total);
            }
            return $resultado;
        }else{
            return $this->listar($tipo);
        }
    }

    public function listar($tipo){
        if(is_numeric($tipo)){
            $conta = $this->contaRepository->listar_relatorio($tipo);
            foreach($conta as $contas){
                $contas->contas->vencimento = $this->auxiliarService->converteDataBrasil($contas->contas->vencimento);
                if(isset($contas->contas->baixa) && $contas->contas->baixa != ''){
                    $contas->contas->baixa = $this->auxiliarService->converteDataBrasil($contas->contas->baixa);
                }
                $contas->contas->valor = $this->auxiliarService->format($contas->contas->valor);
                //$contas->contas->total = $this->auxiliarService->format($contas->contas->total);
            }
            return  $conta;
        }

    }

    public function pagamentotecnico($data){
        $resultado = $this->tecnicoRepository->relatorio_pagamento($data);

        $total_contrato = 0;
        $total_tecnico = 0;

        foreach($resultado as $cliente){
            $total_contrato += $cliente->valor_contrato;
            $total_tecnico += $cliente->valor_tecnico;
            $cliente->total = $this->auxiliarService->porcentagem($cliente->total);
            $cliente->valor_contrato = $this->auxiliarService->format($cliente->valor_contrato);
            $cliente->valor_tecnico = $this->auxiliarService->format($cliente->valor_tecnico);
        }

        if($total_contrato != 0) {
            $total_p = ($total_tecnico / $total_contrato) * 100;
        }else{
            $total_p = 0;
        }

        $total_contrato = $this->auxiliarService->format($total_contrato);
        $total_tecnico = $this->auxiliarService->format($total_tecnico);
        $total_p = $this->auxiliarService->porcentagem($total_p);


        Session::put('total_contrato', $total_contrato);
        Session::put('total_tecnico', $total_tecnico);
        Session::put('total_p', $total_p);

        return $resultado;
    }

    public function relatorio_vendas($tipo, $data){
        $data['inicio'] = $this->auxiliarService->data_banco($data['inicio']);
        $data['fim'] = $this->auxiliarService->data_banco($data['fim']);
        if($tipo == 1){
            $resultado = $this->medicosvendaRepository->relatorio_venda($data);
        }else{
            $resultado = $this->tecnicosvendaRepository->relatorio_venda($data);
        }

        if(sizeof($resultado) != 0) {
            $resultado->total = 0;
            $resultado->totalporcentagem = 0;
            $resultado->totaladm = 0;
            $resultado->totalgeral = 0;
            $resultado->totaladmporcentagem = 0;
            foreach ($resultado as $venda) {
                $resultado->totalgeral += $venda->valor;
                $resultado->total += $venda->valor_medico;
                $resultado->totaladm += $venda->valoradministrativo;

                $venda->adm = ($venda->valoradministrativo / $venda->valor) * 100;
                $venda->adm = $this->auxiliarService->porcentagem($venda->adm);
                $venda->valoradministrativo = $this->auxiliarService->format($venda->valoradministrativo);
                $venda->total = $this->auxiliarService->porcentagem($venda->total);
                $venda->valor_medico = $this->auxiliarService->format($venda->valor_medico);
                $venda->valor = $this->auxiliarService->format($venda->valor);
            }

            $resultado->totalporcentagem = ($resultado->total / $resultado->totalgeral) * 100;
            $resultado->totalporcentagem = $this->auxiliarService->porcentagem($resultado->totalporcentagem);

            $resultado->totaladmporcentagem = ($resultado->totaladm / $resultado->totalgeral) * 100;
            $resultado->totaladmporcentagem = $this->auxiliarService->porcentagem($resultado->totaladmporcentagem);

            $resultado->totalgeral = $this->auxiliarService->format($resultado->totalgeral);
            $resultado->total = $this->auxiliarService->format($resultado->total);
            $resultado->totaladm = $this->auxiliarService->format($resultado->totaladm);
        }

        return $resultado;
    }

}