<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 08:42
 */

namespace App\Services;

use App\Cliente;
use App\Cronograma;
use App\Naoconformidade;
use App\Tecnico;
use App\Repositories\ClienteRepository;
use App\Repositories\LogRepository;
use App\Repositories\NaoconformidadeRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class NaoconformidadeService
{
    private $naoconformidadeRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var ClienteRepository
     */
    private $clienteRepository;

    /**
     * NaoconformidadeService constructor.
     * @param NaoconformidadeRepository $naoconformidadeRepository
     * @param LogRepository $logRepository
     * @param AuxiliarService $auxiliarService
     * @param ClienteRepository $clienteRepository
     */
    public function __construct(NaoconformidadeRepository $naoconformidadeRepository, LogRepository $logRepository, AuxiliarService $auxiliarService, ClienteRepository $clienteRepository)
    {
        $this->naoconformidadeRepository = $naoconformidadeRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
        $this->clienteRepository = $clienteRepository;
    }

    public function create($data){
        $resposta = array();
        $cliente_id = Cliente::find($data['cliente_id']);
        $tecnico_id = Tecnico::find($data['tecnico_id']);
        $cronograma_id = Cronograma::find($data['cronograma_id']);
        $realpotencial = $data['realpotencial'] == 1?'Não Conformidade Real':'Não Conformidade Potencial';
        $materialsistema = $data['materialsistema'] == 1?'Material / Processo':'Sistema / Serviço';
        $foiimplementada = $data['foiimplementada'] == 1?'Ação foi Implementada':'Ação NÃO foi Implementada';
        $causaprovavel_op = $data['causaprovavel_op'] == 1?'Relatório improcedente':'Relatório procedente';
        $eeficaz = $data['eeficaz'] == 1?'É eficaz':'Não é eficaz';
        $i = (DB::table('naoconformidades')->where('ano', '=', date("Y"))->count())+1;
        $numero = (($i >9) ? '' : '0').$i."/".date("Y");

        try {
            if ($this->naoconformidadeRepository->store($data)) {
                $resposta['msg'] = 'Não Conformidade lançada com sucesso.';

                $this->logRepository->store("LANCAR NÃO CONFORMIDADE - USUARIO: ".Auth::user()->nome." - CÓDIGO: ".$data['codigo']." - REVISÃO: ".$data['revisao']." -  VIGÊNCIA: ".$data['vigencia']." - NÚMERO: ".$numero." - ".$realpotencial." - ".$materialsistema." - OCORRÊNCIA: ".$data['ocorrencia']." - RESPONSÁVEL PELA OCORRÊNCIA: ".$data['ocorrencia_responsavel']." - DATA DA OCORRÊNCIA: ".$data['ocorrencia_data']." - AÇÃO IMEDIATA: ".$data['acaoimediata']." - RESPOSÁVEL PELA AÇÃO IMEDIATA: ".$data['acaoimediata_responsavel']." - DATA DA AÇÃO IMEDIATA: ".$data['acaoimediata_data']." - CAUSA PROVÁVEL: ".$data['causaprovavel']." - RESPONSÁVEL PELA CAUSA PROVÁVEL: ".$data['causaprovavel_responsavel']." - DATA DA CAUSA PROVÁVEL: ".$data['causaprovavel_data']." - AÇÃO CORRETIVA: ".$data['acaocorretiva']." - RESPONSÁVEL PELA AÇÃO CORRETIVA: ".$data['acaocorretiva_responsavel']." - PRAZO PARA AÇÃO CORRETIVA: ".$data['acaocorretiva_prazo']." - ".$foiimplementada." - ".$causaprovavel_op." - ".$eeficaz." - OBSERVAÇÕES DA EFICÁCIA: ".$data['acaocorretiva_prazo']." - RESPONSÁVEL PELA EFICÁCIA: ".$data['eficaz_responsavel'] ." - DATA DA EFICÁCIA: ".$data['eficaz_data']." - CLIENTE: ".$cliente_id->razaosocial." - FORNECEDOR: ".$tecnico_id->nome." - CRONOGRAMA: ".$cronograma_id->id."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao lançar a Não Conformidade, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function update($id, $data){
        $cliente_id = Cliente::find($data['cliente_id']);
        $tecnico_id = Tecnico::find($data['tecnico_id']);
        $cronograma_id = Cronograma::find($data['cronograma_id']);
        $realpotencial = $data['realpotencial'] == 1?'Não Conformidade Real':'Não Conformidade Potencial';
        $materialsistema = $data['materialsistema'] == 1?'Material / Processo':'Sistema / Serviço';
        $foiimplementada = $data['foiimplementada'] == 1?'Ação foi Implementada':'Ação NÃO foi Implementada';
        $causaprovavel_op = $data['causaprovavel_op'] == 1?'Relatório improcedente':'Relatório procedente';
        $eeficaz = $data['eeficaz'] == 1?'É eficaz':'Não é eficaz';
        $i = (DB::table('naoconformidades')->where('ano', '=', date("Y"))->count())+1;
        $numero = (($i >9) ? '' : '0').$i."/".date("Y");
        $resposta = array();
        try {
            if ($this->naoconformidadeRepository->update($id,$data)) {
                $resposta['msg'] = 'Naoconformidade editado com sucesso!';

                $this->logRepository->store("LANCAR NÃO CONFORMIDADE - USUARIO: ".Auth::user()->nome." - CÓDIGO: ".$data['codigo']." - REVISÃO: ".$data['revisao']." -  VIGÊNCIA: ".$data['vigencia']." - NÚMERO: ".$numero." - ".$realpotencial." - ".$materialsistema." - OCORRÊNCIA: ".$data['ocorrencia']." - RESPONSÁVEL PELA OCORRÊNCIA: ".$data['ocorrencia_responsavel']." - DATA DA OCORRÊNCIA: ".$data['ocorrencia_data']." - AÇÃO IMEDIATA: ".$data['acaoimediata']." - RESPOSÁVEL PELA AÇÃO IMEDIATA: ".$data['acaoimediata_responsavel']." - DATA DA AÇÃO IMEDIATA: ".$data['acaoimediata_data']." - CAUSA PROVÁVEL: ".$data['causaprovavel']." - RESPONSÁVEL PELA CAUSA PROVÁVEL: ".$data['causaprovavel_responsavel']." - DATA DA CAUSA PROVÁVEL: ".$data['causaprovavel_data']." - AÇÃO CORRETIVA: ".$data['acaocorretiva']." - RESPONSÁVEL PELA AÇÃO CORRETIVA: ".$data['acaocorretiva_responsavel']." - PRAZO PARA AÇÃO CORRETIVA: ".$data['acaocorretiva_prazo']." - ".$foiimplementada." - ".$causaprovavel_op." - ".$eeficaz." - OBSERVAÇÕES DA EFICÁCIA: ".$data['acaocorretiva_prazo']." - RESPONSÁVEL PELA EFICÁCIA: ".$data['eficaz_responsavel'] ." - DATA DA EFICÁCIA: ".$data['eficaz_data']." - CLIENTE: ".$cliente_id->razaosocial." - FORNECEDOR: ".$tecnico_id->nome." - CRONOGRAMA: ".$cronograma_id->id."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar esta não conformidade, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar(){
        return $this->naoconformidadeRepository->listar();

    }
    public function listarPdf(){
        return $this->naoconformidadeRepository->listarPdf();

    }

    public function buscar($texto){
        if((!empty($texto['cliente_id'])) || (!empty($texto['mes'])) || (!empty($texto['ano'])) || (!empty($texto['tecnico_id'])))
            return $this->naoconformidadeRepository->buscar($texto);
        else
            return $this->listar();

    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $naoconformidade = $this->naoconformidadeRepository->buscarPorId($id);
            $naoconformidade->vigencia = $this->auxiliarService->converteData($naoconformidade->vigencia);

            if($naoconformidade->ocorrencia_data != null)
                $naoconformidade->ocorrencia_data = $this->auxiliarService->converteData($naoconformidade->ocorrencia_data);
            if($naoconformidade->acaoimediata_data != null)
                $naoconformidade->acaoimediata_data = $this->auxiliarService->converteData($naoconformidade->acaoimediata_data);
            if($naoconformidade->causaprovavel_data != null)
                $naoconformidade->causaprovavel_data = $this->auxiliarService->converteData($naoconformidade->causaprovavel_data);
            if($naoconformidade->acaocorretiva_prazo != null)
                $naoconformidade->acaocorretiva_prazo = $this->auxiliarService->converteData($naoconformidade->acaocorretiva_prazo);
            if($naoconformidade->eficaz_data != null)
                $naoconformidade->eficaz_data = $this->auxiliarService->converteData($naoconformidade->eficaz_data);

            return $naoconformidade;
        }
    }

    public function buscarPorCronogramaId($id){
        if(is_numeric($id)) {
            return $this->naoconformidadeRepository->buscarPorCronogramaId($id);
        }
    }

    public function remove($id){
        if(is_numeric($id)) {
            return $this->naoconformidadeRepository->remove($id);
        }
    }
}