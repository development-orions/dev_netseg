<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 14:37
 */

namespace App\Services;


use App\Repositories\AsoRepository;
use App\Repositories\LogRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AsoService
{
    protected $asos;
    /**
     * @var AsoRepository
     */
    private $asoRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    public function __construct(AsoRepository $asoRepository, LogRepository $logRepository, AuxiliarService $auxiliarService){

        $this->asoRepository = $asoRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function create($data){

        $resposta = array();

        try {
            $dados = $this->asoRepository->store($data);
            if ($dados) {
                $resposta['msg'] = 'ASO salvo com sucesso.';

                $this->logRepository->store("CADASTRAR ASO - USUARIO: ".Auth::user()->nome." - MÉDICO: ".$data['medico']." - PACIENTE: ".$data['paciente']."");

                $resposta['erro'] = false;
                $resposta['data'] = $dados;
            } else {
                $resposta['msg'] = 'Erro ao salvar o ASO, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }


    public function buscar($medico,$data){
        if(!empty($data['inicio']) || !empty($data['fim'])) {
            $asos = $this->asoRepository->buscar($medico,$data);
            return $asos;
        }else{
            return $this->listar($medico);
        }
    }

    public function listar($medico){
        if(is_numeric($medico)){
            $asos = $this->asoRepository->listar($medico);
            return  $asos;
        }
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $aso = $this->asoRepository->buscarPorId($id);

            if(isset($aso->data_contrato)) {
                $aso->data_contrato = $this->auxiliarService->converteData($aso->data_contrato);
            }
            $aso->valor_contrato = $this->format($aso->valor_contrato);
            if(isset($aso->valor_tecnico)) {
                $aso->valor_tecnico = $this->format($aso->valor_tecnico);
            }
            return $aso;
        }
    }


    public function listar_aso(){
        $asos = $this->asoRepository->listar_aso();

        foreach ($asos as $aso) {
            $clientes = $this->asoRepository->listar_clientes($aso->id);
            $aso->clientes = !empty($clientes) ? $clientes : 'Todos';
        }

        return  $asos;
    }

    public function asos_cliente(){
        $asos = $this->asoRepository->asos_cliente();

        return  $asos;
    }

    public function buscar_aso($data){
        if(!empty($data['aso']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id'])) {
            $asos = $this->asoRepository->buscar_aso($data);

            return $asos;
        }else{
            return $this->listar_aso();
        }
    }

}