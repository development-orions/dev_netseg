<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 08:00
 */

namespace App\Services;


use App\Repositories\CentrocustoRepository;
use App\Repositories\LogRepository;
use App\Repositories\RecebimentoRepository;
use Illuminate\Support\Facades\Auth;

class CentrocustoService
{
    private $centrocustoRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var RecebimentoRepository
     */
    private $recebimentoRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * Centrocusto constructor.
     */

    public function __construct(CentrocustoRepository $centrocustoRepository, LogRepository $logRepository, RecebimentoRepository $recebimentoRepository, AuxiliarService $auxiliarService)
    {
        $this->centrocustoRepository = $centrocustoRepository;
        $this->logRepository = $logRepository;
        $this->recebimentoRepository = $recebimentoRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function create($data){
        $resposta = array();
        try {
            if ($this->centrocustoRepository->store($data)) {
                $resposta['msg'] = 'Centro de custo salvo com sucesso.';

                $this->logRepository->store("CADASTRAR CENTRO DE CUSTO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o centro de custo, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            return $this->centrocustoRepository->buscarPorId($id);
        }
    }

    public function update($id, $data){
        $resposta = array();
        try {
            if ($this->centrocustoRepository->update($id,$data)) {
                $resposta['msg'] = 'Centro de custo editado com sucesso!';

                $this->logRepository->store("EDITAR CENTRO DE CUSTO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este centro de custo, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar(){
        return $this->centrocustoRepository->listar();
    }

    public function cronograma($data){
        $cronograma = $this->centrocustoRepository->cronograma($data);

        foreach($cronograma as $crono){
            foreach ($crono->contas as $conta) {
                $conta->totalvalor = $this->auxiliarService->format($conta->totalvalor);
            }
        }

        return $cronograma;
    }

    public function cronograma_pagamento($data){
        $cronograma = $this->centrocustoRepository->cronograma_pagamento($data);

        foreach($cronograma as $crono){
            foreach ($crono->contas as $conta) {
                $conta->totalvalor = $this->auxiliarService->format($conta->totalvalor);
            }
        }

        return $cronograma;
    }
}