<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Services;


use App\Repositories\LogRepository;
use App\Repositories\AtendimentoRepository;
use App\Repositories\TecnicoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AtendimentoService
{

    /**
     * AtendimentoService constructor.
     */
    private $atendimentoRepository;
    private $tecnicoRepository;
    private $auxiliarService;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(AtendimentoRepository $atendimentoRepository, TecnicoRepository $tecnicoRepository, AuxiliarService $auxiliarService, LogRepository $logRepository)
    {
        $this->atendimentoRepository = $atendimentoRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function create($data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->atendimentoRepository->store($data)) {
                $resposta['msg'] = 'Atendimento salvo com sucesso.';

                $this->logRepository->store("CADASTRAR ATENDIMENTO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['descricao']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){

            $atendimento = $this->atendimentoRepository->buscarPorId($id);
//            $atendimento->data = $this->auxiliarService->converteData($atendimento->data);

            return $atendimento;
        }
    }

    public function update($id, $data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->atendimentoRepository->update($id,$data)) {
                $resposta['msg'] = 'Atendimento editado com sucesso!';

                $this->logRepository->store("EDITAR ATENDIMENTO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['descricao']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar($data){

        if(!empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id'])) {
            $atendimentos = $this->atendimentoRepository->buscar($data);
            return  $atendimentos;
        }else{
            return $this->atendimentoRepository->listar();
        }
    }

    public function medico_exame($data){

        if(!empty($data['inicio']) || !empty($data['fim']) || !empty($data['medico_id'])) {
            $atendimentos = $this->atendimentoRepository->medico_exame($data);
            return  $atendimentos;
        }
        return false;
    }

    public function med_exame($data){

        if(!empty($data['inicio']) || !empty($data['fim']) || !empty($data['medico_id'])) {
            $atendimentos = $this->atendimentoRepository->med_exame($data);
            return  $atendimentos;
        }
        return false;
    }
    public function atendimento_medico($data){

        if(!empty($data['inicio']) || !empty($data['fim']) || !empty($data['medico_id'])) {
            $atendimentos = $this->atendimentoRepository->atendimento_medico($data);
            return  $atendimentos;
        }
        return false;
    }

    public function laboratorio_exame($data){
        if(!empty($data['inicio']) || !empty($data['fim']) || !empty($data['laboratorio_id'])) {
            $atendimentos = $this->atendimentoRepository->laboratorio_exame($data);
            return  $atendimentos;
        }
        return false;
    }

    public function empresa_exame($data){
        if(!empty($data['inicio']) || !empty($data['fim']) || !empty($data['empresa_id'])) {
            $atendimentos = $this->atendimentoRepository->empresa_exame($data);
            return  $atendimentos;
        }
        return false;
    }


    public function adicionar($atendimento){
        $resposta = array();
        $atendimentoobj = $this->atendimentoRepository->buscarPorId(\Illuminate\Support\Facades\Request::get('atendimento_id'));
        $atendimento['atendimento_id'] = $atendimentoobj;


        if(Session::has('atendimento')) {
            $sv = Session::get('atendimento');
            $sv[] = $atendimento;
            Session::put('atendimento', $sv);

        }else{
            $serv[0] = $atendimento;

            Session::put('atendimento', $serv);
        }

        $resposta['msg'] = 'Atendimento adicionado com sucesso!';
        $resposta['erro'] = false;

        return $resposta;

    }

    public function deletar($id)
    {
        if(is_numeric($id)){
            $resposta = array();

            $data = $this->atendimentoRepository->buscarPorId($id);
            try {
                if ($this->atendimentoRepository->remove($id)) {

                    $this->logRepository->store("DELETAR ATENDIMENTO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['descricao']."");

                    $resposta['msg'] = 'Atendimento deletado com sucesso.';

                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao deletar Atendimento.';
                    $resposta['erro'] = true;
                }

            }catch (\Exception $e){
                $resposta['msg'] = $e->getMessage();
                $resposta['erro'] = true;
            };

            return $resposta;
        }
        return false;
    }
}