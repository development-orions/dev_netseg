<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 28/01/2016
 * Time: 17:07
 */

namespace App\Services;


use App\Repositories\ObservacaoRepository;

class ObservacaoService
{
    /**
     * @var ObservacaoRepository
     */
    private $observacaoRepository;

    /**
     * ObservacaoService constructor.
     */
    public function __construct(ObservacaoRepository $observacaoRepository)
    {
        $this->observacaoRepository = $observacaoRepository;
    }

    public function store($id, $data){
        if(is_numeric($id)) {
            return $this->observacaoRepository->store($id, $data);
        }
    }

    public function buscarPorCronogramaId($id){
        if(is_numeric($id)) {
            return $this->observacaoRepository->buscarPorCronogramaId($id);
        }
    }

    public function remove($id){
        if(is_numeric($id)) {
            return $this->observacaoRepository->remove($id);
        }
    }
}