<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 17/12/2015
 * Time: 07:44
 */

namespace App\Services;


use App\Repositories\ClienteRepository;
use App\Repositories\LogRepository;
use App\Repositories\MedicosvendaRepository;
use App\Repositories\ParcelaRepository;
use App\Repositories\TecnicosvendaRepository;
use App\Repositories\VendaRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class VendaService
{

    /**
     * VendaService constructor.
     */
    private $vendaRepository;
    private $clienteRepository;
    private $parcelaRepository;
    /**
     * @var TecnicosvendaRepository
     */
    private $tecnicosvendaRepository;
    /**
     * @var MedicosvendaRepository
     */
    private $medicosvendaRepository;

    public function __construct(VendaRepository $vendaRepository, ClienteRepository $clienteRepository, ParcelaRepository $parcelaRepository, LogRepository $logRepository, AuxiliarService $auxiliarService, TecnicosvendaRepository $tecnicosvendaRepository, MedicosvendaRepository $medicosvendaRepository)
    {
        $this->vendaRepository = $vendaRepository;
        $this->clienteRepository = $clienteRepository;
        $this->parcelaRepository = $parcelaRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
        $this->tecnicosvendaRepository = $tecnicosvendaRepository;
        $this->medicosvendaRepository = $medicosvendaRepository;
    }

    public function create($data){

        /*
         * 0 - venda
         * 1 - orçamento
         * */
        $resposta = array();

        if($data['orcamento'] == 1){ // ORÇAMENTO
            $data['aprovado'] = 0; //não aprovado
            $data['statu'] = 6; // Aguardando aprovação

            try {
                $teste = $this->vendaRepository->store($data);
                if ($teste) {
                    //$this->parcelaRepository->store($teste->id, $data);
                    $resposta['msg'] = 'Orçamento salvo com sucesso.';
                    $resposta['id'] = $teste->id;

                    $this->logRepository->store("CADASTRAR ORÇAMENTO - USUARIO: ".Auth::user()->nome." - VALIDADE: ".$teste->validade." - TEM NOTA FISCAL: ".$teste->temnota." -  CLIENTE: ".$data['cliente_id']." - FORMA DE PAGAMENTO: ".$data['formapagamento_id']." - STATUS: ".$teste->statu_id);

                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao salvar o orçamento, tente novamente.';
                    $resposta['erro'] = true;
                }
            }catch (\Exception $e){
                $resposta['msg'] = $e;
                $resposta['erro'] = true;
            };
        }else{ // VENDA
            $data['aprovado'] = 1;
            $data['statu'] = 7; // Aprovado
            $data['fechamento'] = $this->auxiliarService->data_banco($data['fechamento']);

            if(isset($data['entrega'])) {
                $data['entrega'] = $this->auxiliarService->data_banco($data['entrega']);
            }
            $data['valoradministrativo'] = $this->auxiliarService->moeda($data['valoradministrativo']);
            try {
                $venda = $this->vendaRepository->store($data);
                if ($venda) {
                    $this->parcelaRepository->store($venda->id, $data);

                    for($x=0; $x<3; $x++){
                        if($data['valor_tecnico'][$x] != null) {
                            $data['valor_tecnico'][$x] = $this->auxiliarService->moeda($data['valor_tecnico'][$x]);
                        }
                    }

                    $this->tecnicosvendaRepository->store($venda->id, $data);

                    for($x=0; $x<3; $x++){
                        if($data['valor_medico'][$x] != null) {
                            $data['valor_medico'][$x] = $this->auxiliarService->moeda($data['valor_medico'][$x]);
                        }
                    }
                    $this->medicosvendaRepository->store($venda->id, $data);


                    $resposta['msg'] = 'Venda salva com sucesso.';

                    $this->logRepository->store("CADASTRAR VENDA - USUARIO: ".Auth::user()->nome." - DATA FECHAMENTO: ".$venda->fechamento." - TEM NOTA FISCAL: ".$venda->temnota." -  NOTA FISCAL: ".$data['nf']." -  DATA ENTREGA: ".$data['entrega']." -  CLIENTE: ".$data['cliente_id']." - FORMA DE PAGAMENTO: ".$data['formapagamento_id']." -  NÚMERO BOLETO: ".$data['num_boleto']." - STATUS: ".$venda->statu_id);

                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao salvar a venda, tente novamente.';
                    $resposta['erro'] = true;
                }
            }catch (\Exception $e){
                $resposta['msg'] = $e;
                $resposta['erro'] = true;
            };
        }

        return $resposta;
    }

    public function adicionar($id){
        if(is_numeric($id)){
            $cliente = $this->clienteRepository->adicionar($id);
            Session::forget('cliente_orcamento');
            Session::put('cliente_orcamento', $cliente);

            return true;
        }
    }


    public function buscarPorId($id){
        if(is_numeric($id)){
            $venda = $this->vendaRepository->buscarPorId($id);
            return $venda;
        }
    }

    public function converter_orcamento($id, $data){
        $resposta = array();
        try {
            $data['valoradministrativo'] = $this->auxiliarService->moeda($data['valoradministrativo']);
            if ($venda = $this->vendaRepository->converter_orcamento($id,$data)) {

                if($data['tipo'] != 'medicina') {

                    for($x=0; $x<3; $x++){
                        if($data['valor_tecnico'][$x] != null) {
                            $data['valor_tecnico'][$x] = $this->auxiliarService->moeda($data['valor_tecnico'][$x]);
                        }
                    }

                    $this->tecnicosvendaRepository->store($venda->id, $data);
                }
                if($data['tipo'] == 'medicina' || $data['tipo'] =='ambos') {
                    for($x=0; $x<3; $x++){
                        if($data['valor_medico'][$x] != null) {
                            $data['valor_medico'][$x] = $this->auxiliarService->moeda($data['valor_medico'][$x]);
                        }
                    }
                    $this->medicosvendaRepository->store($venda->id, $data);
                }

                $resposta['msg'] = 'Orçamento convertido em venda com sucesso!';

                //$this->logRepository->store("CONVERTER ORÇAMENTO EM VENDA - USUARIO: ".$user." - NOME: ".$data['nome']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - FORMA JURIDICA: ".$data['formajuridica']." - RESPONSAVEL: ".$data['responsavel']." - FUNÇAO: ".$data['funcao']." - E-MAIL: ".$data['email_responsavel']." - STATUS: ".$data['statu_id']." - VALOR CONTRATO: ".$data['valor_contrato']." - DATA DE CONTRATO: ".$data['data_contrato']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar converter esse orçamento em venda, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }


    public function update($id, $data){
        $resposta = array();
        if($data['orcamento'] == 1) {
            $data['aprovado'] = 0; //não aprovado
            try {
                if ($dados = $this->vendaRepository->update($id, $data)) {
                    //$this->parcelaRepository->delete($dados->id);
                    //$this->parcelaRepository->store($dados->id, $data);

                    if (Session::has('servico'))
                        Session::forget('servico');
                    if (Session::has('cliente_orcamento'))
                        Session::forget('cliente_orcamento');

                    $resposta['msg'] = 'Orçamento editado com sucesso!';

                    $this->logRepository->store("EDITAR ORÇAMENTO - USUARIO: ".Auth::user()->nome." - VALIDADE: ".$dados->validade." - TEM NOTA FISCAL: ".$dados->temnota." -  CLIENTE: ".$data['cliente_id']." - FORMA DE PAGAMENTO: ".$data['formapagamento_id']." - STATUS: ".$dados->statu_id);

                    $resposta['erro'] = false;

                } else {
                    $resposta['msg'] = 'Erro ao tentar editar este orçamento, tente novamente.';
                    $resposta['erro'] = true;
                }
            } catch (\Exception $e) {
                $resposta['msg'] = $e->getMessage();
                $resposta['erro'] = true;
            };
        }
        return $resposta;
    }


    //orçamento sem converter em venda
    public function listar($orcamento){
        if(is_numeric($orcamento)){
            $vendas = $this->vendaRepository->listar($orcamento);
            foreach($vendas as $venda) {
                $venda->valor = $this->auxiliarService->format($venda->valor);
                $venda->tipo = $this->vendaRepository->verificaTipo($venda->id);
            }
            return  $vendas;
        }
    }

    public function buscar($data){
        if(!empty($data['venda']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id'])) {
            $vendas = $this->vendaRepository->buscar($data);
            foreach($vendas as $venda) {
                $venda->valor = $this->auxiliarService->format($venda->valor);
                $venda->tipo = $this->vendaRepository->verificaTipo($venda->id);
            }
            return $vendas;
        }else{
            return $this->listar(1);
        }
    }


    //orçamento sem converter em venda
    public function listar_convertidos($orcamento){
        if(is_numeric($orcamento)){
            $vendas = $this->vendaRepository->listar_convertidos($orcamento);
            foreach($vendas as $venda) {
                $venda->valor = $this->auxiliarService->format($venda->valor);
                $venda->tipo = $this->vendaRepository->verificaTipo($venda->id);
            }
            return  $vendas;

        }

    }

    public function buscar_convertidos($data){
        if(!empty($data['venda']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim'])) {
            $vendas = $this->vendaRepository->buscar_convertidos($data);
            foreach($vendas as $venda) {
                $venda->valor = $this->auxiliarService->format($venda->valor);
                $venda->tipo = $this->vendaRepository->verificaTipo($venda->id);
            }
            return $vendas;

        }else{
            return $this->listar_convertidos(1);
        }
    }


    //vendas
    public function listar_venda($orcamento){
        if(is_numeric($orcamento)){
            $vendas = $this->vendaRepository->listar_venda($orcamento);
            foreach($vendas as $venda) {
                $venda->valor = $this->auxiliarService->format($venda->valor);
                $venda->tipo = $this->vendaRepository->verificaTipo($venda->id);
            }
            return  $vendas;

        }

    }

    public function buscar_venda($data){
        if(!empty($data['venda']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id'])) {
            $vendas = $this->vendaRepository->buscar_venda($data);

            foreach($vendas as $venda) {
                $venda->valor = $this->auxiliarService->format($venda->valor);
                $venda->tipo = $this->vendaRepository->verificaTipo($venda->id);
            }
            return $vendas;
        }else{
            return $this->listar_venda(0);
        }
    }


    /* public function buscar($orcamento, $data){
         if(!empty($data['venda']) || !empty($data['q']) || !empty($data['inicio']) || !empty($data['fim']) || !empty($data['statu_id'])) {
             return $this->vendaRepository->buscar($orcamento, $data);
         }else{
             return $this->listar($orcamento);
         }

     } */
    public function cancelar($id, $data){
        $resposta = array();
        if(is_numeric($id)){
            if ($this->vendaRepository->cancelar($id, $data)){
                $resposta['msg'] = 'Venda cancelada com sucesso!';
                $resposta['erro'] = false;
            }else {
                $resposta['msg'] = 'Erro ao tentar cancelar esta venda, tente novamente.';
                $resposta['erro'] = true;
            }
        }
        return $resposta;
    }

    public function listar_tecnico_venda($venda_id){
        $vendas = $this->tecnicosvendaRepository->listar($venda_id);
        foreach($vendas as $venda){
            $venda->valor = $this->auxiliarService->format($venda->valor);
        }
        return $vendas;
    }

    public function listar_medico_venda($venda_id){
        $vendas = $this->medicosvendaRepository->listar($venda_id);
        foreach($vendas as $venda){
            $venda->valor = $this->auxiliarService->format($venda->valor);
        }
        return $vendas;
    }

    public function aprovarOrcamento($id){
        if(is_numeric($id)){
            return $this->vendaRepository->aprovar($id);
        }
    }

    public function inativarOrcamento($id){
        if(is_numeric($id)){
            return $this->vendaRepository->inativar($id);
        }
    }

    public function ativarOrcamento($id){
        if(is_numeric($id)){
            return $this->vendaRepository->ativar($id);
        }
    }

}