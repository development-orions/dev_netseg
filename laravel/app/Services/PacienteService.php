<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 08:42
 */

namespace App\Services;


use App\Repositories\ClienteRepository;
use App\Repositories\LogRepository;
use App\Repositories\PacienteRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PacienteService
{
    private $pacienteRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var ClienteRepository
     */
    private $clienteRepository;

    /**
     * PacienteService constructor.
     * @param PacienteRepository $pacienteRepository
     * @param LogRepository $logRepository
     * @param AuxiliarService $auxiliarService
     */
    public function __construct(PacienteRepository $pacienteRepository, LogRepository $logRepository, AuxiliarService $auxiliarService, ClienteRepository $clienteRepository)
    {
        $this->pacienteRepository = $pacienteRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
        $this->clienteRepository = $clienteRepository;
    }

    public function inativar($id){
        if(is_numeric($id)){
            return $this->pacienteRepository->inativar($id);
        }
    }
    public function ativar($id){
        if(is_numeric($id)){
            return $this->pacienteRepository->ativar($id);
        }
    }

    public function adicionar($id){
        if(is_numeric($id)){

            $cliente = $this->clienteRepository->adicionar($id);
            Session::forget('id');
            Session::put('id', $cliente);
            return true;
        }
    }

    public function create($data){
        $data['statu_id'] = 1;
        $resposta = array();
        try {
            if ($this->pacienteRepository->store($data)) {
                $resposta['msg'] = 'Paciente salvo com sucesso.';

                $this->logRepository->store("CADASTRAR PACIENTE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RG".$data['rg']." -  CPF: ".$data['cpf']." - DATA DE NASCIMENTO: ".$data['datanascimento']." - GENERO: ".$data['sexo']." - CEP: ".$data['cep']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - ESTADO CIVIL: ".$data['estadocivil']." - NATURALIDADE: ".$data['naturalidade']." - GRAU DE INSTRUÇÃO: ".$data['grau_instrucao']." - FUNÇAO: ".$data['funcao']." - SETOR: ".$data['setor']." - E-MAIL: ".$data['email']." - TELEFONE: ".$data['telefone']." - STATUS: ".$data['statu_id']." - CLIENTE: ".$data['cliente_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o paciente, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function update($id, $data){
        $resposta = array();
        try {
            if ($this->pacienteRepository->update($id,$data)) {
                $resposta['msg'] = 'Paciente editado com sucesso!';

                $this->logRepository->store("EDITAR PACIENTE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - RG".$data['rg']." -  CPF: ".$data['cpf']." - DATA DE NASCIMENTO: ".$data['datanascimento']." - GENERO: ".$data['sexo']." - CEP: ".$data['cep']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - ESTADO CIVIL: ".$data['estadocivil']." - NATURALIDADE: ".$data['naturalidade']." - GRAU DE INSTRUÇÃO: ".$data['grau_instrucao']." - FUNÇAO: ".$data['funcao']." - SETOR: ".$data['setor']." - E-MAIL: ".$data['email']." - TELEFONE: ".$data['telefone']." - STATUS: ".$data['statu_id']." - CLIENTE: ".$data['cliente_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este paciente, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar($status){
        if(is_numeric($status)){
            return $this->pacienteRepository->listar($status);
        }
    }

    public function buscar($status, $texto){
        if(!empty($texto['q']) || !empty($texto['empresa'])) {
            return $this->pacienteRepository->buscar($status, $texto);
        }else{
            return $this->listar($status);
        }
    }

    public function listar_drop($status,$term,$page){
        if(is_numeric($status)){
            return $this->pacienteRepository->listar_drop($status,$term,$page);
        }
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $paciente = $this->pacienteRepository->buscarPorId($id);
            $paciente->idade = $this->auxiliarService->calc_idade($paciente->datanascimento);
            $paciente->nascimento = explode('-',$paciente->datanascimento);
            $paciente->datanascimento = $this->auxiliarService->converteData($paciente->datanascimento);


            if(!Session::has('id'))
                Session::put('id', $paciente->clientes);

            return $paciente;
        }
    }
}