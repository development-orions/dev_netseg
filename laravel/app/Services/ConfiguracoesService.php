<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 09/12/2015
 * Time: 08:13
 */

namespace App\Services;


use App\Repositories\ConfiguracoesRepository;
use App\Repositories\LogRepository;
use Illuminate\Support\Facades\Auth;

class ConfiguracoesService
{

    /**
     * ConfiguracoesService constructor.
     */
    private $configuracoesRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(ConfiguracoesRepository $configuracoesRepository, LogRepository $logRepository)
    {
        $this->configuracoesRepository = $configuracoesRepository;
        $this->logRepository = $logRepository;
    }

    public function buscar(){
        return $this->configuracoesRepository->buscar();
    }

    public function update($data){
        $resposta = array();
        try {
            if ($this->configuracoesRepository->update($data)) {
                $resposta['msg'] = 'Informações salvas com sucesso.';

                $this->logRepository->store("INFORMAÇÕES DA EMPRESA - USUARIO: ".Auth::user()->nome." - NOME FANTASIA: ".$data['nomefantasia']." - RAZÃO SOCIAL".$data['razaosocial']." -  CNPJ: ".$data['cnpj']." - IE: ".$data['ie']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - FORMA JURIDICA: ".$data['formajuridica']." - SITE: ".$data['site']." - E-MAIL: ".$data['email']." - TELEFONE: ".$data['telefone']." - CNAE: ".$data['cnae']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar as informações, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }
}