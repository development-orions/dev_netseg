<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 26/01/2016
 * Time: 07:58
 */

namespace App\Services;


use App\Repositories\ClienteRepository;
use App\Repositories\CronogramaRepository;
use App\Repositories\LogRepository;
use App\Repositories\TecnicoRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CronogramaService
{
    /**
     * @var CronogramaRepository
     */
    private $cronogramaRepository;
    /**
     * @var ClienteRepository
     */
    private $clienteRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var ObservacaoService
     */
    private $observacaoService;
    /**
     * @var TecnicoRepository
     */
    private $tecnicoRepository;
    /**
     * @var NaoconformidadeService
     */
    private $naoconformidadeService;

    /**
     * CronogramaService constructor.
     */
    public function __construct(CronogramaRepository $cronogramaRepository, ClienteRepository $clienteRepository, AuxiliarService $auxiliarService, LogRepository $logRepository, TecnicoRepository $tecnicoRepository, NaoconformidadeService $naoconformidadeService, ObservacaoService $observacaoService)
    {
        $this->cronogramaRepository = $cronogramaRepository;
        $this->clienteRepository = $clienteRepository;
        $this->auxiliarService = $auxiliarService;
        $this->logRepository = $logRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->naoconformidadeService = $naoconformidadeService;
        $this->observacaoService = $observacaoService;
    }

    public function adicionar($id){
        if(is_numeric($id)){
            $cliente = $this->clienteRepository->adicionar($id);

            Session::forget('cliente_cronograma');
            Session::put('cliente_cronograma', $cliente);

            return true;
        }
    }

    public function show($data){
        return $this->cronogramaRepository->findByCliente($data);
    }

    public function store($cliente_id, $tecnico_id, $atividade_id, $ano, $mes, $data){
        $resposta = array();
        $data['vencimento'] = $this->auxiliarService->data_banco($data['vencimento']);
        try {
            $check = $this->cronogramaRepository->verifyCronograma($cliente_id, $tecnico_id, $atividade_id, $ano, date("n", strtotime($data['vencimento'])));
            if($check){
                $resposta['msg'] = 'Já esxiste uma Cronograma neste mês para este técnico com o mesmo cliente e a mesma atividade.';
                $resposta['erro'] = true;
            }else{
                $cronograma = $this->cronogramaRepository->store($cliente_id, $tecnico_id, $atividade_id, $ano, $mes, $data);
                if ($cronograma) {
                    $resposta['msg'] = 'Atividade salva com sucesso.';
                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao salvar a atividade, tente novamente.';
                    $resposta['erro'] = true;
                }
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };

        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $cronograma = $this->cronogramaRepository->buscarPorId($id);
            $cronograma->vencimento = $this->auxiliarService->converteData($cronograma->vencimento);
            return $cronograma;
        }
    }

    public function update($id, $data){
        $resposta = array();
        if(Auth::user()->role_id != 11) {
            $data['vencimento'] = $this->auxiliarService->data_banco($data['vencimento']);
        }
        try {
            $crono = $this->cronogramaRepository->buscarPorId($id);
            $check = $this->cronogramaRepository->verifyCronograma($crono->cliente_id, $crono->tecnico_id, $crono->atividade_id, $crono->ano,Auth::user()->role_id != 11 ? date("n", strtotime($data['vencimento'])) : $crono->vencimento);
            if($check && (Auth::user()->role_id != 11 ? date("n", strtotime($data['vencimento'])) : $crono->vencimento) != $crono->mes){
                $resposta['msg'] = 'Já esxiste uma Cronograma neste mês para este técnico com o mesmo cliente e a mesma atividade.';
                $resposta['erro'] = true;
            }else{
                $cronograma = $this->cronogramaRepository->update($id, $data);
                if ($cronograma) {
                    $resposta['msg'] = 'Atividade editada com sucesso.';
                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao editar a atividade, tente novamente.';
                    $resposta['erro'] = true;
                }
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };

        return $resposta;
    }

    public function remove($id){
        $resposta = array();
        try {

            //Verificando se tem não conformidade antes de excluir
            $naoconformidades = $this->naoconformidadeService->buscarPorCronogramaId($id);
            $observacoes = $this->observacaoService->buscarPorCronogramaId($id);

            if(sizeof($naoconformidades) != 0 || sizeof($observacoes) != 0){ // se houve nao conformidade

                if(sizeof($naoconformidades) != 0) {
                    foreach ($naoconformidades as $naoconformidade) {

                        if (!$this->naoconformidadeService->remove($naoconformidade->id)) {
                            $resposta['msg'] = 'Erro ao excluir o cronograma, porque há não conformidade vinculada. Tente novamente';
                            $resposta['erro'] = true;
                            break;
                        }
                    }
                }

                if(sizeof($observacoes) != 0) {
                    foreach ($observacoes as $observacao) {

                        if (!$this->observacaoService->remove($observacao->id)) {
                            $resposta['msg'] = 'Erro ao excluir o cronograma, porque há observação vinculada. Tente novamente';
                            $resposta['erro'] = true;
                            break;
                        }
                    }
                }


                $cronograma_log = $this->cronogramaRepository->buscarPorId($id);
                $log = $this->logRepository->store("EXCLUIR CRONOGRAMA - USUARIO: ".Auth::user()->nome." - CLIENTE: ".$cronograma_log->cliente_id." - TÉCNICO: ".$cronograma_log->tecnico_id." -  ATIVIDADE: ".$cronograma_log->atividade_id." - VENCIMENTO: ".$cronograma_log->vencimento." - STATUS: ".$cronograma_log->statu_id);

                if ($this->cronogramaRepository->remove($id)) {
                    $resposta['msg'] = 'Cronograma excluido com sucesso.';
                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao excluir o cronograma, tente novamente.';
                    $this->logRepository->remove($log->id);
                    $resposta['erro'] = true;
                }

            }else{
                //se não tiver nao conformidade
                $cronograma_log = $this->cronogramaRepository->buscarPorId($id);
                $log = $this->logRepository->store("EXCLUIR CRONOGRAMA - USUARIO: ".Auth::user()->nome." - CLIENTE: ".$cronograma_log->cliente_id." - TÉCNICO: ".$cronograma_log->tecnico_id." -  ATIVIDADE: ".$cronograma_log->atividade_id." - VENCIMENTO: ".$cronograma_log->vencimento." - STATUS: ".$cronograma_log->statu_id);

                if ($this->cronogramaRepository->remove($id)) {
                    $resposta['msg'] = 'Cronograma excluido com sucesso.';
                    $resposta['erro'] = false;
                } else {
                    $resposta['msg'] = 'Erro ao excluir o cronograma, tente novamente.';
                    $this->logRepository->remove($log->id);
                    $resposta['erro'] = true;
                }
            }

        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };

        return $resposta;
    }

    public function deletar_arquivo($id){
        $resposta = array();
        try {
            $cronograma = $this->cronogramaRepository->deletar_arquivo($id);

            if ($cronograma) {
                $resposta['msg'] = 'Arquivo deletado com sucesso.';
                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao deletar esse arquivo, tente novamente.';
                $resposta['erro'] = true;
            }

        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };

        return $resposta;
    }

    public function buscar($data){
        return $this->cronogramaRepository->buscar($data);
    }
    public function listar(){
        return $this->cronogramaRepository->listar();
    }

    public function listar_por_id_entre_datas($atividade_id, $tecnico_id, $datas)
    {
        return $this->cronogramaRepository->listar_por_id_entre_datas($atividade_id, $tecnico_id, $datas);
    }
}