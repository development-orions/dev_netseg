<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 16/11/2015
 * Time: 10:12
 */

namespace App\Services;


use App\Repositories\AtividadeRepository;
use App\Repositories\LogRepository;
use Illuminate\Support\Facades\Auth;

class AtividadeService
{
    /**
     * @var AtividadeRepository
     */
    private $atividadeRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(AtividadeRepository $atividadeRepository, LogRepository $logRepository){

        $this->atividadeRepository = $atividadeRepository;
        $this->logRepository = $logRepository;
    }

    public function create($data){
        $data['statu_id'] = 1;
        $resposta = array();
        try {
            if ($this->atividadeRepository->store($data)) {
                $resposta['msg'] = 'Atividade salva com sucesso.';

                $this->logRepository->store("CADASTRAR ATIVIDADE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - STATUS: ".$data['statu_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar a atividade, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar($status){
        if(is_numeric($status)){
            return $this->atividadeRepository->listar($status);
        }
    }

    public function listarTodos()
    {
        return $this->atividadeRepository->listarTodos();
    }

    public function update($id, $data){
        $resposta = array();
        try {
            if ($this->atividadeRepository->update($id,$data)) {
                $resposta['msg'] = 'Atividade editada com sucesso!';
                $this->logRepository->store("EDITAR ATIVIDADE - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - STATUS: ".$data['statu_id']."");
                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar esta atividade, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            return $this->atividadeRepository->buscarPorId($id);
        }
    }

    public function inativar($id){
        if(is_numeric($id)){
            return $this->atividadeRepository->inativar($id);
        }
    }
    public function ativar($id){
        if(is_numeric($id)){
            return $this->atividadeRepository->ativar($id);
        }
    }

    public function listar_cronograma($status, $data){
        if(is_numeric($status)){
            return $this->atividadeRepository->listar_cronograma($status, $data);
        }
    }

    public function listar_cronograma_por_tecnico_entre_datas($data)
    {
        return $this->atividadeRepository->listar_cronograma_por_tecnico_entre_datas($data);
    }

    public function listar_cronograma_por_atividade_entre_datas($id, $data)
    {
        return $this->atividadeRepository->listar_cronograma_por_atividade_entre_datas($id, $data);
    }

}