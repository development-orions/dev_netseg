<?php
/**
 * Created by PhpStorm.
 * User: Silvio
 * Date: 11/11/2015
 * Time: 14:37
 */

namespace App\Services;


use App\Repositories\LogRepository;
use App\Repositories\TecnicoRepository;
use Illuminate\Support\Facades\Auth;

class TecnicoService
{
    /**
     * @var TecnicoRepository
     */
    private $tecnicoRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(TecnicoRepository $tecnicoRepository, LogRepository $logRepository){

        $this->tecnicoRepository = $tecnicoRepository;
        $this->logRepository = $logRepository;
    }

    public function create($data){
        $data['statu_id'] = 1;
        $resposta = array();
        try {
            if ($this->tecnicoRepository->store($data)) {
                $resposta['msg'] = 'Tecnico salvo com sucesso.';

                $this->logRepository->store("CADASTRAR SERVIÇO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - NUMERO DE REGISTRO: ".$data['num_registro']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - DESCRIÇÃO: ".$data['descricao']." - E-MAIL: ".$data['email']." - TELEFONE: ".$data['telefone']." - STATUS: ".$data['statu_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o tecnico, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }
    public function listar($status){
        if(is_numeric($status)){
            return $this->tecnicoRepository->listar($status);

        }

    }
    public function buscar($status, $texto){
        if(is_numeric($status)){
            if(!empty($texto['q']))
                return $this->tecnicoRepository->buscar($status, $texto);
            else
                return $this->listar($status);
        }
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            return $this->tecnicoRepository->buscarPorId($id);
        }
    }

    public function update($id, $data){
        $resposta = array();
        try {
            if ($this->tecnicoRepository->update($id,$data)) {
                $resposta['msg'] = 'Tecnico editado com sucesso!';

                $this->logRepository->store("EDITAR SERVIÇO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - NUMERO DE REGISTRO: ".$data['num_registro']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - DESCRIÇÃO: ".$data['descricao']." - E-MAIL: ".$data['email']." - TELEFONE: ".$data['telefone']." - STATUS: ".$data['statu_id']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este tecnico, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function inativar($id){
        if(is_numeric($id)){
            return $this->tecnicoRepository->inativar($id);
        }
    }
    public function ativar($id){
        if(is_numeric($id)){
            return $this->tecnicoRepository->ativar($id);
        }
    }

    public function tecnico_list(){
        return $this->tecnicoRepository->tecnicoCronograma();
    }

}