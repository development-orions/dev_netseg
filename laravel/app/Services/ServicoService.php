<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 24/11/2015
 * Time: 09:30
 */

namespace App\Services;


use App\Repositories\LogRepository;
use App\Repositories\ServicoRepository;
use App\Repositories\TecnicoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ServicoService
{

    /**
     * ServicoService constructor.
     */
    private $servicoRepository;
    private $tecnicoRepository;
    private $auxiliarService;
    /**
     * @var LogRepository
     */
    private $logRepository;

    public function __construct(ServicoRepository $servicoRepository, TecnicoRepository $tecnicoRepository, AuxiliarService $auxiliarService, LogRepository $logRepository)
    {
        $this->servicoRepository = $servicoRepository;
        $this->tecnicoRepository = $tecnicoRepository;
        $this->logRepository = $logRepository;
    }

    public function create($data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->servicoRepository->store($data)) {
                $resposta['msg'] = 'Serviço salvo com sucesso.';

                $this->logRepository->store("CADASTRAR SERVIÇO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            return $this->servicoRepository->buscarPorId($id);
        }
    }

    public function update($id, $data){
        //$data['valor'] = str_replace(',', '.', $data['valor']);
        $resposta = array();
        try {
            if ($this->servicoRepository->update($id,$data)) {
                $resposta['msg'] = 'Serviço editado com sucesso!';

                $this->logRepository->store("EDITAR SERVIÇO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao tentar editar este serviço, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e->getMessage();
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar(){
        return $this->servicoRepository->listar();
    }

    public function adicionar($servico){
        $resposta = array();
        $servicoobj = $this->servicoRepository->buscarPorId(\Illuminate\Support\Facades\Request::get('servico_id'));
        $servico['servico_id'] = $servicoobj;


        if(Session::has('servico')) {
            $sv = Session::get('servico');
            $sv[] = $servico;
            Session::put('servico', $sv);

        }else{
            $serv[0] = $servico;

            Session::put('servico', $serv);
        }

        $resposta['msg'] = 'Serviço adicionado com sucesso!';
        $resposta['erro'] = false;

        return $resposta;

    }
}