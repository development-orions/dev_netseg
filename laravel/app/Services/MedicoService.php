<?php
/**
 * Created by PhpStorm.
 * User: Orions
 * Date: 03/02/2016
 * Time: 08:27
 */

namespace App\Services;


use App\Repositories\LogRepository;
use App\Repositories\MedicoRepository;
use Illuminate\Support\Facades\Auth;

class MedicoService
{
    /**
     * @var MedicoRepository
     */
    private $medicoRepository;
    /**
     * @var LogRepository
     */
    private $logRepository;
    /**
     * @var AuxiliarService
     */
    private $auxiliarService;

    /**
     * MedicoService constructor.
     */
    public function __construct(MedicoRepository $medicoRepository, LogRepository $logRepository, AuxiliarService $auxiliarService)
    {
        $this->medicoRepository = $medicoRepository;
        $this->logRepository = $logRepository;
        $this->auxiliarService = $auxiliarService;
    }

    public function inativar($id){
        if(is_numeric($id)){
            return $this->medicoRepository->inativar($id);
        }
    }
    public function ativar($id){
        if(is_numeric($id)){
            return $this->medicoRepository->ativar($id);
        }
    }

    public function create($data){
        $data['statu_id'] = 1;
        $resposta = array();
        if(!empty($data['crm'])) {
            if (!$this->medicoRepository->verificaCrm($data['crm'])) {
                $resposta['msg'] = 'CRM já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        try {
            if ($this->medicoRepository->store($data)) {
                $resposta['msg'] = 'Médico salvo com sucesso.';

                $this->logRepository->store("CADASTRAR MEDICO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - CRM".$data['crm']." -  NIT: ".$data['nit']." - DATA DE CONTRATO: ".$data['datacontrato']." - CEP: ".$data['cep']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - TELEFONE: ".$data['telefone']." - STATUS: ".$data['statu_id']." - OBSERVAÇÕES: ".$data['observacao']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao salvar o médico, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function update($id, $data){

        $resposta = array();
        if(!empty($data['cnpj'])) {
            if (!$this->medicoRepository->verificaCrmEdicao($id, $data['crm'])) {
                $resposta['msg'] = 'CRM já cadastrado no sistema.';
                $resposta['erro'] = true;
                return $resposta;
            }
        }
        try {
            if ($this->medicoRepository->update($id,$data)) {
                $resposta['msg'] = 'Médico editado com sucesso.';

                $this->logRepository->store("EDITAR MEDICO - USUARIO: ".Auth::user()->nome." - NOME: ".$data['nome']." - CRM".$data['crm']." -  NIT: ".$data['nit']." - DATA DE CONTRATO: ".$data['datacontrato']." - CEP: ".$data['cep']." - CEP: ".$data['cep']." - RUA: ".$data['rua']." - NUMERO: ".$data['numero']." - COMPLEMENTO: ".$data['complemento']." - BAIRRO: ".$data['bairro']." - CIDADE: ".$data['cidade']." - UF: ".$data['estado']." - TELEFONE: ".$data['telefone']." - STATUS: ".$data['statu_id']." - OBSERVAÇÕES: ".$data['observacao']."");

                $resposta['erro'] = false;
            } else {
                $resposta['msg'] = 'Erro ao editar o médico, tente novamente.';
                $resposta['erro'] = true;
            }
        }catch (\Exception $e){
            $resposta['msg'] = $e;
            $resposta['erro'] = true;
        };
        return $resposta;
    }

    public function listar($status){
        if(is_numeric($status)){
            $medicos = $this->medicoRepository->listar($status);
            foreach($medicos as $medico) {
                $medico->datacontrato = $this->auxiliarService->converteDataBrasil($medico->datacontrato);
            }
            return $medicos;
        }
    }

    public function listar_drop($status){
        if(is_numeric($status)){
            $medicos = $this->medicoRepository->listar_drop($status);
            return $medicos;
        }
    }

    public function buscar($status, $texto){
        if(is_numeric($status)){
            if(!empty($texto['q']))
                $medicos = $this->medicoRepository->buscar($status, $texto);
            else
                $medicos = $this->medicoRepository->listar($status);
            foreach($medicos as $medico)
                $medico->datacontrato = $this->auxiliarService->converteDataBrasil($medico->datacontrato);
            return $medicos;
        }
    }

    public function buscarPorId($id){
        if(is_numeric($id)){
            $medico = $this->medicoRepository->buscarPorId($id);
            $medico->datacontrato = $this->auxiliarService->converteData($medico->datacontrato);
            return $medico;
        }
    }

    public function medicoVenda(){
        return $this->medicoRepository->medicoVenda();
    }
}