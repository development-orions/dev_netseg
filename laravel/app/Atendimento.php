<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 * @package App\Models\Admin
 * @version September 25, 2018, 5:06 pm -03
 *
 * @property \Illuminate\Database\Eloquent\Collection clientes
 * @property \Illuminate\Database\Eloquent\Collection cursos
 * @property \Illuminate\Database\Eloquent\Collection MenuExame
 * @property \Illuminate\Database\Eloquent\Collection pagamentos
 * @property \Illuminate\Database\Eloquent\Collection videos
 * @property string nome
 * @property integer drop
 * @property integer ontop
 * @property integer onfooter
 * @property integer ordem
 */
class Atendimento extends Model
{

    public $table = 'atendimentos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/

    public function atendimentoExames()
    {
        return $this->hasMany(\App\AtendimentoExame::class, 'atendimento_id');
    }

    public function statu()
    {
        return $this->belongsTo(\App\Statu::class, 'statu_id');
    }

    public function paciente()
    {
        return $this->belongsTo(\App\Paciente::class, 'paciente_id');
    }

    public function medico()
    {
        return $this->belongsTo(\App\Medico::class, 'medico_id');
    }

    public function exames()
    {
        return $this->belongsToMany(\App\Exame::class,'atendimento_exames', 'atendimento_id','exame_id');
    }

    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }

    public function getDataAttribute($data){
        $data = explode('-', $data);
        $ano = $data[0];
        $dia = $data[2];

        $meses = array('01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');

        $mes = $meses[$data[1]];
        $dataformatada = $dia.' '.$mes.', '.$ano;

        return $dataformatada;
    }

    public function getValorRAttribute()
    {
        return "R$ ".number_format($this->valor, 2, ",", ".");
    }

    public function getValorMedAttribute()
    {
        return "R$ ".number_format($this->valor_medico, 2, ",", ".");
    }



}
