<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Parcela extends Model {

    protected $table = 'parcelas';
    protected $fillable = ['numero', 'data', 'venda_id'];
    public $timestamps = false;
    public function vendas(){
        return $this->belongsTo('App\Venda', 'venda_id');
    }

}
