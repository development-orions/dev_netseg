<?php namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tecnico extends Model {

    protected $table = 'tecnicos';

    protected $fillable = ['nome', 'email', 'num_registro', 'statu_id', 'telefone', 'rua', 'numero', 'bairro', 'complemento', 'estado', 'cidade', 'cep'];
    //
    public function status(){
        return $this->belongsTo('App/Statu', 'statu_id');
    }

    public function itens(){
        return $this-> hasMany('App\Itens', 'tecnico_id');
    }

    public function cronogramas(){
        return $this->hasMany('App\Cronograma', 'tecnico_id');
    }
     public function clientestecnicos(){
        return $this->hasMany('App\Clientestecnico', 'tecnico_id');
    }

}
