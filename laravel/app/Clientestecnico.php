<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientestecnico extends Model {
    protected $table = 'clientestecnicos';

    protected $fillable = ['cliente_id', 'tecnico_id', 'valor_tecnico', 'dia_tecnico'];

    public $timestamps = false;
    public function clientes(){
        return $this->belongsTo('App\Cliente', 'cliente_id');
    }
    public function tecnicos(){
        return $this->belongsTo('App\Tecnico', 'tecnico_id');
    }

}

