<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model {

    protected $table = 'pacientes';
    protected $fillable = ['nome', 'rg', 'cpf', 'datanascimento', 'sexo', 'estadocivil', 'naturalidade', 'grauinstrucao', 'funcao', 'setor', 'email', 'rua', 'numero', 'bairro', 'complemento', 'estado', 'cidade', 'cep', 'telefone'];

    public function clientes(){
        return $this->belongsTo('App\Cliente', 'cliente_id');
    }
    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }

    public function jqcalendar(){
        return $this->hasMany('App/Jqcalendar', 'paciente_id');
    }
    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
}
