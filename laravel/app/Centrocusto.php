<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Centrocusto extends Model {

    protected $table = 'centrocustos';
    public $timestamps = false;
    public function contas(){
        return $this->hasMany('App\Conta', 'centrocusto_id');
    }

}
