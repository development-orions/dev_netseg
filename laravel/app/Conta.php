<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Conta extends Model {

    protected $table = 'contas';

    protected $fillable = ['nome', 'vencimento', 'baixa', 'statu_id', 'valor', 'portador', 'observacao', 'desconto', 'acrescimo', 'nf', 'centrocusto_id', 'formapagamento_id', 'num_boleto'];

    public function pagamentos(){
        return $this->hasMany('App\Pagamento', 'conta_id');
    }
    public function recebimentos(){
        return $this->hasMany('App\Recebimento', 'conta_id');
    }
    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }
    public function centrocustos(){
        return $this->belongsTo('App\Centrocusto', 'centrocusto_id');
    }
    public function formapagamentos(){
        return $this->belongsTo('App\Formapagamento', 'formapagamento_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
}
