<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Medicosvenda extends Model {
    protected $table = 'medicosvendas';
    public $timestamps = false;
    protected $fillable = ['venda_id', 'valor', 'medico_id', 'id'];
    public function vendas()
    {
        return $this->belongsTo('App\Venda', 'venda_id');
    }
    public function medicos()
    {
        return $this->belongsTo('App\Medico', 'medico_id');
    }
    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
}
