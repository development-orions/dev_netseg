<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {
    protected $table = 'clientes';

    protected $fillable = ['nome', 'email_responsavel', 'razaosocial', 'statu_id', 'cnpj', 'ie', 'telefone_responsavel', 'rua', 'numero', 'bairro', 'complemento', 'estado', 'cidade', 'cep', 'formajuridica', 'responsavel', 'funcao', 'valor_contrato', 'data_contrato', 'tecnico_id'];
	//
    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }

    public function recebimentos(){
        return $this->hasMany('App\Recebimento', 'cliente_id');
    }

    public function pacientes(){
        return $this->hasMany('App\Paciente', 'cliente_id');
    }

    public function vendas(){
        return $this->hasMany('App\Venda', 'cliente_id');
    }

    public function cronogramas(){
        return $this->hasMany('App\Cronograma', 'cliente_id');
    }

    public function clientestecnicos(){
        return $this->hasMany('App\Clientestecnico', 'cliente_id');
    }

    public function arquivosclientes(){
        return $this->hasMany('App\Arquivoscliente', 'cliente_id');
    }
}
