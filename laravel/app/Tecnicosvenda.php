<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tecnicosvenda extends Model {

    protected $table = 'tecnicosvendas';
    public $timestamps = false;

    public function vendas()
    {
        return $this->belongsTo('App\Venda', 'venda_id');
    }

    public function tecnicos()
    {
        return $this->belongsTo('App\Tecnico', 'tecnico_id');
    }
    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
}
