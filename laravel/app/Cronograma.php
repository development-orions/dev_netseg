<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Cronograma extends Model {

    protected $table = 'cronogramas';

    public function status(){
        return $this->belongsTo('App\Statu', 'statu_id');
    }
    public function clientes(){
        return $this->belongsTo('App\Cliente', 'cliente_id');
    }
    public function atividades(){
        return $this->belongsTo('App\Atividade', 'atividade_id');
    }
    public function tecnicos(){
        return $this->belongsTo('App\Tecnico', 'tecnico_id');
    }
    public function observacoes(){
        return $this->hasMany('App\Observacoe', 'cronograma_id');
    }

    public function naoconformidades(){
        return $this->belongsTo('App\Naoconformidade', 'cronograma_id');
    }

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

}
